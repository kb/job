$(function(){
    $('#id_area').on('click', function(){ $(this).select(); }); 
});

function set_radius_enabled() {
    if($('#id_data_type').val() == "postcode") {
        $('#id_radius').prop('disabled', false);
    } else {
        $('#id_radius').prop('disabled', true);
    }
}
set_radius_enabled();
// https://github.com/devbridge/jQuery-Autocomplete
$('.autocomplete').autocomplete({
    minChars: 2,
    serviceUrl: '{% url 'job.location.region' %}',
    onInvalidateSelection: function () {
        $('#id_data_type').val("");
            $('#id_code').val("");
            $('#id_radius').prop('disabled', true);
        },
        onSelect: function (suggestion) {
            $('#id_data_type').val(suggestion.data.data_type);
            $('#id_code').val(suggestion.data.code);
            set_radius_enabled();
        }
    });


