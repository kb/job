# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings

from job.models import Candidate, JobFeed
from job.search import JobIndex
from job.service import (
    DiginowXMLJobFeed,
    SimplyLawJsonJobFeed,
    StandardXMLJobFeed,
)
from search.search import SearchIndex


logger = logging.getLogger(__name__)


def _display_index_count(index):
    index_count = index.count()
    logger.info("Job index count - {} records".format(index_count))
    return index_count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def process_feed_legists():
    """Legists Jobs.

    https://www.kbsoftware.co.uk/crm/ticket/3889/

    """
    logger.info("Import Legists Jobs...")
    job_feed = JobFeed.objects.get(slug=JobFeed.LEGISTS)
    logger.info("Downloading XML file...")
    file_name = job_feed.download_ftp("ILSPAJobs.xml")
    logger.info("XML file downloaded: {}".format(file_name))
    feed = DiginowXMLJobFeed(job_feed.slug, file_name)
    logger.info("Start import...")
    count = feed.do_import()
    logger.info("Import Legists Jobs - {} records - complete...".format(count))
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def process_feed_simply_law_jobs():
    """Simple Law Jobs.

    https://www.kbsoftware.co.uk/crm/ticket/3493/

    """
    logger.info("Import Simply Law Jobs...")
    feed = SimplyLawJsonJobFeed(JobFeed.SIMPLY_LAW)
    count = feed.do_import(verbose=True)
    logger.info(f"Import Simply Law Jobs - {count} records - complete...")
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def process_feeds():
    logger.info("Process feeds - Totally Legal")
    totally_legal = StandardXMLJobFeed("totally-legal")
    totally_legal.do_import()
    logger.info("Process feeds - Secs In The City")
    secsinthecity = StandardXMLJobFeed("secsinthecity")
    secsinthecity.do_import()
    logger.info("Process feeds - Complete")


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def rebuild_job_index():
    logger.info("Drop and rebuild job index...")
    index = SearchIndex(JobIndex())
    count = index.rebuild()
    logger.info("Job index rebuilt - {} records - complete".format(count))
    index_count = _display_index_count(index)
    return count, index_count


def refresh_job_index():
    """Only used from a management command at present."""
    logger.info("Refresh job index (without dropping first)...")
    index = SearchIndex(JobIndex())
    count = index.refresh()
    logger.info("Job index refreshed - {} records - complete".format(count))
    index_count = _display_index_count(index)
    return count, index_count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def send_email_latest_jobs():
    logger.info("Send job notification email to candidates...")
    count = Candidate.objects.send_email_latest_jobs()
    logger.info(
        "Send job notification email to candidates "
        "- {} records - complete...".format(count)
    )
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def update_job_index(pk):
    logger.info("Search index - update job {}".format(pk))
    index = SearchIndex(JobIndex())
    count = index.update(pk)
    logger.info("Search index - updated job {} ({} record)".format(pk, count))
    index_count = _display_index_count(index)
    return count, index_count


def schedule_process_feed_legists():
    """APScheduler would like to process the job feed.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    process_feed_legists.send()


def schedule_process_feed_simply_law_jobs():
    """APScheduler would like to process the job feed.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    process_feed_simply_law_jobs.send()


def schedule_process_feeds():
    """APScheduler would like to process the job feed.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    process_feeds.send()


def schedule_rebuild_job_index():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    rebuild_job_index.send()


def schedule_send_email_latest_jobs():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    send_email_latest_jobs.send()
