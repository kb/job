# -*- encoding: utf-8 -*-
import attr
import json
import logging
import math
import os.path
import requests
import tempfile
import time
import urllib.request
import xmltodict

from bs4 import BeautifulSoup
from datetime import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core.files import File
from django.db import transaction
from django.db.models import Count
from django.utils import timezone
from django.utils.text import slugify
from http import HTTPStatus
from itertools import zip_longest
from urllib.parse import urlparse
from xml.sax.saxutils import unescape

from base.form_utils import bleach_clean
from checkout.models import CheckoutState
from stock.models import Product
from .models import (
    Country,
    FEED_RECRUITER,
    get_contact_model,
    Job,
    JobBasket,
    JobBasketItem,
    JobError,
    JobFeed,
    JobFeedItem,
    JobRoleParser,
    JobSettings,
    PracticeAreaParser,
    PRODUCT_JOB_POST,
    PRODUCT_JOB_POST_DISCOUNT,
    Recruiter,
)


logger = logging.getLogger(__name__)


@attr.s
class DiginowJob:
    AdvertiserName = attr.ib()
    AdvertiserType = attr.ib()
    SenderReference = attr.ib()
    Classification = attr.ib()
    Position = attr.ib()
    Description = attr.ib()
    JobURL = attr.ib()
    JobStatus = attr.ib()
    ContactName = attr.ib(
        validator=attr.validators.optional(attr.validators.instance_of(str)),
        default=None,
    )
    Email = attr.ib(
        validator=attr.validators.optional(attr.validators.instance_of(str)),
        default=None,
    )
    Location = attr.ib(
        validator=attr.validators.optional(attr.validators.instance_of(str)),
        default=None,
    )
    LogoURL = attr.ib(
        validator=attr.validators.optional(attr.validators.instance_of(str)),
        default=None,
    )
    SalaryAdditional = attr.ib(
        validator=attr.validators.optional(attr.validators.instance_of(str)),
        default=None,
    )
    StartDate = attr.ib(
        validator=attr.validators.optional(attr.validators.instance_of(str)),
        default=None,
    )
    TypeOfContract = attr.ib(
        validator=attr.validators.optional(attr.validators.instance_of(str)),
        default=None,
    )


def add_jobs_to_basket(basket):
    """Add recruiter jobs to the basket.

    This is a simple function which just adds all jobs at a fixed price.

    """
    product = Product.objects.get(slug=PRODUCT_JOB_POST)
    jobs = basket.recruiter.jobs_pending()
    for job in jobs:
        JobBasketItem.objects.create_job_basket_item(basket, job, product)


def add_jobs_to_basket_discount(basket):
    """Add recruiter jobs to the basket using a discount structure.

    - if the recruiter is posting more than one job, they get the discounted
      rate
    - if they post a single job within one week of posting more than one job,
      they still get the discounted rate

    """
    product_slug = PRODUCT_JOB_POST
    jobs = basket.recruiter.jobs_pending()
    if jobs.count() > 1:
        product_slug = PRODUCT_JOB_POST_DISCOUNT
    else:
        one_week = timezone.now() + relativedelta(days=-7)
        qs = JobBasket.objects.filter(
            created__gte=one_week,
            jobbasketitem__job__recruiter=basket.recruiter,
            checkout_state__slug=CheckoutState.SUCCESS,
        ).annotate(num_jobs=Count("jobbasketitem"))
        for item in qs:
            if item.num_jobs > 1:
                product_slug = PRODUCT_JOB_POST_DISCOUNT
                break
    product = Product.objects.get(slug=product_slug)
    for job in jobs:
        JobBasketItem.objects.create_job_basket_item(basket, job, product)


def grouper(iterable, n, fillvalue=None):
    """ "Collect data into fixed-length chunks or blocks.

    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx

    From
    http://docs.python.org/2/library/itertools.html#recipes

    """
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)


def create_contact(username, **kwargs):
    """Create a contact record.

    No save performed on either contact or user.

    """
    # commit = kwargs.pop('commit', True)
    last_name = kwargs.pop("last_name", None)
    first_name = kwargs.pop("first_name", None)
    password = kwargs.pop("password", None)
    email = kwargs.pop("email", None)
    user = User.objects.create_user(username)
    if password:
        user.set_password(password)
    # this is for data import
    encrypted_password = kwargs.pop("encrypted_password", None)
    if encrypted_password:
        user.password = encrypted_password
    if email:
        user.email = email
    if first_name:
        user.first_name = first_name
    if last_name:
        user.last_name = last_name
    defaults = dict(user=user)
    defaults.update(kwargs)
    return get_contact_model()(**defaults)


def get_or_create_recruiter(username, **kwargs):
    try:
        recruiter = Recruiter.objects.get(contact__user__username=username)
    except Recruiter.DoesNotExist:
        contact = create_contact(username, **kwargs)
        contact.user.save()
        contact.save()

        recruiter = Recruiter(contact=contact)
        recruiter.save()

    return recruiter


def create_user(user_name, password, first_name, last_name, email):
    user = get_user_model().objects.create_user(user_name)
    user.email = email
    user.first_name = first_name
    user.last_name = last_name
    user.set_password(password)
    user.save()
    return user


def login_new_user(request, username, password):
    new_user = authenticate(username=username, password=password)
    login(request, new_user)


def import_postcode_csv(filename):
    pass


def convert_latitude_to_miles(latitude):
    return latitude * Decimal(68.686)


def convert_longitude_to_miles(longitude, latitude):
    return longitude * Decimal((69.171 * math.cos(float(latitude))))


def convert_datetime(date_str):
    try:
        dt = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    except Exception:
        dt = None
    return dt


def convert_date(date_str):
    dt = convert_datetime(date_str)
    if dt:
        d = dt.date()
    else:
        d = None
    return d


def tz_aware_datetime(date_str, conversion_format):
    tim = time.strptime(date_str, conversion_format)
    return timezone.make_aware(
        datetime.fromtimestamp(time.mktime(tim)),
        timezone.get_current_timezone(),
    )


def check_file_exists(file_path):
    if file_path:
        return os.path.isfile(file_path)
    return False


def check_private_file_exists(file_path):
    if file_path:
        return check_file_exists(
            os.path.join(settings.SENDFILE_ROOT, file_path)
        )
    return False


def check_public_file_exists(file_path):
    if file_path:
        return check_file_exists(os.path.join(settings.MEDIA_ROOT, file_path))
    return False


def clean_file_name(orig_file_name, version):
    t = orig_file_name.rpartition(".")
    versionStr = ""
    if version > 1:
        versionStr = "-" + str(version)
    if t[1] == ".":
        file_name = slugify(t[0]) + versionStr + "." + slugify(t[2])
    else:
        file_name = slugify(t[2]) + versionStr
    return file_name


def replace_multi_newline_with_br(text):
    result = []
    for x in text.split("\n"):
        if x.strip():
            result.append(x)
    return "<br>".join(result)


def _save_file(src_dir, dst_dir, file_rel_path, file_name):
    src_dir_path = os.path.join(src_dir, file_rel_path)
    dst_dir_path = os.path.join(dst_dir, file_rel_path)
    dst_path = None
    version = 1

    # ensure path exists
    if not os.path.exists(dst_dir_path):
        os.makedirs(dst_dir_path)

    src_path = os.path.join(src_dir_path, file_name)

    if check_file_exists(src_path):
        while True:
            dst_path = os.path.join(
                file_rel_path, clean_file_name(file_name, version)
            )

            dst_full_path = os.path.join(dst_dir, dst_path)

            if not os.path.isfile(dst_full_path):
                break
            else:
                version += 1

        py_src_file = open(src_path, "rb")
        src_file = File(py_src_file)

        with open(dst_full_path, "wb+") as destination:
            for chunk in src_file.chunks():
                destination.write(chunk)

        src_file.close()
        py_src_file.close()

        # remove the temporary file
        # temp_path = os.path.join(temp_dir_path, file_name)
        # os.remove(temp_path)

    return dst_path


def save_private_file(src_dir, file_rel_path, file_name):
    return _save_file(src_dir, settings.SENDFILE_ROOT, file_rel_path, file_name)


def save_public_file(src_dir, file_rel_path, file_name):
    return _save_file(src_dir, settings.MEDIA_ROOT, file_rel_path, file_name)


class SimplyLawJsonJobFeed:
    def __init__(self, slug):
        self.feed_slug = slug
        self.job_role_parser = JobRoleParser()
        self.practice_area_parser = PracticeAreaParser()

    def _import(self, feed, job_list):
        count = 0
        for job in job_list:
            with transaction.atomic():
                job_ref = job["jobref"]
                job_tit = job["title"]
                if self._process_feed_job(feed, job):
                    logger.info(f"Added {job_ref} {job_tit} to the database.")
                    count = count + 1
        return count

    def _practice_area(self, title, description, industry_sectors):
        soup = BeautifulSoup(description, "html.parser")
        text = title + " " + soup.get_text()

        # With the new Simply Law feed, industry_sectors is a string
        # rather than a list.

        text = " " + industry_sectors
        found = self.practice_area_parser.parse(text)

        return found[0] if found else None

    def _process_feed_job(self, feed, feed_job):
        """Process a job from the feed.

        Returns ``True`` if the job was published.

        """
        result = False
        jobkey = feed_job["jobref"]
        if JobFeedItem.objects.is_new_job_key(feed=feed, jobkey=jobkey):
            # try:
            job = Job(recruiter=feed.recruiter)
            job.title = feed_job["title"]
            job.description = bleach_clean(feed_job["description"], strip=True)
            job.close_date = tz_aware_datetime(
                feed_job["closedate"], "%Y-%m-%d %H:%M:%S"
            )
            # locations

            # country, locality, region = self._locations(
            #     feed_job["locations"], jobkey
            # )

            # country is country, locality is city, region is region
            country = feed_job["country"]
            locality = feed_job["city"]
            region = feed_job["region"]

            # practice_area
            practice_area = self._practice_area(
                job.title, job.description, feed_job["practicearea"]
            )
            if practice_area:
                job.practice_area = practice_area
            job.location_text = locality or region or country or ""
            # add to the list of default jobs for the board
            job.order_key = FEED_RECRUITER
            # parse for job role
            found_roles = self.job_role_parser.parse(job.title)
            if found_roles and (country or locality or region):
                # if there is no job role we don't publish the job
                job.job_role = found_roles[0]
                job.publish = True
                job.publish_date = timezone.now()
                result = True
            # save the job
            job.save()
            feed_item = JobFeedItem(feed=feed, job=job, jobkey=jobkey)
            feed_item.city = locality or ""
            feed_item.state = region or ""
            feed_item.country = country or ""
            feed_item.company = feed_job["company"] or ""
            # set the hour to 9, so timezones don't move us to yesterday
            feed_item.date = tz_aware_datetime(
                feed_job["date"], "%Y-%m-%d %H:%M:%S"
            ) + relativedelta(hours=9, minutes=0, seconds=0)
            # not sure why this is duplicated (copying XML feed)
            feed_item.source = feed_job["company"] or ""
            feed_item.url = feed_job["url"] or ""
            feed_item.save()
            # prettyprinter.cpprint(feed_item)
            # except (AttributeError, KeyError):
            #     # prettyprinter.cpprint(feed_job)
            #     raise
        return result

    def do_import(self, verbose=False):
        """Download and import the jobs feed in JSON format.

        For live sites, the ``identifier``, `username`` and ``password`` are
        set in the Django admin.
        For testing, they can be set in the ``.private`` file and
        ``init_app_job`` will update the feed settings (for more information
        search for ``TEST_SIMPLY_LAW_IDENTIFIER``).

        24/01/2025, No longer using ``requests``:

        Switched to ``urlopen``.
        - https://dev.to/bowmanjd/http-calls-in-python-without-requests-or-other-external-dependencies-5aj1

        Simply Law jobs feed - failing to import
        - https://www.kbsoftware.co.uk/crm/ticket/7517/

        """
        count = 0
        feed = JobFeed.objects.get(slug=JobFeed.SIMPLY_LAW)
        http_request = urllib.request.Request(
            feed.url,
            headers={
                "Accept": "application/json",
                "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
            },
        )
        response = urllib.request.urlopen(http_request)
        if HTTPStatus.OK == response.status:
            res_body = response.read()
            try:
                data = json.loads(res_body)
                jobs = data.get("jobs")
                if jobs is None:
                    raise JobError(
                        f"Cannot find 'jobs' key in the data: '{data.keys()}'"
                    )
                if len(jobs) == 1:
                    job_list = jobs[0]["job"]
                    count = self._import(feed, job_list)
                else:
                    raise JobError(f"No 'job' in 'jobs' {len(jobs)}")
            except json.JSONDecodeError:
                raise JobError(
                    f"Data is not in JSON format: {response.content}"
                )
        else:
            raise JobError(
                f"Cannot get feed from URL: '{feed.url}', "
                f"status: '{response.status}': {response.text}"
            )
        return count


class DiginowXMLJobFeed:
    """Job feed for The Legists, https://www.thelegists.co.uk/.

    Diginow write recruitment websites, so we might come across this format in
    future.

    This feed will process an XML file:
    - The file is normally downloaded via FTP
      (see ``JobFeed``, ``download_ftp`` for the code)

    """

    def __init__(self, slug, file_name):
        self.feed_slug = slug
        self.job_role_parser = JobRoleParser()
        self.practice_area_parser = PracticeAreaParser()
        self.xml_file_name = file_name

    def _close_jobs_not_in_feed(self, feed, job_keys):
        """Close any jobs which are not included in the XML file.

        .. note:: The XML will have all the jobs that should be displayed.
                  Any not on there should be removed from your system.

        To close a job, we will set the ``close_date`` to yesterday.

        """
        current_jobs = Job.objects.current()
        qs = (
            JobFeedItem.objects.filter(feed=feed)
            .filter(job__in=current_jobs)
            .exclude(jobkey__in=job_keys)
        )
        jobs_to_close = [x.job.pk for x in qs]
        for pk in jobs_to_close:
            job = Job.objects.get(pk=pk)
            job.close_date = timezone.now().date() + relativedelta(days=-1)
            job.save()
            logger.info(
                "Closing job {} (not included in XML file)".format(job.pk)
            )

    def _job_in_australia(self, locality, region):
        """Is the job in Australia?

        This is a very simple check to exclude all jobs in Australia
        (it will also exclude jobs in Sydney, Cheshire)!

        For details of the decision, see
        https://www.kbsoftware.co.uk/crm/ticket/3983/

        """
        return "sydney" in " ".join([locality, region]).lower()

    def _name_from_url(self, logo_url):
        x = urlparse(logo_url)
        return os.path.basename(x.path)

    def _practice_area(self, title, description):
        soup = BeautifulSoup(description, "html.parser")
        text = title + " " + soup.get_text()
        found = self.practice_area_parser.parse(text)
        return found[0] if found else None

    def _process_feed_job(self, feed, feed_job):
        """Create a job for the Diginow XML feed (Legists)."""
        result = False
        if JobFeedItem.objects.is_new_job_key(
            feed=feed, jobkey=feed_job.SenderReference
        ):
            start_date = tz_aware_datetime(feed_job.StartDate, "%d/%m/%Y")
            job = Job(recruiter=feed.recruiter)
            job.title = feed_job.Position
            job.description = replace_multi_newline_with_br(
                bleach_clean(feed_job.Description, strip=True)
            )
            job.close_date = start_date + relativedelta(days=60)
            # location
            locality = region = None
            if feed_job.Location:
                locality, region = feed_job.Location.split(",")
                locality = locality.strip()
                region = region.strip()
            # practice_area
            practice_area = self._practice_area(job.title, job.description)
            if practice_area:
                job.practice_area = practice_area
            job.location_text = locality or region or ""
            # add to the list of default jobs for the board
            job.order_key = FEED_RECRUITER
            # parse for job role
            found_roles = self.job_role_parser.parse(job.title)
            if (
                found_roles
                and (locality or region)
                and not self._job_in_australia(locality, region)
            ):
                # if there is no job role we don't publish the job
                job.job_role = found_roles[0]
                job.publish = True
                job.publish_date = timezone.now()
                result = True
            # save the job
            if job.publish and feed_job.LogoURL:
                # download the logo
                r = requests.get(feed_job.LogoURL)
                with tempfile.NamedTemporaryFile("wb", delete=False) as f:
                    f.write(r.content)
                reopen = open(f.name, "rb")
                django_file = File(reopen)
                job.picture.save(
                    self._name_from_url(feed_job.LogoURL),
                    django_file,
                    save=True,
                )
            else:
                job.save()
            feed_item = JobFeedItem(
                feed=feed, job=job, jobkey=feed_job.SenderReference
            )
            feed_item.city = locality or ""
            feed_item.state = region or ""
            feed_item.country = "UK"
            feed_item.company = feed_job.AdvertiserName or ""
            # set the hour to 9, so timezones don't move us to yesterday
            feed_item.date = start_date + relativedelta(
                hours=9, minutes=0, seconds=0
            )
            # not sure why this is duplicated (copying XML feed)
            feed_item.source = feed_job.AdvertiserName or ""
            feed_item.url = feed_job.JobURL
            feed_item.save()
        return result

    def do_import(self, verbose=False):
        """Import jobs from an XML file.

        Ability to avoid <![CDATA[ and ]]> tags to be converted when unparsing:
        https://github.com/martinblech/xmltodict/issues/57

        How can I unescape the escape characters in xml files using Python?
        https://www.quora.com/How-can-I-unescape-the-escape-characters-in-xml-files-using-Python

        """
        count = 0
        job_keys = []
        feed = JobFeed.objects.get(slug=self.feed_slug)
        with open(self.xml_file_name, "rb") as f:
            s = f.read()
        data = s.decode("utf-8", "replace")
        if not data:
            raise JobError(
                "The XML file, '{}', contains no data".format(
                    self.xml_file_name
                )
            )
        batch = xmltodict.parse(data)
        jobs = batch["Jobs"]
        for ignore, data in jobs.items():
            for job in data:
                x = {}
                for k, v in job.items():
                    if k == "@Action":
                        continue
                    text = v or ""
                    text = unescape(text)
                    text = text.replace(r"&lt;", r"<")
                    text = text.replace(r"&gt;", r">")
                    text = unescape(text)
                    text = text.replace(r"&nbsp;", r" ")
                    text = replace_multi_newline_with_br(
                        bleach_clean(text, strip=True)
                    )
                    x[k] = text
                # create the job
                feed_job = DiginowJob(**x)
                job_keys.append(feed_job.SenderReference)
                if self._process_feed_job(feed, feed_job):
                    count = count + 1
        self._close_jobs_not_in_feed(feed, job_keys)
        return count


class StandardXMLJobFeed:
    """Class to process a job feed.

    Expected Feed format::

      ﻿<?xml version="1.0" encoding="utf-8"?>
      <source>
         <publisher>https://www.publisher.co.uk/</publisher>
        <publisherurl>https://www.publisher.co.uk/</publisherurl>
        <lastBuildDate>Mon, 14 May 2018 04:04:07 GMT</lastBuildDate>
        <job>
          <title><![CDATA[Legal Secretary - IP]]></title>
          <date><![CDATA[Wed, 18 Apr 2018 17:13:00 GMT]]></date>
          <expirydate><![CDATA[Wed, 16 May 2018 23:59:00 GMT]]></expirydate>
          <referencenumber><![CDATA[3758729]]></referencenumber>
          <url><![CDATA[https://link.to/job/details]]></url>
          <company><![CDATA[Blogs Recruiting]]></company>
          <city><![CDATA[]]></city>
          <state><![CDATA[London (Greater)]]></state>
          <country><![CDATA[England]]></country>
          <postalcode><![CDATA[]]></postalcode>
          <description><![CDATA[<p>We are recruiting for a Legal
          Secretary to join a top law firm.  The Legal Secretary will
          work within a small department.</p>]]></description>
          <salary><![CDATA[£40,000]]></salary>
        </job>
      </source>

    """

    def __init__(self, slug):
        self.feed_slug = slug
        self.job_settings = JobSettings.load()
        self.job_role_parser = JobRoleParser()
        self.practice_area_parser = PracticeAreaParser()
        self.imported_count = 0

    def get_country(self, country_code):
        try:
            country = Country.objects.get(country_code=country_code)
        except Country.DoesNotExist:
            country = None
        return country

    def process_feed_job(self, feed, feed_job, verbose=False):
        if verbose:
            for key, value in feed_job.items():
                print("{:<20} : {}".format(key, value))

        try:
            jobkey = feed_job["referencenumber"]
            if not jobkey:
                logger.error("Failed on {}".format(feed_job))
                return
        except KeyError as ke:
            logger.error("KeyError({}) Failed on {}".format(ke, feed_job))
            return

        title = feed_job["title"]

        msg = "Error encountered"

        if JobFeedItem.objects.is_new_job_key(feed=feed, jobkey=jobkey):
            job = None
            with transaction.atomic():
                job = Job(recruiter=feed.recruiter)
                feed_item = JobFeedItem(feed=feed)
                feed_item.jobkey = jobkey
                feed_item.company = feed_job["company"] or ""
                feed_item.city = feed_job["city"] or ""
                feed_item.state = feed_job["state"] or ""
                feed_item.country = feed_job["country"] or ""
                feed_item.formattedLocation = feed_job["postalcode"] or ""
                feed_item.source = feed_job["company"] or ""
                # Format of date is 'Mon, 23 Feb 2015 06:31:21 GMT'
                feed_item.date = tz_aware_datetime(
                    feed_job["date"], "%a, %d %b %Y %H:%M:%S %Z"
                )
                feed_item.url = feed_job["url"] or ""

                # Feed does not contain a location co-ordinates ignore
                # feed does not contain the latitude set to default
                # feed_item.latitude = self.job_settings.default_latitude
                # feed does not contain the longitude set to default
                # feed_item.longitude = self.job_settings.default_longitude

                # we don't currently have a postcode field in the jobfeeditem
                # table use formattedLocation
                feed_item.formattedLocationFull = ""
                # job.location = Location.objects.nearest_location(
                #     feed_item.latitude, feed_item.longitude
                # )
                job.title = title
                job.description = feed_job["description"] or ""

                if feed_item.city:
                    job.location_text = feed_item.city
                elif feed_item.state:
                    job.location_text = feed_item.state
                elif job.location:
                    job.location_text = job.location.town

                # parse for job role
                found_roles = self.job_role_parser.parse(job.title)
                if found_roles:
                    job.job_role = found_roles[0]

                # parse for practice_area
                found_practice_areas = self.practice_area_parser.parse(
                    job.title + " " + job.description
                )
                if found_practice_areas:
                    job.practice_area = found_practice_areas[0]

                job.close_date = tz_aware_datetime(
                    feed_job["expirydate"], "%a, %d %b %Y %H:%M:%S %Z"
                )
                # add to the list of default jobs for the board
                job.order_key = FEED_RECRUITER
                job.salary = feed_job["salary"] or ""

                # if there is no job role we don't publish the job
                if job.job_role:
                    job.publish = True
                    job.publish_date = timezone.now()
                job.save()
                feed_item.job = job
                feed_item.save()
                if feed_item.pk:
                    msg = "saved as job {} (feed item {}) - {}".format(
                        job.pk,
                        feed_item.pk,
                        "Published" if job.publish else "Not published",
                    )
                else:
                    msg = "failed to save {} {} {} {}".format(
                        job.pk,
                        feed_item.pk,
                        "Published" if job.publish else "Not published",
                    )
            if verbose:
                print("{} ({}) - {}.".format(title, jobkey, msg))
            self.imported_count += 1
            # if qs.count() == 0:
            #    opts = feed_item._meta
            #    for field in opts.fields + opts.many_to_many:
            #        print(
            #            "{:<20} : {}".format(
            #                field.name, getattr(feed_item, field.name)
            #            )
            #        )
            #    print("-" * 70)
        elif verbose:
            print("{} ({}) - already exists.".format(title, jobkey))

    def get_feed_url(self, feed, start):
        # don't currently use start for these feeds
        return feed.url

    def do_import(self, verbose=False):
        feed = JobFeed.objects.get(slug=self.feed_slug)
        feed_url = self.get_feed_url(feed, 1)
        if verbose:
            print("FEED URL = ", feed_url)
        contents = urllib.request.urlopen(feed_url).read()
        batch = xmltodict.parse(contents)
        publisher = "unknown"
        publisher_url = "unknown"
        build_date = "unknown"
        if batch["source"]:
            for top_key, top_value in batch["source"].items():
                if top_key == "job":
                    if verbose:
                        print(
                            "processing jobs for {} ({}) - {}".format(
                                publisher, publisher_url, build_date
                            )
                        )
                    if top_value:
                        if isinstance(top_value, list):
                            for feed_job in top_value:
                                self.process_feed_job(feed, feed_job, verbose)
                                # if self.imported_count > 5:
                                #     print ("ABORT AFTER 5 JOBS")
                                #     return
                        else:
                            self.process_feed_job(feed, top_value, verbose)
                    elif verbose:
                        print("No jobs received")
                elif top_key == "publisher":
                    publisher = top_value
                    if verbose:
                        print("{:<20} : {}".format(top_key, top_value))
                elif top_key == "publisherurl":
                    publisher_url = top_value
                    if verbose:
                        print("{:<20} : {}".format(top_key, top_value))
                elif top_key == "lastBuildDate":
                    build_date = top_value
                    if verbose:
                        print("{:<20} : {}".format(top_key, top_value))
                elif verbose:
                    print("{:<20} : ({})".format(top_key, top_value))
        elif verbose:
            print("No response received {}".format(batch))

        print("{} jobs imported".format(self.imported_count))
