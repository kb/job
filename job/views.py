# -*- encoding: utf-8 -*-
import logging

from braces.views import (
    AjaxResponseMixin,
    JSONResponseMixin,
    LoginRequiredMixin,
    StaffuserRequiredMixin,
)
from dateutil.relativedelta import relativedelta
from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.views import redirect_to_login
from django.core.paginator import Paginator, EmptyPage
from django.core.exceptions import (
    ObjectDoesNotExist,
    PermissionDenied,
    ValidationError,
)
from django.db import models
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django_sendfile import sendfile
from django.views.decorators.cache import never_cache
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    TemplateView,
    UpdateView,
    View,
)


from base.view_utils import BaseMixin, ElasticPaginator, RedirectNextMixin
from checkout.models import Checkout
from checkout.views import CONTENT_OBJECT_PK
from gdpr.views import ConsentDisplayMixin, ConsentMaintenanceMixin
from job.search import Row
from mail.service import queue_mail_template
from mail.tasks import process_mail
from search.search import Hit, SearchIndex
from stock.models import Product
from .forms import (
    CandidateCreateForm,
    CandidateForm,
    CandidateProfileForm,
    CandidateSearchForm,
    JobApplicationForm,
    JobBasketEmptyForm,
    JobCreditForm,
    JobCreditEmptyForm,
    JobElasticSearchForm,
    JobEmptyForm,
    JobForm,
    JobSearchForm,
    LandingPageCreateForm,
    LandingPageForm,
    LocationForm,
    RecruiterCreateForm,
    StaffRecruiterProfileForm,
    StaffCandidateSearchForm,
    RecruiterProfileForm,
    RecruiterForm,
)
from .models import (
    Candidate,
    CandidateJobLocation,
    CV,
    CVType,
    Experience,
    ILSPA_RECRUITER,
    Job,
    JobApplication,
    JobBasket,
    JobCredit,
    JobError,
    JobFeed,
    JobSettings,
    JobTracking,
    LandingPage,
    Location,
    PRODUCT_JOB_POST,
    PRODUCT_JOB_POST_DISCOUNT,
    Recruiter,
    RECRUITER_APPLICATION_FROM_CANDIDATE,
    Region,
)
from .search import JobIndex
from .service import create_user, login_new_user


logger = logging.getLogger(__name__)


def candidate_search_parameters(request):
    """Get the search parameters for a candidate search."""
    experience = request.GET.get("experience", None)
    last_login = int(request.GET.get("last_login") or 0)
    code = request.GET.get("code")
    data_type = request.GET.get("data_type")
    order_by = request.GET.get("order_by", None)
    search_type = request.GET.get("search_type", Candidate.POSITION_SEARCH)
    postcode = None
    postcode_range = None
    region = None
    if data_type == "region":
        try:
            region = Region.objects.get(region_code=code)
        except Region.DoesNotExist:
            pass
    elif data_type == "postcode":
        try:
            location = Location.objects.get(postcode=code)
            postcode = location.postcode
            postcode_range = int(request.GET.get("radius") or 0)
        except Location.DoesNotExist:
            pass
    if experience:
        try:
            experience = Experience.objects.get(slug=experience)
        except Experience.DoesNotExist:
            pass
    return (
        experience,
        last_login,
        order_by,
        postcode,
        postcode_range,
        region,
        search_type,
    )


def _download_cv(request, cv):
    try:
        path = cv.file_name.path
    except ValueError:
        raise JobError("No file for CV '{}'".format(cv.pk))
    return sendfile(request, path, attachment=True)


def _is_candidate(user):
    try:
        if user.contact:
            return user.contact.is_candidate
    except apps.get_model(settings.CONTACT_MODEL).DoesNotExist:
        pass
    except AttributeError:
        pass
    return False


def _is_contact(user):
    result = False
    try:
        if user.contact:
            result = True
    except apps.get_model(settings.CONTACT_MODEL).DoesNotExist:
        pass
    return result


def _is_recruiter(user):
    try:
        if user.contact:
            return user.contact.is_recruiter
    except apps.get_model(settings.CONTACT_MODEL).DoesNotExist:
        pass
    except AttributeError:
        pass
    return False


# ACCESS MIXINS
class CandidateLoginRequiredMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        try:
            allow = user.is_authenticated and user.contact.is_candidate
        except ObjectDoesNotExist:
            raise JobError(
                "User '{}' does not have a contact record".format(user.username)
            )
        if not allow:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_login_url(),
                    self.get_redirect_field_name(),
                )
        return super().dispatch(request, *args, **kwargs)


class CandidateOrStaffLoginRequiredMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        allow = user.is_staff
        if not allow:
            try:
                allow = user.is_authenticated and user.contact.is_candidate
            except ObjectDoesNotExist:
                raise JobError(
                    "User '{}' does not have a contact record".format(
                        user.username
                    )
                )
        if not allow:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_login_url(),
                    self.get_redirect_field_name(),
                )
        return super().dispatch(request, *args, **kwargs)


class RecruiterLoginRequiredMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        try:
            allow = user.is_authenticated and user.contact.is_recruiter
        except ObjectDoesNotExist:
            raise JobError(
                "User '{}' does not have a contact record".format(user.username)
            )
        if not allow:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_login_url(),
                    self.get_redirect_field_name(),
                )
        return super().dispatch(request, *args, **kwargs)


class RecruiterOrStaffLoginRequiredMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        allow = user.is_staff
        if not allow:
            try:
                allow = user.is_authenticated and user.contact.is_recruiter
            except ObjectDoesNotExist:
                raise JobError(
                    "User '{}' does not have a contact record".format(
                        user.username
                    )
                )
        if not allow:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_login_url(),
                    self.get_redirect_field_name(),
                )
        return super().dispatch(request, *args, **kwargs)


class CandidateRecruiterOrStaffLoginRequiredMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        allow = user.is_staff
        if not allow:
            if user.is_authenticated:
                try:
                    allow = (
                        user.contact.is_recruiter or user.contact.is_candidate
                    )
                except ObjectDoesNotExist:
                    raise JobError(
                        "User '{}' does not have a contact record".format(
                            user.username
                        )
                    )
        if not allow:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_login_url(),
                    self.get_redirect_field_name(),
                )
        return super().dispatch(request, *args, **kwargs)


# VIEW MIXINS
class CandidateCreateMixin(ConsentMaintenanceMixin):
    consent_form_settings = "generic-form"
    form_class = CandidateCreateForm
    model = apps.get_model(settings.CONTACT_MODEL)

    def _data_consent(self):
        return self._consent(Candidate.GDPR_DATA_SLUG)

    def _notify_consent(self):
        return self._consent(Candidate.GDPR_JOBS_NOTIFICATION_SLUG)

    def _newsletter_consent(self):
        return self._consent(Candidate.GDPR_NEWSLETTER_SLUG)

    def _search_consent(self):
        return self._consent(Candidate.GDPR_RECRUITER_SEARCH_SLUG)

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        first_name = form.cleaned_data.get("first_name")
        last_name = form.cleaned_data.get("last_name")
        password = form.cleaned_data.get("password1")
        user_name = form.cleaned_data.get("username")
        data_checkbox = form.cleaned_data.get("data_checkbox") or False
        notify_checkbox = form.cleaned_data.get("notify_checkbox") or False
        search_checkbox = form.cleaned_data.get("search_checkbox") or False
        newsletter_checkbox = (
            form.cleaned_data.get("newsletter_checkbox") or False
        )

        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.user = create_user(
                user_name, password, first_name, last_name, email
            )
            self.object = form.save()
            candidate = Candidate(
                contact=self.object,
                job_experience=self.object.get_job_experience(),
            )
            candidate.save()
            candidate.send_welcome_email()
            self._save_consent(
                self._data_consent(), data_checkbox, self.object.user
            )
            self._save_consent(
                self._notify_consent(), notify_checkbox, self.object.user
            )
            self._save_consent(
                self._search_consent(), search_checkbox, self.object.user
            )
            self._save_consent(
                self._newsletter_consent(),
                newsletter_checkbox,
                self.object.user,
            )
            self.object.update_contact_index(transaction)
        if not self.request.user.is_staff:
            login_new_user(self.request, user_name, password)
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(
            {
                "data_consent": self._data_consent(),
                "notify_consent": self._notify_consent(),
                "search_consent": self._search_consent(),
                "newsletter_consent": self._newsletter_consent(),
            }
        )
        return kwargs

    def get_success_url(self):
        if self.request.user.is_staff:
            return reverse("contact.detail", args=[self.object.pk])
        else:
            next_url = self.request.GET.get("next")
            next_param = "?next={}".format(next_url) if next_url else ""
            return reverse("recruitment.candidate.create.profile") + next_param


class CandidateDashMixin(ConsentDisplayMixin):
    """Candidate dashboard.
    Use as follows:
    CandidateDashMixin, BaseMixin, TemplateView
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        candidate = Candidate.objects.get(contact__user=self.request.user)
        latest_cv = CV.objects.latest(self.request.user.contact)
        job_location = None

        try:
            job_location = Location.objects.get(
                postcode=candidate.job_location_code
            )
        except Location.DoesNotExist:
            pass
        except AttributeError:
            pass

        context.update(
            dict(
                object=candidate,
                job_location=job_location,
                latest_cv=latest_cv,
                notify_consent=self._consent_given(
                    Candidate.GDPR_JOBS_NOTIFICATION_SLUG
                ),
                newsletter_consent=self._consent_given(
                    Candidate.GDPR_NEWSLETTER_SLUG
                ),
                search_consent=self._consent_given(
                    Candidate.GDPR_RECRUITER_SEARCH_SLUG
                ),
            )
        )
        return context


class CandidateDetailMixin:
    """Candidate detail.

    Use as follows::

      RecruiterOrStaffLoginRequiredMixin, CandidateDetailMixin,
      BaseMixin, DetailView

    """

    model = Candidate

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        latest_cv = CV.objects.latest(self.object.contact)
        is_recruiter = (
            self.request.user.contact and self.request.user.contact.is_recruiter
        )
        if (
            self.request.user.is_staff
            or self.request.user.contact.recruiter.can_view_candidate(
                self.object
            )
        ):
            context.update(dict(is_recruiter=is_recruiter, latest_cv=latest_cv))
        else:
            raise PermissionDenied
        return context


class CandidateUpdateMixin(object):
    """Candidate update base class.  Do NOT use this class.
    Use either:
    CandidateUpdateCurrentUserMixin
    or
    CandidateUpdatePrimaryKeyMixin

    """

    form_class = CandidateForm

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        first_name = form.cleaned_data.get("first_name")
        last_name = form.cleaned_data.get("last_name")
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.user.email = email
            self.object.user.first_name = first_name
            self.object.user.last_name = last_name
            self.object.user.save()
            self.object = form.save()
            self.object.update_contact_index(transaction)
        return HttpResponseRedirect(self.get_success_url())

    def get_initial(self):
        return dict(
            first_name=self.object.user.first_name,
            last_name=self.object.user.last_name,
            email=self.object.user.email,
        )


class CandidateUpdateCurrentUserMixin(CandidateUpdateMixin):
    """Update the candidate information for the current user.

    The current user must have a candidate record (or perhaps we should create
    one)?

    Use with:
    CandidateUpdateCurrentUserMixin, BaseMixin, UpdateView

    Note:
    If you are using 'PageFormMixin', then this must come AFTER
    CandidateUpdateCurrentUserMixin e.g:

    CandidateUpdateCurrentUserMixin, PageFormMixin, BaseMixin, UpdateView

    Note:
    This class updates the contact model NOT the candidate.
    """

    def get_object(self):
        model = apps.get_model(settings.CONTACT_MODEL)
        return model.objects.get(user=self.request.user)

    def get_success_url(self):
        return reverse("recruitment.candidate.dashboard")


class CandidateUpdatePrimaryKeyMixin(CandidateUpdateMixin):
    """Update the candidate information for the user with this key.

    Use with:
    CandidateUpdatePrimaryKeyMixin, BaseMixin, UpdateView

    Note:
    This class updates the contact model NOT the candidate.
    """

    model = apps.get_model(settings.CONTACT_MODEL)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(dict(legend="Candidate Contact Details"))
        return context

    def get_success_url(self):
        return reverse("job.candidate.detail", args=[self.object.candidate.pk])


class CandidateProfileMixin(ConsentMaintenanceMixin):
    """
    This class defines the get_success_url and get_context_data methods do
    not use this directly use
        CandidateCreateProfileMixin
            or
        CandidateUpdateProfileMixin
    """

    consent_form_settings = "generic-form"
    form_class = CandidateProfileForm
    model = Candidate
    redirect_text = (
        "Please create your profile (including a CV) to apply for a job"
    )
    normal_text = "Create Profile"

    def _notify_consent(self):
        return self._consent(Candidate.GDPR_JOBS_NOTIFICATION_SLUG)

    def _newsletter_consent(self):
        return self._consent(Candidate.GDPR_NEWSLETTER_SLUG)

    def _search_consent(self):
        return self._consent(Candidate.GDPR_RECRUITER_SEARCH_SLUG)

    def _save_candidate(self, form, type_slug):
        # should be called within a transaction
        self._save_candidate_cv(form, type_slug)
        self._save_candidate_locations(form)
        if not self.object.welcome_email_sent:
            self.object.send_welcome_email()

    def _save_candidate_cv(self, form, type_slug):
        # should be called within a transaction
        cv = None
        upload_cv = form.cleaned_data.get("upload_cv")
        if upload_cv:
            cv_type = CVType.objects.get(slug=type_slug)
            cv = CV(
                contact=self.object.contact,
                cv_type=cv_type,
                file_name=upload_cv,
            )
            cv.save()
        return cv

    def _save_candidate_locations(self, form):
        # should be called within a transaction
        code = form.cleaned_data.get("code")
        data_type = form.cleaned_data.get("data_type")
        locations = []
        radius = 0
        if data_type == "region":
            try:
                region = Region.objects.get(region_code=code)
                locations = Location.objects.region_set(region)
            except Region.DoesNotExist:
                pass
        elif data_type == "postcode":
            try:
                target = Location.objects.get(postcode=code)
                radius = int(form.cleaned_data.get("radius") or 25)
                locations = Location.objects.locations_near(target, radius)
            except Location.DoesNotExist:
                pass
        # get list of existing locations
        existing_job_locations = CandidateJobLocation.objects.candidate(
            self.object
        )
        # delete redundant locations
        for job_loc in existing_job_locations:
            if job_loc.location not in locations:
                job_loc.delete()
        self.object.job_location_code = code
        self.object.job_location_type = data_type
        self.object.job_location_radius = radius
        self.object.save()
        if locations:
            existing_locations = existing_job_locations.values_list("location")
            # add new locations
            for location in locations:
                loc = (location.id,)
                if loc not in existing_locations:
                    candidate_location = CandidateJobLocation(
                        candidate=self.object, location=location
                    )
                    candidate_location.save()

    def get_success_url(self):
        if self.request.user.is_staff:
            return reverse("contact.detail", args=[self.object.contact.pk])
        else:
            next_url = self.request.GET.get("next", None)
            if next_url:
                return next_url
            else:
                return reverse("recruitment.candidate.dashboard")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated and self.object:
            latest_cv = CV.objects.latest(self.object.contact)
        else:
            latest_cv = None

        next_url = self.request.GET.get("next", None)
        if next_url:
            header_text = self.redirect_text
        elif hasattr(self, "_header_text"):
            header_text = self._header_text()
        else:
            header_text = self.normal_text

        if self.request.user.is_staff and self.object:
            legend = "Candidate Profile for '{}' ".format(
                self.object.contact.full_name
            )
        else:
            legend = "Your Candidate Profile"

        context.update(
            dict(
                latest_cv=latest_cv,
                next_url=next_url,
                header_text=header_text,
                legend=legend,
            )
        )
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        next_url = self.request.GET.get("next", None)
        if next_url:
            kwargs["next"] = next_url
            kwargs["user_name"] = self.request.user.username
        kwargs.update(
            {
                "newsletter_consent": self._newsletter_consent(),
                "search_consent": self._search_consent(),
                "notify_consent": self._notify_consent(),
                "updating_my_own_profile": self.updating_my_own_profile,
            }
        )
        return kwargs

    @property
    def show_form_notice(self):
        """Show the privacy notice if I editing my own profile.

        - Hide the privacy notice if I am a member of staff editing a candidate
          profile.

        .. tip:: For more information on the ``form_notice``, search for
                 ``consent_form_settings``.

        """
        result = True
        if not self.updating_my_own_profile:
            result = False
        return result

    @property
    def updating_my_own_profile(self):
        return bool(
            self.object and self.object.contact.user == self.request.user
        )


class CandidateCreateProfileMixin(CandidateProfileMixin):
    """Create the candidate profile.

    Use as follows:
    CandidateCreateProfileMixin, BaseMixin, UpdateView

    Note: If using with 'PageFormMixin', it must come after this mixin
    i.e. as follows:
    CandidateCreateProfileMixin, PageFormMixin, BaseMixin, UpdateView
    """

    redirect_text = (
        "Please create your profile (including a CV) to apply for a job"
    )
    normal_text = "Create your profile"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.contact = self.request.user.contact
            # save the candidate record so we can attach locations and cv to it
            self.object.save()
            self._save_candidate(form, CVType.CANDIDATE_PROFILE)
        return HttpResponseRedirect(self.get_success_url())


class CandidateUpdateProfileMixin(CandidateProfileMixin):
    """Update the candidate profile.

    .. warning:: Do NOT use directly.  Use::
                 ``CandidateUpdateCurrentUserProfileMixin``
                 or
                 ``CandidateUpdatePrimaryKeyProfileMixin``

    """

    def form_valid(self, form):
        notify_checkbox = form.cleaned_data.get("notify_checkbox") or False
        newsletter_checkbox = (
            form.cleaned_data.get("newsletter_checkbox") or False
        )
        search_checkbox = form.cleaned_data.get("search_checkbox") or False
        with transaction.atomic():
            # temporarily update system flags
            self.object = form.save(commit=False)
            self.object = form.save()
            self._save_candidate(form, CVType.CANDIDATE_PROFILE)
            if self.updating_my_own_profile:
                self._save_consent(
                    self._notify_consent(),
                    notify_checkbox,
                    self.object.contact.user,
                )
                self._save_consent(
                    self._search_consent(),
                    search_checkbox,
                    self.object.contact.user,
                )
                self._save_consent(
                    self._newsletter_consent(),
                    newsletter_checkbox,
                    self.object.contact.user,
                )

        return HttpResponseRedirect(self.get_success_url())

    def get_initial(self):
        cv = CV.objects.latest(self.object.contact)
        if cv:
            cv_file_name = cv.file_name
        else:
            cv_file_name = None

        code = ""
        radius = 0
        data_type = ""
        area = ""

        try:
            if self.object.job_location_code:
                code = self.object.job_location_code
                data_type = self.object.job_location_type
                area = self.object.location
                radius = self.object.job_location_radius
        except Location.DoesNotExist:
            pass
        except AttributeError:
            pass

        return dict(
            {
                "upload_cv": cv_file_name,
                "code": code,
                "area": area,
                "radius": radius,
                "data_type": data_type,
                "notify_checkbox": self._user_consent_given(
                    self.object.contact.user, self._notify_consent()
                ),
                "search_checkbox": self._user_consent_given(
                    self.object.contact.user, self._search_consent()
                ),
                "newsletter_checkbox": self._user_consent_given(
                    self.object.contact.user, self._newsletter_consent()
                ),
            }
        )


class CandidateUpdateCurrentUserProfileMixin(CandidateUpdateProfileMixin):
    """Update the candidate profile.
    Use as follows:
    CandidateUpdateCurrentUserProfileMixin, BaseMixin, UpdateView

    Note: If using with 'PageFormMixin', this must come after this mixin
    i.e. as follows:
    CandidateUpdateCurrentUserProfileMixin, PageFormMixin,
    BaseMixin, UpdateView
    """

    redirect_text = (
        "Please update your profile (including a CV) to apply for a job"
    )
    normal_text = "Update your profile"

    def get_object(self):
        candidate = None
        if _is_contact(self.request.user):
            contact = self.request.user.contact
            candidate, created = Candidate.objects.get_or_create(
                contact=contact,
                defaults={"job_experience": contact.get_job_experience()},
            )
            if candidate:
                if candidate.is_deleted:
                    candidate.undelete()
                return candidate
            else:
                raise JobError(
                    "Cannot update candidate profile for user '{}' - unable to "
                    "retrieve or create profile".format(
                        self.request.user.username
                    )
                )
        else:
            raise JobError(
                "Cannot view candidate profile - user '{}' does not have a "
                "contact record".format(self.request.user.username)
            )


class CandidateUpdatePrimaryKeyProfileMixin(CandidateUpdateProfileMixin):
    """Update the candidate profile.
    Use as follows:
    CandidateUpdatePrimaryKeyProfileMixin, BaseMixin, UpdateView

    Note: If using with 'PageFormMixin', this must come after this mixin
    i.e. as follows:
    CandidateUpdatePrimaryKeyProfileMixin, PageFormMixin,
    BaseMixin, UpdateView
    """

    redirect_text = ""
    normal_text = ""

    def get_object(self):
        pk = self.kwargs.get("pk")
        return Candidate.objects.get(pk=pk)

    def get_success_url(self):
        return reverse("contact.detail", args=[self.object.contact.pk])


class JobApplicationMixin(object):
    """Designed to be used with a Django 'CreateView'."""

    model = JobApplication
    form_class = JobApplicationForm

    def _job(self):
        job_pk = self.kwargs.get("job_pk")
        return Job.objects.get(pk=job_pk)

    def _mail_recruiter(self):
        url = self.request.build_absolute_uri(
            reverse("recruitment.application.detail", args=[self.object.pk])
        )
        if self.object.job.applications_email:
            email_to = self.object.job.applications_email
        else:
            email_to = self.object.job.recruiter.contact.user.email
        context = {
            email_to: {
                "name": self.object.job.recruiter.contact.user.first_name.title(),
                "candidate": self.object.candidate.contact.full_name,
                "date": self.object.created.strftime("%d/%m/%Y %H:%M"),
                "job": self.object.job.title,
                "url": url,
            }
        }
        queue_mail_template(
            self.object, RECRUITER_APPLICATION_FROM_CANDIDATE, context
        )
        transaction.on_commit(lambda: process_mail.send())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        job = self._job()
        if job.is_from_feed:
            raise JobError(
                "Cannot apply for a job imported from a feed: '{}'".format(
                    job.pk
                )
            )
        context.update(
            dict(
                job=job, latest_cv=CV.objects.latest(self.request.user.contact)
            )
        )
        return context

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.apply_date = timezone.now().date()
            self.object.candidate = self.request.user.contact.candidate
            self.object.job = self._job()
            existing_cv = CV.objects.latest(self.request.user.contact)
            if existing_cv:
                self.object.cv = existing_cv
            else:
                raise ValidationError(
                    "You must upload a CV before applying for a job. "
                    "Please return to your profile and upload a CV."
                )
            self.object = form.save()
            self._mail_recruiter()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("recruitment.candidate.dashboard")


class JobApplicationDetailMixin(object):
    model = JobApplication

    def get_success_url(self):
        return reverse("job.candidate.dash")


class JobApplicationRedirectMixin(object):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        candidate = None
        redirect_url = None
        job = get_object_or_404(Job, pk=kwargs["job_pk"])
        job_app_url = reverse("recruitment.job.application", args=[job.pk])
        if self.request.user.is_anonymous:
            pass
        else:
            if self.request.user.contact.is_candidate:
                candidate = self.request.user.contact.candidate
            else:
                contact = self.request.user.contact
                candidate = Candidate(
                    contact=contact, job_experience=contact.get_job_experience()
                )
                candidate.save()
        if job.is_from_feed:
            JobTracking.objects.track_application_click(candidate, job)
            redirect_url = job.jobfeeditem.url
        else:
            if candidate:
                JobTracking.objects.track_application_click(candidate, job)
                cv = CV.objects.latest(candidate.contact)
                if cv:
                    redirect_url = job_app_url
                else:
                    redirect_url = "{}?next={}".format(
                        reverse("recruitment.candidate.update.profile"),
                        job_app_url,
                    )
            else:
                # redirect to login
                redirect_url = "{}?next={}".format(
                    reverse("recruitment.candidate.create"),
                    reverse(
                        "recruitment.job.application.redirect", args=[job.pk]
                    ),
                )
        return redirect_url


class JobCreateMixin(object):
    """Create a job for a recruiter.

    Use with:
    JobCreateMixin, BaseMixin, CreateView):

    """

    model = Job
    form_class = JobForm
    public_facing_page = False

    def _get_recruiter(self):
        if not self.public_facing_page:
            pk = self.kwargs.get("pk", None)
            try:
                recruiter = Recruiter.objects.get(pk=int(pk))
            except Recruiter.DoesNotExist:
                raise JobError("Cannot find recruiter: {}".format(pk))
        elif _is_recruiter(self.request.user):
            recruiter = self.request.user.contact.recruiter
        else:
            raise JobError("User is not a recruiter or staff")
        return recruiter

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                sub_heading="Create a new job", job_settings=JobSettings.load()
            )
        )
        return context

    def get_initial(self):
        days = JobSettings.load().job_post_max_days_displayed
        close_date = timezone.now().date() + relativedelta(days=days)
        recruiter = self._get_recruiter()
        return {
            "close_date": close_date,
            "applications_email": recruiter.contact.user.email,
        }

    def form_valid(self, form):
        code = form.cleaned_data.get("code")
        location = Location.objects.get(postcode=code)
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.location = location
            self.object.recruiter = self._get_recruiter()
            self.object.order_key = ILSPA_RECRUITER
            # PK 06/02/2018, I dont' think the ``JobForm`` allows an empty
            # ``close_date``?
            if not self.object.close_date:
                job_settings = JobSettings.load()
                today = timezone.now().date()
                days = job_settings.job_post_max_days_displayed
                self.object.close_date = today + relativedelta(days=days)
            self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        if self.request.user.is_staff:
            recruiter = self._get_recruiter()
            return reverse("contact.detail", args=[recruiter.contact.pk])


class JobDeleteMixin(object):
    """Mark a job as deleted.

    Use with:
    JobDeleteMixin, BaseMixin, UpdateView

    Your views must create the following:
    1) A 'template_name' e.g. 'job/job_confirm_delete.html'
    2) A 'get_success_url' method.

    """

    model = Job
    form_class = JobEmptyForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        messages.info(
            self.request,
            "Deleted job - {} {} ({})".format(
                self.object.recruiter,
                self.object.title,
                self.object.close_date.strftime("%d/%m/%Y"),
            ),
        )
        return HttpResponseRedirect(self.get_success_url())


class JobUndeleteMixin(object):
    """Clear job deleted flag.

    Use with:
    JobUndeleteMixin, BaseMixin, UpdateView

    Your views must create the following:
    1) A 'template_name' e.g. 'job/job_confirm_undelete.html'
    2) A 'get_success_url' method.

    """

    model = Job
    form_class = JobEmptyForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.undelete()
        messages.info(
            self.request,
            "Undeleted job - {} {} ({})".format(
                self.object.recruiter,
                self.object.title,
                self.object.close_date.strftime("%d/%m/%Y"),
            ),
        )
        return HttpResponseRedirect(self.get_success_url())


class JobDetailMixin(object):
    """Job detail.

    Use with the following:

    JobDetailMixin, BaseMixin, DetailView

    """

    model = Job

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        is_candidate = False
        is_recruiter = False
        if self.request.user.is_authenticated:
            is_candidate = _is_candidate(self.request.user)
            is_recruiter = _is_recruiter(self.request.user)
        if not is_recruiter and not self.request.user.is_staff:
            candidate = None
            if self.request.user.is_anonymous:
                pass
            else:
                # if _is_candidate(self.request.user):
                #     candidate = self.request.user.contact.candidate
                # elif _is_contact(self.request.user):
                #     # PK 06/02/2018, Why do we auto-create a candidate record?
                #     # MD 26/11/2019 - so system can track job views
                #     contact = self.request.user.contact
                #     candidate = Candidate(
                #         contact=contact,
                #         job_experience=contact.get_job_experience(),
                #     )
                #     candidate.save()
                if _is_contact(self.request.user):
                    contact = self.request.user.contact
                    candidate, created = Candidate.objects.get_or_create(
                        contact=contact,
                        defaults={
                            "job_experience": contact.get_job_experience(),
                        },
                    )
                else:
                    raise JobError(
                        "Cannot view job detail - user '{}' does not have a "
                        "contact record".format(self.request.user.username)
                    )
            if not self.object.is_from_feed:
                JobTracking.objects.track_view(candidate, self.object)
        applications = self.object.applications(self.request.user)
        tracking = self.object.tracking(self.request.user)
        context.update(
            dict(
                is_candidate=is_candidate,
                is_recruiter=is_recruiter,
                applications=applications,
                tracking=tracking,
            )
        )
        return context


class JobElasticSearchBaseMixin:
    """Base class for job search using ElasticSearch.

    need to define ``paginate_by`` and ``page_kwarg``

    """

    INDEX = "job-index"
    DOC_TYPE = "job"
    page_kwarg = "page_no"
    paginate_by = 20

    def _display_latest_jobs(self, form_data):
        """Display latest jobs if there are no query parameters."""
        display_latest = True
        for key, value in form_data.items():
            if key == self.page_kwarg:
                pass
            elif key and value:
                display_latest = False
                break
        return display_latest

    def _latest_jobs(self, current_page):
        """Return the latest jobs.

        .. note:: This method needs to return only the jobs to be displayed on
                  the page so it's consistant with results returned by
                  ElasticSearch.

        .. note:: Move the data into the ``search`` (ElasticSearch) structure,
                  (``Row`` and ``Hit``).

        """
        result = []
        qs = Job.objects.latest()
        total = qs.count()
        paginator = Paginator(qs, self.paginate_by)
        try:
            page = paginator.page(current_page)
            for row in page.object_list:
                job_role = practice_area = None
                if row.job_role:
                    job_role = row.job_role.title
                if row.practice_area:
                    practice_area = row.practice_area.title
                result.append(
                    Hit(
                        pk=row.pk,
                        data=Row(
                            close_date=row.close_date,
                            description=row.description,
                            job_role=job_role,
                            location=row.location_name,
                            order_key=row.order_key,
                            practice_area=practice_area,
                            publish_date=row.publish_date,
                            recruiter_name=row.recruiter_name,
                            salary=row.salary,
                            title=row.title,
                        ),
                        document_type="job",
                        is_deleted=False,
                        score=0,
                    )
                )
        except EmptyPage:
            pass
        return result, total

    def _page_number(self, form_data):
        page_number = 1
        if self.page_kwarg in form_data:
            try:
                page_number = int(form_data.get(self.page_kwarg))
            except ValueError:
                pass
        return page_number

    def elastic_context_data(self, form_data):
        display_latest = self._display_latest_jobs(form_data)
        page_number = self._page_number(form_data)
        if display_latest:
            job_list, total = self._latest_jobs(page_number)
        else:
            index = SearchIndex(JobIndex())
            job_list, total = index.search(
                form_data, page_number=page_number, page_size=self.paginate_by
            )
        page_obj = ElasticPaginator(page_number, self.paginate_by, total)
        return dict(
            job_list=job_list,
            is_paginated=page_obj.has_other_pages(),
            page_obj=page_obj,
        )


class JobElasticSearchMixin(JobElasticSearchBaseMixin):
    """List of jobs using elastic search.

    Designed to be used with:
    JobElasticSearchMixin, BaseMixin, ListView

    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form_data = self.request.GET.copy()
        form = JobElasticSearchForm(form_data)
        form.is_valid()
        get_parameters = self.request.GET.copy()
        if self.page_kwarg in get_parameters:
            del get_parameters[self.page_kwarg]
        context.update(self.elastic_context_data(form_data))
        # add common context data
        context.update(
            dict(get_parameters=get_parameters.urlencode(), form=form)
        )
        return context


class JobLatestMixin(object):
    """List of latest jobs

    Designed to be used with:
    JobLatestMixin, BaseMixin, ListView

    """

    context_object_name = "job_list"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(form=JobElasticSearchForm({})))
        return context

    def get_queryset(self):
        return Job.objects.latest()


class JobListMixin:
    """List of jobs using conventional search ...

    Designed to be used with::

      JobListMixin, BaseMixin, ListView

    """

    display_unpublished = False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        source = self.kwargs.get("source", None)
        form_data = self.request.GET.copy()
        radius = int(form_data.get("radius") or 25)
        form_data["radius"] = radius
        form = JobSearchForm(form_data)
        form.is_valid()
        get_parameters = self.request.GET.copy()
        if self.page_kwarg in get_parameters:
            del get_parameters[self.page_kwarg]
        context.update(
            dict(
                get_parameters=get_parameters.urlencode(),
                form=form,
                source=source,
                job_settings=JobSettings.load(),
            )
        )
        return context

    def get_queryset(self):
        """Get the search criteria."""
        region = None
        postcode = None
        postcode_range = None
        area = self.request.GET.get("area")
        # PK 06/02/2018 I don't think this code is used?
        # The ``area`` parameter is checked, but then not used?
        if area and len(area) > 1:
            code = self.request.GET.get("code")
            data_type = self.request.GET.get("data_type")
            if data_type == "region":
                try:
                    region = Region.objects.get(region_code=code)
                except Region.DoesNotExist:
                    pass
            elif data_type == "postcode":
                try:
                    location = Location.objects.get(postcode=code)
                    postcode = location.postcode
                    postcode_range = int(self.request.GET.get("radius") or 0)
                except Location.DoesNotExist:
                    pass
        title = self.request.GET.get("title")
        recruiter = self.request.GET.get("recruiter")
        source = self.kwargs.get("source", None)
        qs = Job.objects.search(
            region,
            postcode,
            postcode_range,
            title,
            recruiter,
            self.display_unpublished,
        )
        if source:
            qs = qs.filter(order_key=source)
        return qs


class JobBasketMixin(object):
    """To view the job payment batch details.

    To use this mixin, you probably want to use 'StaffLoginRequiredMixin' e.g:
        (LoginRequiredMixin, StaffuserRequiredMixin,
        JobPayDetailMixin, BaseMixin, DetailView)

    """

    template_name = "job/checkout_basket_detail.html"
    model = JobBasket

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                checkout_audit=Checkout.objects.audit_content_object(
                    self.object
                )
            )
        )
        return context


class JobCreditMixin(object):
    """To view the job credit payment details.

    To use this mixin, you probably want to use 'StaffLoginRequiredMixin' e.g:
        (LoginRequiredMixin, StaffuserRequiredMixin,
        JobCreditMixin, BaseMixin, DetailView)

    """

    template_name = "job/checkout_credit_detail.html"
    model = JobCredit

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                checkout_audit=Checkout.objects.audit_content_object(
                    self.object
                )
            )
        )
        return context


class JobPublishMixin(object):
    model = Job
    form_class = JobEmptyForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.publish_job(self.request.user)
        return HttpResponseRedirect(self.get_success_url())


class JobUpdateMixin(RecruiterOrStaffLoginRequiredMixin, BaseMixin):
    form_class = JobForm
    model = Job

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(sub_heading="Update Job", job_settings=JobSettings.load())
        )
        return context

    def get_initial(self):
        result = {}
        if self.object.location:
            result.update(
                {
                    "area": self.object.location.form_description,
                    "code": self.object.location.postcode,
                }
            )
        return result

    def form_valid(self, form):
        code = form.cleaned_data.get("code")
        location = Location.objects.get(postcode=code)
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.location = location
            self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        if self.request.user.is_staff:
            return reverse("job.detail", args=[self.object.pk])
        else:
            return reverse("recruitment.recruiter.dashboard")


class LandingPageCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    model = LandingPage
    form_class = LandingPageCreateForm

    def form_valid(self, form):
        name = form.cleaned_data["name"]
        self.object = form.save(commit=False)
        with transaction.atomic():
            page = LandingPage.objects.create_page(name)
            self.object.page = page
            result = super().form_valid(form)
        return result

    def get_success_url(self):
        return reverse("job.landing.page.list")


class LandingPageListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = LandingPage

    def get_queryset(self):
        return LandingPage.objects.landing_pages()


class LandingPageMixin(JobElasticSearchBaseMixin):
    """Landing Page Mixin.

    Use as follows::

      PageFormMixin, LandingPageMixin, BaseMixin, TemplateView):

    I used a URL like this::

      url(regex=r'^{}/(?P<menu>[-\w\d]+)/$'.format(PAGE_JOB_LANDING),
          view=RecruitmentJobLandingPageView.as_view(),
          kwargs=dict(page=PAGE_JOB_LANDING),
          name='recruitment.job.landing.page'
          ),

    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        landing_page = LandingPage.objects.get(page=self.get_page())
        data = {
            "location": landing_page.location,
            "job_role": landing_page.job_role,
            "practice_area": landing_page.practice_area,
        }
        context.update(self.elastic_context_data(data))
        context.update(
            dict(
                form=JobElasticSearchForm(data),
                is_landing_page=True,
                is_paginated=False,
                jobs_list_legend=landing_page.page.name,
            )
        )
        return context


class LandingPageUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = LandingPage
    form_class = LandingPageForm

    def get_success_url(self):
        return reverse("job.landing.page.list")


class RecruiterCreateMixin(ConsentMaintenanceMixin):
    """Create a recruiter.

    Used with:
    RecruiterCreateMixin, BaseMixin, CreateView

    """

    consent_form_settings = "generic-form"
    form_class = RecruiterCreateForm
    model = apps.get_model(settings.CONTACT_MODEL)

    def _data_consent(self):
        return self._consent(Recruiter.GDPR_DATA_SLUG)

    def _newsletter_consent(self):
        return self._consent(Recruiter.GDPR_NEWSLETTER_SLUG)

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        first_name = form.cleaned_data.get("first_name")
        last_name = form.cleaned_data.get("last_name")
        password = form.cleaned_data.get("password1")
        user_name = form.cleaned_data.get("username")
        data_checkbox = form.cleaned_data.get("data_checkbox") or False
        newsletter_checkbox = (
            form.cleaned_data.get("newsletter_checkbox") or False
        )
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.user = create_user(
                user_name, password, first_name, last_name, email
            )
            self.object = form.save()
            recruiter = Recruiter(contact=self.object)
            recruiter.save()
            self._save_consent(
                self._data_consent(), data_checkbox, self.object.user
            )
            self._save_consent(
                self._newsletter_consent(),
                newsletter_checkbox,
                self.object.user,
            )
            recruiter.send_welcome_email()
        if not self.request.user.is_staff:
            login_new_user(self.request, user_name, password)
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(
            {
                "data_consent": self._data_consent(),
                "newsletter_consent": self._newsletter_consent(),
            }
        )
        return kwargs


class RecruiterDisplayMixin(BaseMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(job_detail_url_name=self.job_detail_url_name))
        return context


class RecruiterDashMixin(ConsentDisplayMixin):
    """Recruiter dashboard with option to pay for job postings.

    To use this mixin:

    - add into a 'CreateView'
      RecruiterDashMixin, BaseMixin, CreateView
    - implement 'add_jobs_to_basket'
    - implement 'url_payment', 'url_thankyou' and 'url_sorry'

    """

    form_class = JobBasketEmptyForm
    model = JobBasket

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = self.request.user.contact
        context.update(
            dict(
                is_recruiter=contact.is_recruiter,
                object=contact.recruiter,
                job_settings=JobSettings.load(),
                summary=True,
                newsletter_consent=self._consent_given(
                    Recruiter.GDPR_NEWSLETTER_SLUG
                ),
            )
        )
        self.request.session[CONTENT_OBJECT_PK] = contact.recruiter.pk
        return context

    def form_valid(self, form):
        if self.request.user.contact.recruiter.has_subscription:
            with transaction.atomic():
                jobs = self.request.user.contact.recruiter.jobs_pending()
                for job in jobs:
                    job.publish_job(self.request.user)
            return HttpResponseRedirect(
                reverse("recruitment.recruiter.dashboard")
            )
        else:
            with transaction.atomic():
                self.object = form.save(commit=False)
                self.object.recruiter = self.request.user.contact.recruiter
                self.object.save()
                self.add_jobs_to_basket(self.object)
                self.request.session[CONTENT_OBJECT_PK] = self.object.pk
            return HttpResponseRedirect(
                reverse("recruitment.job.checkout", args=[self.object.pk])
            )


class JobCreditCreateBaseMixin(object):
    def get_job_credit_product(self):
        """Get the product for these job credits credits.

        - if the recruiter is purchasing more than ``amount_for discount``
          credits they get the discounted rate
        - override this for a different method of working out the product
        """
        if self.object.credits > self.discount_threshold:
            product_slug = PRODUCT_JOB_POST_DISCOUNT
        else:
            product_slug = PRODUCT_JOB_POST
        return Product.objects.get(slug=product_slug)

    def add_pricing_to_context(self, context):
        """Add pricing structure to context.
        - a Product for the standard price is required, if no discount product
          is available all products will be charged at the standard price
        - override this for a different pricing structure
        """
        product_standard = Product.objects.get(slug=PRODUCT_JOB_POST)
        try:
            discount_price = Product.objects.get(
                slug=PRODUCT_JOB_POST_DISCOUNT
            ).price
        except Product.DoesNotExist:
            discount_price = product_standard.price

        context.update(
            {
                "standard_price": product_standard.price,
                "discount_threshold": self.discount_threshold,
                "discount_price": discount_price,
            }
        )


class RecruiterJobCreditMixin(JobCreditCreateBaseMixin):
    """Recruiter Job Credits with a discount.

    To use this mixin:

    - add into a 'CreateView'
      RecruiterOrStaffLoginRequiredMixin, RecruiterJobCreditDiscountMixin,
      BaseMixin, CreateView

    - you can increase the number credits that need to be purchased before a
      discount is applied by defining a discount threshold (default = 1, i.e.
      purchase of more that one job credit will attract the discount price) or
      override the get_job_credit_product and add_pricing_to_context to use a
      different pricing structure

    - implement 'url_payment', 'url_thankyou' and 'url_sorry'

    """

    discount_threshold = 1
    form_class = JobCreditForm
    model = JobCredit

    def get_job_credit_product(self):
        """Get the product for these job credits credits.

        - if the recruiter is purchasing more than ``amount_for discount``
          credits they get the discounted rate
        - override this for a different method of working out the product
        """
        if self.object.credits > self.discount_threshold:
            product_slug = PRODUCT_JOB_POST_DISCOUNT
        else:
            product_slug = PRODUCT_JOB_POST
        return Product.objects.get(slug=product_slug)

    def add_pricing_to_context(self, context):
        """Add pricing structure to context.
        - a Product for the standard price is required, if no discount product
          is available all products will be charged at the standard price
        - override this for a different pricing structure
        """
        product_standard = Product.objects.get(slug=PRODUCT_JOB_POST)
        try:
            discount_price = Product.objects.get(
                slug=PRODUCT_JOB_POST_DISCOUNT
            ).price
        except Product.DoesNotExist:
            discount_price = product_standard.price

        context.update(
            {
                "standard_price": product_standard.price,
                "discount_threshold": self.discount_threshold,
                "discount_price": discount_price,
            }
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = self.request.user.contact
        context.update(dict(is_recruiter=contact.is_recruiter))
        self.add_pricing_to_context(context)
        self.request.session[CONTENT_OBJECT_PK] = contact.recruiter.pk
        return context

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.recruiter = self.request.user.contact.recruiter
            self.object.product = self.get_job_credit_product()
            self.object.save()
            self.request.session[CONTENT_OBJECT_PK] = self.object.pk
        return HttpResponseRedirect(
            reverse("recruitment.job.checkout", args=[self.object.pk])
        )


class CandidateListMixin:
    """Search for candidates.

    Use as follows:
    CandidateListMixin, BaseMixin, ListView

    """

    model = Candidate
    page_kwarg = "page_no"
    template_name = "job/candidate_list.html"

    def get_search_form(self):
        # override this if the view always uses the CandidateSearchForm
        if self.request.user.is_staff:
            return StaffCandidateSearchForm
        return CandidateSearchForm

    def get_queryset(self):
        """Get the search criteria."""
        (
            experience,
            last_login,
            order_by,
            postcode,
            postcode_range,
            region,
            search_type,
        ) = candidate_search_parameters(self.request)
        can_search = False
        if self.request.user.is_staff:
            can_search = True
        elif (
            self.request.user.contact.is_recruiter
            and self.request.user.contact.recruiter.can_search()
        ):
            can_search = True
        if can_search:
            qs = Candidate.objects.search(
                search_type,
                region,
                postcode,
                postcode_range,
                experience,
                last_login,
                order_by,
                self.request.user.is_staff,
            )
            qs = (
                qs.select_related("contact")
                .select_related("contact__user")
                .select_related("experience")
                .select_related("highest_education")
                .select_related("job_experience")
            )
        else:
            qs = Candidate.objects.none()
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        is_recruiter = _is_recruiter(self.request.user)

        if self.request.user.is_staff:
            can_search = True
        elif is_recruiter:
            can_search = self.request.user.contact.recruiter.can_search()
        else:
            can_search = False
        info_message = None
        if can_search:
            form_data = self.request.GET.copy()
            radius = int(form_data.get("radius") or 25)
            form_data["radius"] = radius
            form = self.get_search_form()(form_data)
            form.is_valid()
            context.update(dict(form=form, job_settings=JobSettings.load()))
        else:
            info_message = (
                "Please post a job in order to have access to our candidate "
                "database"
            )
        get_parameters = self.request.GET.copy()
        if self.page_kwarg in get_parameters:
            del get_parameters[self.page_kwarg]
        context.update(
            dict(
                can_search=can_search,
                get_parameters=get_parameters.urlencode(),
                info_message=info_message,
                is_recruiter=is_recruiter,
            )
        )
        return context


class RecruiterUpdateMixin(object):
    """Recruiter update base class.  Do NOT use this class.

    Use either:
    RecruiterUpdateCurrentUserMixin
    or
    RecruiterUpdatePrimaryKeyMixin

    Note:
    This class updates the contact model NOT the recruiter.

    """

    form_class = RecruiterForm

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        first_name = form.cleaned_data.get("first_name")
        last_name = form.cleaned_data.get("last_name")
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.user.email = email
            self.object.user.first_name = first_name
            self.object.user.last_name = last_name
            self.object.user.save()
            self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(legend="Update Recruiter Details"))
        return context

    def get_initial(self):
        return dict(
            first_name=self.object.user.first_name,
            last_name=self.object.user.last_name,
            email=self.object.user.email,
        )


class RecruiterUpdateCurrentUserMixin(RecruiterUpdateMixin):
    """Update the recruiter information for the current user.

    The current user must have a recruiter record (or perhaps we should create
    one)?

    Use with:
    RecruiterUpdateCurrentUserMixin, BaseMixin, UpdateView

    Note:
    This class updates the contact model NOT the recruiter.

    """

    def get_object(self):
        model = apps.get_model(settings.CONTACT_MODEL)
        return model.objects.get(user=self.request.user)

    def get_success_url(self):
        return reverse("recruitment.recruiter.dashboard")


class RecruiterUpdatePrimaryKeyMixin(RecruiterUpdateMixin):
    """Update the recruiter information for the user with this key.

    Use with:
    RecruiterUpdatePrimaryKeyMixin, BaseMixin, UpdateView

    Note:
    This class updates the contact model NOT the recruiter.

    """

    model = apps.get_model(settings.CONTACT_MODEL)

    def get_object(self):
        """Find the contact object for the recruiter."""
        pk = self.kwargs["pk"]
        model = apps.get_model(settings.CONTACT_MODEL)
        return model.objects.get(recruiter__pk=int(pk))

    def get_success_url(self):
        return reverse("contact.detail", args=[self.object.pk])


class RecruiterUpdateProfileMixin(ConsentMaintenanceMixin):
    """Update the recruiter profile on a web page.

    Use as follows::

      RecruiterLoginRequiredMixin, RecruiterUpdateProfileMixin,
      PageFormMixin, BaseMixin, UpdateView):

    .. note:: If you are using ``PageFormMixin``, then this must come **AFTER**
              ``RecruiterUpdateProfileMixin``

    """

    consent_form_settings = "generic-form"

    def _newsletter_consent(self):
        return self._consent(Recruiter.GDPR_NEWSLETTER_SLUG)

    def get_form_class(self):
        return RecruiterProfileForm

    def get_object(self):
        return Recruiter.objects.get(contact__user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_staff and self.object:
            legend = "Update Recruiter Profile for : " + self.object.full_name
        else:
            legend = "Update Recruiter Profile"
        context.update(dict(legend=legend))
        return context

    def form_valid(self, form):
        newsletter_checkbox = (
            form.cleaned_data.get("newsletter_checkbox") or False
        )
        with transaction.atomic():
            self.object = form.save()
            self._save_consent(
                self._newsletter_consent(),
                newsletter_checkbox,
                self.object.contact.user,
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"newsletter_consent": self._newsletter_consent()})
        return kwargs

    def get_initial(self):
        return dict(
            {
                "newsletter_checkbox": self._user_consent_given(
                    self.object.contact.user, self._newsletter_consent()
                )
            }
        )

    def get_success_url(self):
        return reverse("recruitment.recruiter.dashboard")


# VIEWS


class CandidateCreateView(CandidateCreateMixin, CreateView):
    pass


class CandidateDashView(CandidateDashMixin, TemplateView):
    template_name = "job/candidate_detail.html"


class CandidateListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    CandidateListMixin,
    BaseMixin,
    ListView,
):
    paginate_by = 20


class CandidateUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    CandidateUpdatePrimaryKeyMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "job/candidate_form.html"


class CandidateUpdateProfileView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    CandidateUpdatePrimaryKeyProfileMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "job/candidate_form.html"


@never_cache
def cv_download(request, pk):
    """Download the CV with this primary key.

    https://github.com/johnsensible/django-sendfile

    """
    if request.user.is_anonymous:
        raise PermissionDenied
    contact = request.user.contact
    cv = get_object_or_404(CV, pk=pk)
    if request.user.is_staff:
        # I am a member of staff
        logger.info(
            f"'cv_download' ({cv.pk}) by '{request.user.username}' "
            f"(staff) for '{cv.contact.user.username}'"
        )
        return _download_cv(request, cv)
    elif cv.contact == contact:
        # I am trying to download one of my own CVs
        logger.info(
            f"'cv_download' ({cv.pk}) (my CV) by '{request.user.username}' "
            f"for '{cv.contact.user.username}'"
        )
        return _download_cv(request, cv)
    elif contact.is_recruiter and contact.recruiter.can_download_cv(cv):
        # I am a recruiter and this candidate is applying for one of my jobs
        logger.info(
            f"'cv_download' ({cv.pk}) by '{request.user.username}' "
            f"(recruiter) for '{cv.contact.user.username}'"
        )
        return _download_cv(request, cv)
    else:
        raise PermissionDenied


class JobApplicationCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, JobApplicationMixin, CreateView
):
    template_name = "job/job_application.html"


class JobApplicationDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobApplicationDetailMixin,
    DetailView,
):
    # template_name = "job/jobapplication.html"
    pass


class JobCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobCreateMixin,
    BaseMixin,
    CreateView,
):
    pass


class JobCreditMarkPaidView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    """Recruiter Job Credits mark as paid."""

    template_name = "job/jobcredit_paid_form.html"

    form_class = JobCreditEmptyForm
    model = JobCredit

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.mark_paid()
        self.object.save()
        messages.info(
            self.request,
            "{} Job Credits marked paid for {} ({})".format(
                self.object.credits,
                self.object.recruiter,
                self.object.created.strftime("%d/%m/%Y"),
            ),
        )

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            "contact.detail", args=[self.object.recruiter.contact.pk]
        )


class JobCreditCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobCreditCreateBaseMixin,
    BaseMixin,
    CreateView,
):
    """Staff member view to create Job Credits without a checkout."""

    discount_threshold = 1
    form_class = JobCreditForm
    model = JobCredit

    def _recruiter(self):
        return Recruiter.objects.get(contact__pk=self.kwargs["pk"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.add_pricing_to_context(context)
        context.update(
            dict(sub_heading="Job credit for {}".format(self._recruiter()))
        )
        return context

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.recruiter = self._recruiter()
            self.object.product = self.get_job_credit_product()
            self.object.available_credits = self.object.credits
            self.object.paid = True
            self.object.save()
        return HttpResponseRedirect(
            reverse("contact.detail", args=[self.object.recruiter.contact.pk])
        )


class JobDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobDeleteMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "job/job_confirm_delete.html"

    def get_success_url(self):
        return reverse(
            "contact.detail", args=[self.object.recruiter.contact.pk]
        )


class JobUndeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobUndeleteMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "job/job_confirm_undelete.html"

    def get_success_url(self):
        return reverse(
            "contact.detail", args=[self.object.recruiter.contact.pk]
        )


class JobElasticSearchView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobElasticSearchMixin,
    TemplateView,
):
    template_name = "job/job_elastic_search.html"
    paginate_by = 20


class JobLatestView(
    LoginRequiredMixin, StaffuserRequiredMixin, JobLatestMixin, ListView
):
    template_name = "job/job_list.html"
    model = Job
    paginate_by = 20


class JobPublishView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobPublishMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "job/job_confirm_publish.html"

    def get_success_url(self):
        return reverse(
            "contact.detail", args=[self.object.recruiter.contact.pk]
        )


class JobListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """List of jobs for a member of staff to view."""

    paginate_by = 20
    model = Job

    def _job_feed(self):
        result = None
        job_feed_pk = int(self.kwargs["job_feed_pk"])
        if job_feed_pk:
            try:
                result = JobFeed.objects.get(pk=job_feed_pk)
            except JobFeed.DoesNotExist:
                raise JobError("Cannot find job feed '{}'".format(job_feed_pk))
        return result

    def get_queryset(self):
        """Display a list of jobs.

        Different behaviour depending on the ``job_feed_pk``:

        1. If the ``job_feed_pk`` is zero (``0``), then we display ILSPA jobs
           (``order_key=ILSPA_RECRUITER``).
        2. If the ``job_feed_pk`` is *not* zero, then we find the job feed and
           display the jobs for that feed.

        """
        job_feed = self._job_feed()
        if job_feed:
            qs = job_feed.jobs()
        else:
            qs = Job.objects.filter(order_key=ILSPA_RECRUITER)
        return qs.order_by("-created")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        job_feeds = JobFeed.objects.current().order_by("slug")
        context.update(dict(job_feed=self._job_feed(), job_feeds=job_feeds))
        return context


class JobSearchView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobListMixin,
    BaseMixin,
    ListView,
):
    """List of jobs for a member of staff to view."""

    display_unpublished = True
    page_kwarg = "page_no"
    paginate_by = 15
    template_name = "job/job_search.html"


class JobUpdateView(JobUpdateMixin, UpdateView):
    pass


class JobDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobDetailMixin,
    RedirectNextMixin,
    BaseMixin,
    DetailView,
):
    pass


class JobBasketDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobBasketMixin,
    BaseMixin,
    DetailView,
):
    pass


class JobCreditDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JobCreditMixin,
    BaseMixin,
    DetailView,
):
    pass


class LocationCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = LocationForm
    model = Location

    def get_success_url(self):
        return reverse("job.location.list")


class LocationDetailView(BaseMixin, DetailView):
    model = Location


class LocationListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = Location


class LocationUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = LocationForm
    model = Location

    def get_success_url(self):
        return reverse("job.location.list")


class RecruiterCreateView(RecruiterCreateMixin, CreateView):
    pass


class RecruiterDashView(
    LoginRequiredMixin, StaffuserRequiredMixin, RecruiterDashMixin, TemplateView
):
    template_name = "job/recruiter_detail.html"


class RecruiterListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 15
    model = Recruiter


class RecruiterUpdateProfileView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    template_name = "job/recruiter_form.html"

    def get_form_class(self):
        return StaffRecruiterProfileForm

    def get_object(self):
        pk = self.kwargs["pk"]
        return Recruiter.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_staff and self.object:
            legend = "Update Recruiter Profile for : " + self.object.full_name
        else:
            legend = "Update Recruiter Profile"
        context.update(dict(legend=legend))
        return context

    def get_success_url(self):
        return reverse("contact.detail", args=[self.object.contact.pk])


class RecruiterUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RecruiterUpdatePrimaryKeyMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "job/recruiter_form.html"


class JobLocationAjaxMixin(object):
    def location(self, query):
        result = []
        qs = Location.objects.filter(
            models.Q(postcode__icontains=query)
            | models.Q(town__icontains=query)
        ).order_by("postcode")
        for l in qs:
            result.append(
                {
                    "value": "{}".format(l.form_description),
                    "data": {"code": l.postcode, "data_type": "postcode"},
                }
            )
        return result

    def region(self, query):
        result = []
        qs = Region.objects.filter(name__icontains=query).order_by("name")
        for r in qs:
            result.append(
                {
                    "value": r.name,
                    "data": {"code": r.region_code, "data_type": "region"},
                }
            )
        return result

    def prepare_response(self, suggestions):
        json_dict = {"query": "Unit", "suggestions": suggestions}
        return self.render_json_response(json_dict)


class JobLocationAjaxView(
    JobLocationAjaxMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    """Return the matching location."""

    def get_ajax(self, request, *args, **kwargs):
        suggestions = []
        query = self.request.GET.get("query")
        if query:
            suggestions = self.location(query)
        return self.prepare_response(suggestions)


class JobLocationRegionAjaxView(
    JobLocationAjaxMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    """Return the matching location AND region."""

    def get_ajax(self, request, *args, **kwargs):
        suggestions = []
        query = self.request.GET.get("query")
        if query:
            suggestions = suggestions + self.location(query)
            suggestions = suggestions + self.region(query)
        return self.prepare_response(suggestions)
