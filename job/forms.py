# -*- encoding: utf-8 -*-
from django import forms
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils import timezone
from django_recaptcha.fields import ReCaptchaField
from django_recaptcha.widgets import ReCaptchaV3

from base.form_utils import (
    RequiredFieldForm,
    set_widget_required,
    FileDropInput,
)
from gdpr.forms import ConsentFieldFormMixin
from login.forms import UserUsernameCreationForm
from .models import (
    Candidate,
    CV,
    CVType,
    Job,
    JobApplication,
    JobRole,
    JobBasket,
    JobCredit,
    LandingPage,
    Location,
    Recruiter,
    Experience,
)


LAST_LOGIN_CHOICES = (
    (3, "3 Months"),
    (6, "6 Months"),
    (12, "12 Months"),
    (0, "12 Months+"),
)

SEARCH_TYPE_CHOICES = (
    ("position", "Position (exclude Candidates without a CV)"),
    ("mailshot", "Mailshot (include Candidates without a CV)"),
)


class LandingPageCreateForm(forms.ModelForm):
    name = forms.CharField(max_length=100)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = LandingPage
        fields = ("name", "location", "job_role", "practice_area")


class LandingPageForm(forms.ModelForm):
    class Meta:
        model = LandingPage
        fields = ("location", "job_role", "practice_area")


class LocationLookupForm(forms.Form):
    """Form for doing location search."""

    RADIUS_CHOICES = [
        # (None,  ''),
        (5, "5 miles"),
        (10, "10 miles"),
        (25, "25 miles"),
        (50, "50 miles"),
        (100, "100 miles"),
        (999, "Unlimited"),
    ]

    area = forms.CharField(max_length=200, required=False)
    radius = forms.ChoiceField(choices=RADIUS_CHOICES, required=False)

    code = forms.CharField(max_length=30)
    # data type of region or postcode (depending on 'autocomplete' selection)
    data_type = forms.CharField(max_length=30)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["code"].widget = forms.HiddenInput()
        self.fields["data_type"].widget = forms.HiddenInput()
        self.fields["area"].widget.attrs.update(
            {"class": "autocomplete pure-input-2-3"}
        )
        self.fields["radius"].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["area"].label = "Postcode or Town"
        self.fields["radius"].label = "Radius"

    def clean(self):
        area = self.cleaned_data.get("area")
        data_type = self.cleaned_data.get("data_type")
        code = self.cleaned_data.get("code")

        if data_type and code:
            pass
        elif area:
            raise ValidationError(
                "Please select a valid postcode or region. To select a "
                "postcode (or region) start typing the postcode (or region) "
                "and then click on the correct entry from the list"
            )
        return self.cleaned_data

    class Meta:
        fields = ("postcode", "radius")


class CVForm(RequiredFieldForm):
    """Form for CV."""

    contact = forms.CharField(max_length=100)

    def __init__(self, *args, **kwargs):
        cv_type_id = kwargs.pop("cv_type_id", 0)
        contact_id = kwargs.pop("contact_id", 0)
        super(CVForm, self).__init__(*args, **kwargs)
        for name in ("file_name", "cv_type"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["file_name"].label = "Upload CV"
        if contact_id:
            self.fields["contact"].widget = forms.HiddenInput()
            self.fields["contact"].initial = contact_id
            self.fields["contact"].label = ""
        if cv_type_id:
            self.fields["cv_type"].widget = forms.HiddenInput()
            self.fields["cv_type"].initial = cv_type_id
            self.fields["cv_type"].label = ""
        else:
            # flakes - undefined name 'CVType'
            self.fields["cv_type"] = forms.ChoiceField(
                choices=[(o.id, o.name) for o in CVType.objects.all()],
                label="CV Type",
            )

        self.fields["file_name"].widget = FileDropInput(
            default_text="Drop your CV here...",
            click_text="or click here to select your CV",
        )

    class Meta:
        model = CV
        fields = ("file_name", "contact", "title", "cv_type")


class CandidateForm(RequiredFieldForm):
    """Form for Candidate (uses the 'Contact' model)."""

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(CandidateForm, self).__init__(*args, **kwargs)
        for name in (
            "title",
            "first_name",
            "last_name",
            "email",
            "address_1",
            "address_2",
            "address_3",
            "town",
            "county",
            "country",
            "postcode",
        ):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        for name in (
            "title",
            "first_name",
            "last_name",
            "email",
            "address_1",
            "town",
            "county",
            "country",
            "postcode",
        ):
            set_widget_required(self.fields[name])
        for name in ("phone", "mobile"):
            self.fields[name].widget.attrs[
                "placeholder"
            ] = "Either a phone or mobile number is required"

    def clean(self):
        phone = self.cleaned_data.get("phone")
        mobile = self.cleaned_data.get("mobile")
        if phone or mobile:
            pass
        else:
            raise ValidationError("Please enter a phone or mobile number.")
        return self.cleaned_data

    class Meta:
        model = apps.get_model(settings.CONTACT_MODEL)
        fields = (
            "title",
            "first_name",
            "last_name",
            "phone",
            "mobile",
            "email",
            "address_1",
            "address_2",
            "address_3",
            "town",
            "county",
            "country",
            "postcode",
        )


class CandidateCreateForm(
    ConsentFieldFormMixin, CandidateForm, UserUsernameCreationForm
):
    """Form for Candidate."""

    notify_checkbox = forms.BooleanField(required=False)
    newsletter_checkbox = forms.BooleanField(required=False)
    search_checkbox = forms.BooleanField(required=False)
    data_checkbox = forms.BooleanField(required=False)
    captcha = ReCaptchaField(widget=ReCaptchaV3)

    def __init__(self, *args, **kwargs):
        self.data_consent = kwargs.pop("data_consent")
        self.newsletter_consent = kwargs.pop("newsletter_consent")
        self.notify_consent = kwargs.pop("notify_consent")
        self.search_consent = kwargs.pop("search_consent")
        super().__init__(*args, **kwargs)
        for name in ("username", "password1", "password2"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

        self._setup_checkbox("notify_checkbox", self.notify_consent)
        self._setup_checkbox("search_checkbox", self.search_consent)
        self._setup_checkbox("newsletter_checkbox", self.newsletter_consent)
        self._setup_checkbox("data_checkbox", self.data_consent)

    def clean_data_checkbox(self):
        return self._clean_checkbox("data_checkbox", self.data_consent)

    def clean_notify_checkbox(self):
        return self._clean_checkbox("notify_checkbox", self.notify_consent)

    def clean_newsletter_checkbox(self):
        return self._clean_checkbox(
            "newsletter_checkbox", self.newsletter_consent
        )

    def clean_search_checkbox(self):
        return self._clean_checkbox("search_checkbox", self.search_consent)

    class Meta:
        model = apps.get_model(settings.CONTACT_MODEL)
        fields = (
            "title",
            "first_name",
            "last_name",
            "phone",
            "mobile",
            "email",
            "address_1",
            "address_2",
            "address_3",
            "town",
            "county",
            "country",
            "postcode",
            "username",
            "password1",
            "password2",
        )


class CandidateProfileForm(
    ConsentFieldFormMixin, LocationLookupForm, RequiredFieldForm
):
    # consents
    notify_checkbox = forms.BooleanField(required=False)
    search_checkbox = forms.BooleanField(required=False)
    newsletter_checkbox = forms.BooleanField(required=False)

    # used to validate if user has cv
    next_url = None
    user_name = None

    upload_cv = forms.FileField(
        label="Upload a new CV",
        help_text="You must upload a cv to apply for a job",
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.notify_consent = kwargs.pop("notify_consent")
        self.search_consent = kwargs.pop("search_consent")
        self.newsletter_consent = kwargs.pop("newsletter_consent")
        self.updating_my_own_profile = kwargs.pop("updating_my_own_profile")
        self.user_name = kwargs.pop("user_name", None)
        self.next_url = kwargs.pop("next", None)
        super().__init__(*args, **kwargs)

        self.fields["job_experience"].label = "Experience"
        self.fields["job_experience"].help_text = ""
        self.fields["job_experience"].required = True
        self.fields["job_experience"].queryset = Experience.objects.display()
        self.fields["area"].label = "I'm based in"
        self.fields["radius"].label = "Looking for work within"

        # not required
        self.fields["code"].required = False
        self.fields["data_type"].required = False

        # widget
        self.fields["upload_cv"].widget = FileDropInput(
            default_text="Drop your CV here...",
            click_text="or click here to select your CV",
        )
        # self.fields["picture"].widget = forms.FileInput()
        self.fields["picture"].widget = FileDropInput(
            zone_id="filedrop-1-zone",
            default_text="Drop your profile picture here...",
            click_text="or click here to select your profile picture",
        )
        if self.updating_my_own_profile:
            self._setup_checkbox("notify_checkbox", self.notify_consent)
            self._setup_checkbox("search_checkbox", self.search_consent)
            self._setup_checkbox("newsletter_checkbox", self.newsletter_consent)
        else:
            del self.fields["notify_checkbox"]
            del self.fields["search_checkbox"]
            del self.fields["newsletter_checkbox"]

    def clean_notify_checkbox(self):
        return self._clean_checkbox("notify_checkbox", self.notify_consent)

    def clean_newsletter_checkbox(self):
        return self._clean_checkbox(
            "newsletter_checkbox", self.newsletter_consent
        )

    def clean_search_checkbox(self):
        return self._clean_checkbox("search_checkbox", self.search_consent)

    class Meta:
        model = Candidate
        fields = (
            "job_experience",
            "area",
            "radius",
            "upload_cv",
            "picture",
        )

    def clean(self):
        cleaned_data = super().clean()

        if self.next_url:
            # the user was redirected to the candidate profile view so the user
            # must either upload a CV or have one one file
            if not cleaned_data.get("upload_cv"):
                cv = None
                try:
                    contact_model = apps.get_model(settings.CONTACT_MODEL)
                    contact = contact_model.objects.get(
                        user__username=self.user_name
                    )
                    cv = CV.objects.latest(contact)
                except CV.DoesNotExist:
                    pass

                if not cv:
                    raise ValidationError("You must upload a CV")

        return cleaned_data


class CandidateSearchForm(LocationLookupForm):
    experience = forms.ChoiceField()

    order_by = forms.CharField(max_length=30)
    last_login = forms.ChoiceField(
        choices=LAST_LOGIN_CHOICES, required=False, label="Last Active"
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["experience"] = forms.ChoiceField(
            choices=[(o.slug, o.title) for o in Experience.objects.display()]
            + [("", "All Levels")],
            label="Experience",
            required=False,
        )
        # self.fields['radius'].choices = [
        #     # (None,  ''),
        #     (25,    '25 miles'),
        #     (50,    '50 miles'),
        #     (999,   'Unlimited')
        # ]
        self.fields["order_by"].widget = forms.HiddenInput()

        # Remove the radius field supplied by the LocationLookupForm.
        # It's not required for search
        del self.fields["radius"]

    class Meta:
        fields = ("area", "radius", "experience", "last_login")


class StaffCandidateSearchForm(LocationLookupForm):
    search_type = forms.ChoiceField(choices=SEARCH_TYPE_CHOICES, required=False)
    experience = forms.ChoiceField()
    last_login = forms.ChoiceField(choices=LAST_LOGIN_CHOICES, required=False)

    order_by = forms.CharField(max_length=30)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["experience"] = forms.ChoiceField(
            choices=[("", "")]
            + [(o.slug, o.title) for o in Experience.objects.display()],
            label="Experience",
            required=False,
        )
        self.fields["order_by"].widget = forms.HiddenInput()
        # N.B. don't include the fields "area" or "radius" here as they've
        # already been setup in LocationForm. The "area" field needs
        # class="autocomplete pure-input-2-3"
        for field in ["search_type", "experience", "last_login"]:
            self.fields[field].widget.attrs.update({"class": "pure-input-2-3"})
        # Remove the radius field supplied by the LocationLookupForm.
        # It's not required for search
        del self.fields["radius"]

    class Meta:
        fields = ("search_type", "area", "radius", "experience", "last_login")


class JobApplicationForm(RequiredFieldForm):
    """Form for Job Application."""

    def __init__(self, *args, **kwargs):
        super(JobApplicationForm, self).__init__(*args, **kwargs)
        self.fields["cover"].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["cover"].required = True
        self.fields["cover"].label = "Covering letter"

    def clean(self):
        cover = self.cleaned_data.get("cover")
        if not cover:
            raise ValidationError(
                "Please write a covering letter before submitting your "
                "application."
            )
        return self.cleaned_data

    class Meta:
        model = JobApplication
        fields = ("cover",)


class JobApplicationSubmitForm(forms.Form):
    application = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super(JobApplicationSubmitForm, self).__init__(*args, **kwargs)
        self.fields["application"].widget = forms.HiddenInput()
        self.fields["application"].required = False
        self.fields["application"].label = ""

    class Meta:
        model = JobApplication
        fields = ("application",)


# class JobBasketCheckoutForm(ActionCheckoutAddressForm):
#    class Meta:
#        model = JobBasket
#        fields = ("action",)


class JobEmptyForm(forms.ModelForm):
    class Meta:
        model = Job
        fields = ()


class JobForm(RequiredFieldForm):
    """Form for job.create / update

    Require: 'title', 'description', 'salary', 'location_text', 'close_date',
    'job_role', 'practice_area', 'applications_email'
    """

    area = forms.CharField(
        max_length=200,
        label="Postcode district",
        widget=forms.TextInput(
            attrs={"class": "autocomplete pure-input-2-3", "autocomplete": "on"}
        ),
    )
    # postcode (hidden)
    code = forms.CharField(
        max_length=30, required=False, widget=forms.HiddenInput()
    )

    def __init__(self, *args, **kwargs):
        super(JobForm, self).__init__(*args, **kwargs)
        set_widget_required(self.fields["title"])
        set_widget_required(self.fields["description"])
        set_widget_required(self.fields["close_date"])

        set_widget_required(self.fields["salary"])
        set_widget_required(self.fields["location_text"])
        set_widget_required(self.fields["job_role"])
        set_widget_required(self.fields["practice_area"])
        set_widget_required(self.fields["applications_email"])
        self.fields["close_date"].widget.attrs.update(
            {"class": "datepicker_60"}
        )
        for name in (
            "title",
            "location_text",
            "salary",
            "job_role",
            "practice_area",
            "description",
            "applications_email",
        ):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

        self.fields["applications_email"].label = "Applications email"
        self.fields["area"].widget.attrs.update({"autocomplete": "nope"})

    def clean(self):
        area = self.cleaned_data.get("area")
        code = self.cleaned_data.get("code")
        if code:
            pass
        elif area:
            raise ValidationError(
                "Please select a valid postcode district or region.  To "
                "select a postcode district (or region) start typing the "
                "postcode, post town or region) and then click on the correct "
                "entry from the list"
            )

        close_date = self.cleaned_data.get("close_date")

        if not close_date:
            raise ValidationError(
                "A closing date for applications must be specified"
            )

        if close_date < timezone.now().date():
            raise ValidationError("Job closing date cannot be in the past.")
        return self.cleaned_data

    class Meta:
        model = Job
        fields = (
            "title",
            "area",
            "code",
            "location_text",
            "job_role",
            "practice_area",
            "salary",
            "close_date",
            "description",
            "applications_email",
        )


class JobBasketEmptyForm(forms.ModelForm):
    class Meta:
        model = JobBasket
        fields = ()


# class JobCreditCheckoutForm(ActionCheckoutAddressForm):
#    class Meta:
#        model = JobCredit
#        fields = ("action",)


class JobCreditEmptyForm(forms.ModelForm):
    class Meta:
        model = JobCredit
        fields = ()


class JobCreditForm(forms.ModelForm):
    class Meta:
        model = JobCredit
        fields = ("credits",)


class JobCreditUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        set_widget_required(self.fields["credits"])

        for name in ("credits", "available_credits"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = JobCredit
        fields = ("credits", "available_credits", "paid")


class JobRoleForm(forms.ModelForm):
    class Meta:
        model = JobRole
        fields = ("order", "title", "default")


class JobSearchForm(LocationLookupForm):
    """Form for job search."""

    job_role = forms.CharField(max_length=30, required=False)
    practice_area = forms.CharField(max_length=30, required=False)
    recruiter = forms.CharField(max_length=30, required=False)
    title = forms.CharField(max_length=30, required=False)
    page_no = forms.CharField(max_length=30)

    def __init__(self, *args, **kwargs):
        super(JobSearchForm, self).__init__(*args, **kwargs)
        for name in ("recruiter", "title"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["recruiter"].label = "Company"
        self.fields["title"].label = "Job Title"
        self.fields["page_no"].widget = forms.HiddenInput()

    class Meta:
        fields = (
            "postcode",
            "radius",
            "recruiter",
            "job_role",
            "practice_area",
            "title",
            "page_no",
        )


class JobElasticSearchForm(forms.Form):
    """Form for job elastic search."""

    location = forms.CharField(
        max_length=30,
        required=False,
        label="",
        help_text="e.g. London",
        widget=forms.TextInput(
            attrs={"placeholder": "Location", "class": "search-input"}
        ),
    )
    job_role = forms.CharField(
        max_length=30,
        required=False,
        label="",
        help_text="e.g. Legal Secretary or Legal PA",
        widget=forms.TextInput(
            attrs={"placeholder": "Job Role", "class": "search-input"}
        ),
    )
    practice_area = forms.CharField(
        max_length=30,
        required=False,
        label="",
        help_text="e.g. Litigation (optional)",
        widget=forms.TextInput(
            attrs={"placeholder": "Area of Law", "class": "search-input"}
        ),
    )
    experience = forms.CharField(
        max_length=30,
        required=False,
        label="",
        help_text="e.g. Trainee, Junior or Experienced (optional)",
        widget=forms.TextInput(
            attrs={
                "placeholder": "Level of Experience",
                "class": "search-input",
            }
        ),
    )
    page_no = forms.CharField(
        max_length=30, required=False, widget=forms.HiddenInput()
    )

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)

    class Meta:
        fields = ("location", "job_role", "practice_area", "page_no")


class LocationForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        for name in ("postcode", "town", "region", "country"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = Location
        fields = ("postcode", "town", "region", "country")


class RecruiterForm(RequiredFieldForm):
    """Form for Recruiter."""

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in (
            "company_name",
            "title",
            "first_name",
            "last_name",
            "email",
            "address_1",
            "address_2",
            "address_3",
            "town",
            "county",
            "country",
            "postcode",
        ):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        for name in (
            "company_name",
            "title",
            "first_name",
            "last_name",
            "email",
            "address_1",
            "town",
            "county",
            "country",
            "postcode",
        ):
            set_widget_required(self.fields[name])
        for name in ("phone", "mobile"):
            self.fields[name].widget.attrs[
                "placeholder"
            ] = "Either a phone or mobile number is required"

    def clean(self):
        phone = self.cleaned_data.get("phone")
        mobile = self.cleaned_data.get("mobile")
        if phone or mobile:
            pass
        else:
            raise ValidationError("Please enter a phone or mobile number.")
        return self.cleaned_data

    class Meta:
        model = apps.get_model(settings.CONTACT_MODEL)
        fields = (
            "company_name",
            "title",
            "first_name",
            "last_name",
            "phone",
            "mobile",
            "email",
            "website",
            "address_1",
            "address_2",
            "address_3",
            "town",
            "county",
            "country",
            "postcode",
        )


class RecruiterCreateForm(
    ConsentFieldFormMixin, RecruiterForm, UserUsernameCreationForm
):
    """Form to create Recruiter.

    Adds Username, and password entry to RecruiterForm

    """

    newsletter_checkbox = forms.BooleanField(required=False)
    data_checkbox = forms.BooleanField(required=False)
    captcha = ReCaptchaField(widget=ReCaptchaV3)

    def __init__(self, *args, **kwargs):
        self.newsletter_consent = kwargs.pop("newsletter_consent")
        self.data_consent = kwargs.pop("data_consent")
        super().__init__(*args, **kwargs)
        for name in ("username", "password1", "password2"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        self._setup_checkbox("newsletter_checkbox", self.newsletter_consent)
        self._setup_checkbox("data_checkbox", self.data_consent)

    def clean_data_checkbox(self):
        return self._clean_checkbox("data_checkbox", self.data_consent)

    def clean_newsletter_checkbox(self):
        return self._clean_checkbox(
            "newsletter_checkbox", self.newsletter_consent
        )

    class Meta:
        model = apps.get_model(settings.CONTACT_MODEL)
        fields = (
            "company_name",
            "title",
            "first_name",
            "last_name",
            "phone",
            "mobile",
            "email",
            "website",
            "address_1",
            "address_2",
            "address_3",
            "town",
            "county",
            "country",
            "postcode",
            "website",
            "username",
            "password1",
            "password2",
        )


class RecruiterProfileForm(ConsentFieldFormMixin, RequiredFieldForm):
    """Form for editing the recruiter profile."""

    newsletter_checkbox = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        self.newsletter_consent = kwargs.pop("newsletter_consent", None)
        super(RecruiterProfileForm, self).__init__(*args, **kwargs)
        for name in ("picture", "profile"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["picture"].label = "Company logo"
        self.fields["picture"].widget = FileDropInput(
            default_text="Drop your Company logo here...",
            click_text="or click here to select your logo",
        )
        if self.newsletter_consent:
            self._setup_checkbox("newsletter_checkbox", self.newsletter_consent)
        else:
            del self.fields["newsletter_checkbox"]

    def clean_newsletter_checkbox(self):
        return self._clean_checkbox(
            "newsletter_checkbox", self.newsletter_consent
        )

    class Meta:
        model = Recruiter
        fields = ("business_type", "profile", "picture")


class StaffRecruiterProfileForm(RecruiterProfileForm):
    class Meta:
        model = Recruiter
        fields = (
            "business_type",
            "subscription_expires",
            "search_expires",
            "profile",
            "picture",
        )
        # widgets = {
        #     'picture': forms.FileInput,
        # }


# class RecruiterCheckoutForm(ActionCheckoutAddressForm):
#    class Meta:
#        model = Recruiter
#        fields = ("action",)


class JobPayStripeForm(forms.ModelForm):
    stripeToken = forms.CharField()

    class Meta:
        model = Job
        fields = ()
