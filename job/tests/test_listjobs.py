# -*- encoding: utf-8 -*-
from job.templatetags.listjobs import (
    first_sentence,
    json_script_ld,
    remove_paras,
)


def test_first_sentence():
    first = first_sentence("<p>test</p>abc\ndef\rghi<br>jkl")
    assert first == "<p>test"


def test_json_script_ld():
    assert (
        '<script id="def" type="application/ld+json">"abc"</script>'
        == json_script_ld("abc", "def")
    )


def test_remove_paras():
    plain = remove_paras("<p>test</p>abc\ndef\rghi<br>jkl")
    assert plain == " test abc def ghi jkl"
