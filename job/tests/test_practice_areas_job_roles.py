# -*- encoding: utf-8 -*-
"""Test the JobRole and PracticeArea models and parsers.
"""
import pytest

from .factories import (
    JobRoleFactory,
    JobRoleKeywordFactory,
    PracticeAreaFactory,
    PracticeAreaKeywordFactory,
)

from job.models import JobRole, PracticeArea, JobRoleParser, PracticeAreaParser


def setup_job_roles():
    pa = JobRoleFactory(title="Legal PA", order=2)
    JobRoleKeywordFactory(job_role=pa, word=" pa ")
    JobRoleKeywordFactory(job_role=pa, word="assistant")
    sec = JobRoleFactory(title="Legal Secretary", order=1, default=True)
    JobRoleKeywordFactory(job_role=sec, word="secret")
    JobRoleKeywordFactory(job_role=sec, word="typing")
    JobRoleKeywordFactory(job_role=sec, word="paralegal")

    return pa, sec


def setup_job_roles_to_match_live():
    """Copy of live job roles, 13/07/2021."""
    job_role_1 = JobRoleFactory(title="Legal Secretary", order=1)
    # JobRoleKeywordFactory(job_role=job_role_1, word="assistant")
    JobRoleKeywordFactory(job_role=job_role_1, word="secret")
    JobRoleKeywordFactory(job_role=job_role_1, word="typing")
    JobRoleKeywordFactory(job_role=job_role_1, word="typist")
    # JobRoleKeywordFactory(job_role=job_role_1, word="paralegal")
    job_role_2 = JobRoleFactory(title="Legal PA", order=2)
    JobRoleKeywordFactory(job_role=job_role_2, word=" pa ")
    # JobRoleKeywordFactory(job_role=job_role_2, word="executive")
    JobRoleKeywordFactory(job_role=job_role_2, word="personal assistant")
    job_role_3 = JobRoleFactory(title="Legal Administrator", order=3)
    JobRoleKeywordFactory(job_role=job_role_3, word="legal admin")


@pytest.mark.django_db
def test_all_job_roles():
    pa, sec = setup_job_roles()
    job_roles = JobRole.objects.all_job_roles()

    assert 2 == len(job_roles)
    assert job_roles[0]["name"] == "Legal Secretary"
    assert job_roles[0]["default"]
    assert job_roles[0]["keywords"] == ["paralegal", "secret", "typing"]
    assert job_roles[1]["name"] == "Legal PA"
    assert not job_roles[1]["default"]
    assert job_roles[1]["keywords"] == ["assistant", " pa "]


@pytest.mark.django_db
@pytest.mark.parametrize(
    "job_title,expect",
    [
        ("Paralegal", []),
        ("Legal Assistants", []),
        ("Legal Executive", []),
    ],
)
def test_job_role_parser_issue_5679(job_title, expect):
    """Fix parser issues for Paralegal, Legal Assistants and Legal Executives.

    email, 12/07/2021

      ... there are a few positions in there which aren't suitable for our
      jobs board - Paralegal, Legal Assistants and Legal Executive vacancies.
      We should only be advertising jobs which are for Legal Secretaries, Legal
      Administrators or Legal PAs.

    """
    setup_job_roles_to_match_live()
    job_role_parser = JobRoleParser()
    roles = job_role_parser.parse(job_title)
    assert expect == [x.title for x in roles], job_title


@pytest.mark.django_db
@pytest.mark.parametrize(
    "job_title,expect",
    [
        ("Family Partner", []),
        ("Residential Property Senior Associate - Partner", []),
        (
            "this is a story about a pa who had a secret",
            ["Legal Secretary", "Legal PA"],
        ),
        ("this is a story about a pa", ["Legal PA"]),
    ],
)
def test_job_role_parser_pa(job_title, expect):
    """

    Parser accepting:
    1. Company Commercial (Life Sciences) - Cambridge
    2. Residential Property Senior Associate - Partner
    3. Family Partner

    https://www.kbsoftware.co.uk/crm/ticket/5679/
    """
    job_role_1 = JobRoleFactory(title="Legal PA", order=2)
    JobRoleKeywordFactory(job_role=job_role_1, word=" pa ")
    JobRoleKeywordFactory(job_role=job_role_1, word="assistant")
    job_role_2 = JobRoleFactory(title="Legal Secretary", order=1)
    JobRoleKeywordFactory(job_role=job_role_2, word="secret")
    JobRoleKeywordFactory(job_role=job_role_2, word="typing")
    JobRoleKeywordFactory(job_role=job_role_2, word="paralegal")

    job_role_parser = JobRoleParser()
    roles = job_role_parser.parse(job_title)
    assert expect == [x.title for x in roles], job_title


@pytest.mark.django_db
def test_job_role_parser_single():
    pa, sec = setup_job_roles()
    job_title_and_desc = "this is a story about a pa"
    job_role_parser = JobRoleParser()
    roles = job_role_parser.parse(job_title_and_desc)

    assert [pa] == roles


@pytest.mark.django_db
def test_job_role_parser_multiple():
    pa, sec = setup_job_roles()
    job_title_and_desc = "this is a story about a pa who had a secret"
    job_role_parser = JobRoleParser()
    roles = job_role_parser.parse(job_title_and_desc)

    assert [sec, pa] == roles


@pytest.mark.django_db
def test_job_role_parser_default():
    pa, sec = setup_job_roles()
    job_title_and_desc = "this is a story about a nothing at all"
    job_role_parser = JobRoleParser()
    roles = job_role_parser.parse(job_title_and_desc)

    assert [sec] == roles


@pytest.mark.django_db
def test_job_role_parser_no_text():
    pa, sec = setup_job_roles()
    job_title_and_desc = ""
    job_role_parser = JobRoleParser()
    roles = job_role_parser.parse(job_title_and_desc)

    assert [sec] == roles


@pytest.mark.django_db
def test_job_role_parser_no_default():
    pa, sec = setup_job_roles()
    # set no default - expect empty list returned
    sec.default = False
    sec.save()
    job_title_and_desc = "this is a story about a nothing at all"
    job_role_parser = JobRoleParser()
    roles = job_role_parser.parse(job_title_and_desc)

    assert not roles


def setup_practice_areas():
    conveyancing = PracticeAreaFactory(title="conveyancing", order=2)
    PracticeAreaKeywordFactory(practice_area=conveyancing, word="key_2")
    PracticeAreaKeywordFactory(practice_area=conveyancing, word="key_1")
    commercial = PracticeAreaFactory(title="commercial", order=1, default=True)
    PracticeAreaKeywordFactory(practice_area=commercial, word="key_4")
    PracticeAreaKeywordFactory(practice_area=commercial, word="key_5")
    PracticeAreaKeywordFactory(practice_area=commercial, word="key_3")
    return conveyancing, commercial


@pytest.mark.django_db
def test_all_practice_areas():
    conveyancing, commercial = setup_practice_areas()
    practice_areas = PracticeArea.objects.all_practice_areas()

    assert 2 == len(practice_areas)
    assert practice_areas[0]["name"] == commercial.title
    assert practice_areas[0]["default"]
    assert practice_areas[0]["keywords"] == ["key_3", "key_4", "key_5"]
    assert practice_areas[1]["name"] == conveyancing.title
    assert not practice_areas[1]["default"]
    assert practice_areas[1]["keywords"] == ["key_1", "key_2"]


@pytest.mark.django_db
def test_practice_area_parser_single():
    conveyancing, commercial = setup_practice_areas()
    job_title_and_desc = "this has key_1"
    practice_area_parser = PracticeAreaParser()
    areas = practice_area_parser.parse(job_title_and_desc)

    assert [conveyancing.pk] == [area.pk for area in areas]


@pytest.mark.django_db
def test_practice_area_parser_multiple():
    conveyancing, commercial = setup_practice_areas()
    job_title_and_desc = "this has key_4 and key_1"
    practice_area_parser = PracticeAreaParser()
    areas = practice_area_parser.parse(job_title_and_desc)

    assert [commercial.pk, conveyancing.pk] == [area.pk for area in areas]


@pytest.mark.django_db
def test_practice_area_parser_default():
    conveyancing, commercial = setup_practice_areas()
    job_title_and_desc = "this is some text but no keywords"
    practice_area_parser = PracticeAreaParser()
    areas = practice_area_parser.parse(job_title_and_desc)

    assert [commercial.pk] == [area.pk for area in areas]


@pytest.mark.django_db
def test_practice_area_parser_no_text():
    conveyancing, commercial = setup_practice_areas()
    job_title_and_desc = ""
    practice_area_parser = PracticeAreaParser()
    areas = practice_area_parser.parse(job_title_and_desc)

    assert [commercial.pk] == [area.pk for area in areas]


@pytest.mark.django_db
def test_practice_area_parser_no_default():
    conveyancing, commercial = setup_practice_areas()
    # set no default - expect empty list returned
    commercial.default = False
    commercial.save()
    job_title_and_desc = "this is some text but no keywords"
    practice_area_parser = PracticeAreaParser()
    areas = practice_area_parser.parse(job_title_and_desc)

    assert not areas
