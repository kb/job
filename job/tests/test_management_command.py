# -*- encoding: utf-8 -*-
"""Test management commands.

See ``example_job/tests/test_management_command.py`` for testing of:

1. ``rebuild_job_index``
2. ``refresh_job_index``

"""
import pytest
import responses

from django.core.management import call_command
from unittest.mock import patch

from job.models import JobError, JobFeed
from job.tests.factories import JobRoleFactory


@pytest.mark.django_db
def test_candidate_welcome_email():
    call_command("2937-candidate-welcome-email")


@pytest.mark.django_db
def test_demo_data():
    call_command("demo-data-login")
    call_command("demo_data_job")


@patch("ftplib.FTP")
@pytest.mark.django_db
def test_import_legists(mock_ftp):
    """Test the management command.

    .. note:: Remove the mock to download the actual file.

    .. note:: I cannot work out how to mock an FTP file download for now.
              If the test gets as far as throwing the exception, then it has
              tested up to the point where other tests take over.

    """
    call_command("init_app_job")
    with pytest.raises(JobError) as e:
        call_command("import_legists")
    assert "The XML file" in str(e.value)
    assert "contains no data" in str(e.value)


@pytest.mark.django_db
@responses.activate
def test_import_simply_law_feed():
    call_command("init_app_job")
    JobRoleFactory(title="Legal PA")
    null = None
    responses.add(
        responses.GET,
        "https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json",
        json={
            "jobs": [
                {
                    "job": [
                        {
                            "category": "Insolvency & Debt Recovery,Partner",
                            "jobref": 1027121,
                            "date": "2023-03-02 11:01:59",
                            "title": "Legal Secretary",
                            "company": "G2 Legal Limited",
                            "email": "london@g2legal.com",
                            "url": "https://www.simplylawjobs.com/job/1027121",
                            "salary": "From \u00a3100000",
                            "jobtype": "full-time",
                            "country": "UK",
                            "region": null,
                            "city": "Hampshire",
                            "description": "Are you an experienced qualified Insolvency Solicitor looking to take that step up to Partnership?   Or are you currently a Partner but looking for a new challenge for 2023?<br> <br> My client is a leading, Legal 150, Southampton-based law firm with a great reputation and a proven track record for success. They are looking to grow their successful Insolvency team in this this critical hire at Partner level. The team handles both non-contentious and contentious insolvency matters to include corporate insolvency, restructuring, and solvent reorganisation matters. <br><br>You will be experienced in restructuring and insolvency transactional and technical advisory work, but may also have some contentious experience.<br><br>Management experience is required and you will be involved in the growth and development of the team. My client offers a generous benefits package, including flexible hybrid working. <br><br>Your role as an Insolvency Associate Solicitor/Senior Associate will involve: <br>\r\n<ul><li>Exposure to high quality work from day one </li><li>Excellent career opportunities to progress from the outset</li><li>Being active in business development, gaining new clients and growing the department </li></ul>\r\n<br>My client is eager to shortlist for interviews as soon as possible.<br> <br> Please contact Chris Rodriguez at G2 Legal ASAP to discuss this Insolvency Partner role based in Southampton, or send over your up-to-date CV confidentially via the link below.<br><br>(Please note that salary is just a guideline).<br><br><strong>#G2MSS</strong>",
                            "image": "https://assets-jb.fmg-services.co.uk/SLJ/uploads/company/1_500/c8a6e2b543c27bb1a226dda90e5a2c8d81f570b5.jpg",
                            "closedate": "2023-03-30 11:01:59",
                            "companytype": "Recruitment Agency",
                            "practicearea": "Insolvency & Debt Recovery",
                            "role": "Partner",
                            "AdditionalFields": "",
                        }
                    ]
                }
            ]
        },
        status=200,
    )
    call_command("import-simply-law-feed")


@pytest.mark.django_db
def test_init_app_job():
    call_command("init_app_job")
    feed = JobFeed.objects.get(slug=JobFeed.SIMPLY_LAW)
    assert JobFeed.SIMPLY_LAW == feed.recruiter.contact.user.username
    assert feed.recruiter.notify_application is False
    assert (
        "https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json"
        == feed.url
    )


@pytest.mark.django_db
def test_send_email_latest_jobs():
    call_command("send_email_latest_jobs")
