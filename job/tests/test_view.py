# -*- encoding: utf-8 -*-
import pytest

from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.test import RequestFactory
from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from gdpr.tests.factories import ConsentFactory, ConsentFormSettingsFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from stock.tests.factories import ProductFactory
from job.views import JobLatestView
from job.models import (
    Candidate,
    FEED_RECRUITER,
    ILSPA_RECRUITER,
    Job,
    JobCredit,
)
from job.tests.factories import (
    CandidateFactory,
    ContactFactory,
    CVFactory,
    JobFactory,
    JobFeedFactory,
    JobFeedItemFactory,
    RecruiterFactory,
)


@pytest.mark.django_db
def test_candidate_profile_update(client):
    """I am a member of staff editing the profile of a candidate.

    .. note:: I don't get an option to edit the notify, newsletter or search
              check-boxes.
              If a member of staff wants to update the checkboxes then they need
              to use the GDPR consent views which make you enter a comment.

    """
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # candidate
    candidate = CandidateFactory(contact=ContactFactory(user=UserFactory()))
    cv_candidate = CVFactory(contact=candidate.contact)
    # staff
    user = UserFactory(is_staff=True)
    staff = ContactFactory(user=user)
    CVFactory(contact=staff)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(
        reverse("job.candidate.profile.update", args=[candidate.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "latest_cv" in response.context
    assert cv_candidate == response.context["latest_cv"]
    assert "form" in response.context
    # for more information, search for 'consent_form_settings'
    assert "form_notice" not in response.context
    form = response.context["form"]
    assert set(
        [
            "area",
            "code",
            "data_type",
            "job_experience",
            "picture",
            "radius",
            "upload_cv",
        ]
    ) == set(form.fields.keys())


@pytest.mark.django_db
def test_internal_list(client):
    today = timezone.now()
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    feed_recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=feed_recruiter)
    JobFactory(
        recruiter=ilspa_recruiter,
        order_key=ILSPA_RECRUITER,
        title="ilspa",
        publish=True,
        publish_date=today,
    )
    feed_job = JobFactory(
        recruiter=feed_recruiter,
        order_key=FEED_RECRUITER,
        title="indeed",
        publish=True,
        publish_date=today,
    )
    JobFeedItemFactory(feed=feed, job=feed_job)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("job.list", args=["0"]))
    assert HTTPStatus.OK == response.status_code
    assert len(response.context["job_list"]) == 1
    assert response.context["job_list"][0].title == "ilspa"


@pytest.mark.django_db
def test_feed_list(client):
    today = timezone.now()
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    feed_recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=feed_recruiter)
    JobFactory(
        recruiter=ilspa_recruiter,
        order_key=ILSPA_RECRUITER,
        title="ilspa",
        publish=True,
        publish_date=today,
    )
    feed_job = JobFactory(
        recruiter=feed_recruiter,
        order_key=FEED_RECRUITER,
        title="indeed",
        publish=True,
        publish_date=today,
    )
    JobFeedItemFactory(feed=feed, job=feed_job)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("job.list", args=[feed.pk]))
    assert HTTPStatus.OK == response.status_code
    assert len(response.context["job_list"]) == 1
    assert response.context["job_list"][0].title == "indeed"


@pytest.mark.django_db
def test_job_list_feed(client):
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    indeed_recruiter = RecruiterFactory(contact=ContactFactory())
    job_feed = JobFeedFactory(slug="indeed", recruiter=indeed_recruiter)
    today = timezone.now()
    yesterday = timezone.now() - relativedelta(days=1)
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 1",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=today,
    )
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 2",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 2",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 1",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=today,
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("job.list", args=[job_feed.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "job_list" in response.context
    job_list = response.context["job_list"]
    assert ["INDEED 2", "INDEED 1"] == [j.title for j in job_list]
    assert "job_feeds" in response.context
    assert ["indeed"] == [x.slug for x in response.context["job_feeds"]]


@pytest.mark.django_db
def test_job_list_ilspa(client):
    """If we use zero (``0``) as the parameter, then return ILSPA jobs."""
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    indeed_recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(slug="indeed", recruiter=indeed_recruiter)
    today = timezone.now()
    yesterday = timezone.now() - relativedelta(days=1)
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 1",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=today,
    )
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 2",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 2",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 1",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=today,
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("job.list", args=[0]))
    assert HTTPStatus.OK == response.status_code
    assert "job_list" in response.context
    job_list = response.context["job_list"]
    assert ["ILSPA 1", "ILSPA 2"] == [j.title for j in job_list]
    assert "job_feeds" in response.context
    assert ["indeed"] == [x.slug for x in response.context["job_feeds"]]


@pytest.mark.django_db
def test_latest(client):
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    indeed_recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=indeed_recruiter)
    today = timezone.now()
    yesterday = timezone.now() - relativedelta(days=1)
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 1",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=today,
    )
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 2",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 2",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 1",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=today,
    )
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("job.latest"))
    assert 200 == response.status_code
    request = RequestFactory().get("/job/latest")
    request.user = user
    view = JobLatestView.as_view()
    response = view(request)
    context = response.context_data
    assert "job_list" in context
    job_list = context["job_list"]
    assert ["ILSPA 1", "ILSPA 2", "INDEED 1", "INDEED 2"] == [
        j.title for j in job_list
    ]


@pytest.mark.django_db
def test_job_undelete(client):
    recruiter = RecruiterFactory(contact=ContactFactory())
    today = timezone.now()
    yesterday = timezone.now() + relativedelta(days=-1)
    JobFactory(
        recruiter=recruiter,
        title="ILSPA 2",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    job = JobFactory(
        recruiter=recruiter,
        title="ILSPA 1",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=today,
    )
    user = UserFactory(username="staff", is_staff=True)
    job.set_deleted(recruiter.contact.user)
    assert ["ILSPA 2"] == [j.title for j in Job.objects.current()]
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.post(reverse("job.undelete", args=[job.pk]), {})
    assert 302 == response.status_code
    assert reverse("contact.detail", args=[recruiter.contact.pk]) in (
        response["Location"]
    )
    assert ["ILSPA 1", "ILSPA 2"] == [
        j.title for j in Job.objects.current().order_by("title")
    ]


@pytest.mark.django_db
def test_recruiter_profile_update(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    url = reverse("job.recruiter.profile.update", args=[recruiter.pk])
    response = client.get(url)
    assert 200 == response.status_code
    assert recruiter == response.context_data["object"]


@pytest.mark.django_db
def test_recruiter_job_credit(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    ProductFactory(slug="job-post", price=Decimal("150.00"))
    ProductFactory(slug="job-post-discount", price=Decimal("100.00"))
    url = reverse("job.credit.create", args=[recruiter.contact.pk])
    response = client.post(url, {"credits": 1})
    assert 302 == response.status_code
    assert reverse("contact.detail", args=[recruiter.contact.pk]) in (
        response["Location"]
    )
    job_credit = JobCredit.objects.first()
    assert job_credit
    assert job_credit.recruiter == recruiter
    assert job_credit.product.slug == "job-post"
    assert job_credit.credits == 1
    assert job_credit.available_credits == 1
    assert job_credit.paid


@pytest.mark.django_db
def test_recruiter_job_credits(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    ProductFactory(slug="job-post", price=Decimal("150.00"))
    ProductFactory(slug="job-post-discount", price=Decimal("100.00"))
    url = reverse("job.credit.create", args=[recruiter.contact.pk])
    response = client.post(url, {"credits": 2})
    assert 302 == response.status_code
    assert reverse("contact.detail", args=[recruiter.contact.pk]) in (
        response["Location"]
    )
    job_credit = JobCredit.objects.first()
    assert job_credit
    assert job_credit.recruiter == recruiter
    assert job_credit.product.slug == "job-post-discount"
    assert job_credit.credits == 2
    assert job_credit.available_credits == 2
    assert job_credit.paid


# class TestView(TestCase):
#
#     def setUp(self):
#         default_scenario_login()
#         demo_data()
#         staff = get_user_staff()
#         # update simple content
#         self.assertTrue(
#             self.client.login(
#                 username=staff.username, password=TEST_PASSWORD
#                 )
#         )
#     def test_create(self):
#         today = datetime.today().date()
#         close_date = today + timedelta(days=28)
#         response = self.client.post(
#             reverse('job.create'),
#             dict(
#                 recruiter=RecruiterFactory(),
#                 close_date=close_date,
#                 title='Hatherleigh',
#             )
#         )
#         self.assertEqual(response.status_code, 302)
#         # check job
#         try:
#             Job.objects.get(title='Hatherleigh')
#         except Job.DoesNotExist:
#             self.fail('cannot find new job')
#
#     def test_create_no_title(self):
#         today = datetime.today().date()
#         close_date = today + timedelta(days=28)
#         response = self.client.post(
#             reverse('job.create'),
#             dict(
#                 recruiter=RecruiterFactory(),
#                 close_date=close_date,
#             )
#         )
#         self.assertEqual(response.status_code, 200)
#         form = response.context_data['form']
#         self.assertEqual(
#             {'title': ['This field is required.']},
#             form.errors,
#         )
#
#     def test_delete(self):
#         b = get_alpe_d_huez()
#         response = self.client.post(
#             reverse('job.delete', kwargs=dict(pk=b.pk)),
#         )
#         self.assertEqual(response.status_code, 302)
#         # check job
#         try:
#             b = get_alpe_d_huez()
#             self.fail('job was not deleted')
#         except Job.DoesNotExist:
#             pass
