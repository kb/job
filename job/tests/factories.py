# -*- encoding: utf-8 -*-
import factory

from datetime import timedelta
from decimal import Decimal
from django.apps import apps
from django.conf import settings
from django.utils import timezone

from block.tests.factories import PageFactory
from login.tests.factories import UserFactory
from job.models import (
    Candidate,
    CandidateJobLocation,
    Country,
    CV,
    CVType,
    Experience,
    HighestEducation,
    Job,
    JobApplication,
    JobBasket,
    JobBasketItem,
    JobCredit,
    JobFeed,
    JobFeedItem,
    JobRole,
    JobRoleKeyword,
    JobSettings,
    JobTracking,
    LandingPage,
    Location,
    PracticeArea,
    PracticeAreaKeyword,
    Recruiter,
    Region,
    RegionType,
    YearsExperience,
)
from stock.tests.factories import ProductFactory


class ContactFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = apps.get_model(settings.CONTACT_MODEL)

    @factory.sequence
    def company_name(n):
        return "company_name_{:02d}".format(n)


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country

    alpha2_code = "GB"
    country_code = "GBR"
    name = "United Kingdom"
    numeric_code = 826


class CVTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CVType


class CVFactory(factory.django.DjangoModelFactory):
    """Create a CV.

    e.g::

      contact = ContactFactory()
      cv = CVFactory(contact=contact)

    """

    class Meta:
        model = CV

    cv_type = factory.SubFactory(CVTypeFactory)
    file_name = factory.django.FileField(filename="cv.pdf")

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class LandingPageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = LandingPage

    page = factory.SubFactory(PageFactory)


class RegionTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RegionType

    slug = "two-tier-county"
    name = "Two Tier County"


class RegionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Region

    region_code = "DEV"
    name = "Devon"
    slug = "devon"
    alternate_name = ""
    alternate_slug = ""
    country = factory.SubFactory(CountryFactory)
    region_type = factory.SubFactory(RegionTypeFactory)


class LocationFactory(factory.django.DjangoModelFactory):
    """Location.

    Note: I don't think the eastings, northings, latitude and longitude match
    the town and country.  Please feel free to fix it...

    """

    class Meta:
        model = Location

    country = factory.SubFactory(CountryFactory)
    eastings = 99
    northings = 99
    latitude = Decimal("9.9")
    longitude = Decimal("9.9")
    postcode = "EX20"
    town = "Okehampton"
    region = factory.SubFactory(RegionFactory)


class HighestEducationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = HighestEducation

    @factory.sequence
    def order(n):
        return n + 1

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class CandidateFactory(factory.django.DjangoModelFactory):
    """Create a candidate.

    ::

      CandidateFactory(contact=ContactFactory())

    """

    class Meta:
        model = Candidate


class ExperienceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Experience

    @factory.sequence
    def order(n):
        return n


class CandidateJobLocationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CandidateJobLocation

    candidate = factory.SubFactory(CandidateFactory)
    location = factory.SubFactory(LocationFactory)


class RecruiterFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Recruiter


class JobCreditFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = JobCredit

    recruiter = factory.SubFactory(RecruiterFactory)
    product = factory.SubFactory(ProductFactory)


class JobFactory(factory.django.DjangoModelFactory):
    """Create a job (see note below).

    Note: To use this factory we need a contact for the recruiter.  We don't
    have a contact model in this app because we will use one from the project.
    For testing, we use the example 'Contact' model in the 'example_job' app
    e.g::

      JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))

    To create a job which hasn't been published::

      JobFactory(publish=False, recruiter=recruiter)

    """

    class Meta:
        model = Job

    close_date = timezone.now().date() + timedelta(days=60)
    location = factory.SubFactory(LocationFactory)
    publish = True
    salary = Decimal(9999)

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class JobFeedFactory(factory.django.DjangoModelFactory):
    """To use this factory, I did the following:

    recruiter_feed = RecruiterFactory(
        contact=ContactFactory(company_name='Indeed'),
    )
    job_feed = JobFeedFactory(recruiter=recruiter_feed)
    job = JobFactory(title='1', recruiter=recruiter_feed)
    JobFeedItemFactory(feed=job_feed, job=job)

    """

    class Meta:
        model = JobFeed

    recruiter = factory.SubFactory(RecruiterFactory)


class JobFeedItemFactory(factory.django.DjangoModelFactory):
    """See 'JobFeedFactory' for usage example."""

    class Meta:
        model = JobFeedItem

    feed = factory.SubFactory(JobFeedFactory)


class JobApplicationFactory(factory.django.DjangoModelFactory):
    """Create a job application.

    Use it like this:

    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    application = JobApplicationFactory(
        candidate=CandidateFactory(contact=ContactFactory()),
        job=job,
    )

    """

    class Meta:
        model = JobApplication


class JobBasketFactory(factory.django.DjangoModelFactory):
    """Create a job basket.

    Note: To use this factory we need a contact for the recruiter.  We don't
    have a contact model in this app because we will use one from the project.
    For testing, we use the example 'Contact' model in the 'example_job' app
    e.g::

      recruiter = RecruiterFactory(contact=ContactFactory())
      job1 = JobFactory(recruiter=recruiter)
      job2 = JobFactory(recruiter=recruiter)
      basket = JobBasketFactory(recruiter=recruiter)

    """

    class Meta:
        model = JobBasket


class JobBasketItemFactory(factory.django.DjangoModelFactory):
    """Create an item for the job basket.

    Note: To use this factory we need a contact for the recruiter.  We don't
    have a contact model in this app because we will use one from the project.
    For testing, we use the example 'Contact' model in the 'example_job' app
    e.g::

      recruiter = RecruiterFactory(contact=ContactFactory())
      job1 = JobFactory(recruiter=recruiter)
      job2 = JobFactory(recruiter=recruiter)

      basket = JobBasketFactory()
      JobBasketItemFactory(job=job1, basket=basket)
      JobBasketItemFactory(job=job2, basket=basket)

    """

    class Meta:
        model = JobBasketItem

    product = factory.SubFactory(ProductFactory)


class JobRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = JobRole


class JobRoleKeywordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = JobRoleKeyword

    job_role = factory.SubFactory(JobRoleFactory)


class JobSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = JobSettings


class JobTrackingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = JobTracking


class PracticeAreaFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PracticeArea


class PracticeAreaKeywordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PracticeAreaKeyword

    practice_area = factory.SubFactory(PracticeAreaFactory)


class YearsExperienceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = YearsExperience

    @factory.sequence
    def order(n):
        return n + 1

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)
