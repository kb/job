# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone
from dateutil.relativedelta import relativedelta

from gdpr.tests.factories import ConsentFactory
from job.models import Candidate, UserConsent
from job.tests.factories import ContactFactory, CandidateFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_candidate_registered_between():
    """
    Candidate registered between
    """
    now = timezone.now()

    dates = []
    dates.append(now - relativedelta(days=16))
    dates.append(now - relativedelta(days=15))
    dates.append(now - relativedelta(days=14))
    dates.append(now - relativedelta(days=9))
    dates.append(now - relativedelta(days=8))
    dates.append(now - relativedelta(days=7))

    candidates = []
    idx = 1
    for create_date in dates:
        can = CandidateFactory(
            contact=ContactFactory(
                user=UserFactory(username="user_" + str(idx))
            )
        )
        # fake the create date
        can.created = create_date
        can.save()
        candidates.append(can)
        idx += 1
    qs = Candidate.objects.registered_between(dates[1].date(), dates[4].date())
    assert list(qs) == candidates[1:5]
    qs = Candidate.objects.registered_between(dates[1].date(), None)
    assert list(qs) == candidates[1:]
    qs = Candidate.objects.registered_between(None, dates[4].date())
    assert list(qs) == candidates[:5]


@pytest.mark.django_db
def test_consent_candidate_newsletter():
    candidate = CandidateFactory(contact=ContactFactory())
    consent = ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    UserConsent.objects.set_consent(consent, True, candidate.contact.user)
    assert candidate.consent_candidate_newsletter() is True


@pytest.mark.django_db
def test_consent_candidate_newsletter_no_consent_object():
    candidate = CandidateFactory(contact=ContactFactory())
    assert candidate.consent_candidate_newsletter() is False


@pytest.mark.django_db
def test_consent_candidate_newsletter_not():
    candidate = CandidateFactory(contact=ContactFactory())
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    assert candidate.consent_candidate_newsletter() is False


@pytest.mark.django_db
def test_consent_given():
    candidate = CandidateFactory(contact=ContactFactory())
    consent = ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    UserConsent.objects.set_consent(consent, True, candidate.contact.user)
    assert candidate.consent_given(consent) is True


@pytest.mark.django_db
def test_consent_given_not():
    candidate = CandidateFactory(contact=ContactFactory())
    consent = ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    assert candidate.consent_given(consent) is False


@pytest.mark.django_db
def test_consent_notify_jobs():
    candidate = CandidateFactory(contact=ContactFactory())
    consent = ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    UserConsent.objects.set_consent(consent, True, candidate.contact.user)
    assert candidate.consent_notify_jobs() is True


@pytest.mark.django_db
def test_consent_notify_jobs_no_consent_object():
    candidate = CandidateFactory(contact=ContactFactory())
    assert candidate.consent_notify_jobs() is False


@pytest.mark.django_db
def test_consent_notify_jobs_not():
    candidate = CandidateFactory(contact=ContactFactory())
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    assert candidate.consent_notify_jobs() is False
