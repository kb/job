# -*- encoding: utf-8 -*-
import logging
import pytest

from decimal import Decimal
from django.urls import reverse
from http import HTTPStatus

# from gdpr.tests.factories import ConsentFactory
from job.tests.factories import Candidate
from job.views import cv_download
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from stock.tests.factories import ProductFactory
from .factories import (
    CandidateFactory,
    ContactFactory,
    CVFactory,
    JobApplicationFactory,
    JobFactory,
    LandingPageFactory,
    RecruiterFactory,
)


@pytest.mark.django_db
def test_cv_download_my_own_cv(rf, caplog):
    request = rf.get("/")
    request_user = UserFactory(username="pat", is_staff=False)
    contact = ContactFactory(user=request_user)
    request.user = request_user
    # CV - a probably a candidate
    cv = CVFactory(contact=contact)
    # test
    response = cv_download(request, cv.pk)
    assert HTTPStatus.OK == response.status_code
    # logging
    assert (f"'cv_download' ({cv.pk}) (my CV) by 'pat' for 'pat'") in str(
        [str(x.msg) for x in caplog.records if x.levelno == logging.INFO]
    )


@pytest.mark.django_db
def test_cv_download_recruiter(rf, caplog):
    recruiter = RecruiterFactory(
        contact=ContactFactory(user=UserFactory(username="rem"))
    )
    job = JobFactory(recruiter=recruiter)
    job.publish_job(recruiter.contact.user)
    # CV - a probably a candidate
    candidate = CandidateFactory(
        contact=ContactFactory(user=UserFactory(username="can"))
    )
    cv = CVFactory(contact=candidate.contact)
    application = JobApplicationFactory(job=job, candidate=candidate)
    # request
    request = rf.get("/")
    request.user = recruiter.contact.user
    # test
    response = cv_download(request, cv.pk)
    assert HTTPStatus.OK == response.status_code
    assert (f"'cv_download' ({cv.pk}) by 'rem' (recruiter) for 'can'") in str(
        [str(x.msg) for x in caplog.records if x.levelno == logging.INFO]
    )


@pytest.mark.django_db
def test_cv_download_staff(rf, caplog):
    request = rf.get("/")
    request_user = UserFactory(username="sam", is_staff=True)
    ContactFactory(user=request_user)
    request.user = request_user
    # CV - a probably a candidate
    cv = CVFactory(contact=ContactFactory(user=UserFactory(username="can")))
    # test
    response = cv_download(request, cv.pk)
    assert HTTPStatus.OK == response.status_code
    assert (f"'cv_download' ({cv.pk}) by 'sam' (staff) for 'can'") in str(
        [str(x.msg) for x in caplog.records if x.levelno == logging.INFO]
    )


@pytest.mark.django_db
def test_job_landing_create(perm_check):
    url = reverse("job.landing.page.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_job_landing_list(perm_check):
    url = reverse("job.landing.page.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_job_landing_update(perm_check):
    obj = LandingPageFactory()
    url = reverse("job.landing.page.update", args=[obj.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_recruiter_profile_update(perm_check):
    recruiter = RecruiterFactory(contact=ContactFactory())
    url = reverse("job.recruiter.profile.update", args=[recruiter.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_staff_job_application(perm_check):
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    job.publish_job(job.recruiter.contact.user)
    candidate = CandidateFactory(contact=ContactFactory())
    application = JobApplicationFactory(job=job, candidate=candidate)
    url = reverse("job.application.detail", args=[application.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_staff_jobcredit(perm_check):
    recruiter = RecruiterFactory(contact=ContactFactory())
    ProductFactory(slug="job-post", price=Decimal("100.00"))
    url = reverse("job.credit.create", args=[recruiter.contact.pk])
    perm_check.staff(url)
