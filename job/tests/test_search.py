# -*- encoding: utf-8 -*-
"""

Other search tests in ``example_job/tests/test_search.py``

"""
import pytest

from job.search import get_location_name, JobIndex
from search.search import SearchIndex


def test_get_location_name():
    assert get_location_name("East London", "Greater London") == "East London"
    assert get_location_name("East London", "city of London") == "East London"
    assert get_location_name("West London", "Greater London") == "West London"
    assert get_location_name("North London", "Greater London") == "North London"
    assert get_location_name("South London", "Greater London") == "South London"
    assert get_location_name("London", "Greater London") == "London"
    assert get_location_name("London", "City of London") == "London"
    assert (
        get_location_name("North Woolich", "Greater London")
        == "North Woolich Greater London"
    )
    assert (
        get_location_name("Woolich", "Greater London")
        == "Woolich Greater London"
    )
    assert (
        get_location_name("Woolich", "City of London")
        == "Woolich City of London"
    )
    assert get_location_name("Birmingham", "Birmingham") == "Birmingham"


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_drop_create():
    search_index = SearchIndex(JobIndex())
    search_index.drop_create()
