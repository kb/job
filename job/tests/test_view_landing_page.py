# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from block.tests.factories import PageFactory, TemplateFactory
from job.models import LandingPage
from login.tests.factories import TEST_PASSWORD, UserFactory
from .factories import JobSettingsFactory, LandingPageFactory


@pytest.mark.django_db
def test_landing_page_create(client):
    template = TemplateFactory(template_name="example/landing.html")
    JobSettingsFactory(
        landing_page_slug="landing", landing_page_template=template
    )
    u = UserFactory(username="staff", is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    url = reverse("job.landing.page.create")
    data = {
        "name": "Farmer in Hatherleigh",
        "job_role": "Farmer",
        "location": "Hatherleigh",
        "practice_area": "Dairy",
    }
    response = client.post(url, data)
    assert 302 == response.status_code
    assert reverse("job.landing.page.list") in response["Location"]
    obj = LandingPage.objects.get(page__slug="landing")
    assert "Dairy" == obj.practice_area
    assert "Hatherleigh" == obj.location
    assert "Farmer" == obj.job_role
    assert "Farmer in Hatherleigh" == obj.page.name
    assert "farmer-in-hatherleigh" == obj.page.slug_menu


@pytest.mark.django_db
def test_landing_page_update(client):
    page = PageFactory(slug="landing")
    obj = LandingPageFactory(
        page=page,
        job_role="Artist",
        location="London",
        practice_area="Classical",
    )
    u = UserFactory(username="staff", is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    url = reverse("job.landing.page.update", args=[obj.pk])
    data = {
        "job_role": "Farmer",
        "location": "Hatherleigh",
        "practice_area": "Dairy",
    }
    response = client.post(url, data)
    assert 302 == response.status_code
    assert reverse("job.landing.page.list") in response["Location"]
    obj = LandingPage.objects.get(page__slug="landing")
    assert "Dairy" == obj.practice_area
    assert "Hatherleigh" == obj.location
    assert "Farmer" == obj.job_role
