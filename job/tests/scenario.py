# -*- encoding: utf-8 -*-
from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from decimal import Decimal

from base.tests.model_maker import clean_and_save
from login.tests.model_maker import make_user
from job.models import (
    Candidate,
    get_contact_model,
    Job,
    JobRole,
    PracticeArea,
    Recruiter,
)


def _job_roles():
    JobRole(title="Legal Secretary").save()
    JobRole(title="Legal Administrator").save()


def _practice_area():
    PracticeArea(title="Conveyancing").save()
    PracticeArea(title="Criminal").save()
    PracticeArea(title="Litigation").save()


def get_alpe_d_huez():
    return Job.objects.get(title="Alpe D Huez")


def make_contact(username, **kwargs):
    last_name = kwargs.pop("last_name")
    first_name = kwargs.pop("first_name")
    contact_model = get_contact_model()
    try:
        contact = contact_model.objects.get(user__username=username)
    except:
        user = make_user(username, last_name=last_name, first_name=first_name)

        defaults = dict(user=user)
        defaults.update(kwargs)
        contact = clean_and_save(contact_model(**defaults))
    return contact


def make_recruiter(contact, **kwargs):
    try:
        recruiter = Recruiter.objects.get(contact=contact)
    except:
        defaults = dict(contact=contact)
        defaults.update(kwargs)
        recruiter = clean_and_save(Recruiter(**defaults))
    return recruiter


def make_candidate(contact, **kwargs):
    try:
        recruiter = Candidate.objects.get(contact=contact)
    except:
        defaults = dict(contact=contact)
        defaults.update(kwargs)
        recruiter = clean_and_save(Candidate(**defaults))
    return recruiter


def next_weekday(d, weekday):
    """Find the date for the next weekday.

    Copied from:
    http://stackoverflow.com/questions/6558535/python-find-the-date-for-the-first-monday-after-a-given-a-date

    """
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0:  # Target day already happened this week
        days_ahead += 7
    return d + timedelta(days_ahead)


def demo_data():
    today = datetime.today().date()
    contact = make_contact(
        "carter",
        company_name="Carter, Spencer & Dunne Solictors",
        first_name="Mike",
        last_name="Carter",
    )
    recruiter = make_recruiter(contact)
    close_date = next_weekday(today + relativedelta(months=1, day=11), 3)
    Job.objects.create_job(
        recruiter,
        close_date=close_date,
        title="Legal Secretary in Bath",
        salary=Decimal("12000"),
    )
    close_date = next_weekday(today + relativedelta(months=1, day=17), 2)
    Job.objects.create_job(
        recruiter,
        close_date=close_date,
        title="Legal Secretary in Paris",
        salary=Decimal("12000"),
    )
    close_date = next_weekday(today + relativedelta(months=1, day=27), 5)
    Job.objects.create_job(
        recruiter,
        close_date=close_date,
        title="Legal Secretary in Wells",
        salary=Decimal("12000"),
    )
    contact = make_contact(
        "freeman",
        company_name="Freeman, Hardy & Willis Solictors",
        first_name="Trevor",
        last_name="Freeman",
    )
    recruiter = make_recruiter(contact)
    close_date = next_weekday(today + relativedelta(months=1, day=1), 2)
    Job.objects.create_job(
        recruiter,
        close_date=close_date,
        title="Legal Secretary in Plymouth",
        salary=Decimal("12000"),
    )
    close_date = next_weekday(today + relativedelta(months=1, day=9), 3)
    Job.objects.create_job(
        recruiter,
        close_date=close_date,
        title="Legal Secretary in Lyme Regis",
        salary=Decimal("12000"),
    )
    close_date = next_weekday(today + relativedelta(months=1, day=23), 4)
    Job.objects.create_job(
        recruiter,
        close_date=close_date,
        title="Legal Secretary in Alpe D Huez",
        salary=Decimal("12000"),
    )
    close_date = next_weekday(today + relativedelta(months=1, day=19), 5)
    Job.objects.create_job(
        recruiter,
        close_date=close_date,
        title="Legal Secretary in London",
        salary=Decimal("12000"),
    )
    make_candidate(make_contact("cano", first_name="Olivia", last_name="Can"))
    make_candidate(make_contact("canc", first_name="Candy", last_name="Can"))
    make_candidate(make_contact("cant", first_name="Tony", last_name="Can"))
    make_candidate(make_contact("canr", first_name="Rose", last_name="Can"))
    _job_roles()
    _practice_area()
