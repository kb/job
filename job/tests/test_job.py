# -*- encoding: utf-8 -*-
"""Test the 'Job' model.

Note: We test the 'Job' model in the 'example_job' app because we need a
'Contact' model for the recruiter.  A 'Contact' model is normally found in the
main project...

"""
import pytest
import pytz

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from freezegun import freeze_time

from job.models import FEED_RECRUITER, ILSPA_RECRUITER, Job
from job.tests.factories import (
    CandidateFactory,
    ContactFactory,
    CVFactory,
    JobFactory,
    JobFeedFactory,
    JobSettingsFactory,
    JobTrackingFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_cut_off_date():
    """Test the cut off date.

    .. note:: I am not sure what this is doing, but I moved ``cut_off_date``
              into a separate method so thought I should test it!

    """
    JobSettingsFactory(job_post_max_days_displayed=10)
    with freeze_time("2017-05-21 14:23:56"):
        cut_off_date = Job.objects.cut_off_date()
        assert datetime(
            2017, 5, 10, 23, 0, 0, tzinfo=pytz.utc
        ) == cut_off_date.astimezone(pytz.utc)


@pytest.mark.django_db
def test_double_job():
    """Don't allow a double job.

    Not going to write the code to check this (for now).
    """
    pass


@pytest.mark.django_db
def test_job_tracking_stats_weekly():
    """Job Tracking Stats."""
    can1 = CandidateFactory(contact=ContactFactory())
    can2 = CandidateFactory(contact=ContactFactory())
    can3 = CandidateFactory(contact=ContactFactory())
    can4 = CandidateFactory(contact=ContactFactory())
    can5 = CandidateFactory(contact=ContactFactory())
    can6 = CandidateFactory(contact=ContactFactory())
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    last_week = date.today() - timedelta(days=8)
    this_week = date.today() - timedelta(days=7)
    earlier = date.today() - timedelta(days=14)

    # before last week
    # Anonymous
    JobTrackingFactory(job=job, click_date=earlier, view_count=1)
    # Logged in Candidates
    JobTrackingFactory(
        job=job,
        candidate=can1,
        click_date=earlier,
        view_count=1,
        application_click_count=1,
    )
    JobTrackingFactory(
        job=job,
        candidate=can5,
        click_date=earlier,
        view_count=1,
        application_click_count=1,
    )

    # clicks last week
    # logged in candidates
    JobTrackingFactory(
        job=job, candidate=can1, click_date=last_week, view_count=1
    )
    JobTrackingFactory(
        job=job,
        candidate=can2,
        click_date=last_week,
        view_count=1,
        application_click_count=1,
    )
    # Anonymous
    JobTrackingFactory(job=job, click_date=last_week, view_count=3)
    JobTrackingFactory(job=job, click_date=last_week, view_count=2)

    # clicks this week
    # logged in candidates
    JobTrackingFactory(
        job=job,
        candidate=can3,
        click_date=this_week,
        view_count=1,
        application_click_count=1,
    )
    JobTrackingFactory(
        job=job, candidate=can4, click_date=this_week, view_count=1
    )
    JobTrackingFactory(
        job=job,
        candidate=can4,
        click_date=date.today(),
        application_click_count=1,
    )
    JobTrackingFactory(
        job=job,
        candidate=can6,
        click_date=date.today(),
        view_count=1,
        application_click_count=1,
    )
    # anonymous
    JobTrackingFactory(job=job, click_date=date.today(), view_count=5)
    JobTrackingFactory(job=job, click_date=this_week, view_count=2)

    job.refresh_from_db()
    stats = job.tracking_stats()
    assert stats["views"] == (3 + 7 + 10)
    assert stats["applications"] == (2 + 1 + 3)
    assert stats["views_last"] == 7
    assert stats["applications_last"] == 1
    assert stats["views_current"] == 10
    assert stats["applications_current"] == 3


@pytest.mark.django_db
def test_job_tracking_stats_monthly():
    """
    Job Tracking Stats Monthly
    """
    can1 = CandidateFactory(contact=ContactFactory())
    can2 = CandidateFactory(contact=ContactFactory())
    can3 = CandidateFactory(contact=ContactFactory())
    can4 = CandidateFactory(contact=ContactFactory())
    can5 = CandidateFactory(contact=ContactFactory())
    can6 = CandidateFactory(contact=ContactFactory())
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    today = date.today()
    this_month = today.replace(day=1)
    last_month = this_month - timedelta(days=7)
    earlier = date.today() - relativedelta(months=2)
    print("this:", this_month, "last: ", last_month, "earlier:", earlier)

    # before last month
    # Anonymous
    JobTrackingFactory(job=job, click_date=earlier, view_count=1)
    # Logged in Candidates
    JobTrackingFactory(
        job=job,
        candidate=can1,
        click_date=earlier,
        view_count=1,
        application_click_count=1,
    )
    JobTrackingFactory(
        job=job,
        candidate=can5,
        click_date=earlier,
        view_count=1,
        application_click_count=1,
    )

    # clicks last month
    # logged in candidates
    JobTrackingFactory(
        job=job, candidate=can1, click_date=last_month, view_count=1
    )
    JobTrackingFactory(
        job=job,
        candidate=can2,
        click_date=last_month,
        view_count=1,
        application_click_count=1,
    )
    # Anonymous
    JobTrackingFactory(job=job, click_date=last_month, view_count=3)
    JobTrackingFactory(job=job, click_date=last_month, view_count=2)

    # clicks this month
    # logged in candidates
    JobTrackingFactory(
        job=job,
        candidate=can3,
        click_date=this_month,
        view_count=1,
        application_click_count=1,
    )
    JobTrackingFactory(
        job=job, candidate=can4, click_date=this_month, view_count=1
    )
    JobTrackingFactory(
        job=job,
        candidate=can5,
        click_date=date.today(),
        application_click_count=1,
    )
    JobTrackingFactory(
        job=job,
        candidate=can6,
        click_date=date.today(),
        view_count=1,
        application_click_count=1,
    )
    # anonymous
    JobTrackingFactory(job=job, click_date=date.today(), view_count=5)
    JobTrackingFactory(job=job, click_date=this_month, view_count=2)
    job.refresh_from_db()
    stats = job.tracking_stats(calendar_month=True)
    assert stats["views"] == (3 + 7 + 10)
    assert stats["applications"] == (2 + 1 + 3)
    assert stats["views_last"] == 7
    assert stats["applications_last"] == 1
    assert stats["views_current"] == 10
    assert stats["applications_current"] == 3


@pytest.mark.django_db
def test_latest():
    """A list of the latest jobs."""
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    indeed_recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=indeed_recruiter)
    today = timezone.now()
    yesterday = timezone.now() + relativedelta(days=-1)
    last_month = timezone.now() + relativedelta(months=-1)
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 1",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=today,
    )
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 2",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 3",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=last_month,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 2",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 1",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=last_month,
    )
    assert ["ILSPA 2", "ILSPA 1", "INDEED 1", "INDEED 2"] == [
        x.title for x in Job.objects.latest()
    ]


@pytest.mark.django_db
def test_latest_cv():
    contact = ContactFactory(user=UserFactory())
    candidate = CandidateFactory(contact=contact)
    cv = CVFactory(contact=contact)
    assert cv == candidate.latest_cv()


@pytest.mark.django_db
def test_latest_cv_none():
    contact = ContactFactory(user=UserFactory())
    candidate = CandidateFactory(contact=contact)
    assert candidate.latest_cv() is None


@pytest.mark.django_db
def test_latest_cv_ordering():
    contact = ContactFactory(user=UserFactory())
    candidate = CandidateFactory(contact=contact)
    CVFactory(contact=contact)
    cv_2 = CVFactory(contact=contact)
    CVFactory(contact=contact, deleted=True)
    assert cv_2 == candidate.latest_cv()


@pytest.mark.django_db
def test_latest_cv_pk():
    contact = ContactFactory(user=UserFactory())
    candidate = CandidateFactory(contact=contact)
    cv = CVFactory(contact=contact)
    assert cv.pk == candidate.latest_cv_pk()


@pytest.mark.django_db
def test_latest_cv_pk_none():
    contact = ContactFactory(user=UserFactory())
    candidate = CandidateFactory(contact=contact)
    assert candidate.latest_cv_pk() is None


@pytest.mark.django_db
def test_latest_cv_pk_ordering():
    contact = ContactFactory(user=UserFactory())
    candidate = CandidateFactory(contact=contact)
    CVFactory(contact=contact)
    cv_2 = CVFactory(contact=contact)
    CVFactory(contact=contact, deleted=True)
    assert cv_2.pk == candidate.latest_cv_pk()


@pytest.mark.django_db
def test_schema_data():
    recruiter = RecruiterFactory(
        contact=ContactFactory(
            company_name="PK",
            user=UserFactory(first_name="Pat"),
            website="https://www.pkimber.net",
        ),
    )
    with freeze_time("2017-05-21 14:23:56"):
        job = JobFactory(
            recruiter=recruiter,
            close_date=timezone.now(),
            location_text="Hatherleigh",
            title="Milking",
            publish_date=timezone.now(),
            description="Dairy Farmer",
        )
    # from rich.pretty import pprint
    # pprint(job.schema_data(), expand_all=True)
    assert {
        "@context": "http://schema.org/",
        "@type": "JobPosting",
        "title": "Milking",
        "description": "Salary 9999, Dairy Farmer",
        "identifier": {
            "@type": "PropertyValue",
            "name": "Pat",
            "value": job.pk,
        },
        "datePosted": "2017-05-21",
        "validThrough": "2017-05-21T23:59",
        "employmentType": "FULL_TIME",
        "industry": "Legal",
        "hiringOrganization": {
            "@type": "Organization",
            "name": "PK",
            "sameAs": "https://www.pkimber.net",
        },
        "jobLocation": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "",
                "addressLocality": "Hatherleigh",
                "addressRegion": "Devon",
                "postalCode": "EX20",
                "addressCountry": "United Kingdom",
            },
        },
    } == job.schema_data()


@pytest.mark.django_db
def test_welcome_email_sent():
    candidate = CandidateFactory(
        contact=ContactFactory(), date_welcome_email=timezone.now()
    )
    assert candidate.welcome_email_sent is True


@pytest.mark.django_db
def test_welcome_email_sent_not():
    candidate = CandidateFactory(contact=ContactFactory())
    assert candidate.welcome_email_sent is False
