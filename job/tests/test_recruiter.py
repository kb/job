# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory
from job.models import Candidate
from job.tests.factories import (
    CandidateFactory,
    ContactFactory,
    JobFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory


def _recruiter_for_candidate_search():
    user = UserFactory()
    recruiter = RecruiterFactory(
        contact=ContactFactory(user=user), search_expires=timezone.now().date()
    )
    job = JobFactory(recruiter=recruiter)
    job.publish_job(user)
    return recruiter


@pytest.mark.django_db
def test_can_view_candidate_consent():
    consent = ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    recruiter = _recruiter_for_candidate_search()
    candidate = CandidateFactory(contact=ContactFactory(user=UserFactory()))
    UserConsent.objects.set_consent(consent, True, candidate.contact.user)
    assert recruiter.can_view_candidate(candidate) is True


@pytest.mark.django_db
def test_can_view_candidate_consent_none():
    """No user consent (for the candidate) so don't allow search."""
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    recruiter = _recruiter_for_candidate_search()
    candidate = CandidateFactory(contact=ContactFactory(user=UserFactory()))
    assert recruiter.can_view_candidate(candidate) is False


@pytest.mark.django_db
def test_can_view_candidate_consent_not():
    consent = ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    recruiter = _recruiter_for_candidate_search()
    candidate = CandidateFactory(contact=ContactFactory(user=UserFactory()))
    UserConsent.objects.set_consent(consent, False, candidate.contact.user)
    assert recruiter.can_view_candidate(candidate) is False
