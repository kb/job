# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from job.tests.factories import JobCreditFactory, RecruiterFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_jobcredit_mark_paid(client):
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    recruiter = RecruiterFactory(contact=contact)
    credit = JobCreditFactory(recruiter=recruiter, credits=1)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("job.credit.mark-paid", args=[credit.pk])
    response = client.post(url)
    assert 302 == response.status_code
    assert reverse("contact.detail", args=[contact.pk]) == response.url
