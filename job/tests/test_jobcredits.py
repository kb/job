# -*- encoding: utf-8 -*-
"""Test the 'JobCredit' model."""
import pytest

from decimal import Decimal, getcontext
from django.contrib.contenttypes.models import ContentType

from checkout.models import CheckoutAction, CheckoutState
from checkout.tests.factories import CheckoutFactory
from finance.tests.factories import VatSettingsFactory
from job.models import (
    Recruiter,
    JobCredit,
    PRODUCT_JOB_POST,
    PRODUCT_JOB_POST_DISCOUNT,
)
from job.tests.factories import (
    ContactFactory,
    JobCreditFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory
from mail.tests.factories import MailTemplateFactory
from stock.models import Product
from stock.tests.factories import ProductFactory


def get_recruiter():
    try:
        return Recruiter.objects.get(contact__user__username="rec")
    except Recruiter.DoesNotExist:
        return RecruiterFactory(
            contact=ContactFactory(
                user=UserFactory(username="rec", email="rec@example.com"),
                company_name="Recruiters Unlimited",
            )
        )


def get_product():
    try:
        return Product.objects.get(slug=PRODUCT_JOB_POST)
    except Product.DoesNotExist:
        return ProductFactory(
            slug=PRODUCT_JOB_POST, name="Job Single", price=149
        )


def get_product_discount():
    try:
        return Product.objects.get(slug=PRODUCT_JOB_POST_DISCOUNT)
    except Product.DoesNotExist:
        return ProductFactory(
            slug=PRODUCT_JOB_POST_DISCOUNT, name="Job Discount", price=97
        )


@pytest.mark.django_db
def test_jobcredit_default():
    """Job credit on creation should have 0 available credits"""

    job_credit = JobCredit(
        recruiter=get_recruiter(), credits=1, product=get_product()
    )
    job_credit.save()
    job_credit.refresh_from_db()
    assert job_credit.available_credits == 0
    assert job_credit.checkout_state.slug == CheckoutState.PENDING


# @pytest.mark.django_db
# def test_jobcredit_checkout():
#     """
#         Check that Job Credit checkout functions return correct data
#     """
#
#     MailTemplateFactory(slug="job_credits")
#     VatSettingsFactory()
#     job_credit = JobCreditFactory(
#         recruiter=get_recruiter(), credits=2, product=get_product_discount()
#     )
#     assert job_credit.checkout_can_charge
#     getcontext().prec = 5
#     assert job_credit.checkout_total == Decimal(2) * Decimal(97) * Decimal(1.2)
#     assert job_credit.checkout_description == ["2 x Job Discount @ 97 + VAT"]
#
#     checkout = CheckoutFactory(
#         action=CheckoutAction.objects.payment,
#         object_id=job_credit.pk,
#         content_type=ContentType.objects.get_for_model(job_credit),
#         content_object=job_credit,
#         total=job_credit.checkout_total,
#     )
#     job_credit.checkout_success(checkout)
#     job_credit.refresh_from_db()
#     assert job_credit.available_credits == 2
