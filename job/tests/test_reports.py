# -*- encoding: utf-8 -*-
"""
Test reports for the jobs board.

.. note:: Our customer project has some additional (project specific) reports
          in the ``member/reports.py`` module.

"""
import csv
import pytest
import pytz

from datetime import datetime
from django.utils import timezone
from freezegun import freeze_time

from job.models import ILSPA_RECRUITER
from job.reports import JobListReport, JobsBoardMonthlyStats, RecruiterReport
from job.tests.factories import (
    CandidateFactory,
    ContactFactory,
    JobFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory
from report.models import ReportSchedule, ReportSpecification


@pytest.mark.django_db
def test_job_list():
    JobListReport().init_report()
    freeze_date = datetime(2017, 5, 21, 18, 54, 12, tzinfo=pytz.utc)
    with freeze_time(freeze_date):
        recruiter = RecruiterFactory(contact=ContactFactory(company_name="ABC"))
        JobFactory(
            recruiter=recruiter,
            order_key=ILSPA_RECRUITER,
            publish=True,
            publish_date=timezone.now(),
            title="Orange",
        )
    # report data
    CandidateFactory(contact=ContactFactory(user=UserFactory()))
    # schedule report
    report_specification = ReportSpecification.objects.get(
        slug=JobListReport.REPORT_SLUG
    )
    report_specification.schedule(UserFactory(), parameters=None)
    # Run the report
    schedule_pks = ReportSchedule.objects.process()
    # Find the schedule
    assert 1 == len(schedule_pks)
    schedule_pk = schedule_pks[0]
    schedule = ReportSchedule.objects.get(pk=schedule_pk)
    # Check the report output::
    reader = csv.reader(open(schedule.output_file.path), "excel")
    first_row = None
    result = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            result.append(row)
    assert [
        "Publish date",
        "Recruiter",
        "Title",
        "Applications Received",
        "Application clicks - all",
        "Application clicks - this month",
        "Application clicks - last month",
        "Views - all",
        "Views - this month",
        "Views - last month",
    ] == first_row
    assert [
        ["2017-05-21", "ABC", "Orange", "0", "0", "0", "0", "0", "0", "0"]
    ] == result


@pytest.mark.django_db
def test_monthly_stats():
    JobsBoardMonthlyStats().init_report()
    freeze_date = datetime(2017, 5, 21, 18, 54, 12, tzinfo=pytz.utc)
    with freeze_time(freeze_date):
        # report data
        CandidateFactory(contact=ContactFactory(user=UserFactory()))
        # schedule report
        report_specification = ReportSpecification.objects.get(
            slug=JobsBoardMonthlyStats.REPORT_SLUG
        )
        report_specification.schedule(UserFactory(), parameters=None)
        # Run the report
        schedule_pks = ReportSchedule.objects.process()
    # Find the schedule
    assert 1 == len(schedule_pks)
    schedule_pk = schedule_pks[0]
    schedule = ReportSchedule.objects.get(pk=schedule_pk)
    # Check the report output::
    reader = csv.reader(open(schedule.output_file.path), "excel")
    first_row = None
    result = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            result.append(row)
    assert [
        "Month",
        "New Candidates registered",
        "Total Candidates",
        "New Recruiters registered",
        "Total Recruiters",
        "New ILSPA Job Posts",
        "Total ILSPA Job Posts",
        "New feed Job Posts",
        "Total feed Job Posts",
    ] == first_row
    assert [["May 2017", "1", "1", "0", "0", "0", "0", "0", "0"]] == result


@pytest.mark.django_db
def test_recruiter():
    RecruiterReport().init_report()
    # report data
    freeze_date = datetime(2017, 5, 21, 18, 54, 12, tzinfo=pytz.utc)
    with freeze_time(freeze_date):
        RecruiterFactory(
            contact=ContactFactory(
                company_name="KB",
                user=UserFactory(
                    first_name="Andy", last_name="Kimber", email="a@test.com"
                ),
            )
        )
    # schedule report
    report_specification = ReportSpecification.objects.get(
        slug=RecruiterReport.REPORT_SLUG
    )
    report_specification.schedule(UserFactory(), parameters=None)
    # Run the report
    schedule_pks = ReportSchedule.objects.process()
    # Find the schedule
    assert 1 == len(schedule_pks)
    schedule_pk = schedule_pks[0]
    schedule = ReportSchedule.objects.get(pk=schedule_pk)
    # Check the report output::
    reader = csv.reader(open(schedule.output_file.path), "excel")
    first_row = None
    result = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            result.append(row)
    assert [
        "Company Name",
        "Contact",
        "First Name",
        "Last Name",
        "Email Address",
        "Registration Date",
    ] == first_row
    assert [
        ["KB", "Andy Kimber", "Andy", "Kimber", "a@test.com", "2017-05-21"]
    ] == result
