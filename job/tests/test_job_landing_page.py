# -*- encoding: utf-8 -*-
import pytest

from django.core.exceptions import ValidationError

from block.tests.factories import PageFactory, TemplateFactory
from job.models import LandingPage

from .factories import JobSettingsFactory, LandingPageFactory


@pytest.mark.django_db
def test_job_landing_clean():
    obj = LandingPageFactory()
    with pytest.raises(ValidationError):
        obj.full_clean()


@pytest.mark.django_db
def test_job_landing_create_page():
    template = TemplateFactory(template_name="example/landing.html")
    JobSettingsFactory(
        landing_page_slug="landing", landing_page_template=template
    )
    page = LandingPage.objects.create_page("Farming Jobs in Hatherleigh")
    assert "landing" == page.slug
    assert "farming-jobs-in-hatherleigh" == page.slug_menu


@pytest.mark.django_db
def test_job_landing_page_str():
    page = PageFactory(slug="landing", slug_menu="123")
    obj = LandingPageFactory(page=page)
    assert "landing/123" == str(obj)


@pytest.mark.django_db
def test_job_landing_pages():
    LandingPageFactory(page=PageFactory(slug="landing", slug_menu="a"))
    LandingPageFactory(
        page=PageFactory(deleted=True, slug="landing", slug_menu="b")
    )
    LandingPageFactory(page=PageFactory(slug="landing", slug_menu="c"))
    qs = LandingPage.objects.landing_pages()
    assert ["a", "c"] == [obj.page.slug_menu for obj in qs]
