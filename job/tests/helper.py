# -*- encoding: utf-8 -*-
from django.urls import reverse


def check_contact(model_instance):
    # fields
    model_instance.address_1
    model_instance.address_2
    model_instance.address_3
    model_instance.company_name
    model_instance.country
    model_instance.county
    model_instance.dob
    model_instance.mobile
    model_instance.nationality
    model_instance.phone
    model_instance.position
    model_instance.postcode
    model_instance.town
    model_instance.user
    model_instance.website
    # methods
    model_instance.full_name
    model_instance.is_candidate
    model_instance.is_recruiter
    # url
    reverse("contact.detail", args=[model_instance.pk])
