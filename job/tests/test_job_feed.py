# -*- encoding: utf-8 -*-
"""

Don't forget::

  example_job.tests.test_job_feed.py

"""
import os
import pytest

from unittest.mock import patch

from job.models import JobError
from job.tests.factories import JobFeedFactory


@patch("ftplib.FTP")
@pytest.mark.django_db
def test_download_ftp(mock_ftp):
    """Download a file using FTP.

    .. note:: Remove the mock to download the actual file.

    """
    url = os.environ.get("TEST_LEGISTS_URL")
    username = os.environ.get("TEST_LEGISTS_USERNAME", "")
    password = os.environ.get("TEST_LEGISTS_PASSWORD", "")
    job_feed = JobFeedFactory(
        recruiter=None, url=url, username=username, password=password
    )
    job_feed.download_ftp("ILSPAJobs.xml")


@pytest.mark.parametrize(
    "file_name,url,username,password",
    [
        ("", "192.168.1.1", "patrick", "letmein"),
        ("apple.txt", "", "patrick", "letmein"),
        ("apple.txt", "192.168.1.1", "", "letmein"),
        ("apple.txt", "192.168.1.1", "patrick", ""),
    ],
)
@pytest.mark.django_db
def test_download_ftp_missing_parameters(file_name, url, username, password):
    job_feed = JobFeedFactory(
        recruiter=None, url=url, username=username, password=password
    )
    with pytest.raises(JobError) as e:
        job_feed.download_ftp(file_name)
    assert "needs a file name, URL, user name and password" in str(e.value)
