# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import JobSettings, JobFeed


class JobSettingsAdmin(admin.ModelAdmin):
    pass


admin.site.register(JobFeed)
admin.site.register(JobSettings, JobSettingsAdmin)
