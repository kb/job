# -*- encoding: utf-8 -*-
from datetime import date

from job.models import Candidate, ILSPA_RECRUITER, Job, Recruiter
from report.service import ReportMixin


class JobsBoardMonthlyStats(ReportMixin):
    """Reports, Jobs Board Reports, Monthly Stats.

    Are students who have registered for a course, but not filled in a
    candidate profile included in the monthly stats report?

    - I think all contacts with a "Candidate" record are included:

      From looking at the source code, the candidate profile is created:

      1. "CandidateCreateMixin": When a user registers as a candidate.
      2. "CandidateProfileMixin": When a student fills in a candidate profile.
      3. "JobDetailMixin": When a user views a job posting.
      4. "JobApplicationRedirectMixin": When a user applies for a job.

      There might be other places where the 'Candidate' record is created.

    - https://www.kbsoftware.co.uk/crm/ticket/2742/

    """

    REPORT_SLUG = "job-monthly-stats"
    REPORT_TITLE = "Monthly Stats"

    stats = []

    def get_month_stats(self, month, year):
        month_stats = None

        for stat in self.stats:
            if stat["month"] == month and stat["year"] == year:
                month_stats = stat

        if not month_stats:
            month_stats = {
                "month": month,
                "year": year,
                "candidates": {"month": 0, "total": 0},
                "recruiters": {"month": 0, "total": 0},
                "ilspa_jobs": {"month": 0, "total": 0},
                "feed_jobs": {"month": 0, "total": 0},
            }
            self.stats.append(month_stats)

        return month_stats

    def colate_monthly_stats(self):
        self.stats = []
        current_month = None
        candidate_total = 0
        recruiter_total = 0
        ilspa_job_total = 0
        feed_job_total = 0

        for c in Candidate.objects.exclude(contact__deleted=True).order_by(
            "created"
        ):
            if (
                not current_month
                or c.created.month != current_month["month"]
                or c.created.year != current_month["year"]
            ):
                current_month = self.get_month_stats(
                    c.created.month, c.created.year
                )
                current_month["candidates"]["total"] = candidate_total

            current_month["candidates"]["month"] += 1
            current_month["candidates"]["total"] += 1
            candidate_total += 1

        for r in Recruiter.objects.exclude(contact__deleted=True).order_by(
            "created"
        ):
            if (
                not current_month
                or r.created.month != current_month["month"]
                or r.created.year != current_month["year"]
            ):
                current_month = self.get_month_stats(
                    r.created.month, r.created.year
                )
                current_month["recruiters"]["total"] = recruiter_total

            current_month["recruiters"]["month"] += 1
            current_month["recruiters"]["total"] += 1
            recruiter_total += 1

        for j in Job.objects.order_by("created"):
            if (
                not current_month
                or j.created.month != current_month["month"]
                or j.created.year != current_month["year"]
            ):
                current_month = self.get_month_stats(
                    j.created.month, j.created.year
                )
                current_month["ilspa_jobs"]["total"] = ilspa_job_total
                current_month["feed_jobs"]["total"] = feed_job_total

            if j.order_key == 1:
                current_month["ilspa_jobs"]["month"] += 1
                current_month["ilspa_jobs"]["total"] += 1
                ilspa_job_total += 1
            else:
                current_month["feed_jobs"]["month"] += 1
                current_month["feed_jobs"]["total"] += 1
                feed_job_total += 1

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        self.colate_monthly_stats()
        csv_writer.writerow(
            (
                "Month",
                "New Candidates registered",
                "Total Candidates",
                "New Recruiters registered",
                "Total Recruiters",
                "New ILSPA Job Posts",
                "Total ILSPA Job Posts",
                "New feed Job Posts",
                "Total feed Job Posts",
            )
        )
        reverse_stats = sorted(
            self.stats,
            key=lambda s: date(day=1, month=s["month"], year=s["year"]),
            reverse=True,
        )
        for stat in reverse_stats:
            count = count + 1
            mDate = date(day=1, month=stat["month"], year=stat["year"])
            csv_writer.writerow(
                (
                    mDate.strftime("%B %Y"),
                    stat["candidates"]["month"],
                    stat["candidates"]["total"],
                    stat["recruiters"]["month"],
                    stat["recruiters"]["total"],
                    stat["ilspa_jobs"]["month"],
                    stat["ilspa_jobs"]["total"],
                    stat["feed_jobs"]["month"],
                    stat["feed_jobs"]["total"],
                )
            )
        return count


class JobListReport(ReportMixin):
    """

    This report was originally called ``csv_job_list``.

    """

    REPORT_SLUG = "job-list"
    REPORT_TITLE = "Job list"

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(
            (
                "Publish date",
                "Recruiter",
                "Title",
                "Applications Received",
                "Application clicks - all",
                "Application clicks - this month",
                "Application clicks - last month",
                "Views - all",
                "Views - this month",
                "Views - last month",
            )
        )
        qs = Job.objects.filter(order_key=ILSPA_RECRUITER).order_by(
            "-publish_date"
        )
        for job in qs:
            count = count + 1
            track_stats = job.tracking_stats(calendar_month=True)
            csv_writer.writerow(
                (
                    (
                        job.publish_date.strftime("%Y-%m-%d")
                        if job.publish_date
                        else "Not Published"
                    ),
                    job.recruiter,
                    job.title,
                    (
                        job.application_stats()["total"]
                        if job.publish_date
                        else ""
                    ),
                    track_stats["applications"] if job.publish_date else "",
                    (
                        track_stats["applications_current"]
                        if job.publish_date
                        else ""
                    ),
                    (
                        track_stats["applications_last"]
                        if job.publish_date
                        else ""
                    ),
                    track_stats["views"] if job.publish_date else "",
                    track_stats["views_current"] if job.publish_date else "",
                    track_stats["views_last"] if job.publish_date else "",
                )
            )
        return count


class RecruiterReport(ReportMixin):
    """List of recruiters.

    This report was originally called ``csv_recruiter_list``.

    """

    REPORT_SLUG = "job-recruiter"
    REPORT_TITLE = "List of recruiters"

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(
            (
                "Company Name",
                "Contact",
                "First Name",
                "Last Name",
                "Email Address",
                "Registration Date",
            )
        )
        qs = Recruiter.objects.filter(jobfeed=None).order_by(
            "contact__company_name",
            "contact__user__last_name",
            "contact__user__first_name",
            "contact__user__email",
        )
        for x in qs:
            count = count + 1
            csv_writer.writerow(
                (
                    x.contact.company_name,
                    x.contact.full_name,
                    x.contact.user.first_name,
                    x.contact.user.last_name,
                    x.contact.user.email,
                    x.created.strftime("%Y-%m-%d"),
                )
            )
        return count
