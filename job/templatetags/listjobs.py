# -*- encoding: utf-8 -*-
import json

from django.template import Library, Node, TemplateSyntaxError

from job.models import Job


register = Library()


class LatestJobNode(Node):
    def __init__(self, num):
        self.num = num

    def render(self, context):
        context["recent_links"] = Job.objects.all()[: self.num]
        return ""


# # Copy of '_json_script_escapes' from 'django/utils/html.py'.
# _json_script_escapes = {
#     ord(">"): "\\u003E",
#     ord("<"): "\\u003C",
#     ord("&"): "\\u0026",
# }


def get_latest_jobs(parser, token):
    bits = token.contents.split()
    if len(bits) != 2:
        raise TemplateSyntaxError(
            "get_latest_jobs tag takes exactly one argument"
        )
    return LatestJobNode(bits[1])


get_latest_jobs = register.tag(get_latest_jobs)


@register.filter
def first_sentence(value):
    # split if an end of sentence or paragraph but not if a web address
    parts = value.split("</p>")
    parts = parts[0].split(". ")
    parts = parts[0].split("? ")
    parts = parts[0].split(".&nbsp;")
    parts = parts[0].split(".</p>")
    parts = parts[0].split(".\n")
    parts = parts[0].split(".\r")
    return parts[0]


@register.filter
def json_script_ld(value, element_id):
    """
    Escape all the HTML/XML special characters with their unicode escapes, so
    value is safe to be output anywhere except for inside a tag attribute. Wrap
    the escaped JSON in a script tag.

    12/07/2022, Copy of built in template tag, ``json_script``,
    using ``application/ld+json`` rather than ``application/json``.

    Safely Including Data for JavaScript in a Django Template
    https://adamj.eu/tech/2020/02/18/safely-including-data-for-javascript-in-a-django-template/

    """
    from django.core.serializers.json import DjangoJSONEncoder
    from django.utils.html import _json_script_escapes, format_html
    from django.utils.safestring import mark_safe

    json_str = json.dumps(value, cls=DjangoJSONEncoder).translate(
        _json_script_escapes
    )
    return format_html(
        '<script id="{}" type="application/ld+json">{}</script>',
        element_id,
        mark_safe(json_str),
    )


@register.filter
def remove_paras(value):
    # remove paragraph markers
    parts = value.replace("<p>", " ")
    parts = parts.replace("&nbsp;", " ")
    parts = parts.replace("</p>", " ")
    parts = parts.replace("<br>", " ")
    parts = parts.replace("\n", " ")
    parts = parts.replace("\r", " ")
    return parts
