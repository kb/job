# -*- encoding: utf-8 -*-
import attr
import logging

from dateutil import parser

from job.models import Job, JobRoleParser, PracticeAreaParser
from search.search import SearchError


logger = logging.getLogger(__name__)


def json_dump(query):
    import json

    print(json.dumps(query, indent=2))


def get_location_name(location_name, region_name):
    if (
        location_name == region_name
        or region_name.lower() == "greater " + location_name.lower()
        or region_name.lower() == "city of " + location_name.lower()
    ):
        location = location_name
    else:
        location = None
        if location_name:
            location_parts = location_name.lower().split(" ")
            location_prefix = location_parts[0]
            location_rest = " ".join(location_parts[1:])
            if location_prefix in ["east", "north", "south", "west"]:
                if (
                    region_name.lower() == location_rest
                    or region_name.lower() == "greater " + location_rest
                    or region_name.lower() == "city of " + location_rest
                ):
                    location = location_name
        if not location:
            location = "{} {}".format(location_name, region_name)
    return location


@attr.s
class Row:
    close_date = attr.ib()
    description = attr.ib()
    job_role = attr.ib()
    location = attr.ib()
    order_key = attr.ib()
    practice_area = attr.ib()
    publish_date = attr.ib()
    recruiter_name = attr.ib()
    salary = attr.ib()
    title = attr.ib()


class JobIndexMixin:
    def _data(self, source):
        close_date = job_role = practice_area = publish_date = salary = None
        if "close_date" in source:
            close_date = parser.parse("{close_date}".format(**source))
        if "practice_area" in source:
            practice_area = "{practice_area}".format(**source)
        if "publish_date" in source:
            publish_date = parser.parse("{publish_date}".format(**source))
        if "job_role" in source:
            job_role = "{job_role}".format(**source)
        if "salary" in source:
            salary = "{salary}".format(**source)
        return close_date, job_role, practice_area, publish_date, salary

    def _settings(self):
        """Index settings."""
        return {
            "number_of_shards": 1,
            "analysis": {
                "analyzer": {
                    "job_analyser": {
                        "tokenizer": "standard",
                        "filter": ["standard", "lowercase", "my_metaphone"],
                    }
                },
                "filter": {
                    "my_metaphone": {
                        "type": "phonetic",
                        "encoder": "metaphone",
                        "replace": False,
                    }
                },
            },
        }

    @property
    def configuration(self):
        """Index configuration.  Used by the Elasticsearch 'create' method."""
        return {
            "mappings": {
                self.document_type: {
                    "properties": {
                        "close_date": {"type": "date"},
                        "coordinates": {"type": "geo_point"},
                        "description": {
                            "type": "text",
                            "analyzer": "job_analyser",
                        },
                        "job_role": {
                            "type": "text",
                            "analyzer": "job_analyser",
                        },
                        "location": {
                            "type": "text",
                            "analyzer": "job_analyser",
                        },
                        "location_name": {
                            "type": "text",
                            "analyzer": "job_analyser",
                        },
                        "postcode_district": {
                            "type": "text",
                            "analyzer": "job_analyser",
                        },
                        "practice_area": {
                            "type": "text",
                            "analyzer": "job_analyser",
                        },
                        "publish_date": {"type": "date"},
                        "recruiter_name": {
                            "type": "text",
                            "analyzer": "job_analyser",
                        },
                        "region": {"type": "text", "analyzer": "job_analyser"},
                        "title": {"type": "text", "analyzer": "job_analyser"},
                        "town": {"type": "text", "analyzer": "job_analyser"},
                    }
                }
            },
            "settings": {"index": self._settings()},
        }

    def data_as_attr(self, source, pk):
        """A row of data returned as part of the search results.

        .. note:: Check the ``source`` method.  Not all fields are added to the
                  Elasticsearch index.

        """
        close_date, job_role, practice_area, publish_date, salary = self._data(
            source
        )
        return Row(
            close_date=close_date,
            description="{description}".format(**source),
            job_role=job_role,
            location="{location_name}".format(**source),
            order_key="{order_key}".format(**source),
            practice_area=practice_area,
            publish_date=publish_date,
            recruiter_name="{recruiter_name}".format(**source),
            title="{title}".format(**source),
            salary=salary,
        )

    @property
    def document_type(self):
        """Elasticsearch document type."""
        return "job"

    @property
    def index_name(self):
        """Name of the Elasticsearch index (will have the domain added)."""
        return "job"

    def query(self, criteria):
        """Build the query for the Elasticsearch data.

        Criteria (data) is as follows (see JobElasticSearchForm)::

          data = {
              'location': <location entered on form> or landing_page.location,
              'job_role': <job_role entered on form> or landing_page.job_role,
              'practice_area': <practice_area entered on form> or
                                landing_page.practice_area,
              'experience': <experience entered on form>
          }

        .. note:: Elasticsearch Fields queried are: ``location``, ``job_role``,
                  ``practice_area``, ``title`` and ``description``
                  data passed in the experience field is queried in the
                  job_role and title fields

        """
        should = []
        boost_ilspa_jobs = 0.50
        for field, value in criteria.items():
            if value:
                fuzzy_field = field
                #  the search query for each field is as follows:
                # location      - match location field - boost 5x
                #               - match location field - fuzziness=1
                #               - match location field - phonetic
                # job_role      - match title field - boost 2x
                #               - match job_role field
                #               - match job_role field - fuzziness=1
                #               - match job_role field - phonetic
                # experience    - match title field - boost 2x
                #               - match job_role field
                #               - match job_role field - fuzziness=1
                #               - match job_role field - phonetic
                # practice_area - match title field - boost 2x
                #               - match practice_area field
                #               - match description field
                #               - match practice_area field - fuzziness=1
                #               - match practice_area field - - phonetic
                # to get ilspa jobs to the top we do this
                # order_key     - match to value=1 field - reduce by 50%
                if field == "location":
                    should.append(
                        {"match": {field: {"query": value, "boost": 5}}}
                    )
                elif field == "experience":
                    fuzzy_field = "job_role"
                    should.append(
                        {"match": {"title": {"query": value, "boost": 2}}}
                    )
                    should.append({"match": {"job_role": {"query": value}}})
                else:
                    should.append(
                        {"match": {"title": {"query": value, "boost": 2}}}
                    )
                    should.append({"match": {field: {"query": value}}})
                    if field == "practice_area":
                        should.append(
                            {"match": {"description": {"query": value}}}
                        )

                metaphone = "{}.metaphone".format(fuzzy_field)
                should.append(
                    {
                        "bool": {
                            "should": [
                                {
                                    "match": {
                                        fuzzy_field: {
                                            "query": value,
                                            "fuzziness": 1,
                                        }
                                    }
                                },
                                {"match": {metaphone: {"query": value}}},
                            ]
                        }
                    }
                )
        # if not should:
        #     # no criteria entered return latest jobs
        #     return self._latest_jobs(current_page)

        # can't use a sort as that puts ILSPA jobs above all other jobs
        # so if criteria has been added we add the ILSPA order_key value
        # as a search criteria to boost the score of ILSPA jobs
        should = should + [
            {"match": {"order_key": {"query": "1", "boost": boost_ilspa_jobs}}}
        ]
        query = {"query": {"bool": {"should": should}}}
        return query

    def queryset(self, pk):
        """Build the Elasticsearch index using this queryset."""
        if pk is None:
            qs = Job.objects.current()
        else:
            qs = Job.objects.filter(pk=pk)
        return qs

    def source(self, row):
        """Build the index using this data from the 'queryset'."""
        if not row.publish_date:
            raise SearchError(
                "Cannot index job '{}'.  It has no 'publish_date".format(row.pk)
            )
        result = {
            "description": row.description,
            "location_name": row.location_name,
            "order_key": row.order_key,
            "publish_date": row.publish_date.isoformat(),
            "recruiter_name": row.recruiter_name,
            "title": row.title,
        }
        if row.location:
            result.update(
                {
                    "town": row.location.town,
                    "region": row.location.region.name,
                    "location": "{} {}".format(
                        row.location_name, row.location.region.name
                    ),
                    "postcode_district": row.location.postcode,
                    "coordinates": {
                        "lat": str(row.location.latitude),
                        "lon": str(row.location.longitude),
                    },
                }
            )
        else:
            result.update({"location": row.location_name})
        # only include the close date if it's an ilspa job
        if row.close_date and not row.is_from_feed:
            result.update({"close_date": row.close_date.isoformat()})
        if row.salary:
            result.update({"salary": row.salary})
        if row.practice_area:
            result.update({"practice_area": row.practice_area.title})
        else:
            practice_area_parser = PracticeAreaParser()
            areas = practice_area_parser.parse(
                row.title + " " + row.description
            )
            if areas:
                result.update(
                    {"practice_area": " ".join([area.title for area in areas])}
                )
        if row.job_role:
            result.update({"job_role": row.job_role.title})
        else:
            job_role_parser = JobRoleParser()
            roles = job_role_parser.parse(row.title)
            if roles:
                result.update(
                    {"job_role": " ".join([role.title for role in roles])}
                )
        return result


class JobIndex(JobIndexMixin):
    @property
    def is_soft_delete(self):
        return False
