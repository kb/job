# -*- encoding: utf-8 -*-
"""
Jobs board reports.

.. note:: Our customer project has some additional (project specific) reports
          in the ``member/reports.py`` module.

"""
import ftplib
import json
import logging
import math
import os
import pytz
import tempfile
import urllib.parse

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from decimal import Decimal, getcontext
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db import transaction
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from reversion import revisions as reversion

from base.model_utils import (
    private_file_store,
    TimeStampedModel,
    TimedCreateModifyDeleteModel,
)
from base.singleton import SingletonModel
from block.models import Page, Template
from checkout.models import (
    Checkout,
    CheckoutAction,
    CheckoutState,
    default_checkout_state,
)
from finance.models import VatSettings
from gdpr.models import Consent, UserConsent, UserConsentUnsubscribe
from mail.models import Mail, Notify, RejectedEmail
from mail.service import queue_mail_message, queue_mail_template
from mail.tasks import process_mail
from stock.models import Product


logger = logging.getLogger(__name__)


ILSPA_RECRUITER = 1
FEED_RECRUITER = 2

# slug for products
PRODUCT_JOB_POST = "job-post"
PRODUCT_JOB_POST_DISCOUNT = "job-post-discount"
PRODUCT_JOB_POST_SUBSCRIPTION = "job-post-subscription"

# slug for email templates
RECRUITER_APPLICATION_FROM_CANDIDATE = "recruiter_application_from_candidate"
RECRUITER_PAYMENT_THANKYOU = "recruiter_payment_thankyou"


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


def vat(total):
    vat_settings = VatSettings.objects.settings()
    vat_code = vat_settings.standard_vat_code
    return total * vat_code.rate


def total_with_vat(total):
    return total + vat(total)


class JobError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class JobSettings(SingletonModel):
    job_post_allows_days_search = models.PositiveIntegerField(default=60)
    job_post_max_days_displayed = models.PositiveIntegerField(default=60)
    job_post_chargeable = models.BooleanField(default=True)

    default_latitude = models.DecimalField(
        max_digits=7, decimal_places=5, default=Decimal("52.47872")
    )
    default_longitude = models.DecimalField(
        max_digits=7, decimal_places=5, default=Decimal("-1.90723")
    )
    landing_page_slug = models.SlugField(
        max_length=100, blank=True, help_text="Slug to use for landing pages"
    )
    landing_page_template = models.ForeignKey(
        Template,
        blank=True,
        null=True,
        help_text="Template to use for landing pages",
        on_delete=models.CASCADE,
    )
    pdf_heading = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name = "Job settings"

    def __str__(self):
        return (
            "Job settings (job_post_allows_days_search: {}; "
            "default_latitude: {}; default_longitude: {})".format(
                self.job_post_allows_days_search,
                self.default_latitude,
                self.default_longitude,
            )
        )


reversion.register(JobSettings)


class CVTypeManager(models.Manager):
    @property
    def membership_application(self):
        return self.model.objects.get(slug=CVType.MEMBERSHIP_APPLICATION)


class CVType(models.Model):
    APPLICATION = "job-application"
    CANDIDATE_PROFILE = "job-profile"
    MEMBERSHIP_APPLICATION = "member"

    slug = models.SlugField(max_length=20)
    title = models.CharField(max_length=50)
    objects = CVTypeManager()

    class Meta:
        ordering = ("slug",)
        verbose_name = "CV Type"
        verbose_name_plural = "CV Types"

    def __str__(self):
        return "{}".format(self.title)


reversion.register(CVType)


class CVManager(models.Manager):
    def contact(self, contact):
        return (
            self.model.objects.filter(contact=contact)
            .exclude(deleted=True)
            .order_by("-created")
        )

    def latest(self, contact):
        return self.contact(contact).first()


class CV(TimedCreateModifyDeleteModel):
    contact = models.ForeignKey(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    cv_type = models.ForeignKey(CVType, on_delete=models.CASCADE)
    # PJK TODO Rename this field?
    file_name = models.FileField(
        upload_to="job/candidate/cv", storage=private_file_store
    )
    title = models.CharField(max_length=50, blank=True)
    objects = CVManager()

    class Meta:
        ordering = ("created",)
        verbose_name = "CV"
        verbose_name_plural = "CVs"

    def __str__(self):
        return "{} ({})".format(self.cv_type.title, self.file_name)

    def get_absolute_url(self):
        return reverse("job.cv.download", args=[self.pk])

    def str_for_dropdown(self):
        if self.title:
            title = self.title
        else:
            title = self.cv_type
        return "{} (uploaded {})".format(
            title, self.created.strftime("%d/%m/%Y")
        )

    @property
    def base_file_name(self):
        return os.path.basename(self.file_name.url)


reversion.register(CV)


class Country(models.Model):
    country_code = models.CharField(max_length=3)
    alpha2_code = models.CharField(max_length=2)
    numeric_code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)
    supranational = models.ForeignKey(
        "self", blank=True, null=True, on_delete=models.CASCADE
    )

    class Meta:
        ordering = ("country_code",)
        verbose_name = "Country"
        verbose_name_plural = "Countries"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(Country)


class RegionType(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)

    class Meta:
        ordering = ("name",)
        verbose_name = "Region Type"
        verbose_name_plural = "Region Types"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(RegionType)


class RegionManager(models.Manager):
    def in_use(self):
        return self.model.objects.exclude(location=None)


class Region(models.Model):
    region_code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    # welsh name
    alternate_region_code = models.CharField(max_length=3)
    alternate_name = models.CharField(max_length=100)
    alternate_slug = models.SlugField(max_length=100)
    region_type = models.ForeignKey(
        RegionType, blank=True, null=True, on_delete=models.CASCADE
    )
    country = models.ForeignKey(
        Country, blank=True, null=True, on_delete=models.CASCADE
    )
    objects = RegionManager()

    class Meta:
        ordering = ("region_code",)
        verbose_name = "Region"
        verbose_name_plural = "Regions"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(Region)


class LocationManager(models.Manager):
    def _add_miles_to_latitude(self, latitude, miles):
        return latitude + Decimal(float(miles) * 0.014559)

    def _add_miles_to_longitude(self, longitude, latitude, miles):
        convert_factor = 1 / (69.171 * math.cos(math.radians(float(latitude))))

        if convert_factor < 0:
            convert_factor = 0 - convert_factor

        return longitude + Decimal(float(miles) * convert_factor)

    def _get_postcode_outward(self, postcode):
        if len(postcode) > 4:
            outward = postcode[:-3].rstrip()
        else:
            outward = postcode
        return outward.upper()

    def create_location(self, postcode, latitude, longitude, region, country):
        location = self.model(
            postcode=postcode,
            latitude=latitude,
            longitude=longitude,
            region=region,
            country=country,
        )
        location.save()
        return location

    def region_set(self, region_name, exact=True):
        region_slug = slugify(region_name)
        if exact:
            qs = Location.objects.filter(region__slug=region_slug)
        else:
            qs = Location.objects.filter(region__slug__contains=region_slug)
        return qs

    def locations_near_postcode(self, postcode, miles):
        outward = self._get_postcode_outward(postcode)
        try:
            location = Location.objects.filter(postcode=outward)[0]
        except Location.DoesNotExist:
            return None
        return self.locations_near(location, miles)

    def locations_near(self, location, miles):
        start_lat = self._add_miles_to_latitude(location.latitude, 0 - miles)
        end_lat = self._add_miles_to_latitude(location.latitude, miles)
        start_lng = self._add_miles_to_longitude(
            location.longitude, start_lat, 0 - miles
        )
        end_lng = self._add_miles_to_longitude(
            location.longitude, end_lat, miles
        )
        qs = Location.objects.filter(
            latitude__range=(start_lat, end_lat),
            longitude__range=(start_lng, end_lng),
        )
        return qs

    def nearest_location(self, lat_str, lng_str, fuzz_amount=0.1):
        location = None
        save_prec = getcontext().prec
        getcontext().prec = 7
        lat = Decimal(lat_str)
        lng = Decimal(lng_str)
        fuzz_amount = Decimal(fuzz_amount)
        start_lat = lat - fuzz_amount
        start_lng = lng - fuzz_amount
        end_lng = lng + fuzz_amount
        end_lat = lat + fuzz_amount
        qs = Location.objects.filter(
            latitude__range=(start_lat, end_lat),
            longitude__range=(start_lng, end_lng),
        )
        if qs.count() > 0:
            loc_diff = 1000
            for l in qs:
                diff = abs(lat - l.latitude) * abs(lng - l.longitude)
                if diff < loc_diff:
                    location = l
                    loc_diff = diff
        elif qs.count() == 1:
            location = qs[0]
        else:
            self.nearest_location(
                lat_str, lng_str, (fuzz_amount + Decimal(0.1))
            )
        getcontext().prec = save_prec
        return location


class Location(models.Model):
    postcode = models.CharField(max_length=10)
    eastings = models.IntegerField()
    northings = models.IntegerField()
    latitude = models.DecimalField(max_digits=7, decimal_places=5)
    longitude = models.DecimalField(max_digits=7, decimal_places=5)
    town = models.CharField(max_length=200)
    region = models.ForeignKey(
        Region, blank=True, null=True, on_delete=models.CASCADE
    )
    country = models.ForeignKey(
        Country, blank=True, null=True, on_delete=models.CASCADE
    )
    notes = models.CharField(max_length=200, blank=True)
    objects = LocationManager()

    class Meta:
        ordering = ("town", "postcode")
        verbose_name = "Location"
        verbose_name_plural = "Locations"

    def __str__(self):
        return "{} ({})".format(self.town, self.postcode)

    @property
    def form_description(self):
        return "{} ({}{})".format(
            self.postcode,
            self.town,
            (", " + self.region.name) if self.region.name != self.town else "",
        )


reversion.register(Location)


class BusinessType(models.Model):
    slug = models.SlugField(max_length=20)
    title = models.CharField(max_length=50)

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        ordering = ("slug",)
        verbose_name = "Business Type"
        verbose_name_plural = "Business Types"


reversion.register(BusinessType)


class RecruiterManager(models.Manager):
    def pks_with_email(self, email, object_pks):
        """Find the IDs of recruiters matching the email address.

        Keyword arguments:
        email -- find recruiters matching this email address
        object_pks -- only include recruiters from ``object_pks``

        """
        return (
            self.model.objects.exclude(contact__deleted=True)
            .filter(contact__user__email__iexact=email)
            .filter(pk__in=object_pks)
            .values_list("pk", flat=True)
        )


class Recruiter(TimeStampedModel):
    GDPR_DATA_SLUG = "job-app-recruiter-data"
    GDPR_NEWSLETTER_SLUG = "job-app-recruiter-newsletter"
    MAIL_TEMPLATE_JOB_SUBSCRIPTION = "job_subscription"
    MAIL_TEMPLATE_JOB_SUBSCRIPTION_INVOICE = "job_subscription_invoice"
    RECRUITER_WELCOME = "recruiter_welcome_to_jobs_board"

    contact = models.OneToOneField(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    business_type = models.ForeignKey(
        BusinessType, blank=True, null=True, on_delete=models.CASCADE
    )
    picture = models.ImageField(upload_to="job/recruiter/images", blank=True)
    profile = models.TextField(blank=True)
    profile_file_name = models.FileField(blank=True, null=True)
    subscription_expires = models.DateField(blank=True, null=True)
    search_expires = models.DateField(blank=True, null=True)
    notify_application = models.BooleanField(default=False)
    objects = RecruiterManager()

    class Meta:
        ordering = ("contact__company_name",)
        verbose_name = "Recruiter"
        verbose_name_plural = "Recruiters"

    def __str__(self):
        if self.contact.company_name:
            return "{}".format(self.contact.company_name)
        else:
            return "{}, {}".format(
                self.contact.user.last_name, self.contact.user.first_name
            )

    def get_absolute_url(self):
        return reverse("contact.detail", args=[self.contact.pk])

    def _candidate_recruiter_search(self, candidate):
        """Has the candidate given consent for recruiter search?"""
        consent = Consent.objects.get(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
        return candidate.consent_given(consent)

    @property
    def _product_job_post_subscription(self):
        return Product.objects.get(slug=PRODUCT_JOB_POST_SUBSCRIPTION)

    def can_view_candidate(self, candidate):
        """Can the recruiter view the candidate information?

        Can view candidate if the candidate has applied for a job with
        recruiter or candidate allows search and recruiter is searching
        within the search expires period

        """
        # candidate applied for job
        can_view = bool(
            self.job_set.filter(jobapplication__candidate=candidate).count()
        )
        if not can_view:
            # allowed to search and candidate allows recruiters to search
            can_view = self.can_search() and self._candidate_recruiter_search(
                candidate
            )
        return can_view

    def can_download_cv(self, cv):
        """Can download if can view the candidate"""
        if cv.contact.is_candidate:
            return self.can_view_candidate(cv.contact.candidate)

        return False

    @property
    def checkout_actions(self):
        return [CheckoutAction.INVOICE, CheckoutAction.PAYMENT]

    @property
    def checkout_can_charge(self):
        return not self.has_subscription

    @property
    def checkout_description(self):
        product = self._product_job_post_subscription
        return [
            "{} x {} @ {:.2f} + {:.2f} VAT".format(
                Decimal("1"), product.name, product.price, vat(product.price)
            )
        ]

    @property
    def checkout_email(self):
        return self.contact.user.email

    def checkout_fail(self, checkout):
        """I don't think I need to do anything here."""
        pass

    def checkout_fail_url(self, checkout_pk):
        return reverse("recruitment.checkout.fail")

    @property
    def checkout_name(self):
        return self.full_name

    def checkout_success(self, checkout):
        if checkout.action.slug == CheckoutAction.PAYMENT:
            self.subscription_expires = date.today() + relativedelta(years=1)
            self.save()
        self.email_contact(checkout)

    def email_contact(self, checkout):
        context = dict(
            description=checkout.description,
            name=self.checkout_first_name.title(),
            total="£{:.2f}".format(checkout.total),
        )
        if checkout.action.slug == CheckoutAction.INVOICE:
            context.update(dict(invoice=", ".join(checkout.invoice_data)))
            template_name = self.MAIL_TEMPLATE_JOB_SUBSCRIPTION_INVOICE
        else:
            template_name = self.MAIL_TEMPLATE_JOB_SUBSCRIPTION
        queue_mail_template(self, template_name, {self.checkout_email: context})

    def checkout_success_url(self, checkout_pk):
        return reverse("recruitment.checkout.success", args=[checkout_pk])

    @property
    def checkout_total(self):
        product = self._product_job_post_subscription
        return total_with_vat(product.price)

    @property
    def has_subscription(self):
        return (
            self.subscription_expires
            and self.subscription_expires >= date.today()
        )

    @property
    def available_credits(self):
        credits = self.jobcredit_set.aggregate(models.Sum("available_credits"))
        return credits["available_credits__sum"]

    @property
    def can_publish_job(self):
        job_settings = JobSettings.load()
        return (
            not job_settings.job_post_chargeable
            or self.has_subscription
            or bool(self.available_credits)
        )

    @property
    def is_feed(self):
        try:
            if self.jobfeed:
                return True
        except JobFeed.DoesNotExist:
            pass
        return False

    @property
    def full_name(self):
        if self.contact.company_name:
            return self.contact.company_name
        else:
            return self.contact.full_name

    def update_search_expires(self, today):
        job_settings = JobSettings.load()
        new_search_expires = (
            today + relativedelta(days=job_settings.job_post_allows_days_search)
        ).date()
        if not self.search_expires or new_search_expires > self.search_expires:
            self.search_expires = new_search_expires
            self.save()

    def spend_job_credit(self):
        credit = self.jobcredit_set.filter(available_credits__gt=0).first()
        if credit:
            credit.available_credits -= 1
            credit.save()
        return credit

    def can_search(self):
        return bool(self.search_expires and self.search_expires >= date.today())

    def current_jobs(
        self,
        include_closed=False,
        include_unpublished=False,
        include_deleted=False,
    ):
        if include_deleted:
            qs = self.job_set.all()
        else:
            qs = self.job_set.exclude(deleted=True)

        if not include_closed:
            qs = qs.exclude(close_date__lt=date.today())
        if not include_unpublished:
            qs = qs.exclude(publish=False)
        return qs

    def all_current_jobs(self):
        return self.current_jobs(include_closed=True, include_unpublished=True)

    def jobs_pending(self):
        return self.job_set.filter(publish=False).exclude(deleted=True)

    def jobs_published(self):
        return (
            self.job_set.filter(publish=True)
            .exclude(deleted=True)
            .order_by("-publish_date")
        )

    def send_welcome_email(self):
        email_to = self.contact.user.email
        context = {
            email_to: {
                "name": self.contact.user.first_name.title(),
                "username": self.contact.user.username,
                "register_date": self.created.strftime("%d/%m/%Y %H:%M"),
            }
        }
        queue_mail_template(self, self.RECRUITER_WELCOME, context)
        transaction.on_commit(lambda: process_mail.send())


reversion.register(Recruiter)


class PracticeAreaParser(object):
    practice_areas = None
    default_practice_area = None

    def __init__(self):
        self.practice_areas = PracticeArea.objects.all()
        for practice_area in self.practice_areas:
            if practice_area.default:
                self.default_practice_area = practice_area
                break

    def parse(self, text):
        if text:
            lower_text = text.lower()
        else:
            lower_text = ""

        found_practice_areas = []
        for practice_area in self.practice_areas:
            for keyword in practice_area.practiceareakeyword_set.all():
                if keyword.word.lower() in lower_text:
                    found_practice_areas.append(practice_area)
                    break

        if not found_practice_areas and self.default_practice_area:
            found_practice_areas.append(self.default_practice_area)

        return found_practice_areas


class PracticeAreaManager(models.Manager):
    def all_practice_areas(self):
        practice_areas = []
        for practice_area in self.model.objects.all():
            practice_areas.append(
                {
                    "name": practice_area.title,
                    "keywords": [
                        k.word
                        for k in practice_area.practiceareakeyword_set.all()
                    ],
                    "default": practice_area.default,
                }
            )
        return practice_areas

    def parser(self):
        return PracticeAreaParser()


class PracticeArea(models.Model):
    title = models.CharField(max_length=50)
    order = models.PositiveIntegerField(default=1)
    default = models.BooleanField(default=False)
    objects = PracticeAreaManager()

    class Meta:
        ordering = ("order", "title")
        verbose_name = "Practice Area"
        verbose_name_plural = "Practice Areas"

    def __str__(self):
        return "{}".format(self.title)


reversion.register(PracticeArea)


class PracticeAreaKeyword(models.Model):
    """Practice area keywords.

    For a legal jobs board, example ``word`` are as follows:

    - accounts
    - banking
    - commercial
    - corporate
    - criminal
    - employ
    - family
    - matrimonial

    """

    practice_area = models.ForeignKey(PracticeArea, on_delete=models.CASCADE)
    word = models.CharField(max_length=50)

    class Meta:
        ordering = ("practice_area", "word")
        verbose_name = "Practice Area Keyword"
        verbose_name_plural = "Practice Area Keywords"

    def __str__(self):
        return "{} {}".format(self.practice_area, self.word)


reversion.register(PracticeAreaKeyword)


class LandingPageManager(models.Manager):
    def create_page(self, name):
        slug_menu = slugify(name)
        job_settings = JobSettings.load()
        template = job_settings.landing_page_template
        if not template:
            raise JobError("No landing page template in job settings.")
        slug_page = job_settings.landing_page_slug
        if not slug_page:
            raise JobError("No landing page slug in job settings.")
        page = Page.objects.init_page(
            slug_page=slug_page,
            slug_menu=slug_menu,
            name=name,
            order=0,
            template=template,
        )
        page.refresh_sections_from_template()
        return page

    def landing_pages(self):
        return self.model.objects.exclude(page__deleted=True)


class LandingPage(models.Model):
    """Landing page for a list of jobs generated from search parameters.

    Job search fields copied form ``JobElasticSearchForm``

    """

    page = models.OneToOneField(Page, on_delete=models.CASCADE)
    location = models.CharField(
        max_length=30, blank=True, help_text="Location e.g. Exeter"
    )
    job_role = models.CharField(
        max_length=30, blank=True, help_text="Job Role e.g. Legal Secretary"
    )
    practice_area = models.CharField(
        max_length=30, blank=True, help_text="Practice Area e.g. Conveyancing"
    )
    objects = LandingPageManager()

    def clean(self):
        if not (self.location or self.job_role or self.practice_area):
            raise ValidationError(
                "A landing page must have a location, job role or "
                "practice area "
            )

    def __str__(self):
        return "{}/{}".format(self.page.slug, self.page.slug_menu)

    class Meta:
        verbose_name = "Landing Page"
        verbose_name_plural = "Landing Pages"


reversion.register(LandingPage)


class JobCreditManager(models.Manager):
    def pks_with_email(self, email, object_pks):
        """Find the IDs of job credits matching the email address.

        Keyword arguments:
        email -- find job credits matching this email address
        object_pks -- only include job credits from ``object_pks``

        """
        return (
            self.model.objects.exclude(recruiter__contact__deleted=True)
            .filter(recruiter__contact__user__email__iexact=email)
            .filter(pk__in=object_pks)
            .values_list("pk", flat=True)
        )


class JobCredit(TimeStampedModel):
    """JobCredit - record credits purchased by recruiter.

    - credits - the number of job posting purchased (or being purchased)
    - available credits - the number of job postings paid for and not yet used
    - product - the product purchased -
    """

    MAIL_TEMPLATE_JOB_CREDITS = "job_credits"
    CREDIT_CHOICES = (
        (1, "1 Job"),
        (2, "2 Jobs"),
        (3, "3 Jobs"),
        (4, "4 Jobs"),
        (5, "5 Jobs"),
        (6, "6 Jobs"),
        (7, "7 Jobs"),
        (8, "8 Jobs"),
        (9, "9 Jobs"),
        (10, "10 Jobs"),
    )
    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE)
    checkout_state = models.ForeignKey(
        CheckoutState, default=default_checkout_state, on_delete=models.CASCADE
    )
    credits = models.PositiveIntegerField(default=0, choices=CREDIT_CHOICES)
    available_credits = models.IntegerField(default=0)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    paid = models.BooleanField(default=False)
    objects = JobCreditManager()

    class Meta:
        verbose_name = "Job Credit"
        verbose_name_plural = "Job Credits"

    def __str__(self):
        return "id: {} {} job credit{} for {} checkout '{}'".format(
            self.pk,
            self.credits,
            "s" if self.credits > 1 else "",
            self.recruiter.full_name,
            self.checkout_state.slug,
        )

    def get_absolute_url(self):
        return reverse("job.checkout.credit.detail", kwargs=dict(pk=self.pk))

    @property
    def checkout(self):
        try:
            return Checkout.objects.for_content_object(self)
        except Checkout.DoesNotExist:
            return None

    @property
    def checkout_actions(self):
        return [CheckoutAction.INVOICE, CheckoutAction.PAYMENT]

    @property
    def checkout_can_charge(self):
        """Check we can take the payment."""
        return self.credits and self.checkout_state.slug in (
            CheckoutState.FAIL,
            CheckoutState.PENDING,
        )

    @property
    def checkout_description(self):
        if self.credits > 0 and self.product:
            return [
                "{} x {} @ {} + VAT".format(
                    self.credits, self.product.name, self.product.price
                )
            ]
        else:
            # TODO - use correct error here
            raise Exception("Invalid number of credits")
        # build the description

    @property
    def checkout_email(self):
        return self.recruiter.contact.user.email

    def checkout_fail(self, checkout):
        """Update the object to record the payment failure.

        Called from within a transaction so you can update the model.

        """
        self.checkout_state = CheckoutState.objects.fail
        self.save()

    def checkout_fail_url(self, checkout_pk):
        return reverse("recruitment.checkout.fail")

    @property
    def checkout_name(self):
        return self.recruiter.full_name

    def mark_paid(self):
        self.available_credits = self.credits
        self.paid = True

    def checkout_success(self, checkout):
        """Update the object to record a successful payment.

        Called from within a transaction so you can update the model.

        """
        if checkout.action.slug == CheckoutAction.PAYMENT:
            self.mark_paid()
        self.checkout_state = checkout.state
        self.save()
        self.email_contact(checkout)

    def email_contact(self, checkout):
        context = dict(
            description=checkout.description,
            name=self.checkout_first_name.title(),
            total="£{:.2f}".format(checkout.total),
        )
        queue_mail_template(
            self, self.MAIL_TEMPLATE_JOB_CREDITS, {self.checkout_email: context}
        )

    def checkout_success_url(self, checkout_pk):
        return reverse("recruitment.checkout.success", args=[checkout_pk])

    @property
    def checkout_total(self):
        total = Decimal(self.credits * self.product.price)
        return total_with_vat(total)

    def allow_pay_later(self):
        return False

    @property
    def mail_template_name(self):
        return RECRUITER_PAYMENT_THANKYOU


reversion.register(JobCredit)


class JobRoleParser:
    job_roles = None
    default_job_role = None

    def __init__(self):
        self.job_roles = JobRole.objects.all()

        for job_role in self.job_roles:
            if job_role.default:
                self.default_job_role = job_role
                break

    def parse(self, text):
        if text:
            lower_text = text.lower()
        else:
            lower_text = ""
        # append / prepend a space, so we can find ' pa '
        lower_text = " {} ".format(lower_text)

        found_job_roles = []
        for job_role in self.job_roles:
            for keyword in job_role.jobrolekeyword_set.all():
                if keyword.word.lower() in lower_text:
                    found_job_roles.append(job_role)
                    break

        if not found_job_roles and self.default_job_role:
            found_job_roles.append(self.default_job_role)

        return found_job_roles

    def data(self):
        """Put the information into a 'dict' for debug."""
        result = {"default_job_role": self.default_job_role}
        for job_role in self.job_roles:
            keywords = []
            for keyword in job_role.jobrolekeyword_set.all():
                keywords.append(keyword.word.lower())
            result[job_role.title] = keywords
        return result


class JobRoleManager(models.Manager):
    def all_job_roles(self):
        job_roles = []
        for job_role in self.model.objects.all():
            job_roles.append(
                {
                    "name": job_role.title,
                    "keywords": [
                        k.word for k in job_role.jobrolekeyword_set.all()
                    ],
                    "default": job_role.default,
                }
            )
        return job_roles

    def parser(self):
        return JobRoleParser()


class JobRole(models.Model):
    """Job roles.

    For a legal jobs board, example ``title`` are as follows:

    - Legal Administrator
    - Legal PA
    - Legal Secretary

    """

    title = models.CharField(max_length=50)
    order = models.PositiveIntegerField(default=1)
    default = models.BooleanField(default=False)
    objects = JobRoleManager()

    class Meta:
        ordering = ("order", "title")
        verbose_name = "Job Role"
        verbose_name_plural = "Job Roles"

    def __str__(self):
        return "{}".format(self.title)


reversion.register(JobRole)


class JobRoleKeyword(models.Model):
    """Job role keywords.

    For a legal jobs board, example ``word`` are as follows:

    - assistant
    - paralegal
    - secret
    - typing
    - executive

    """

    job_role = models.ForeignKey(JobRole, on_delete=models.CASCADE)
    word = models.CharField(max_length=50)

    class Meta:
        ordering = ("job_role", "word")
        verbose_name = "Job Role Keyword"
        verbose_name_plural = "Job Role Keywords"

    def __str__(self):
        return "{} {}".format(self.job_role, self.word)


reversion.register(JobRoleKeyword)


class JobManager(models.Manager):
    def create_job(self, recruiter, close_date, title, salary):
        obj = self.model(
            recruiter=recruiter,
            close_date=close_date,
            salary=salary,
            title=title,
            order_key=ILSPA_RECRUITER,
        )
        obj.save()
        return obj

    def current(self, include_closed=None, include_unpublished=None):
        """Return all current jobs."""
        qs = self.model.objects.exclude(deleted=True)
        if not include_closed:
            qs = qs.exclude(close_date__lt=timezone.now().date()).exclude(
                publish_date__lt=self.cut_off_date()
            )
        if not include_unpublished:
            qs = qs.exclude(publish=False)
        return qs

    def current_all(self):
        return self.current(include_closed=True, include_unpublished=True)

    def current_our(self):
        """List of *our* jobs."""
        return self.current().exclude(order_key=FEED_RECRUITER)

    def cut_off_date(self):
        job_settings = JobSettings.load()
        return pytz.timezone(timezone.get_default_timezone_name()).localize(
            datetime.combine(date.today(), datetime.min.time())
            - timedelta(days=job_settings.job_post_max_days_displayed)
        )

    def latest(self):
        """Latest jobs.

        - Select all ILSPA jobs.
        - Select jobs from a feed for the last 7 days only.

        """
        target_date = timezone.now() + relativedelta(days=-7)
        return (
            self.model.objects.current()
            .filter(
                models.Q(order_key=ILSPA_RECRUITER)
                | models.Q(
                    models.Q(order_key=FEED_RECRUITER)
                    & models.Q(publish_date__gt=target_date)
                )
            )
            .order_by("order_key", "-publish_date")[:200]
        )

    def search(
        self,
        region,
        postcode,
        postcode_range,
        title,
        recruiter,
        include_unpublished=False,
    ):
        """Search for jobs.

        For the recruiter field, attempt to use the same fields as
        'recruiter_name' (see the 'Job' model).  The fields are::

          if self.recruiter.is_feed:
              if self.jobfeeditem.company:
                  return self.jobfeeditem.company           # option 1
              elif self.jobfeeditem.source:
                  return self.jobfeeditem.source            # option 2
          return self.recruiter.full_name                   # option 3

        """
        qs = Job.objects.current(include_unpublished=include_unpublished)

        if recruiter:
            qs = qs.filter(
                # option 1
                models.Q(jobfeeditem__company__icontains=recruiter)
                |
                # option 2
                models.Q(
                    jobfeeditem__company="",
                    jobfeeditem__source__icontains=recruiter,
                )
                |
                # option 3
                models.Q(
                    jobfeeditem__isnull=True,
                    recruiter__contact__company_name__icontains=recruiter,
                )
            )
        if title:
            qs = qs.filter(title__icontains=title)
        # locations
        if postcode:
            locations = Location.objects.locations_near_postcode(
                postcode, postcode_range
            )
        elif region:
            locations = Location.objects.filter(region=region)
        else:
            locations = Location.objects.none()
        if locations:
            qs = qs.filter(location__in=locations)
        return qs.order_by("order_key", "-publish_date")


class Job(TimedCreateModifyDeleteModel):
    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, blank=True)
    salary = models.CharField(max_length=200, blank=True, null=True)
    close_date = models.DateField(help_text="(dd/mm/yyyy)")
    location = models.ForeignKey(
        Location, blank=True, null=True, on_delete=models.CASCADE
    )
    location_text = models.CharField(max_length=200, verbose_name="Location")
    description = models.TextField(blank=True)
    picture = models.ImageField(upload_to="job/images", blank=True)
    applications_email = models.EmailField(
        blank=True, null=True, verbose_name="Application Email address"
    )
    appointed_date = models.DateField(blank=True, null=True)
    appointed_via_board = models.BooleanField(default=False)
    # closed = models.BooleanField(default=False)
    # order_key 1 = Job from ILSPA Recruiter 2 = Job from Feed
    # used primarily to raise the ILSPA jobs to the top of the list
    # has also been used for queries for the source of jobs
    order_key = models.PositiveIntegerField(default=0)
    practice_area = models.ForeignKey(
        PracticeArea, blank=True, null=True, on_delete=models.CASCADE
    )
    job_role = models.ForeignKey(
        JobRole, blank=True, null=True, on_delete=models.CASCADE
    )
    job_credit = models.ForeignKey(
        JobCredit, blank=True, null=True, on_delete=models.CASCADE
    )
    publish = models.BooleanField(default=False)
    publish_date = models.DateTimeField(blank=True, null=True)
    # deleted = models.BooleanField(default=False)
    objects = JobManager()

    class Meta:
        ordering = ("order_key", "title")
        verbose_name = "Job"
        verbose_name_plural = "Jobs"

    def __str__(self):
        """Used by the sitemap for the job detail page."""
        return "{} - {} - {}".format(
            self.recruiter_name, self.title, self.location_name
        )

    def applications(self, user):
        """Return all the job applications that 'user' can see.
        Very similar to the 'tracking' method (see below).
        """

        if user.is_staff:
            qs = self.jobapplication_set.all()
        elif user.is_anonymous:
            # users not logged in should not see any application
            qs = self.jobapplication_set.none()
        elif user.contact.is_recruiter and self.recruiter.contact.user == user:
            qs = self.jobapplication_set.exclude(apply_date=None)
        elif user.contact.is_candidate:
            # will return a maximum of 1 row
            qs = self.jobapplication_set.filter(candidate__contact__user=user)
        else:
            qs = self.jobapplication_set.none()

        return qs.order_by("-apply_date")

    def application_stats(self):
        target_date = datetime.combine(
            datetime.now().date(), datetime.min.time()
        ) - timedelta(days=7)
        applications = self.jobapplication_set.exclude(apply_date=None)
        total = applications.count()
        this_week = applications.exclude(apply_date__lt=target_date).count()
        return dict(total=total, this_week=this_week)

    def as_html(self):
        """Job as HTML text for using in a Mandrill template."""
        absolute_url = self.get_absolute_url()
        job_url = urllib.parse.urljoin(settings.HOST_NAME, absolute_url)
        return (
            "<strong>{}</strong><br />"
            "Salary: {}<br />"
            "Location: {}<br />"
            "Closing Date: {}<br />"
            '<a href="{}">Click here to view and apply...</a><br />'
        ).format(
            self.title,
            self.salary,
            self.location,
            self.close_date.strftime("%A %-d %B %Y"),
            job_url,
        )

    def get_absolute_url(self):
        return reverse(
            "recruitment.candidate.job.detail", kwargs=dict(pk=self.pk)
        )

    def schema_data(self):
        """Job schema data.

        Designed to be used with the ``json_script_ld`` template tag e.g::

          {% load listjobs %}
          {{ object.schema_data|json_script_ld:"hello-data" }}

        Safely Including Data for JavaScript in a Django Template
        https://adamj.eu/tech/2020/02/18/safely-including-data-for-javascript-in-a-django-template/

        """
        date_posted = salary = ""
        if self.publish_date:
            date_posted = self.publish_date.strftime("%Y-%m-%d")
        if self.salary:
            salary = "Salary {}, ".format(self.salary)
        data = {
            "@context": "http://schema.org/",
            "@type": "JobPosting",
            "title": self.title,
            "description": "{}{}".format(salary, self.description),
            "identifier": {
                "@type": "PropertyValue",
                "name": self.recruiter.contact.full_name,
                "value": self.pk,
            },
            "datePosted": date_posted,
            "validThrough": "{}T23:59".format(
                self.close_date.strftime("%Y-%m-%d")
            ),
            "employmentType": "FULL_TIME",
            "industry": "Legal",
            "hiringOrganization": {
                "@type": "Organization",
                "name": self.recruiter_name,
            },
            "jobLocation": {
                "@type": "Place",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "",
                    "addressLocality": self.location_text,
                    "addressRegion": self.location.region.name,
                    "postalCode": self.location.postcode,
                    "addressCountry": self.location.country.name,
                },
            },
        }
        if self.recruiter.contact.website:
            data["hiringOrganization"][
                "sameAs"
            ] = self.recruiter.contact.website
        return data

    def tracking_stats(self, calendar_month=False):
        # target date is
        if calendar_month:
            # monthly
            current_start_date = datetime.combine(
                datetime.now().date(), datetime.min.time()
            )
            current_start_date = current_start_date.replace(day=1)
            last_start_date = current_start_date - relativedelta(months=1)
        else:
            current_start_date = datetime.combine(
                datetime.now().date(), datetime.min.time()
            ) - timedelta(days=7)
            last_start_date = datetime.combine(
                datetime.now().date(), datetime.min.time()
            ) - timedelta(days=13)
        tracks = self.jobtracking_set.all()
        all = tracks.aggregate(
            view_total=models.Sum("view_count"),
            apply_total=models.Sum("application_click_count"),
        )
        last_period = (
            tracks.exclude(click_date__lt=last_start_date)
            .exclude(click_date__gte=current_start_date)
            .aggregate(
                view_total=models.Sum("view_count"),
                apply_total=models.Sum("application_click_count"),
            )
        )
        current_period = tracks.exclude(
            click_date__lt=current_start_date
        ).aggregate(
            view_total=models.Sum("view_count"),
            apply_total=models.Sum("application_click_count"),
        )
        return dict(
            units="month" if calendar_month else "week",
            views=all["view_total"] or 0,
            applications=all["apply_total"] or 0,
            views_last=last_period["view_total"] or 0,
            applications_last=last_period["apply_total"] or 0,
            views_current=current_period["view_total"] or 0,
            applications_current=current_period["apply_total"] or 0,
        )

    @property
    def recruiter_name(self):
        if self.is_from_feed:
            # a feed item should have a company or a source
            if self.jobfeeditem.company:
                return self.jobfeeditem.company
            elif self.jobfeeditem.source:
                return self.jobfeeditem.source
        # not a feed or feed does not contain source or company
        return self.recruiter.full_name

    @property
    def recruiter_address(self):
        address = ""

        if not self.is_from_feed:
            if self.recruiter.contact.address_1:
                if address:
                    address = address + ", " + self.recruiter.contact.address_1
                else:
                    address = self.recruiter.contact.address_1

            if self.recruiter.contact.address_2:
                if address:
                    address = address + ", " + self.recruiter.contact.address_2
                else:
                    address = self.recruiter.contact.address_2

            if self.recruiter.contact.address_3:
                if address:
                    address = address + ", " + self.recruiter.contact.address_3
                else:
                    address = self.recruiter.contact.address_3

            if self.recruiter.contact.town:
                if address:
                    address = address + ", " + self.recruiter.contact.town
                else:
                    address = self.recruiter.contact.town

            if self.recruiter.contact.county:
                if address:
                    address = address + ", " + self.recruiter.contact.county
                else:
                    address = self.recruiter.contact.county

            if self.recruiter.contact.postcode:
                if address:
                    address = address + ", " + self.recruiter.contact.postcode
                else:
                    address = self.recruiter.contact.postcode

            if self.recruiter.contact.country:
                if address:
                    address = address + ", " + self.recruiter.contact.country
                else:
                    address = self.recruiter.contact.country
        return address

    @property
    def location_name(self):
        if self.location_text:
            return self.location_text
        if self.is_from_feed:
            # a feed item should have a city
            if self.jobfeeditem.city:
                loc_name = self.jobfeeditem.city
                if self.location and (
                    self.jobfeeditem.city.lower() != self.location.town.lower()
                ):
                    loc_name = "{} ({})".format(loc_name, self.location.town)
                return loc_name

        # not a feed or feed does not contain city
        if self.location:
            return self.location.town
        return ""

    @property
    def is_closed(self):
        return self.close_date and self.close_date < timezone.now().date()

    @property
    def is_current(self):
        return not self.is_closed

    @property
    def is_from_feed(self):
        is_a_feed_job = False
        try:
            if self.jobfeeditem:
                is_a_feed_job = True
        except JobFeedItem.DoesNotExist:
            pass
        return is_a_feed_job

    @property
    def is_paid(self):
        qs = self.jobbasketitem_set.filter(
            basket__checkout_state__slug=CheckoutState.SUCCESS
        )
        return bool(qs.count() > 0)

    def set_deleted(self, user):
        super().set_deleted(user)
        from job.tasks import update_job_index

        transaction.on_commit(
            lambda: update_job_index.send_with_options(args=(self.pk,))
        )

    def undelete(self):
        super().undelete()
        from job.tasks import update_job_index

        transaction.on_commit(
            lambda: update_job_index.send_with_options(args=(self.pk,))
        )

    def publish_job(self, user):
        job_settings = JobSettings.load()
        if (
            not job_settings.job_post_chargeable
            or (user.is_staff and self.recruiter.contact.user != user)
            or user.contact.recruiter.has_subscription
        ):
            can_publish = True
            credit = None
        else:
            credit = self.recruiter.spend_job_credit()
            can_publish = bool(credit)
        if can_publish:
            self.publish = True
            self.job_credit = credit
            self.publish_date = timezone.now()
            self.recruiter.update_search_expires(self.publish_date)
            self.save()
            from job.tasks import update_job_index

            transaction.on_commit(
                lambda: update_job_index.send_with_options(args=(self.pk,))
            )
        return can_publish

    def tracking(self, user):
        """Return all the tracking information for jobs that 'user' can see.
        Very similar to the 'applications' method (see above).
        """
        if user.is_staff:
            qs = self.jobtracking_set.order_by("-click_date")
        else:
            qs = self.jobtracking_set.none()
        return qs


reversion.register(Job)


class JobFeedManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)


class JobFeed(TimedCreateModifyDeleteModel):
    """Job feed recruiter and URL.

    The Simply Law Jobs feed needs a ``username``, ``password``
    and ``identifier``.

    """

    LEGISTS = "legists"
    SIMPLY_LAW = "simply-law"

    slug = models.SlugField(max_length=100)
    recruiter = models.OneToOneField(
        Recruiter, blank=True, null=True, on_delete=models.CASCADE
    )
    # url is a text field as it may contain placeholders
    url = models.TextField(max_length=512, blank=True, null=True)
    username = models.CharField(max_length=50, blank=True)
    password = models.CharField(max_length=50, blank=True)
    identifier = models.CharField(max_length=100, blank=True)
    objects = JobFeedManager()

    class Meta:
        ordering = ("recruiter__contact__company_name",)
        verbose_name = "Job Feed"
        verbose_name_plural = "Job Feeds"

    def __str__(self):
        result = "('{}')".format(self.slug)
        if self.recruiter:
            return "{} {}".format(self.recruiter.full_name, result)
        return result

    def download_ftp(self, file_name):
        result = None
        if file_name and self.url and self.username and self.password:
            with ftplib.FTP(
                self.url, user=self.username, passwd=self.password
            ) as ftp:
                with tempfile.NamedTemporaryFile("wb", delete=False) as f:
                    # with open(file_path_name, "wb") as f:
                    ftp.retrbinary("RETR " + file_name, f.write)
                result = f.name
        else:
            raise JobError(
                "FTP download for the '{}' job feed needs a file name, "
                "URL, user name and password".format(self.slug)
            )
        return result

    def jobs(self):
        """List of published jobs for a feed."""
        return Job.objects.filter(recruiter=self.recruiter).exclude(
            publish_date=None
        )

    def stats(self):
        """Some simple stats for a job feed."""
        today = timezone.now()
        twenty_four_hours = today + relativedelta(days=-1)
        week = twenty_four_hours + relativedelta(weeks=-1)
        qs = Job.objects.filter(recruiter=self.recruiter)
        return {
            "24 hours": qs.filter(created__gte=twenty_four_hours).count(),
            "week": qs.filter(
                created__gt=week, created__lt=twenty_four_hours
            ).count(),
        }


reversion.register(JobFeed)


class JobFeedItemManager(models.Manager):
    def is_new_job_key(self, feed, jobkey):
        """Is this a new job key for the feed?"""
        qs = JobFeedItem.objects.filter(feed=feed, jobkey=jobkey)
        return not qs.exists()


class JobFeedItem(models.Model):
    feed = models.ForeignKey(
        JobFeed, blank=True, null=True, on_delete=models.CASCADE
    )
    job = models.OneToOneField(
        Job, blank=True, null=True, on_delete=models.CASCADE
    )
    jobkey = models.CharField(max_length=100)
    company = models.CharField(max_length=100, blank=True)
    source = models.CharField(max_length=100, blank=True)
    url = models.URLField(max_length=1000)
    formattedLocation = models.CharField(max_length=100, blank=True)
    formattedLocationFull = models.CharField(max_length=100, blank=True)
    date = models.DateTimeField(blank=True, null=True)
    city = models.CharField(max_length=100, blank=True)
    state = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True)
    # temporary for testing
    latitude = models.DecimalField(
        max_digits=7, decimal_places=5, blank=True, null=True
    )
    longitude = models.DecimalField(
        max_digits=7, decimal_places=5, blank=True, null=True
    )
    objects = JobFeedItemManager()

    class Meta:
        ordering = ("job__title",)
        verbose_name = "Job Feed Item"
        verbose_name_plural = "Job Feed Items"

    def __str__(self):
        result = self.jobkey
        if self.feed and self.feed.recruiter:
            result = "{}: {}".format(str(self.feed.recruiter), result)
        if self.job:
            result = "{} ({})".format(result, self.job.title)
        return result


reversion.register(JobFeedItem)


class JobBasketManager(models.Manager):
    def pks_with_email(self, email, object_pks):
        """Find the IDs of job baskets matching the email address.

        Keyword arguments:
        email -- find job baskets matching this email address
        object_pks -- only include job baskets from ``object_pks``

        """
        return (
            self.model.objects.exclude(recruiter__contact__deleted=True)
            .filter(recruiter__contact__user__email__iexact=email)
            .filter(pk__in=object_pks)
            .values_list("pk", flat=True)
        )


class JobBasket(TimeStampedModel):
    """Job basket - allow a recruiter to pay for multiple jobs.

    This payment may fail, so a job can appear in more than one basket.

    """

    MAIL_TEMPLATE_JOB_PAYMENT = "job_payment"

    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE)
    checkout_state = models.ForeignKey(
        CheckoutState, default=default_checkout_state, on_delete=models.CASCADE
    )
    objects = JobBasketManager()

    class Meta:
        verbose_name = "Job basket"
        verbose_name_plural = "Job baskets"

    def __str__(self):
        return "id: {} checkout '{}' for {} jobs".format(
            self.pk, self.checkout_state.slug, self.jobbasketitem_set.count()
        )

    def get_absolute_url(self):
        return reverse("job.checkout.basket.detail", kwargs=dict(pk=self.pk))

    @property
    def checkout_actions(self):
        return [CheckoutAction.INVOICE, CheckoutAction.PAYMENT]

    @property
    def checkout_can_charge(self):
        """Check we can take the payment."""
        # initial checks
        first_item = self.jobbasketitem_set.first()
        if not first_item:
            raise JobError("No job postings in the basket")
        # check each item in the basket
        for item in self.jobbasketitem_set.all():
            if not self.recruiter == item.job.recruiter:
                raise JobError(
                    "Attached jobs are linked to different recruiters"
                )
            if not item.job:
                raise JobError(
                    "No job attached to the item in the basket: {}".format(
                        item.pk
                    )
                )
            if item.job.is_paid:
                raise JobError(
                    "Job {} has already been paid".format(item.job.pk)
                )
        return self.checkout_state.slug in (
            CheckoutState.FAIL,
            CheckoutState.PENDING,
        )

    @property
    def checkout_description(self):
        result = []
        name = {}
        price = {}
        quantity = {}
        for item in self.jobbasketitem_set.all():
            if item.product.slug in quantity:
                quantity[item.product.slug] = quantity[item.product.slug] + 1
            else:
                name[item.product.slug] = item.product.name
                price[item.product.slug] = item.product.price
                quantity[item.product.slug] = 1
        # build the description
        for slug in sorted(quantity.keys()):
            result.append(
                "{} x {} @ {} + VAT".format(
                    quantity[slug], name[slug], price[slug]
                )
            )
        return result

    @property
    def checkout_email(self):
        return self.recruiter.contact.user.email

    def checkout_fail(self, checkout):
        """Update the object to record the payment failure.

        Called from within a transaction so you can update the model.

        """
        self.checkout_state = CheckoutState.objects.fail
        self.save()

    def checkout_fail_url(self, checkout_pk):
        return reverse("recruitment.checkout.fail")

    @property
    def checkout_name(self):
        return self.recruiter.full_name

    def checkout_success(self, checkout):
        """Update the object to record a successful payment.

        Called from within a transaction so you can update the model.

        """
        if checkout.action.slug == CheckoutAction.PAYMENT:
            for item in self.jobbasketitem_set.all():
                item.job.publish_job(item.job.recruiter.contact.user)
        self.save()
        self.email_contact(checkout)

    def email_contact(self, checkout):
        context = dict(
            description=checkout.description,
            name=self.checkout_first_name.title(),
            total="£{:.2f}".format(checkout.total),
        )
        queue_mail_template(
            self, self.MAIL_TEMPLATE_JOB_PAYMENT, {self.checkout_email: context}
        )

    def checkout_success_url(self, checkout_pk):
        return reverse("recruitment.checkout.success", args=[checkout_pk])

    @property
    def checkout_total(self):
        total = Decimal()
        for item in self.jobbasketitem_set.all():
            total = total + item.product.price
        return total_with_vat(total)

    def allow_pay_later(self):
        return False

    @property
    def mail_template_name(self):
        return RECRUITER_PAYMENT_THANKYOU


reversion.register(JobBasket)


class JobBasketItemManager(models.Manager):
    def create_job_basket_item(self, basket, job, product):
        obj = self.model(basket=basket, job=job, product=product)
        obj.save()
        return obj


class JobBasketItem(models.Model):
    basket = models.ForeignKey(JobBasket, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    objects = JobBasketItemManager()

    class Meta:
        unique_together = ("basket", "job")
        ordering = ("basket", "product", "job")
        verbose_name = "Job basket item"
        verbose_name_plural = "Job basket items"

    def __str__(self):
        return "{} {}: {}".format(self.basket.pk, self.pk, self.product.slug)


reversion.register(JobBasketItem)


class ExperienceManager(models.Manager):
    def init_experience(self, slug, order, title):
        updated = False
        try:
            exp = self.model.objects.get(slug=slug)
        except self.model.DoesNotExist:
            exp = self.model(slug=slug)
            updated = True
        if exp.order != order:
            exp.order = order
            updated = True
        if exp.title != title:
            exp.title = title
            updated = True
        if updated:
            exp.save()

        return exp

    def display(self):
        return self.model.objects.filter(order__lt=1000)


class Experience(models.Model):
    """Candidate Experience."""

    DEFAULT = "default"
    EXPERIENCED = "experienced"
    QUALIFIED = "qualified"
    TRAINEE = "trainee"

    slug = models.SlugField(max_length=20)
    title = models.CharField(max_length=50)
    order = models.PositiveIntegerField()  # only 999 or less is displayed
    objects = ExperienceManager()

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        ordering = ("order", "slug")
        verbose_name = "Experience"


reversion.register(Experience)


class YearsExperience(models.Model):
    slug = models.SlugField(max_length=20)
    title = models.CharField(max_length=50)
    order = models.PositiveIntegerField()

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        ordering = ("slug",)
        verbose_name = "Experience as a legal Secretary"


reversion.register(YearsExperience)


class HighestEducation(models.Model):
    slug = models.SlugField(max_length=20)
    title = models.CharField(max_length=50)
    order = models.PositiveIntegerField()

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        ordering = ("slug",)
        verbose_name = "Highest Education Level attained"


reversion.register(HighestEducation)


class CandidateManager(models.Manager):
    def current(self):
        # Return all current candidates.
        return self.model.objects.exclude(contact__deleted=True)

    def email_latest_jobs(self):
        result = {}
        # candidates
        for candidate in Candidate.objects.current():
            # check GDPR consent
            if candidate.consent_notify_jobs():
                result[candidate.contact.user.email] = candidate.contact.pk
        # check the previous six days to see if an email has already been sent
        previous_reminders = Mail.objects.emails_for_template(
            timezone.now() + relativedelta(days=-6),
            timezone.now(),
            self.model.MAIL_TEMPLATE_NOTIFY_JOBS,
        )
        # remove email addresses which have already been sent a reminder
        for email in previous_reminders:
            result.pop(email, None)
        return result

    def jobs_as_html(self, latest_jobs, total=None):
        """Latest jobs as HTML text (for sending to a Mandrill template)."""
        count = 0
        if total is None:
            total = 3
        result = ""
        for job in latest_jobs:
            count = count + 1
            if result:
                # separate each job with a line break
                result = result + "<br />"
            result = result + job.as_html()
            if count >= total:
                break
        return result

    def registered_between(self, start_date, end_date):
        # Return all current candidates.
        qs = self.model.objects.current()

        if start_date:
            start_time = datetime.combine(start_date, datetime.min.time())
            start_time = timezone.make_aware(start_time)
            qs = qs.exclude(created__lt=start_time)
        if end_date:
            end_time = datetime.combine(end_date, datetime.max.time())
            end_time = timezone.make_aware(end_time)
            qs = qs.exclude(created__gt=end_time)

        return qs

    def search(
        self,
        search_type,
        region,
        postcode,
        postcode_range,
        job_experience,
        last_login,
        order_by,
        is_staff,
    ):
        """Search for candidates
        - two search types:
            - Candidate.POSITION_SEARCH - excludes candidates without a CV
            - Candidate.MAILSHOT_SEARCH - all candidates
        - location search is either by:
            - postcode and a radius (if both specified this will be used)
            - region
        - job_experience will always include candidates that are set to
          the default experience (i.e. slug=Experience.DEFAULT)
        """
        qs = Candidate.objects.current()
        if not is_staff:
            # A search by a member of staff will ignore consent #3309
            consent = Consent.objects.get_consent(
                Candidate.GDPR_RECRUITER_SEARCH_SLUG
            )
            user_pks = UserConsent.objects.user_consent_given(consent)
            qs = qs.filter(contact__user__pk__in=user_pks)
        if search_type == Candidate.POSITION_SEARCH:
            qs = qs.exclude(contact__cv__isnull=True)
        if last_login:
            today = timezone.now()
            target_date = today + relativedelta(months=-last_login)
            qs = qs.filter(contact__user__last_login__gte=target_date)
        if job_experience:
            qs = qs.filter(
                models.Q(job_experience__order=job_experience.order)
                | models.Q(job_experience__slug=Experience.DEFAULT)
            )
        # locations
        if postcode:
            locations = Location.objects.locations_near_postcode(
                postcode, postcode_range
            )
        elif region:
            locations = Location.objects.filter(region=region)
        else:
            locations = Location.objects.none()
        if locations:
            qs = qs.filter(candidatejoblocation__location__in=locations)

        ascending = not order_by or order_by[0] != "-"

        if order_by in ["job_experience", "-job_experience"]:
            order_fields = (order_by + "__order", "id" if ascending else "-id")
            if ascending:
                distinct_fields = order_fields
            else:
                distinct_fields = (order_by[1:] + "__order", "id")
        elif order_by in ["created", "-created"]:
            order_fields = (order_by, "id" if ascending else "-id")
            if ascending:
                distinct_fields = order_fields
            else:
                distinct_fields = (order_by[1:], "id")
        elif order_by in ["last_login", "-last_login"]:
            distinct_fields = ("contact__user__last_login", "id")
            if ascending:
                order_fields = distinct_fields
            else:
                order_fields = ("-contact__user__last_login", "-id")
        elif order_by == "-name":
            order_fields = (
                "-contact__user__last_name",
                "-contact__user__first_name",
                "-id",
            )
            distinct_fields = (
                "contact__user__last_name",
                "contact__user__first_name",
                "id",
            )
        else:
            # default is order by name
            order_fields = (
                "contact__user__last_name",
                "contact__user__first_name",
                "id",
            )
            distinct_fields = order_fields

        if order_fields and distinct_fields:
            qs = qs.order_by(*order_fields).distinct(*distinct_fields)

        return qs

    def send_email_latest_jobs(self):
        count = 0
        latest_jobs = Job.objects.latest()
        if latest_jobs.exists():
            jobs_as_html = self.jobs_as_html(latest_jobs)
            contact_model = get_contact_model()
            for email, contact_pk in self.email_latest_jobs().items():
                contact = contact_model.objects.get(pk=contact_pk)
                if RejectedEmail.objects.has_clean_history(email):
                    self.send_email_latest_jobs_contact(
                        email, contact, jobs_as_html
                    )
                    count = count + 1
            # email status to notify list
            email_addresses = [n.email for n in Notify.objects.all()]
            if email_addresses:
                # Use the first notify email as the content object
                queue_mail_message(
                    Notify.objects.first(),
                    email_addresses,
                    "Jobs Board - Notify Candidates",
                    "Sent {} reminder emails".format(count),
                )
            else:
                logger.error(
                    "Cannot send email notification of "
                    "reminder emails for the latest jobs.  "
                    "No email addresses set-up in 'enquiry.models.Notify'"
                )
            transaction.on_commit(lambda: process_mail.send())
        return count

    def send_email_latest_jobs_contact(self, email, contact, jobs_as_html):
        with transaction.atomic():
            token = UserConsentUnsubscribe.objects.create_unsubscribe_token(
                contact.user, Candidate.GDPR_JOBS_NOTIFICATION_SLUG
            )
            url = urllib.parse.urljoin(
                settings.HOST_NAME, reverse("web.unsubscribe", args=[token])
            )
            context = {
                email: {
                    "name": contact.user.first_name.title(),
                    "jobs": jobs_as_html,
                    "unsub": url,
                }
            }
            queue_mail_template(
                contact, self.model.MAIL_TEMPLATE_NOTIFY_JOBS, context
            )


def _get_default_experience():
    return Experience.objects.get(slug=Experience.DEFAULT).pk


class Candidate(TimedCreateModifyDeleteModel):
    CANDIDATE_WELCOME = "candidate_welcome_to_jobs_board"
    GDPR_DATA_SLUG = "job-app-candidate-data"
    GDPR_JOBS_NOTIFICATION_SLUG = "job-app-jobs-notification"
    GDPR_NEWSLETTER_SLUG = "job-app-candidate-newsletter"
    GDPR_RECRUITER_SEARCH_SLUG = "job-app-recruiter-search"
    MAIL_TEMPLATE_NOTIFY_JOBS = "candidate-notify-weekly"
    MAILSHOT_SEARCH = "mailshot"
    POSITION_SEARCH = "position"

    contact = models.OneToOneField(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    looking_for_work = models.BooleanField(default=True)
    available_from = models.DateField(blank=True, null=True)
    # replaced with the gdpr user consent ('GDPR_RECRUITER_SEARCH_SLUG')
    allow_search = models.BooleanField(default=True)
    # desired location and radius is stored so we can redisplay it to the user
    # search uses CandidateJobLocation model
    job_location_code = models.CharField(max_length=50, blank=True, null=True)
    job_location_type = models.CharField(max_length=50, blank=True, null=True)
    job_location_radius = models.PositiveIntegerField(default=0)
    job_experience = models.ForeignKey(
        Experience,
        default=_get_default_experience,
        help_text="Experience",
        on_delete=models.CASCADE,
    )
    experience = models.ForeignKey(
        YearsExperience, blank=True, null=True, on_delete=models.CASCADE
    )
    highest_education = models.ForeignKey(
        HighestEducation, blank=True, null=True, on_delete=models.CASCADE
    )
    skills = models.TextField(blank=True, null=True)
    employment_history = models.TextField(blank=True, null=True)
    notes = models.TextField(blank=True)
    picture = models.ImageField(upload_to="job/candidate/images", blank=True)
    date_welcome_email = models.DateTimeField(blank=True, null=True)
    # replaced with the gdpr user consent model ('GDPR_JOBS_NOTIFICATION_SLUG')
    notify_new_jobs = models.BooleanField(
        help_text=(
            "Please untick this box if you don't want to receive job "
            "notification emails"
        ),
        default=True,
    )
    objects = CandidateManager()

    class Meta:
        ordering = ("contact__user__last_name", "contact__user__first_name")
        verbose_name = "Candidate"
        verbose_name_plural = "Candidates"

    def __str__(self):
        return "{}, {}".format(
            self.contact.user.last_name, self.contact.user.first_name
        )

    def consent_candidate_newsletter(self):
        """Has the candidate asked to be sent the Candidate Newsletter?"""
        result = False
        try:
            user_consent = UserConsent.objects.get(
                user=self.contact.user,
                consent__slug=self.GDPR_NEWSLETTER_SLUG,
            )
            result = user_consent.consent_given
        except UserConsent.DoesNotExist:
            pass
        return result

    def consent_given(self, consent):
        """Has the candidate given consent?"""
        return UserConsent.objects.consent_given(self.contact.user, consent)

    def consent_notify_jobs(self):
        """Has the candidate asked to be notified of new jobs?"""
        result = False
        try:
            user_consent = UserConsent.objects.get(
                user=self.contact.user,
                consent__slug=self.GDPR_JOBS_NOTIFICATION_SLUG,
            )
            result = user_consent.consent_given
        except UserConsent.DoesNotExist:
            pass
        return result

    def job_applications(self):
        """Job applications - most recent first."""
        return self.jobapplication_set.all().order_by("-created")

    @property
    def location(self):
        job_location = None
        try:
            if self.job_location_type == "postcode":
                job_location = Location.objects.get(
                    postcode=self.job_location_code
                )
            elif self.job_location_type == "region":
                job_location = Region.objects.get(
                    region_code=self.job_location_code
                )
        except Region.DoesNotExist:
            pass
        except Location.DoesNotExist:
            pass
        return job_location

    def latest_cv(self):
        return CV.objects.latest(self.contact)

    def latest_cv_pk(self):
        """For use in templates where we need access to the primary key."""
        cv = self.latest_cv()
        return cv.pk if cv else None

    def send_welcome_email(self):
        email_to = self.contact.user.email
        context = {
            email_to: {
                "name": self.contact.user.first_name.title(),
                "username": self.contact.user.username,
                "register_date": self.created.strftime("%d/%m/%Y %H:%M"),
            }
        }
        queue_mail_template(self, self.CANDIDATE_WELCOME, context)
        self.date_welcome_email = timezone.now()
        self.save()
        transaction.on_commit(lambda: process_mail.send())

    @property
    def welcome_email_sent(self):
        return self.date_welcome_email is not None


reversion.register(Candidate)


class CandidateJobLocationManager(models.Manager):
    def candidate(self, candidate):
        return self.model.objects.filter(candidate=candidate)

    def location(self, location_list):
        return self.model.objects.filter(location__in=location_list)

    def candidate_in_location_list(self, location_list):
        return (
            self.model.objects.filter(location__in=location_list)
            .order_by(
                "candidate__contact__user__last_name",
                "candidate__contact__user__first_name",
                "candidate__id",
            )
            .distinct(
                "candidate__contact__user__last_name",
                "candidate__contact__user__first_name",
                "candidate__id",
            )
        )


class CandidateJobLocation(TimeStampedModel):
    candidate = models.ForeignKey(
        Candidate, blank=True, null=True, on_delete=models.CASCADE
    )
    location = models.ForeignKey(
        Location, blank=True, null=True, on_delete=models.CASCADE
    )
    objects = CandidateJobLocationManager()

    class Meta:
        ordering = ("candidate", "location")
        unique_together = ("candidate", "location")
        verbose_name = "Candidate Job Location"
        verbose_name_plural = "Candidates Job Locations"

    def __str__(self):
        return "{}, {} ({})".format(
            self.candidate.contact.full_name,
            self.location.town,
            self.location.postcode,
        )


# no reversion, because it takes too much space


class JobApplication(TimeStampedModel):
    candidate = models.ForeignKey(
        Candidate, blank=True, null=True, on_delete=models.CASCADE
    )
    job = models.ForeignKey(
        Job, blank=True, null=True, on_delete=models.CASCADE
    )
    cv = models.ForeignKey(CV, blank=True, null=True, on_delete=models.CASCADE)
    cover = models.TextField(blank=True)
    apply_date = models.DateField(blank=True, null=True)
    interview_date = models.DateField(blank=True, null=True)
    offer_date = models.DateField(blank=True, null=True)
    accepted_date = models.DateField(blank=True, null=True)
    closed_date = models.DateField(blank=True, null=True)

    class Meta:
        ordering = ("job", "candidate")
        unique_together = ("candidate", "job")
        verbose_name = "Job Application"
        verbose_name_plural = "Job Applications"

    def __str__(self):
        return "{} ({}), {}".format(
            self.job.title,
            self.job.recruiter.contact.company_name,
            self.candidate.contact.full_name,
        )


reversion.register(JobApplication)


class JobTrackingManager(models.Manager):
    def create_job_tracking(self, candidate, job, view=False):
        obj = self.model(candidate=candidate, click_date=date.today(), job=job)
        if view:
            obj.view_count = 1
        else:
            obj.application_click_count = 1
        obj.save()
        return obj

    def track_application_click(self, candidate, job):
        try:
            obj = self.model.objects.get(
                candidate=candidate, job=job, click_date=date.today()
            )
            obj.application_click_count += 1
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_job_tracking(candidate, job)
        return obj

    def track_view(self, candidate, job):
        try:
            obj = self.model.objects.get(
                candidate=candidate, job=job, click_date=date.today()
            )
            obj.view_count += 1
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_job_tracking(candidate, job, view=True)
        return obj


class JobTracking(TimeStampedModel):
    """Count application clicks and views per candidate per job per day"""

    candidate = models.ForeignKey(
        Candidate, blank=True, null=True, on_delete=models.CASCADE
    )
    job = models.ForeignKey(
        Job, blank=True, null=True, on_delete=models.CASCADE
    )
    click_date = models.DateField()
    application_click_count = models.IntegerField(default=0)
    view_count = models.IntegerField(default=0)
    objects = JobTrackingManager()

    class Meta:
        ordering = ("job", "candidate")
        unique_together = ("candidate", "job", "click_date")
        verbose_name = "Job Tracking"
        verbose_name_plural = "Job Tracking"

    def __str__(self):
        candidate = ""
        if self.candidate:
            candidate = ", {}".format(self.candidate.contact.full_name)
        return "{} ({}){}".format(
            self.job.title, self.job.recruiter.contact.company_name, candidate
        )


# no reversion, because it takes too much space for job tracking #1403


# These are for import Probably don't need them long term
IMPORT_MEMBER_MATCH = 1
IMPORT_CREATE_NEW = 2


class CandidateImportData(models.Model):
    candidate = models.ForeignKey(
        Candidate, blank=True, null=True, on_delete=models.CASCADE
    )
    import_status = models.PositiveIntegerField(default=0)
    user_id = models.CharField(max_length=20)
    IP = models.CharField(max_length=20)
    SignupDate = models.CharField(max_length=20)
    FirstName = models.CharField(max_length=100)
    LastName = models.CharField(max_length=100)
    Username = models.CharField(max_length=50)
    Password = models.CharField(max_length=100)
    Email = models.CharField(max_length=100)
    Newsletter = models.CharField(max_length=20)
    Notification1 = models.CharField(max_length=20)  # Auto notify on new jobs
    Notification2 = models.CharField(max_length=20)
    Validated = models.CharField(max_length=20)
    login_date = models.CharField(max_length=20)
    logout_date = models.CharField(max_length=20)
    last_request_time = models.CharField(max_length=20)
    login_count = models.CharField(max_length=20)
    alert_keywords = models.CharField(max_length=100)
    alert_last_run = models.CharField(max_length=20)
    alert_email = models.CharField(max_length=100)
    newsletter_last_run = models.CharField(max_length=20)
    lang = models.CharField(max_length=20)
    alert_query = models.CharField(max_length=100)
    # profile Information
    resume_id = models.CharField(max_length=20, blank=True, default="")
    list_on_web = models.CharField(max_length=10, blank=True, default="")
    resume_date = models.CharField(max_length=20, blank=True, default="")
    hits = models.CharField(max_length=20, blank=True, default="")
    anon = models.CharField(max_length=20, blank=True, default="")
    status = models.CharField(max_length=20, blank=True, default="")
    Experience = models.TextField(blank=True, default="")
    DOB = models.CharField(max_length=20, blank=True, default="")
    Nationality = models.CharField(max_length=50, blank=True, default="")
    profile_Email = models.CharField(max_length=100, blank=True, default="")
    Mobile = models.CharField(max_length=50, blank=True, default="")
    Telephone = models.CharField(max_length=50, blank=True, default="")
    StreetAddress = models.CharField(max_length=200, blank=True, default="")
    Town = models.CharField(max_length=100, blank=True, default="")
    County = models.CharField(max_length=50, blank=True, default="")
    Postcode = models.CharField(max_length=20, blank=True, default="")
    Country = models.CharField(max_length=50, blank=True, default="")
    Skills = models.TextField(blank=True, default="")
    EmploymentHistory = models.TextField(blank=True, default="")
    Available = models.CharField(max_length=20, blank=True, default="")
    HighestEducation = models.CharField(max_length=10, blank=True, default="")
    notes = models.TextField(blank=True, default="")
    LocationPref = models.CharField(max_length=500, blank=True, default="")
    Positions = models.CharField(max_length=500, blank=True, default="")
    Name = models.CharField(max_length=100, blank=True, default="")
    Photo = models.CharField(max_length=200, blank=True, default="")
    cv_file_name = models.CharField(max_length=200, blank=True, default="")

    class Meta:
        ordering = ("candidate",)
        verbose_name = "Candidate Import Data"

    def __str__(self):
        return "{} {} ({}, {})".format(
            self.FirstName, self.LastName, self.Username, self.user_id
        )

    def __iter__(self):
        for field_name in self._meta.get_all_field_names():
            try:
                value = getattr(self, field_name)
            except AttributeError:
                value = None
            yield (field_name, value)


reversion.register(CandidateImportData)


class RecruiterImportData(models.Model):
    recruiter = models.ForeignKey(
        Recruiter, blank=True, null=True, on_delete=models.CASCADE
    )
    import_status = models.PositiveIntegerField(default=0)
    user_id = models.CharField(max_length=20, blank=True, default="")
    IP = models.CharField(max_length=20, blank=True, default="")
    SignupDate = models.CharField(max_length=20, blank=True, default="")
    FirstName = models.CharField(max_length=100, blank=True, default="")
    LastName = models.CharField(max_length=100, blank=True, default="")
    Username = models.CharField(max_length=50, blank=True, default="")
    Password = models.CharField(max_length=100, blank=True, default="")
    Email = models.CharField(max_length=100, blank=True, default="")
    Newsletter = models.CharField(max_length=20, blank=True, default="")
    Notification1 = models.CharField(max_length=20, blank=True, default="")
    Notification2 = models.CharField(max_length=20, blank=True, default="")
    Validated = models.CharField(max_length=20, blank=True, default="")
    CompName = models.CharField(max_length=100, blank=True, default="")
    login_date = models.CharField(max_length=20, blank=True, default="")
    logout_date = models.CharField(max_length=20, blank=True, default="")
    login_count = models.CharField(max_length=20, blank=True, default="")
    last_request_time = models.CharField(max_length=20, blank=True, default="")
    lang = models.CharField(max_length=20, blank=True, default="")
    alert_last_run = models.CharField(max_length=20, blank=True, default="")
    alert_email = models.CharField(max_length=100, blank=True, default="")
    posts_balance = models.CharField(max_length=20, blank=True, default="")
    premium_posts_balance = models.CharField(
        max_length=20, blank=True, default=""
    )
    subscription_can_view_resume = models.CharField(
        max_length=20, blank=True, default=""
    )
    subscription_can_premium_post = models.CharField(
        max_length=20, blank=True, default=""
    )
    subscription_can_post = models.CharField(
        max_length=20, blank=True, default=""
    )
    newsletter_last_run = models.CharField(
        max_length=20, blank=True, default=""
    )
    alert_query = models.CharField(max_length=200, blank=True, default="")
    can_view_blocked = models.CharField(max_length=20, blank=True, default="")
    alert_keywords = models.CharField(max_length=20, blank=True, default="")
    views_quota = models.CharField(max_length=20, blank=True, default="")
    p_posts_quota = models.CharField(max_length=20, blank=True, default="")
    posts_quota = models.CharField(max_length=20, blank=True, default="")
    views_quota_tally = models.CharField(max_length=20, blank=True, default="")
    p_posts_quota_tally = models.CharField(
        max_length=20, blank=True, default=""
    )
    posts_quota_tally = models.CharField(max_length=20, blank=True, default="")
    quota_timestamp = models.CharField(max_length=20, blank=True, default="")
    # profile data
    profile_id = models.CharField(max_length=20, blank=True, default="")
    profile_date = models.CharField(max_length=20, blank=True, default="")
    expired = models.CharField(max_length=20, blank=True, default="")
    BusinessName = models.CharField(max_length=100, blank=True, default="")
    Logo = models.CharField(max_length=200, blank=True, default="")
    BusinessType = models.CharField(max_length=50, blank=True, default="")
    ContactName = models.CharField(max_length=100, blank=True, default="")
    Position = models.CharField(max_length=100, blank=True, default="")
    WebsiteURL = models.CharField(max_length=100, blank=True, default="")
    profile_Email = models.CharField(max_length=100, blank=True, default="")
    Telephone = models.CharField(max_length=50, blank=True, default="")
    Mobile = models.CharField(max_length=50, blank=True, default="")
    StreetAddress = models.CharField(max_length=200, blank=True, default="")
    Town = models.CharField(max_length=100, blank=True, default="")
    County = models.CharField(max_length=50, blank=True, default="")
    Postcode = models.CharField(max_length=20, blank=True, default="")
    Country = models.CharField(max_length=50, blank=True, default="")
    About = models.TextField(blank=True, default="")
    file_name = models.CharField(max_length=200, blank=True, default="")

    class Meta:
        ordering = ("recruiter",)
        verbose_name = "Recruiter Import Data"

    def __str__(self):
        return "{}".format(self.candidate.contact.full_name, self.ID)

    def __iter__(self):
        for field_name in self._meta.get_all_field_names():
            try:
                value = getattr(self, field_name)
            except AttributeError:
                value = None
            yield (field_name, value)


reversion.register(RecruiterImportData)


class JobImportData(models.Model):
    job = models.ForeignKey(
        Job, blank=True, null=True, on_delete=models.CASCADE
    )
    post_id = models.CharField(max_length=20, blank=True, default="")
    post_date = models.CharField(max_length=20, blank=True, default="")
    post_mode = models.CharField(max_length=20, blank=True, default="")
    user_id = models.CharField(max_length=20, blank=True, default="")
    pin_x = models.CharField(max_length=20, blank=True, default="")
    pin_y = models.CharField(max_length=20, blank=True, default="")
    approved = models.CharField(max_length=20, blank=True, default="")
    JobTitle = models.CharField(max_length=200, blank=True, default="")
    Description = models.TextField(blank=True, null=True)
    Classification = models.CharField(max_length=20, blank=True, default="")
    PostedBy = models.CharField(max_length=50, blank=True, default="")
    Salary = models.CharField(max_length=50, blank=True, default="")
    ContactTelephone = models.CharField(max_length=50, blank=True, default="")
    Area = models.CharField(max_length=20, blank=True, default="")
    JobType = models.CharField(max_length=20, blank=True, default="")
    Location = models.CharField(max_length=200, blank=True, default="")
    Deadline = models.CharField(max_length=20, blank=True, default="")
    applications = models.CharField(max_length=20, blank=True, default="")
    hits = models.CharField(max_length=20, blank=True, default="")
    reason = models.CharField(max_length=20, blank=True, default="")
    StartDate = models.CharField(max_length=50, blank=True, default="")
    JobFunction = models.CharField(max_length=200, blank=True, default="")
    Email = models.CharField(max_length=200, blank=True, default="")
    guid = models.CharField(max_length=20, blank=True, default="")
    source = models.CharField(max_length=20, blank=True, default="")
    cached_summary = models.CharField(max_length=20, blank=True, default="")
    expired = models.CharField(max_length=20, blank=True, default="")
    app_type = models.CharField(max_length=20, blank=True, default="")
    app_url = models.CharField(max_length=200, blank=True, default="")
    JobReference = models.CharField(max_length=50, blank=True, default="")
    ContactURL = models.CharField(max_length=200, blank=True, default="")
    ContactName = models.CharField(max_length=200, blank=True, default="")

    class Meta:
        ordering = ("job",)
        verbose_name = "Job Import Data"

    def __str__(self):
        return "{} (#{})".format(self.JobTitle, self.post_id)

    def __iter__(self):
        for field_name in self._meta.get_all_field_names():
            try:
                value = getattr(self, field_name)
            except AttributeError:
                value = None
            yield (field_name, value)


reversion.register(JobImportData)
