# -*- encoding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from job.models import JobFeed


class Command(BaseCommand):
    help = "Delete Indeed Jobs Board Feed #4036"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        feed = JobFeed.objects.get(slug="indeed")
        user_model = get_user_model()
        user = user_model.objects.get(username="patrick.kimber")
        feed.set_deleted(user)
        self.stdout.write("{} ({}) - Complete".format(self.help, feed.slug))
