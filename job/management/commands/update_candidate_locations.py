# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from job.models import Candidate, CandidateJobLocation, Location

# from member.models import Contact


class Command(BaseCommand):
    """
    def check_locations(self):
        for contact in Contact.objects.all():
            postcode_area = None
            location = None

            if len(contact.postcode) > 5:
                postcode_area = contact.postcode.rstrip()[:-3].rstrip().upper()
            elif len(contact.postcode) > 1:
                postcode_area = contact.postcode.rstrip().upper()

            if postcode_area:
                try:
                    location = Location.objects.get(postcode=postcode_area)
                except:
                    location = "DOES NOT EXIST"
            print(
                contact.full_name, contact.postcode, " === ",
                postcode_area, " --- ", location
            )
    """

    def update_locations(self):
        for candidate in Candidate.objects.current():
            postcode_area = None
            location = None

            if len(candidate.contact.postcode) > 5:
                postcode = candidate.contact.postcode
                postcode_area = postcode.rstrip()[:-3].rstrip().upper()
            elif len(candidate.contact.postcode) > 1:
                postcode_area = candidate.contact.postcode.rstrip().upper()

            if postcode_area:
                try:
                    location = Location.objects.get(postcode=postcode_area)
                except:
                    pass
            print(
                candidate.contact.full_name,
                candidate.contact.postcode,
                " === ",
                postcode_area,
                " --- ",
                location,
            )

            if location and candidate.candidatejoblocation_set.count() == 0:
                print(
                    "Creating Location for",
                    candidate.contact.full_name,
                    "-",
                    location,
                )
                candidate_job_location = CandidateJobLocation(
                    candidate=candidate, location=location
                )
                candidate_job_location.save()

    def handle(self, *args, **options):
        self.update_locations()
        # self.check_locations()
        print("Location Update complete...")
