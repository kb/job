# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from job.tasks import refresh_job_index


class Command(BaseCommand):
    help = "Refresh job index (without dropping first)..."

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count, index_count = refresh_job_index()
        self.stdout.write(
            "{} - Complete.  {} jobs, {} in the index".format(
                self.help, count, index_count
            )
        )
