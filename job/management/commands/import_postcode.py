# -*- encoding: utf-8 -*-
import csv
import os.path
import sys

from decimal import Decimal

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils.text import slugify

from job.models import Location, Region, Country


class Command(BaseCommand):
    def to_integer(self, int_as_str):
        try:
            ret = int(int_as_str)
        except:
            ret = 0

        return ret

    def to_decimal(self, dec_as_str):
        try:
            ret = Decimal(dec_as_str.strip(' "'))
        except:
            ret = Decimal(0)

        return ret

    def write_message(self, msg):
        print(msg, file=sys.stderr)
        print(msg)

    def get_region_from_str(self, region_str):
        region_slug = slugify(region_str)

        try:
            region = Region.objects.get(
                Q(slug=region_slug) | Q(alternate_slug=region_slug)
            )
        except Region.DoesNotExist:
            region = None
        except Region.MultipleObjectsReturned:
            self.write_message(
                "{} ({}) region returned multiple results".format(
                    region_str, region_slug
                )
            )
            region = None

        if not region:
            self.write_message(
                "{} ({}) region not found".format(region_str, region_slug)
            )

        return region

    def get_country(self, country_code):
        try:
            if len(country_code) == 2:
                country = Country.objects.get(alpha2_code=country_code)
            else:
                country = Country.objects.get(country_code=country_code)

        except Country.DoesNotExist:
            country = None

        if not country:
            self.write_message("{} country not found".format(country_code))
        return country

    def import_postcode_csv(self, file_name):
        csv.register_dialect(
            "default", skipinitialspace=True, quoting=csv.QUOTE_ALL
        )
        reader = csv.reader(open(file_name), "default")
        for row in reader:
            if row[0] != "postcode":  # ignore the header row
                print("\n{}".format("-" * 70))
                location_values = {}
                postcode = row[0]
                location_values["eastings"] = self.to_integer(row[1])
                location_values["northings"] = self.to_integer(row[2])
                location_values["latitude"] = self.to_decimal(row[3])
                location_values["longitude"] = self.to_decimal(row[4])
                location_values["town"] = row[5]
                location_values["region"] = self.get_region_from_str(row[6])
                if not location_values["town"] and location_values["region"]:
                    location_values["town"] = location_values["region"].name
                location_values["country"] = self.get_country(row[7])
                if (
                    not location_values["region"]
                    or not location_values["country"]
                ):
                    location_values["notes"] = (
                        "Town='{}', Region='{}', Country='{}'".format(
                            row[5], row[6], row[7]
                        )
                    )
                location_tuple = Location.objects.get_or_create(
                    postcode=postcode, defaults=location_values
                )
                print("Postcode  : {} ({})".format(postcode, row[0]))
                print(
                    "eastings  : {} ({})".format(
                        location_values["eastings"], row[1]
                    )
                )
                print(
                    "northings : {} ({})".format(
                        location_values["northings"], row[2]
                    )
                )
                print(
                    "latitude  : {} ({})".format(
                        location_values["latitude"], row[3]
                    )
                )
                print(
                    "longitude : {} ({})".format(
                        location_values["longitude"], row[4]
                    )
                )
                print(
                    "town      : {} ({})".format(
                        location_values["town"], row[5]
                    )
                )
                print(
                    "region    : {} ({})".format(
                        location_values["region"], row[6]
                    )
                )
                print(
                    "country   : {} - {} ({})".format(
                        location_values["country"],
                        location_values["country"].country_code,
                        row[7],
                    )
                )
                print("CREATED   : {}".format(location_tuple[1]))

    def add_arguments(self, parser):
        parser.add_argument("--path", help="path to the 'postcodes.csv' file")

    def handle(self, *args, **options):
        file_name = options["path"]
        if os.path.isfile(file_name):
            self.import_postcode_csv(file_name)
        else:
            print("\n{} does not exist\n".format(file_name))
