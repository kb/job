# -*- encoding: utf-8 -*-
import csv
import os
import tempfile

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from job.models import CV


class Command(BaseCommand):
    help = "Check CV files exist"

    def add_arguments(self, parser):
        parser.add_argument(
            "--set-deleted",
            action="store_true",
            dest="set-deleted",
            default=False,
            help="Delete CV if file does not exist",
        )

    def handle(self, *args, **options):
        self.stdout.write("Check CVs to see if the file exists...")
        set_deleted = options["set-deleted"]
        if set_deleted:
            self.stdout.write("Will mark CVs with missing files as deleted")
        with tempfile.NamedTemporaryFile(mode="w", delete=False) as fp:
            count = 0
            csv_writer = csv.writer(fp, dialect="excel-tab")
            csv_writer.writerow(["id", "created", "email"])
            to_delete = []
            for cv in CV.objects.exclude(deleted=True):
                try:
                    file_field = cv.file_name
                    path = file_field.path
                    exists = os.path.exists(path)
                except ValueError:
                    exists = False
                if not exists:
                    count = count + 1
                    self.stdout.write(
                        "{} {} [{}] {}".format(
                            count,
                            cv.created.strftime("%d/%m/%Y %H:%M"),
                            cv.pk,
                            cv.contact.user.username,
                        )
                    )
                    csv_writer.writerow(
                        [
                            cv.pk,
                            cv.created.strftime("%d/%m/%Y %H:%M"),
                            cv.contact.user.email,
                        ]
                    )
                    to_delete.append(cv.pk)
            if set_deleted:
                self.stdout.write("Marking CVs as deleted...")
                user = get_user_model().objects.get(username="patrick.kimber")
                for pk in to_delete:
                    cv = CV.objects.get(pk=pk)
                    if not cv.is_deleted:
                        cv.set_deleted(user)
                self.stdout.write("Deleted {} CVs...".format(len(to_delete)))
            self.stdout.write(
                "email addresses of {} contacts with missing CVs "
                "in: {}".format(count, fp.name)
            )
            self.stdout.write("Finished checking CV files...")
