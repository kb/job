# -*- encoding: utf-8 -*-
import sys
from django.core.management.base import BaseCommand

from job.models import JobFeed


class Command(BaseCommand):
    """
    Updates the job feed URL for a single record.
    Used for changing the Simply Law feed.

    As of 01/03/2023, the correct URL for the Simply Law feed is
    https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json
    """

    def get_urls(self):
        try:
            slug = "simply-law"
            url = "https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json"
            feed = JobFeed.objects.get(slug=slug)
            feed.url = url
            feed.save()

            # Get the actual url property of the saved record
            print(f"URL set to: {JobFeed.objects.get(slug=slug).url}")

            return True

        except JobFeed.DoesNotExist:
            print()
            print(f'Slug "{slug}" doesn\'t exist.')
            print()
            print("Available records are:")
            for x in JobFeed.objects.all():
                print(x.id, x.slug, x.url)
            print()

            return False

    def handle(self, *args, **options):
        if self.get_urls():
            print("Complete...")
