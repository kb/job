# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from job.tasks import send_email_latest_jobs


class Command(BaseCommand):
    """Send a test job notification email.

    https://www.kbsoftware.co.uk/crm/ticket/4403/

    """

    help = "Send job notification emails to candidates"

    def handle(self, *args, **options):
        self.stdout.write("{} ...".format(self.help))
        count = send_email_latest_jobs()
        self.stdout.write("{} - {} emails - Complete".format(self.help, count))
