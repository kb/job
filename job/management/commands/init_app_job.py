# -*- encoding: utf-8 -*-
import os

from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction

from job.models import (
    Candidate,
    JobBasket,
    JobFeed,
    Recruiter,
    RECRUITER_APPLICATION_FROM_CANDIDATE,
)
from mail.models import MailTemplate


class Command(BaseCommand):
    help = "Initialise 'job' application"

    def _init_job_feed_recruiter(self, slug, name):
        user_model = get_user_model()
        # create the user
        try:
            user = user_model.objects.get(username=slug)
        except user_model.DoesNotExist:
            user = user_model.objects.create_user(
                slug, first_name=name, last_name="Feed"
            )
            user.is_active = False
            user.save()
        # create the contact
        contact_model = apps.get_model(settings.CONTACT_MODEL)
        try:
            contact = contact_model.objects.get(user=user)
        except contact_model.DoesNotExist:
            contact = contact_model(user=user)
            contact.save()
        # create the recruiter
        try:
            recruiter = Recruiter.objects.get(contact=contact)
        except Recruiter.DoesNotExist:
            recruiter = Recruiter(contact=contact, notify_application=False)
            recruiter.save()
        # create the jobs board feed
        try:
            feed = JobFeed.objects.get(slug=slug)
        except JobFeed.DoesNotExist:
            feed = JobFeed(slug=slug, recruiter=recruiter)
        return feed

    def _init_job_feed_legists(self):
        with transaction.atomic():
            feed = self._init_job_feed_recruiter(JobFeed.LEGISTS, "Legists")
            if feed.url:
                self.stdout.write(
                    "Feed '{}' already initialised...".format(feed.slug)
                )
            else:
                self.stdout.write("Initialise '{}' feed...".format(feed.slug))
                feed.url = os.environ.get("TEST_LEGISTS_URL", "")
                feed.username = os.environ.get("TEST_LEGISTS_USERNAME", "")
                feed.password = os.environ.get("TEST_LEGISTS_PASSWORD", "")
                feed.save()

    def _init_job_feed_simply_law(self):
        with transaction.atomic():
            feed = self._init_job_feed_recruiter(
                JobFeed.SIMPLY_LAW, "Simply Law Jobs"
            )
            if feed.url:
                self.stdout.write(
                    "Feed '{}' already initialised...".format(feed.slug)
                )
            else:
                self.stdout.write("Initialise '{}' feed...".format(feed.slug))
                # "https://www.simplylawjobs.com/api/job_feed.json",
                # URL changed 04/04/23
                feed.url = "https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json"
                feed.identifier = os.environ.get(
                    "TEST_SIMPLY_LAW_IDENTIFIER", ""
                )
                feed.username = os.environ.get("TEST_SIMPLY_LAW_USERNAME", "")
                feed.password = os.environ.get("TEST_SIMPLY_LAW_PASSWORD", "")
                feed.save()

    def _init_mail_template(self):
        MailTemplate.objects.init_mail_template(
            Candidate.MAIL_TEMPLATE_NOTIFY_JOBS,
            "Latest vacancies on our Legal Secretary Jobs Board",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} first name of the candidate\n"
                "{{ jobs }} list of jobs\n"
                "{{ unsub }} unsubscribe url"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            is_system=True,
        )
        MailTemplate.objects.init_mail_template(
            RECRUITER_APPLICATION_FROM_CANDIDATE,
            "Re: job application",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the recruiter.\n"
                "{{ candidate }} name of the candidate\n"
                "{{ date }} date of the application\n"
                "{{ job }} job title\n"
                "{{ url }} url of the job"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Job application",
            description="A candidate has applied for one of your jobs.",
        )
        MailTemplate.objects.init_mail_template(
            JobBasket.MAIL_TEMPLATE_JOB_PAYMENT,
            "Thank you for your payment",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the customer.\n"
                "{{ description }} transaction detail.\n"
                "{{ total }} total value of the transaction."
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Thank you for your payment",
            description="The job(s) are now live on the jobs board.",
        )
        MailTemplate.objects.init_mail_template(
            Recruiter.MAIL_TEMPLATE_JOB_SUBSCRIPTION,
            "Thank you for your payment",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the customer.\n"
                "{{ description }} transaction detail.\n"
                "{{ total }} total value of the transaction."
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Thank you for your payment",
            description="You have a one year subscription for posting jobs.",
        )
        MailTemplate.objects.init_mail_template(
            Recruiter.MAIL_TEMPLATE_JOB_SUBSCRIPTION_INVOICE,
            "Thank you for your order for a job subscription",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the customer.\n"
                "{{ description }} transaction detail.\n"
                "{{ invoice }} invoice detail.\n"
                "{{ total }} total value of the transaction."
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Thank you for your payment",
            description="You have a one year subscription for posting jobs.",
        )

    def handle(self, *args, **options):
        self.stdout.write("{} ...".format(self.help))
        self._init_job_feed_legists()
        self._init_job_feed_simply_law()
        self._init_mail_template()
        self.stdout.write("{} - Complete".format(self.help))
