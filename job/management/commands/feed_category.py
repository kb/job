# -*- encoding: utf-8 -*-
import csv
from django.core.management.base import BaseCommand
from job.models import Job, JobRole, PracticeArea

practice_areas = None
job_roles = None
default_practice_area = None
default_job_role = None


def initialise_practice_areas_and_roles():
    global practice_areas
    global job_roles
    global default_practice_area
    global default_job_role

    practice_areas = PracticeArea.objects.all_practice_areas()

    for practice_area in practice_areas:
        if practice_area["default"]:
            default_practice_area = practice_area
            break

    job_roles = JobRole.objects.all_job_roles()

    for job_role in job_roles:
        if job_role["default"]:
            default_job_role = job_role
            break
    print(
        "Default practice area:",
        default_practice_area,
        "Default job role     :",
        default_job_role,
    )


def find_practice_areas_and_roles(job):
    job_text = (job.title + " " + job.description).lower()

    found_practice_areas = []
    found_roles = []
    for practice_area in practice_areas:
        for keyword in practice_area["keywords"]:
            if keyword.lower() in job_text:
                if practice_area not in found_practice_areas:
                    found_practice_areas.append(practice_area)
                    break

    if not found_practice_areas:
        found_practice_areas.append(default_practice_area)

    for job_role in job_roles:
        for keyword in job_role["keywords"]:
            if keyword.lower() in job.title.lower():
                if job_role not in found_roles:
                    found_roles.append(job_role)
                    break

    return found_practice_areas, found_roles


class Command(BaseCommand):
    def allocate_practice_areas_and_roles(self, csv_writer, job):
        if job is None:
            for practice_area in practice_areas:
                print(
                    practice_area["name"],
                    practice_area["keywords"],
                    practice_area["default"],
                )

            for job_role in job_roles:
                print(
                    job_role["name"], job_role["keywords"], job_role["default"]
                )
            return
        found_practice_areas, found_roles = find_practice_areas_and_roles(job)
        if found_practice_areas or found_roles:
            csv_writer.writerow(
                (
                    job.title,
                    job.description,
                    job.job_role,
                    job.practice_area,
                    ", ".join(typ["name"] for typ in found_roles),
                    ", ".join(cat["name"] for cat in found_practice_areas),
                )
            )

    def handle(self, *args, **options):
        initialise_practice_areas_and_roles()

        file_name = "/tmp/job_practice_areas.csv"
        with open(file_name, "w", newline="") as csv_file:
            csv_writer = csv.writer(csv_file, dialect="excel")
            csv_writer.writerow(
                (
                    "Title",
                    "Description",
                    "Job Role",
                    "Practice Area",
                    "Possible Job Roles",
                    "Possible Practice Areas",
                )
            )
            self.allocate_practice_areas_and_roles(csv_writer, None)
            for job in Job.objects.exclude(jobfeeditem=None):
                self.allocate_practice_areas_and_roles(csv_writer, job)
