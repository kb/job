# -*- encoding: utf-8 -*-
from datetime import datetime
import pytz
from time import mktime, strptime

from django.core.management.base import BaseCommand

from job.models import Candidate, CandidateImportData


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write(
            "Update original signup date for imported candidates (#1134)"
        )
        updated = 0
        for cid in CandidateImportData.objects.all():
            tim = strptime(cid.SignupDate, "%Y-%m-%d %H:%M:%S")
            dt = pytz.utc.localize(datetime.fromtimestamp(mktime(tim)))
            # update candidate created date with the original signup date from
            # old jobs board
            if cid.candidate.created.date() != dt.date():
                print(
                    cid.candidate,
                    cid.candidate.created,
                    dt,
                    cid.id,
                    cid.candidate.id,
                )
                updated += 1
                cid.candidate.created = dt
                cid.candidate.save()

        print("Signup date updated for {} candidates".format(updated))

        self.stdout.write(
            "Update candidate record reference data for job location (#1134)"
        )
        updated = 0
        for c in Candidate.objects.all():
            # candidatejoblocation record was converted from contact record but
            # job_location_ fields on candidate record were not updated
            if (
                c.job_location_code is None
                and c.candidatejoblocation_set.all().count() == 1
            ):
                updated += 1
                l = c.candidatejoblocation_set.first()
                print(c, l.location.postcode)
                c.job_location_code = l.location.postcode
                c.job_location_type = "postcode"
                c.job_location_radius = 25
                c.save()
        print(
            "Location reference data updated for {} candidates".format(updated)
        )
