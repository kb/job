# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand

from job.models import Candidate
from mail.models import MailTemplate


class Command(BaseCommand):
    help = "Create candidate welcome email #2937"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        MailTemplate.objects.init_mail_template(
            Candidate.CANDIDATE_WELCOME,
            "Welcome to the jobs board.",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} first name of the candidate.\n"
                "{{ username }} user name of the candidate.\n"
                "{{ register_date }} registration date.\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Welcome to the jobs board",
            description="Welcome to the jobs board",
        )
        self.stdout.write("{} - Complete".format(self.help))
