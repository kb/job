# -*- encoding: utf-8 -*-
import csv
from django.core.management.base import BaseCommand
from job.models import Candidate, Experience

experiences = [
    {"slug": "trainee", "title": "Trainee", "order": 1},
    {"slug": "qualified", "title": "Qualified", "order": 2},
    {"slug": "experienced", "title": "Experienced", "order": 3},
    {"slug": "default", "title": "Stated on CV", "order": 1001},
]


class Command(BaseCommand):
    def create_experience(self, slug, order, title):
        print("Adding/updating Experience:", title)
        Experience.objects.init_experience(slug, order, title)

    def create_experiences(self):
        print("Adding Experiences...")
        for role in experiences:
            self.create_experience(role["slug"], role["order"], role["title"])

        print("Done")

    # def convert_experience(self):
    #     experienced = Experience.objects.get(slug=Experience.EXPERIENCED)
    #     qualified = Experience.objects.get(slug=Experience.QUALIFIED)
    #     trainee = Experience.objects.get(slug=Experience.TRAINEE)
    #     for candidate in Candidate.objects.all():
    #         existing = trainee
    #         if candidate.highest_education and (
    #             candidate.highest_education.slug == '07-ilspa' or
    #             candidate.highest_education.slug == '08-other-sec'
    #         ):
    #             existing = qualified
    #         if candidate.experience and candidate.experience.order > 1:
    #             existing = experienced
    #         experience = candidate.contact.get_job_experience()
    #         if existing.order > experience.order:
    #             candidate.job_experience = experience
    #         else:
    #             candidate.job_experience = existing
    #         candidate.save()

    def import_experience(self, file_name):
        experiences = dict(
            [(exp.slug, exp) for exp in Experience.objects.all()]
        )

        for experience in experiences:
            print(experience)

        csv.register_dialect(
            "default", skipinitialspace=True, quoting=csv.QUOTE_ALL
        )
        reader = csv.reader(open(file_name, encoding="latin1"), "default")
        for row in reader:
            # columns in csv
            # 0 - Candidate Name, 1 - First Name, 2 - Last Name, 3 - Location,
            # 4 - Email Address, 5 - Experience, 6 - Highest Education,
            # 7 - Membership Level, 8 - Expiry Date, 9 - Registered,
            # 10 - Last Login, 11 - Notify by Email,
            # 12 - Courses Pass Date and Title, 13 - New Experience Level

            if row[0] != "Candidate Name" and row[13]:
                exp_slug = row[13].lower()
                # print ("{} - {} - {}".format(row[4], row[0], exp_slug))
                experience = experiences[exp_slug]
                candidates = Candidate.objects.filter(
                    contact__user__email=row[4],
                    contact__user__first_name=row[1],
                    contact__user__last_name=row[2],
                    contact__deleted=False,
                )
                if candidates.count() == 1:
                    self.stdout.write(
                        "{} - {} - {} FOUND {}".format(
                            row[4], row[0], row[13], candidates[0]
                        )
                    )
                elif candidates.count() == 0:
                    self.stdout.write(
                        "{} - {} - {} DOES NOT EXIST".format(
                            row[4], row[0], row[13]
                        )
                    )
                else:
                    self.stdout.write(
                        "{} - {} - {} MULTIPLE RECORDS FOUND".format(
                            row[4], row[0], row[13]
                        )
                    )

                for candidate in candidates:
                    candidate.job_experience = experience
                    candidate.save()
                    self.stdout.write(
                        "UPDATED - "
                        + ", ".join(
                            list(candidate.contact.get_summary_description())
                        )
                    )

    def handle(self, *args, **options):
        self.create_experiences()
        self.import_experience("jobs-board-candidate-10_3_17.csv")
