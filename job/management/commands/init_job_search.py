# -*- encoding: utf-8 -*-
import json

from django.core.management.base import BaseCommand
from elasticsearch import Elasticsearch

from job.models import Job, JobRoleParser, PracticeAreaParser
from job.service import JobIndexMaintenance


class Command(BaseCommand):
    DOC_TYPE = "job"
    INDEX = "job-index"

    job_role_parser = JobRoleParser()
    practice_area_parser = PracticeAreaParser()

    help = "Initialise 'job' search"

    def _analyzer(self, es, text):
        result = es.indices.analyze(
            index=self.INDEX, analyzer="my_analyzer", text=text
        )
        self.stdout.write("{}".format(json.dumps(result, indent=2)))

    def update_job_index(self, es, job):
        # self.stdout.write('{}) {}'.format(job.pk, job.title))
        doc = {
            "title": job.title,
            "description": job.description,
            "publish_date": job.publish_date.isoformat(),
            "close_date": job.close_date.isoformat(),
            "recruiter_name": job.recruiter_name,
        }
        if job.location:
            location = "{} {}".format(
                job.location.town, job.location.region.name
            )
            self.stdout.write(
                " close_date:   {} {}".format(
                    job.close_date, job.close_date.isoformat()
                )
            )
            self.stdout.write(
                " publish_date:   {} {}".format(
                    job.publish_date, job.publish_date.isoformat()
                )
            )
            self.stdout.write(" location_name:   {}".format(job.location.town))
            # self.stdout.write(' town:   {}'.format(job.location.town))
            # self.stdout.write(' region: {}'.format(job.location.region.name))
            # self.stdout.write(' location: {}'.format(location))
            # self.stdout.write(" {}.{}".format(
            #    job.location.latitude, job.location.longitude))
            doc.update(
                {
                    # this will be a separate field use the town for now
                    "location_name": job.location.town,
                    "town": job.location.town,
                    "region": job.location.region.name,
                    "location": location,
                    "postcode_district": job.location.postcode,
                    "coordinates": {
                        "lat": str(job.location.latitude),
                        "lon": str(job.location.longitude),
                    },
                }
            )
        if job.salary:
            doc.update({"salary": job.salary})

        if job.practice_area:
            doc.update({"practice_area": job.practice_area.title})
        else:
            areas = self.practice_area_parser.parse(
                job.title + " " + job.description
            )
            if areas:
                doc.update(
                    {"practice_area": " ".join([area.title for area in areas])}
                )

            # self.stdout.write('  Class:  {}'.format(
            #     job.practice_area.title
            # ))
            pass
        if job.job_role:
            doc.update({"job_role": job.job_role.title})
        else:
            roles = self.job_role_parser.parse(job.title)
            if roles:
                doc.update(
                    {"job_role": " ".join([role.title for role in roles])}
                )

        self.stdout.write("{}".format(json.dumps(doc, indent=2)))
        result = es.index(
            index=self.INDEX, doc_type=self.DOC_TYPE, id=job.pk, body=doc
        )
        # self.stdout.write('  Create: {}'.format(result['created']))
        # if i > 100:
        #    break
        return result

    def _build(self, es):
        for i, job in enumerate(Job.objects.current().order_by("-pk")):
            self.update_job_index(es, job)
        es.indices.refresh(index=self.INDEX)

    def _build_test(self, es):
        # for i, region in enumerate(['Heaney', 'Heavey']):
        for i, location in enumerate(
            ["Plymouth", "Plym", "Plymothian", "Plymouuth"]
        ):
            doc = {"location": location}
            result = es.index(
                index=self.INDEX, doc_type=self.DOC_TYPE, id=i, body=doc
            )
        es.indices.refresh(index=self.INDEX)
        return result

    def _create(self):
        job_index = JobIndexMaintenance()
        job_index.delete_index()
        job_index.create_index()

    def _search(self, es, query):
        result = es.search(index=self.INDEX, body=query)
        self.stdout.write("\n\nFound {} hits".format(result["hits"]["total"]))
        # self.stdout.write("{}".format(json.dumps(result, indent=2)))
        for hit in result["hits"]["hits"]:
            source = hit["_source"]
            self.stdout.write("\nID:            {_id}".format(**hit))
            self.stdout.write("  location:      {location}".format(**source))
            self.stdout.write("  score:           {_score}".format(**hit))

    def handle(self, *args, **options):
        self._create()
        es = Elasticsearch()
        self._build(es)
        # self._build_test(es)
        # query = {
        #     "query": {
        #         "match_all": {}
        #     }
        # }
        # self._search(es, query)
        print("\n\nanalyzer _________________________________________________")
        # self._analyzer(es, 'London')
        # self._analyzer(es, 'plymouth')

        # From
        # http://stackoverflow.com/questions/20632042/ \
        #    elasticsearch-searching-for-human-names
        #
        # I'd skip using the "porter" stemmer entirely for this, however.
        # I kept it just to show how multi_field works. Using a combination of
        # match, match with fuzziness and phonetic matching should get you far.
        #
        # (Make sure you don't allow fuzziness when you do phonetic matching
        # - or you'll get uselessly fuzzy matching. :-)
        location = "plymouth"
        query = {
            "query": {
                "bool": {
                    "should": [
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match": {
                                            "location": {"query": location}
                                        }
                                    },
                                    {
                                        "match": {
                                            "location": {
                                                "query": location,
                                                "fuzziness": 1,
                                            }
                                        }
                                    },
                                    {
                                        "match": {
                                            "location.metaphone": {
                                                "query": location
                                            }
                                        }
                                    },
                                    # {
                                    #     "match": {
                                    #         "location.porter": {
                                    #             "query": location,
                                    #         }
                                    #     }
                                    # }
                                ]
                            }
                        }
                    ]
                }
            }
        }
        self._search(es, query)
        return

        # print("\n\ngeo ___________________________________________________")
        # query = {
        #     "query": {
        #         "filtered": {
        #             "filter": {
        #                 "geo_distance": {
        #                     "distance": "100km",
        #                     "coordinates": {
        #                         "lat": "50.37074",
        #                         "lon": "-4.15317",
        #                     }
        #                 }
        #             }
        #         }
        #     }
        # }
        # self._search(es, query)

        print("\n\ncase sensitive ___________________________________________")
        query = {
            "query": {
                "match": {
                    "location": {"query": "london", "analyzer": "my_analyzer"}
                }
            }
        }
        self._search(es, query)
        return

        print("\n\ncase insensitive _________________________________________")
        query = {"query": {"match": {"location": "Plymouth"}}}
        self._search(es, query)
        print("\n\nwildcard _________________________________________________")
        query = {"query": {"wildcard": {"location": "ply*"}}}
        self._search(es, query)
        # query = {
        #     "aggs": {
        #         "wheretiz": {
        #             "terms": {"field": "location"}
        #         }
        #     }
        # }
        # self._search(es, query)
        self.stdout.write("Initialised 'job' search...")
