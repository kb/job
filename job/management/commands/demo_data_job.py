# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from job.tests.scenario import demo_data


class Command(BaseCommand):
    help = "Create demo data for 'job'"

    def handle(self, *args, **options):
        demo_data()
        print("Created 'job' demo data...")
