# -*- encoding: utf-8 -*-
import csv
import os.path

from django.core.management.base import BaseCommand
from django.utils.text import slugify

from job.models import Country, Region, RegionType


class Command(BaseCommand):
    def get_region_type(self, region_type_name):
        region_type_slug = slugify(region_type_name)
        region_type = RegionType.objects.get_or_create(
            slug=region_type_slug, defaults={"name": region_type_name}
        )[0]
        return region_type

    def get_country(self, country_code):
        try:
            country = Country.objects.get(country_code=country_code)
        except Country.DoesNotExist:
            country = None
        return country

    def import_region_csv(self, file_name):
        csv.register_dialect(
            "default", skipinitialspace=True, quoting=csv.QUOTE_ALL
        )
        reader = csv.reader(open(file_name), "default")
        for row in reader:
            if row[0] != "code":  # skip the field name row
                region_values = {}
                region_code = row[0]
                region_values["name"] = row[1]
                region_values["slug"] = slugify(row[1])
                region_values["region_type"] = self.get_region_type(row[2])
                # skip Country = GB
                region_values["country"] = self.get_country(row[4])
                region_values["alternate_region_code"] = row[5]
                region_values["alternate_name"] = row[6]
                if row[6]:
                    region_values["alternate_slug"] = slugify(row[6])
                region_tuple = Region.objects.get_or_create(
                    region_code=region_code, defaults=region_values
                )
                region = region_tuple[0]
                print("\n{}".format("-" * 70))
                print("code     : {}".format(region.region_code))
                print("name     : {}".format(region.name))
                print("type     : {}".format(region.region_type))
                print("country  : {}".format(region.country))
                print("alt code : {}".format(region.alternate_region_code))
                print("alt name : {}".format(region.alternate_name))
                print("CREATED  : {}".format(region_tuple[1]))

    def add_arguments(self, parser):
        parser.add_argument("--path", help="path to the 'region.csv' file")

    def handle(self, *args, **options):
        file_name = options["path"]
        if os.path.isfile(file_name):
            self.import_region_csv(file_name)
        else:
            print("\n{} does not exist\n".format(file_name))
