# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from job.models import Candidate, get_contact_model, Job
from mail.tasks import process_mail


class Command(BaseCommand):
    help = "Send a test job notification email #4403"

    def _contact(self, email):
        result = None
        qs = (
            get_contact_model()
            .objects.exclude(deleted=True)
            .filter(user__email=email)
        )
        count = qs.count()
        if count:
            result = qs.first()
            if count > 1:
                self.stdout.write(
                    "Found '{}' contacts with this email address".format(count)
                )
                for x, contact in enumerate(qs):
                    self.stdout.write(
                        "{}. {} ({})".format(
                            x + 1, contact.full_name, contact.user.username
                        )
                    )
            return result
        else:
            raise CommandError(
                "Cannot find a contact with email address '{}'".format(email)
            )

    def add_arguments(self, parser):
        parser.add_argument("email", nargs="+", type=str)

    def handle(self, *args, **options):
        self.stdout.write("{} ...".format(self.help))
        for email in options["email"]:
            self.stdout.write("Sending test email to '{}'".format(email))
            contact = self._contact(email)
            jobs_as_html = Candidate.objects.jobs_as_html(Job.objects.latest())
            Candidate.objects.send_email_latest_jobs_contact(
                email, contact, jobs_as_html
            )
            self.stdout.write("email sent to {}".format(email))
        transaction.on_commit(lambda: process_mail.send())
        self.stdout.write("{} - Complete".format(self.help))
