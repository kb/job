# -*- encoding: utf-8 -*-

from django.core.management.base import BaseCommand

from job.models import JobSettings, JobFeed

from job.service import get_or_create_recruiter


class Command(BaseCommand):
    job_settings = JobSettings.load()

    def get_or_create_feed_recruiter(self, username):
        return get_or_create_recruiter(
            username=username,
            last_name="feed",
            first_name=username,
            email="{}@kbsoftware.co.uk".format(username),
            company_name="{} feed".format(username).title(),
        )

    def create_job_feed(self, username, slug):
        print("Creating feed for username:", username, "slug:", slug)

        recruiter = self.get_or_create_feed_recruiter(username)

        if recruiter:
            try:
                job_feed = JobFeed.objects.get(slug=slug)
            except:
                job_feed = None

            if job_feed:
                print("ERROR: Feed with slug", slug, "already exists")
            else:
                job_feed = JobFeed(slug=slug, recruiter=recruiter)
                job_feed.save()
                print("Created job feed for username ", username, "slug", slug)
        else:
            print(
                "ERROR: Recruiter with username '"
                + username
                + "' does not exist"
            )

    def add_arguments(self, parser):
        parser.add_argument(
            "--username", help="username of recruiter providing the feed"
        )

        parser.add_argument("--slug", help="slug for the feed")

    def handle(self, *args, **options):
        feed_username = options["username"]
        feed_slug = options["slug"]

        if feed_slug is None and feed_username:
            feed_slug = feed_username

        if feed_slug and feed_username:
            self.create_job_feed(feed_username, feed_slug)
        else:
            print()
            print(
                "ERROR: You must specify a username (--username USERNAME)"
                "and if different, a slug (--slug SLUG)"
            )
            print()
