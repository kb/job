# -*- encoding: utf-8 -*-
from decimal import Decimal

from django.core.management.base import BaseCommand

from job.models import PRODUCT_JOB_POST_SUBSCRIPTION
from stock.models import Product, ProductCategory


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Product - Job Post Subscription #1069")
        product_category = ProductCategory.objects.get(slug="recruitment")
        Product.objects.init_product(
            PRODUCT_JOB_POST_SUBSCRIPTION,
            "Job post subscription",
            "Post unlimited jobs for one year",
            Decimal("1000.00"),
            product_category,
        )
