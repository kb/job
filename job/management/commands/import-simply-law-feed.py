# -*- encoding: utf-8 -*-
"""

Import jobs from the Simply Law Jobs feed.

- Using the ``SimplyLawJsonJobFeed`` class

For testing purposes, I deleted all imported jobs using the following code::


"""
# import prettyprinter

from django.core.management.base import BaseCommand

from job.models import (
    JobFeed,
    JobFeedItem,
    JobRole,
    JobRoleKeyword,
    JobRoleParser,
)
from job.service import SimplyLawJsonJobFeed


class Command(BaseCommand):
    help = "Import Simply Law Jobs"

    def _configure_feed(self):
        job_feed = JobFeed.objects.get(slug=JobFeed.SIMPLY_LAW)
        # Original values:
        # url='https://www.simplylawjobs.com/api/job_feed.json',
        # password='ilspa2018',
        # username='slj_ILSPA',
        #
        # version 1
        #
        # Update: 25/02/2023
        # url=https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json

        job_feed.identifier = "institute_legal_secretaries_all_jobs"
        job_feed.password = ""
        job_feed.username = ""
        # job_feed.url = "https://www.simplylawjobs.com/api/job_feed.json"
        job_feed.url = (
            "https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json"
        )
        job_feed.save()
        # prettyprinter.cpprint(job_feed)

    def _debug_parser(self):
        job_role_parser = JobRoleParser()
        print(job_role_parser.data())
        # prettyprinter.cpprint(job_role_parser.data())
        # prettyprinter.cpprint(
        #     job_role_parser.parse(
        #         "Company Commercial (Life Sciences) - Cambridge"
        #     )
        # )

    def _import(self):
        self.stdout.write("Get the 'JobFeed'")
        job_feed = JobFeed.objects.get(slug=JobFeed.SIMPLY_LAW)
        self.stdout.write("Delete previous records...")
        JobFeedItem.objects.filter(feed=job_feed).delete()
        self.stdout.write(
            "Create 'StandardJsonJobFeed' for '{}'".format(JobFeed.SIMPLY_LAW)
        )
        feed = SimplyLawJsonJobFeed(JobFeed.SIMPLY_LAW)
        self.stdout.write("do_import...")
        count = feed.do_import(verbose=True)
        return count

    def _reconfigure_parser(self):
        self.stdout.write("Update Job Role Keywords #5679")
        count = JobRoleKeyword.objects.filter(word="pa").update(word=" pa ")
        self.stdout.write("1. Update 'pa' to ' pa ': {}".format(count))
        count = JobRoleKeyword.objects.filter(word="assistant").delete()
        self.stdout.write("2. Remove 'assistant': {}".format(count))
        count = JobRoleKeyword.objects.filter(word="paralegal").delete()
        self.stdout.write("3. Remove 'paralegal': {}".format(count))
        count = JobRoleKeyword.objects.filter(word="executive").delete()
        self.stdout.write("4. Remove 'executive': {}".format(count))

    def handle(self, *args, **options):
        count = 0
        # prettyprinter.prettyprinter.install_extras(["attrs", "django"])
        self.stdout.write("{}".format(self.help))
        self._configure_feed()
        # self._debug_parser()
        # self._reconfigure_parser()
        count = self._import()
        self.stdout.write(
            "{} - Complete.  Published {} jobs".format(self.help, count)
        )
