# -*- encoding: utf-8 -*-
import csv
import os.path

from django.core.management.base import BaseCommand

from job.models import Country


class Command(BaseCommand):
    def get_country(self, country_code):
        try:
            if len(country_code) == 2:
                country = Country.objects.get(alpha2_code=country_code)
            else:
                country = Country.objects.get(country_code=country_code)

        except Country.DoesNotExist:
            country = None

        return country

    def import_country_csv(self, file_name):
        csv.register_dialect(
            "default", skipinitialspace=True, quoting=csv.QUOTE_ALL
        )
        reader = csv.reader(open(file_name), "default")
        for row in reader:
            if row[0] != "code":
                country_values = {}
                country_code = row[0]
                country_values["alpha2_code"] = row[1]
                country_values["numeric_code"] = row[2]
                country_values["name"] = row[3]
                country_values["supranational"] = self.get_country(row[4])

                country_tuple = Country.objects.get_or_create(
                    country_code=country_code, defaults=country_values
                )
                country = country_tuple[0]

                print("\n{}".format("-" * 70))
                print("country code  : {}".format(country.country_code))
                print("alpha2 code   : {}".format(country.alpha2_code))
                print("numeric code  : {}".format(country.numeric_code))
                print("name          : {}".format(country.name))
                print("supranational : {}".format(country.supranational))
                print("CREATED       : {}".format(country_tuple[1]))

    def add_arguments(self, parser):
        parser.add_argument("--path", help="path to the 'uk-country.csv' file")

    def handle(self, *args, **options):
        file_name = options["path"]
        if os.path.isfile(file_name):
            self.import_country_csv(file_name)
        else:
            print("\n{} does not exist\n".format(file_name))
