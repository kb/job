# -*- encoding: utf-8 -*-
from decimal import Decimal

from django.core.management.base import BaseCommand

from job.models import Location


class Command(BaseCommand):
    def handle(self, *args, **options):
        lat = None
        lng = None
        miles = None

        for arg in args:
            if not lat:
                lat = Decimal(float(arg))
            elif not lng:
                lng = Decimal(float(arg))
            elif not miles:
                miles = Decimal(float(arg))

        for option in options:
            # print("option: {} {}".format(option, options[option]))
            if option == "latitude":
                lat = Decimal(float(options[option]))
            elif option == "longitude":
                lng = Decimal(float(options[option]))
            elif option == "miles":
                miles = Decimal(float(options[option]))

        if lat and lng:
            location = Location.objects.nearest_location(lat, 0 - lng)
            print("Location is ", location)

            if miles:
                locations = Location.objects.locations_near(location, miles)
                print(
                    "Locations within {} miles of {}:".format(miles, location)
                )
                for l in locations.order_by("postcode"):
                    print("({}, {}) {}".format(l.latitude, l.longitude, l))
        else:
            print("\nNo latitude and longitude specified\n")
            print("Usage:")
            print(
                "\tdjango-admin check_location <latitude> <longitude (enter "
                "as a positive will be negated)> [<miles>]\n"
            )
