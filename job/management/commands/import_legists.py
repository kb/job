# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from job.tasks import process_feed_legists


class Command(BaseCommand):
    help = "Import Legists Jobs"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = process_feed_legists()
        self.stdout.write(
            "{} - Complete.  Published {} jobs".format(self.help, count)
        )
