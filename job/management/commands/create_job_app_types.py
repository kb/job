# -*- encoding: utf-8 -*-
from decimal import Decimal

from django.core.management.base import BaseCommand
from django.utils.text import slugify

from job.models import (
    BusinessType,
    HighestEducation,
    # JobClassification,
    # JobType,
    YearsExperience,
)
from job.models import PRODUCT_JOB_POST, PRODUCT_JOB_POST_DISCOUNT
from stock.models import Product, ProductCategory, ProductType


class Command(BaseCommand):
    def create_years_experience(self, slug, title, order):
        slug = slugify(slug)
        try:
            yrs_exp = YearsExperience.objects.get(slug=slug)
        except YearsExperience.DoesNotExist:
            yrs_exp = YearsExperience(slug=slug)
        if yrs_exp.title != title or yrs_exp.order != order:
            print("    - Creating / updating", title)
            yrs_exp.title = title
            yrs_exp.order = order
            yrs_exp.save()
        else:
            print("    - '" + title + "' Already Exists")

    def create_highest_education(self, slug, title, order):
        slug = slugify(slug)
        try:
            high_educ = HighestEducation.objects.get(slug=slug)
        except HighestEducation.DoesNotExist:
            high_educ = HighestEducation(slug=slug)
        if high_educ.title != title or high_educ.order != order:
            print("    - Creating / updating", title)
            high_educ.title = title
            high_educ.order = order
            high_educ.save()
        else:
            print("    - '" + title + "' Already Exists")

    def create_business_type(self, slug, title):
        slug = slugify(slug)
        try:
            bus_type = BusinessType.objects.get(slug=slug)
        except BusinessType.DoesNotExist:
            bus_type = BusinessType(slug=slug)

        if bus_type.title != title:
            print("    - Creating", title)
            bus_type.title = title
            bus_type.save()
        else:
            print("    - '" + title + "' Already Exists")

    # def create_job_classification(self, slug, title, feed_order):
    #     slug = slugify(slug)
    #     try:
    #         job_class = JobClassification.objects.get(slug=slug)
    #     except JobClassification.DoesNotExist:
    #         job_class = JobClassification(slug=slug)
    #     update = False
    #     if job_class.title != title:
    #         print("    - Creating", title)
    #         job_class.title = title
    #         update = True
    #     if job_class.feed_order != feed_order:
    #         job_class.feed_order = feed_order
    #         update = True
    #     if update:
    #         job_class.save()
    #     else:
    #         print("    - '" + title + "' Already Exists")

    # def create_job_type(self, slug, title):
    #     slug = slugify(slug)
    #     try:
    #         job_type = JobType.objects.get(slug=slug)
    #     except JobType.DoesNotExist:
    #         job_type = JobType(slug=slug)
    #     if job_type.title != title:
    #         print("    - Creating", title)
    #         job_type.title = title
    #         job_type.save()
    #     else:
    #         print("    - '" + title + "' Already Exists")

    def configure_job_payment(self):
        print("Ensuring Recruitment Product type exists...")
        product_type = ProductType.objects.init_product_type(
            "recruitment", "Recruitment"
        )
        print("Ensuring Recruitment Product category exists...")
        product_category = ProductCategory.objects.init_product_category(
            "recruitment", "Recruitment", product_type
        )
        print("Ensuring Recruitment Products exists...")
        # NB init_product do not update only created - use the user
        # interface to update
        Product.objects.init_product(
            PRODUCT_JOB_POST,
            "Job post",
            "Post a job",
            Decimal("150.00"),
            product_category,
        )
        Product.objects.init_product(
            PRODUCT_JOB_POST_DISCOUNT,
            "Job post (Discount)",
            "Post a job (discounted price)",
            Decimal("100.00"),
            product_category,
        )

    def configure_job_app_types(self):
        print("Creating Years Experience Types...")
        self.create_years_experience("00-02-yrs", "0 - 2 years", 0)
        self.create_years_experience("03-06-yrs", "3 - 6 years", 1)
        self.create_years_experience("07-10-yrs", "7 - 10 years", 2)
        self.create_years_experience("11-plus-yrs", "11+ years", 3)
        print("Creating Highest Education Types...")
        self.create_highest_education("01-o-level", "GCSE / O Level", 0)
        self.create_highest_education("02-gnvq", "GNVQ / NVQ", 1)
        self.create_highest_education("03-as-level", "AS Level", 2)
        self.create_highest_education("04-a-level", "A Level", 3)
        self.create_highest_education(
            "05-hec", "Higher Education Certificate", 4
        )
        self.create_highest_education("06-hed", "Higher Education Diploma", 5)
        self.create_highest_education(
            "07-ilspa", "ILSPA's Legal Secretaries Diploma", 6
        )
        self.create_highest_education(
            "08-other-sec", "Other Legal Secretary Qualification", 7
        )
        self.create_highest_education("09-foundation", "Foundation Degree", 8)
        self.create_highest_education("10-bachelor", "Bachelors Degree", 9)
        self.create_highest_education(
            "11-bachelor-hons", "Bachelors Degree with Honours", 10
        )
        self.create_highest_education("12-masters", "Masters Degree", 11)
        self.create_highest_education("13-doctorate", "Doctorate", 12)
        print("Creating Business Types...")
        self.create_business_type("government", "Government Institution")
        self.create_business_type("individual", "Individual")
        self.create_business_type("law-firm", "Law Firm")
        self.create_business_type("private", "Private Organisation")
        self.create_business_type("recruitment", "Recruitment / Consulting")

        # print("Creating Job Types...")
        # self.create_job_type('contract', "Contract")
        # self.create_job_type('part-time', "Part-time")
        # self.create_job_type('permanent', "Permanent")
        # self.create_job_type('temporary', "Temporary")

        # print("Creating Job Classifications...")
        # self.create_job_classification(
        #     'commercial', "Commercial Property", 1)
        # self.create_job_classification(
        #     'corporate', "Commercial/Corporate", 1)
        # self.create_job_classification('conveyancing', "Conveyancing", 1)
        # self.create_job_classification('criminal', "Criminal", 1)
        # self.create_job_classification('employment', "Employment", 1)
        # self.create_job_classification('family', "Family", 1)
        # self.create_job_classification('float', "Float", 1)
        # self.create_job_classification('junior', "Junior", 1)
        # self.create_job_classification('litigation', "Litigation", 1)
        # self.create_job_classification('other', "Other", 9)
        # self.create_job_classification('personal', "Personal Injury", 1)
        # self.create_job_classification('probate', "Probate", 1)
        # self.create_job_classification(
        #     'training', "Training Opportunities", 1)

        print("Creating product Information...")
        self.configure_job_payment()
        print("Done")

    def handle(self, *args, **options):
        self.configure_job_app_types()
