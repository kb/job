# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from job.tasks import process_feed_simply_law_jobs


class Command(BaseCommand):
    help = "Process Simply Law Jobs #7517"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        process_feed_simply_law_jobs()
        self.stdout.write(f"{self.help} - Complete")
