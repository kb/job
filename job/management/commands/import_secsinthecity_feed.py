# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from job.service import StandardXMLJobFeed


class Command(BaseCommand):
    def handle(self, *args, **options):
        feed = StandardXMLJobFeed("secsinthecity")
        feed.do_import(verbose=True)
