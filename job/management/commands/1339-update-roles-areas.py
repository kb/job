# -*- encoding: utf-8 -*-
import csv
from django.core.management.base import BaseCommand
from job.models import (
    Job,
    JobRole,
    JobRoleKeyword,
    JobRoleParser,
    PracticeArea,
    PracticeAreaKeyword,
    PracticeAreaParser,
)


practice_areas = [
    {"name": "Conveyancing", "keywords": ["convey"], "default": False},
    {
        "name": "Corporate/Commercial",
        "keywords": [
            "accountant",
            "accounts",
            "asset",
            "audit",
            "bank",
            "banking",
            "billing",
            "business",
            "cashier",
            "commercial",
            "company",
            "competition",
            "compliance",
            "contract",
            "corporate",
            "corporation",
            "credit",
            "fiduciary",
            "finance",
            "financial",
        ],
        "default": False,
    },
    {
        "name": "Criminal",
        "keywords": ["crime", "criminal", "prosecution"],
        "default": False,
    },
    {
        "name": "Employment",
        "keywords": ["career", "employ", "human resource", "hr", "pension"],
        "default": False,
    },
    {
        "name": "Family",
        "keywords": ["child", "family", "matrimonial", "parent"],
        "default": False,
    },
    {
        "name": "Litigation",
        "keywords": ["litigate", "litigation", "litigator", "negligence"],
        "default": False,
    },
    {
        "name": "Property",
        "keywords": [
            "construction",
            "estate agent",
            "estate agency",
            "land",
            "property",
            "real estate",
            "residential",
        ],
        "default": False,
    },
    {
        "name": "Wills & Probate",
        "keywords": ["inheritance", "legacy", "probate", "testament"],
        "default": False,
    },
    {
        "name": "Personal Injury",
        "keywords": ["personal", "injury"],
        "default": False,
    },
    {
        "name": "Immigration",
        "keywords": ["immigration", "refugee"],
        "default": False,
    },
    {"name": "Contract", "keywords": ["contract", "tort"], "default": False},
    {
        "name": "Banking & Finance",
        "keywords": ["banking", "finance"],
        "default": False,
    },
    {
        "name": "Private Client",
        "keywords": ["private client"],
        "default": False,
    },
    {"name": "Float", "keywords": ["float"], "default": False},
    {"name": "Other", "keywords": [], "default": True},
]


job_roles = [
    {
        "name": "Legal Secretary",
        "keywords": ["assistant", "paralegal", "secret", "typing", "typist"],
        "default": False,
    },
    {
        "name": "Legal PA",
        "keywords": ["executive", "pa", "personal assistant"],
        "default": False,
    },
    {
        "name": "Legal Administrator",
        "keywords": ["legal admin"],
        "default": False,
    },
]


class Command(BaseCommand):
    def create_practice_area(self, order, title, default, keywords):
        print("Adding Practice Area:", title)
        updated = False
        try:
            practice_area = PracticeArea.objects.get(title=title)
        except PracticeArea.DoesNotExist:
            practice_area = PracticeArea(title=title)
            updated = True

        if practice_area.default != default:
            practice_area.default = default
            updated = True
        if practice_area.order != order:
            practice_area.order = order
            updated = True
        if updated:
            practice_area.save()
        else:
            print("    - '" + title + "' Already Exists")

        for keyword in keywords:
            print(
                "Adding Practice Area Keyword: {} {}".format(
                    practice_area, keyword
                )
            )
            try:
                PracticeAreaKeyword.objects.get(
                    practice_area=practice_area, word=keyword
                )
            except PracticeAreaKeyword.DoesNotExist:
                keyword = PracticeAreaKeyword(
                    practice_area=practice_area, word=keyword
                )
                keyword.save()

        return practice_area

    def create_job_role(self, order, title, default, keywords):
        print("Adding Job Role:", title)
        updated = False
        try:
            job_role = JobRole.objects.get(title=title)
        except JobRole.DoesNotExist:
            job_role = JobRole(title=title)
            updated = True

        if job_role.default != default:
            job_role.default = default
            updated = True
        if job_role.order != order:
            job_role.order = order
            updated = True
        if updated:
            job_role.save()
        else:
            print("    - '" + title + "' Already Exists")

        for keyword in keywords:
            print("Adding Job Role Keyword: {} {}".format(job_role, keyword))
            try:
                JobRoleKeyword.objects.get(job_role=job_role, word=keyword)
            except JobRoleKeyword.DoesNotExist:
                keyword = JobRoleKeyword(job_role=job_role, word=keyword)
                keyword.save()

        return job_role

    def create_jobs_and_roles(self):
        print("Adding Job Roles...")
        index = 1
        for role in job_roles:
            self.create_job_role(
                index, role["name"], role["default"], role["keywords"]
            )
            index += 1

        print("Adding Practice Areas...")
        index = 1
        for practice_area in practice_areas:
            self.create_practice_area(
                index,
                practice_area["name"],
                practice_area["default"],
                practice_area["keywords"],
            )
            index += 1
        print("Done")

    def output_csv(self, csv_writer, job, areas, roles):
        csv_writer.writerow(
            (
                job.title,
                job.description,
                job.job_role,
                job.practice_area,
                ", ".join(typ.title for typ in roles),
                ", ".join(cat.title for cat in areas),
            )
        )

    def update_roles_and_areas(self):
        job_role_parser = JobRoleParser()
        practice_area_parser = PracticeAreaParser()

        job_role_parser.return_one = False
        practice_area_parser.return_one = False

        for order_key in [1, 2]:
            file_name = "/tmp/job-roles-areas-update-{}.csv".format(order_key)
            with open(file_name, "w", newline="") as csv_file:
                csv_writer = csv.writer(csv_file, dialect="excel")
                csv_writer.writerow(
                    (
                        "Title",
                        "Description",
                        "Job Role",
                        "Practice Area",
                        "Possible Job Roles",
                        "Possible Practice Areas",
                    )
                )
                for job in Job.objects.filter(order_key=order_key):
                    job_updated = False
                    areas = practice_area_parser.parse(
                        job.title + " " + job.description
                    )
                    if areas and not job.practice_area:
                        job.practice_area = areas[0]
                        job_updated = True
                    roles = job_role_parser.parse(job.title)
                    if roles and not job.job_role:
                        job.job_role = roles[0]
                        job_updated = True
                    if job_updated:
                        job.save()
                    self.output_csv(csv_writer, job, areas, roles)

    def handle(self, *args, **options):
        self.create_jobs_and_roles()
        self.update_roles_and_areas()
