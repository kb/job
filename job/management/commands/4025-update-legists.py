# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from job.models import JobFeed, JobFeedItem, FEED_RECRUITER


class Command(BaseCommand):
    help = "Update Legists Jobs Board Feed #4025"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = 0
        feed = JobFeed.objects.get(slug=JobFeed.LEGISTS)
        qs = JobFeedItem.objects.filter(feed=feed)
        for x in qs:
            x.job.order_key = FEED_RECRUITER
            x.job.save()
            count = count + 1
        self.stdout.write(
            "{} (updated {} jobs) - Complete".format(self.help, count)
        )
