# -*- encoding: utf-8 -*-
import sys
import csv
import os.path
from optparse import make_option
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

# from base.form_utils import bleach_clean
from bs4 import BeautifulSoup

from job.models import (
    BusinessType,
    Job,
    JobImportData,
    Location,
    RecruiterImportData,
    IMPORT_CREATE_NEW,
    ILSPA_RECRUITER,
)

from job.service import convert_datetime


def convert_username(orig):
    conv = orig.split("@")
    return conv[0]


def convert_business_type(bus_type_str):
    bus_type = bus_type_str.strip()

    if bus_type == "G":
        return BusinessType.objects.get(slug="government")
    elif bus_type == "I":
        return BusinessType.objects.get(slug="individual")
    elif bus_type == "L":
        return BusinessType.objects.get(slug="law-firm")
    elif bus_type == "P":
        return BusinessType.objects.get(slug="private")
    elif bus_type == "R":
        return BusinessType.objects.get(slug="recruitment")


def convert_url(site_address):
    if site_address and not site_address.startswith("http"):
        return "http://" + site_address

    return site_address


class Command(BaseCommand):
    def import_jobs_csv(self, file_name):
        csv.register_dialect(
            "default", skipinitialspace=True, quoting=csv.QUOTE_ALL
        )
        reader = csv.reader(open(file_name, encoding="latin1"), "default")
        row_count = 0
        ignore_count = 0
        import_count = 0
        recruiter_found_count = 0
        cut_off_date = datetime(2015, 5, 1, 0, 0)
        for row in reader:
            # print (row)
            # post_id,post_date,post_mode,user_id,pin_x,pin_y,approved,Job Title,Description,Classification,Posted By,Salary,Contact Telephone,Area,Job Type,Location,Deadline,applications,hits,reason,Start Date,Job Function,Email,guid,source,cached_summary,expired,app_type,app_url,Job Reference,Contact URL,Contact Name

            if row[0] != "post_id":
                row_count = row_count + 1
                post_date = convert_datetime(row[1])
                if post_date < cut_off_date:
                    print(
                        "Y -------------------------------------------------------------------"
                    )
                    print(
                        "Ignoring {} {} post date ({}) is less than {}".format(
                            row[0],
                            row[7],
                            post_date.date(),
                            cut_off_date.date(),
                        )
                    )
                    ignore_count = ignore_count + 1
                    continue
                if row[15] == "Texas":
                    print(
                        "Y -------------------------------------------------------------------"
                    )
                    print(
                        "Ignoring {} {} not a valid job post - location 'Texas'".format(
                            row[0], row[7]
                        )
                    )
                    ignore_count = ignore_count + 1
                    continue

                print(
                    "Z -------------------------------------------------------------------"
                )
                for idx, num in enumerate(range(0, len(row))):
                    print(
                        "Z{:3} - {:<20} : {:>6} : {}".format(
                            idx,
                            field_name_row[num][:20],
                            len(row[num]),
                            row[num],
                        )
                    )

                job_data = None

                try:
                    # just in case we have run the import before
                    job_data = JobImportData.objects.get(post_id=row[0])
                    print(
                        "Found a previous job import record ({}) - overwriting".format(
                            row[0]
                        )
                    )
                except:
                    job_data = JobImportData(post_id=row[0])
                    job_data.import_status = IMPORT_CREATE_NEW

                job_data.post_date = row[1]
                job_data.post_mode = row[2]
                job_data.user_id = row[3]
                job_data.pin_x = row[4]
                job_data.pin_y = row[5]
                job_data.approved = row[6]
                job_data.JobTitle = row[7]
                job_data.Description = row[8]
                job_data.Classification = row[9]
                job_data.PostedBy = row[10]
                job_data.Salary = row[11]
                job_data.ContactTelephone = row[12]
                job_data.Area = row[13]
                job_data.JobType = row[14]
                job_data.Location = row[15]
                job_data.Deadline = row[16]
                job_data.applications = row[17]
                job_data.hits = row[18]
                job_data.reason = row[18]
                job_data.StartDate = row[20]
                job_data.JobFunction = row[21]
                job_data.Email = row[22]
                job_data.guid = row[23]
                job_data.source = row[24]
                job_data.cached_summary = row[25]
                job_data.expired = row[26]
                job_data.app_type = row[27]
                job_data.app_url = row[28]
                job_data.JobReference = row[29]
                job_data.ContactURL = row[30]
                job_data.ContactName = row[31]

                try:
                    recruiter_data = RecruiterImportData.objects.get(
                        user_id=job_data.user_id
                    )
                    recruiter = recruiter_data.recruiter
                    recruiter_found_count = recruiter_found_count + 1
                except:
                    print(
                        "ERROR: RECRUITER DOES NOT EXIST user_id = {}".format(
                            job_data.user_id
                        )
                    )
                    continue

                try:
                    if job_data.job:
                        job = job_data.job
                    elif recruiter:
                        job = Job(recruiter=recruiter)
                    else:
                        raise ValueError("No Recruiter")
                except:
                    print("ERROR: CANNOT CREATE Job")
                    continue

                job.title = job_data.JobTitle
                desc = BeautifulSoup(job_data.Description, "html.parser")
                spans = desc.find_all("span")
                paras = desc.find_all("p")
                divs = desc.find_all("div")
                strongs = desc.find_all("strong")
                lis = desc.find_all("li")
                uls = desc.find_all("ul")

                for tag in spans + paras + divs + strongs + lis + uls:
                    try:
                        if tag["style"]:
                            # tag['style'] = None
                            del tag["style"]
                    except KeyError:
                        pass
                    except:
                        e = sys.exc_info()[0]
                        print("ERROR PROCESSING TAG", tag, ":", e)

                job.description = desc.prettify()
                job.salary = job_data.Salary
                job.order_key = ILSPA_RECRUITER

                locations = None
                loc_str = job_data.Location.split(",")[0]
                locations = Location.objects.filter(town__contains=loc_str)
                if locations:
                    print("Found Town")
                else:
                    loc_str = loc_str.split(" ")[0]

                    locations = Location.objects.filter(region__name=loc_str)
                    if locations:
                        print("Found County")
                    else:
                        print("NONE", job_data.Location)

                if not locations:
                    if "Central London" == job_data.Location:
                        locations = Location.objects.filter(postcode="EC1")
                    if "north London" == job_data.Location:
                        locations = Location.objects.filter(postcode="N1")
                    if "South West London" == job_data.Location:
                        locations = Location.objects.filter(postcode="SW1")
                    elif "Blackheath" == job_data.Location:
                        locations = Location.objects.filter(postcode="SE3")
                    elif "Mayfair, London" == job_data.Location:
                        locations = Location.objects.filter(postcode="W1K")
                    elif "Gatwick" == job_data.Location:
                        locations = Location.objects.filter(postcode="RH6")
                    elif "Ukfield" == job_data.Location:
                        # assume it's Uckfield
                        locations = Location.objects.filter(postcode="TN22")
                    elif "Daventry" == job_data.Location:
                        locations = Location.objects.filter(postcode="NN11")
                    elif "Eton" == job_data.Location:
                        locations = Location.objects.filter(postcode="SL4")
                    elif "Weybridge, Surrey" == job_data.Location:
                        locations = Location.objects.filter(postcode="KT13")
                    elif "Stratton on the Fosse" == job_data.Location:
                        locations = Location.objects.filter(postcode="BA3")
                    elif "Hove" == job_data.Location:
                        locations = Location.objects.filter(postcode="BN3")

                if locations.count() > 0:
                    loc_str = ""

                    for location in locations:
                        if loc_str:
                            loc_str = loc_str + "," + str(location)
                        else:
                            loc_str = str(location)

                    loc = locations[0]

                    pc_prefix = loc.postcode[:2]

                    if len(pc_prefix) == 2 and pc_prefix[1].isdigit():
                        pc_prefix = pc_prefix[0]

                    pc = pc_prefix + "1"

                    try:
                        location_to_use = locations.get(postcode=pc)
                    except:
                        location_to_use = locations[0]

                    job.location = location_to_use

                    print(
                        "X {} Location ({}) chose {} found: {}".format(
                            job_data.post_id,
                            job_data.Location,
                            location_to_use,
                            loc_str,
                        )
                    )
                    # for location in locations:
                    #    print ("X    {}".format(location))
                else:
                    print(
                        "X No locations found for {} ({})".format(
                            job_data.post_id, job_data.Location
                        )
                    )

                job.location = location_to_use
                job.publish = True
                job.publish_date = post_date
                close_date = convert_datetime(job_data.Deadline)

                if close_date:
                    job.close_date = convert_datetime(job_data.post_date)
                else:
                    job.close_date = post_date + timedelta(days=42)

                job.save()
                job_data.job = job
                job_data.save()
                recruiter.update_search_expires(job.publish_date)

                import_count = import_count + 1
            else:
                # flakes - 'field_name_row' is assigned to but never used
                field_name_row = row[:]
        print(
            "Imported {} records - of {} records (recruiter found {}) ignored {} old jobs".format(
                import_count, row_count, recruiter_found_count, ignore_count
            )
        )

    option_list = BaseCommand.option_list + (
        make_option(
            "--login",
            action="store_true",
            dest="login",
            default=False,
            help="Import recruiter login data",
        ),
        make_option(
            "--profile",
            action="store_true",
            dest="profile",
            default=False,
            help="Import recruiter profile data",
        ),
    )

    def handle(self, *args, **options):
        file_name = ""

        for arg in args:
            file_name = arg

        if file_name and os.path.isfile(file_name):
            self.import_jobs_csv(file_name)
        else:
            if file_name:
                print("\n{} does not exist\n".format(file_name))
            else:
                print("\nNo file name specified\n")
            print("Usage:")
            print("\tdjango-admin import_jobs <file name>\n")
