# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import job.models


def _init_experience(model, slug, title, order):
    try:
        model.objects.get(slug=slug)
    except model.DoesNotExist:
        instance = model(**dict(title=title, slug=slug, order=order))
        instance.save()
        instance.full_clean()
    return model.objects.get(slug=slug)


def default_experience(apps, schema_editor):
    _init_experience(
        apps.get_model("job", "Experience"), "default", "Stated on CV", 1001
    )


class Migration(migrations.Migration):
    dependencies = [("job", "0021_auto_20170322_1533")]

    operations = [
        migrations.RunPython(default_experience),
        migrations.AlterModelOptions(
            name="experience",
            options={
                "ordering": ("order", "slug"),
                "verbose_name": "Experience",
            },
        ),
        migrations.AlterField(
            model_name="candidate",
            name="job_experience",
            field=models.ForeignKey(
                default=job.models._get_default_experience,
                help_text="Experience",
                on_delete=django.db.models.deletion.CASCADE,
                to="job.Experience",
            ),
        ),
    ]
