# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0010_remove_jobbasket_payment_state")]

    operations = [
        migrations.AlterField(
            model_name="candidatejoblocation",
            name="candidate",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.Candidate",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="candidatejoblocation",
            name="location",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.Location",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="jobapplication",
            name="candidate",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.Candidate",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="jobapplication",
            name="job",
            field=models.ForeignKey(
                blank=True, null=True, to="job.Job", on_delete=models.CASCADE
            ),
        ),
        migrations.AlterField(
            model_name="jobfeed",
            name="recruiter",
            field=models.OneToOneField(
                blank=True,
                null=True,
                to="job.Recruiter",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="jobfeeditem",
            name="feed",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.JobFeed",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="jobfeeditem",
            name="job",
            field=models.OneToOneField(
                blank=True, null=True, to="job.Job", on_delete=models.CASCADE
            ),
        ),
        migrations.AlterField(
            model_name="jobtracking",
            name="candidate",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.Candidate",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="jobtracking",
            name="job",
            field=models.ForeignKey(
                blank=True, null=True, to="job.Job", on_delete=models.CASCADE
            ),
        ),
        migrations.AlterField(
            model_name="location",
            name="country",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.Country",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="region",
            name="country",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.Country",
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name="region",
            name="region_type",
            field=models.ForeignKey(
                blank=True,
                null=True,
                to="job.RegionType",
                on_delete=models.CASCADE,
            ),
        ),
    ]
