# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("job", "0014_auto_20160311_1503"),
    ]

    operations = [
        migrations.AddField(
            model_name="job",
            name="date_deleted",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="job",
            name="user_deleted",
            field=models.ForeignKey(
                related_name="+",
                to=settings.AUTH_USER_MODEL,
                blank=True,
                null=True,
                on_delete=models.CASCADE,
            ),
        ),
    ]
