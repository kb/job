# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0003_auto_20150615_1614")]

    operations = [
        migrations.AlterModelOptions(
            name="jobfeed",
            options={
                "verbose_name_plural": "Job Feeds",
                "ordering": ("recruiter__contact__company_name",),
                "verbose_name": "Job Feed",
            },
        ),
        migrations.AlterField(
            model_name="candidate",
            name="picture",
            field=models.ImageField(
                blank=True, upload_to="job/candidate/images"
            ),
        ),
        migrations.AlterField(
            model_name="job",
            name="picture",
            field=models.ImageField(blank=True, upload_to="job/images"),
        ),
    ]
