# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import models, migrations

# import pay.models
from decimal import Decimal
import django.core.files.storage


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.CONTACT_MODEL),
        ("stock", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="BusinessType",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("slug", models.SlugField(max_length=20)),
                ("title", models.CharField(max_length=50)),
            ],
            options={
                "verbose_name": "Business Type",
                "ordering": ("slug",),
                "verbose_name_plural": "Business Types",
            },
        ),
        migrations.CreateModel(
            name="Candidate",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("looking_for_work", models.BooleanField(default=False)),
                ("available_from", models.DateField(blank=True, null=True)),
                ("allow_search", models.BooleanField(default=False)),
                ("skills", models.TextField(blank=True, null=True)),
                ("employment_history", models.TextField(blank=True, null=True)),
                ("notes", models.TextField(blank=True)),
                (
                    "picture",
                    models.ImageField(blank=True, upload_to="candidate"),
                ),
                ("newsletter", models.BooleanField(default=False)),
                ("notify_new_jobs", models.BooleanField(default=False)),
                (
                    "contact",
                    models.OneToOneField(
                        to=settings.CONTACT_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Candidate",
                "ordering": (
                    "contact__user__last_name",
                    "contact__user__first_name",
                ),
                "verbose_name_plural": "Candidates",
            },
        ),
        migrations.CreateModel(
            name="CandidateImportData",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("import_status", models.PositiveIntegerField(default=0)),
                ("user_id", models.CharField(max_length=20)),
                ("IP", models.CharField(max_length=20)),
                ("SignupDate", models.CharField(max_length=20)),
                ("FirstName", models.CharField(max_length=100)),
                ("LastName", models.CharField(max_length=100)),
                ("Username", models.CharField(max_length=50)),
                ("Password", models.CharField(max_length=100)),
                ("Email", models.CharField(max_length=100)),
                ("Newsletter", models.CharField(max_length=20)),
                ("Notification1", models.CharField(max_length=20)),
                ("Notification2", models.CharField(max_length=20)),
                ("Validated", models.CharField(max_length=20)),
                ("login_date", models.CharField(max_length=20)),
                ("logout_date", models.CharField(max_length=20)),
                ("last_request_time", models.CharField(max_length=20)),
                ("login_count", models.CharField(max_length=20)),
                ("alert_keywords", models.CharField(max_length=100)),
                ("alert_last_run", models.CharField(max_length=20)),
                ("alert_email", models.CharField(max_length=100)),
                ("newsletter_last_run", models.CharField(max_length=20)),
                ("lang", models.CharField(max_length=20)),
                ("alert_query", models.CharField(max_length=100)),
                (
                    "resume_id",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "list_on_web",
                    models.CharField(default="", blank=True, max_length=10),
                ),
                (
                    "resume_date",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "hits",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "anon",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "status",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                ("Experience", models.TextField(default="", blank=True)),
                (
                    "DOB",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "Nationality",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "profile_Email",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Mobile",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "Telephone",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "StreetAddress",
                    models.CharField(default="", blank=True, max_length=200),
                ),
                (
                    "Town",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "County",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "Postcode",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "Country",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                ("Skills", models.TextField(default="", blank=True)),
                ("EmploymentHistory", models.TextField(default="", blank=True)),
                (
                    "Available",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "HighestEducation",
                    models.CharField(default="", blank=True, max_length=10),
                ),
                ("notes", models.TextField(default="", blank=True)),
                (
                    "LocationPref",
                    models.CharField(default="", blank=True, max_length=500),
                ),
                (
                    "Positions",
                    models.CharField(default="", blank=True, max_length=500),
                ),
                (
                    "Name",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Photo",
                    models.CharField(default="", blank=True, max_length=200),
                ),
                (
                    "cv_file_name",
                    models.CharField(default="", blank=True, max_length=200),
                ),
                (
                    "candidate",
                    models.ForeignKey(
                        null=True,
                        to="job.Candidate",
                        blank=True,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "Candidate Import Data",
                "ordering": ("candidate",),
            },
        ),
        migrations.CreateModel(
            name="CandidateJobLocation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "candidate",
                    models.ForeignKey(
                        null=True, to="job.Candidate", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Candidate Job Location",
                "ordering": ("candidate", "location"),
                "verbose_name_plural": "Candidates Job Locations",
            },
        ),
        migrations.CreateModel(
            name="Country",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("country_code", models.CharField(max_length=3)),
                ("alpha2_code", models.CharField(max_length=2)),
                ("numeric_code", models.CharField(max_length=3)),
                ("name", models.CharField(max_length=100)),
                (
                    "supranational",
                    models.ForeignKey(
                        null=True,
                        to="job.Country",
                        blank=True,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "Country",
                "ordering": ("country_code",),
                "verbose_name_plural": "Countries",
            },
        ),
        migrations.CreateModel(
            name="CV",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "file_name",
                    models.FileField(
                        upload_to="job/candidate/cv",
                        storage=django.core.files.storage.FileSystemStorage(
                            location="media-private"
                        ),
                    ),
                ),
                ("title", models.CharField(blank=True, max_length=50)),
                (
                    "contact",
                    models.ForeignKey(
                        to=settings.CONTACT_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "CV",
                "ordering": ("created",),
                "verbose_name_plural": "CVs",
            },
        ),
        migrations.CreateModel(
            name="CVType",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("slug", models.SlugField(max_length=20)),
                ("title", models.CharField(max_length=50)),
            ],
            options={
                "verbose_name": "CV Type",
                "ordering": ("slug",),
                "verbose_name_plural": "CV Types",
            },
        ),
        migrations.CreateModel(
            name="HighestEducation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("slug", models.SlugField(max_length=20)),
                ("title", models.CharField(max_length=50)),
                ("order", models.PositiveIntegerField()),
            ],
            options={
                "verbose_name": "Highest Education Level attained",
                "ordering": ("slug",),
            },
        ),
        migrations.CreateModel(
            name="Job",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("title", models.CharField(blank=True, max_length=200)),
                ("salary", models.PositiveIntegerField()),
                ("close_date", models.DateField(help_text="(dd/mm/yyyy)")),
                ("description", models.TextField(blank=True)),
                ("picture", models.ImageField(blank=True, upload_to="job")),
                ("appointed_date", models.DateField(blank=True, null=True)),
                ("appointed_via_board", models.BooleanField(default=False)),
                ("order_key", models.PositiveIntegerField(default=0)),
                ("publish", models.BooleanField(default=False)),
                ("publish_date", models.DateTimeField(blank=True, null=True)),
                ("deleted", models.BooleanField(default=False)),
            ],
            options={
                "verbose_name": "Job",
                "ordering": ("order_key", "title"),
                "verbose_name_plural": "Jobs",
            },
        ),
        migrations.CreateModel(
            name="JobApplication",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("cover", models.TextField(blank=True)),
                ("apply_date", models.DateField(blank=True, null=True)),
                ("interview_date", models.DateField(blank=True, null=True)),
                ("offer_date", models.DateField(blank=True, null=True)),
                ("accepted_date", models.DateField(blank=True, null=True)),
                ("closed_date", models.DateField(blank=True, null=True)),
                (
                    "candidate",
                    models.ForeignKey(
                        null=True, to="job.Candidate", on_delete=models.CASCADE
                    ),
                ),
                (
                    "cv",
                    models.ForeignKey(
                        null=True,
                        to="job.CV",
                        blank=True,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "job",
                    models.ForeignKey(
                        null=True, to="job.Job", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Job Application",
                "ordering": ("job", "candidate"),
                "verbose_name_plural": "Job Applications",
            },
        ),
        migrations.CreateModel(
            name="JobBasket",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                # ('payment_state', models.ForeignKey(default=0, to='pay.PaymentState', on_delete=models.CASCADE)),
            ],
            options={
                "verbose_name": "Job basket",
                "verbose_name_plural": "Job baskets",
            },
        ),
        migrations.CreateModel(
            name="JobBasketItem",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "basket",
                    models.ForeignKey(
                        to="job.JobBasket", on_delete=models.CASCADE
                    ),
                ),
                (
                    "job",
                    models.ForeignKey(to="job.Job", on_delete=models.CASCADE),
                ),
                (
                    "product",
                    models.ForeignKey(
                        to="stock.Product", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Job basket item",
                "ordering": ("basket", "product", "job"),
                "verbose_name_plural": "Job basket items",
            },
        ),
        migrations.CreateModel(
            name="JobClassification",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("slug", models.SlugField(max_length=20)),
                ("title", models.CharField(max_length=50)),
                ("feed_order", models.PositiveIntegerField(default=0)),
            ],
            options={
                "verbose_name": "Job Classification",
                "ordering": ("slug",),
                "verbose_name_plural": "Job Classifications",
            },
        ),
        migrations.CreateModel(
            name="JobFeed",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("slug", models.SlugField(default="", max_length=100)),
            ],
            options={
                "verbose_name": "Job Feed",
                "ordering": ("recruiter__company_name",),
                "verbose_name_plural": "Job Feeds",
            },
        ),
        migrations.CreateModel(
            name="JobFeedItem",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("jobkey", models.CharField(max_length=100)),
                ("company", models.CharField(blank=True, max_length=100)),
                ("source", models.CharField(blank=True, max_length=100)),
                ("url", models.URLField(max_length=500)),
                (
                    "formattedLocation",
                    models.CharField(blank=True, max_length=100),
                ),
                (
                    "formattedLocationFull",
                    models.CharField(blank=True, max_length=100),
                ),
                ("date", models.DateTimeField(blank=True, null=True)),
                ("city", models.CharField(blank=True, max_length=100)),
                ("state", models.CharField(blank=True, max_length=100)),
                ("country", models.CharField(blank=True, max_length=100)),
                (
                    "latitude",
                    models.DecimalField(
                        blank=True, null=True, max_digits=7, decimal_places=5
                    ),
                ),
                (
                    "longitude",
                    models.DecimalField(
                        blank=True, null=True, max_digits=7, decimal_places=5
                    ),
                ),
                (
                    "feed",
                    models.ForeignKey(
                        null=True, to="job.JobFeed", on_delete=models.CASCADE
                    ),
                ),
                (
                    "job",
                    models.OneToOneField(
                        to="job.Job", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Job Feed Item",
                "ordering": ("job__title",),
                "verbose_name_plural": "Job Feed Items",
            },
        ),
        migrations.CreateModel(
            name="JobSettings",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "job_post_allows_days_search",
                    models.PositiveIntegerField(default=42),
                ),
                (
                    "default_latitude",
                    models.DecimalField(
                        default=Decimal("52.47872"),
                        max_digits=7,
                        decimal_places=5,
                    ),
                ),
                (
                    "default_longitude",
                    models.DecimalField(
                        default=Decimal("-1.90723"),
                        max_digits=7,
                        decimal_places=5,
                    ),
                ),
                ("pdf_heading", models.CharField(blank=True, max_length=200)),
            ],
            options={"verbose_name": "Job settings"},
        ),
        migrations.CreateModel(
            name="JobTracking",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("click_date", models.DateField()),
                ("count", models.IntegerField()),
                (
                    "candidate",
                    models.ForeignKey(
                        null=True, to="job.Candidate", on_delete=models.CASCADE
                    ),
                ),
                (
                    "job",
                    models.ForeignKey(
                        null=True, to="job.Job", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Job Application",
                "ordering": ("job", "candidate"),
                "verbose_name_plural": "Job Applications",
            },
        ),
        migrations.CreateModel(
            name="JobType",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("slug", models.SlugField(max_length=20)),
                ("title", models.CharField(max_length=50)),
            ],
            options={
                "verbose_name": "Job Type",
                "ordering": ("slug",),
                "verbose_name_plural": "Job Types",
            },
        ),
        migrations.CreateModel(
            name="Location",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("postcode", models.CharField(max_length=10)),
                ("eastings", models.IntegerField()),
                ("northings", models.IntegerField()),
                (
                    "latitude",
                    models.DecimalField(max_digits=7, decimal_places=5),
                ),
                (
                    "longitude",
                    models.DecimalField(max_digits=7, decimal_places=5),
                ),
                ("town", models.CharField(max_length=200)),
                ("notes", models.CharField(blank=True, max_length=200)),
                (
                    "country",
                    models.ForeignKey(
                        null=True, to="job.Country", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Location",
                "ordering": ("town", "postcode"),
                "verbose_name_plural": "Locations",
            },
        ),
        migrations.CreateModel(
            name="Recruiter",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "picture",
                    models.ImageField(
                        blank=True, upload_to="job/recruiter/images"
                    ),
                ),
                ("profile", models.TextField(blank=True)),
                (
                    "profile_file_name",
                    models.FileField(blank=True, null=True, upload_to=""),
                ),
                ("search_expires", models.DateField(blank=True, null=True)),
                ("notify_application", models.BooleanField(default=False)),
                ("newsletter", models.BooleanField(default=False)),
                (
                    "business_type",
                    models.ForeignKey(
                        null=True,
                        to="job.BusinessType",
                        blank=True,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "contact",
                    models.OneToOneField(
                        to=settings.CONTACT_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Recruiter",
                "ordering": ("contact__company_name",),
                "verbose_name_plural": "Recruiters",
            },
        ),
        migrations.CreateModel(
            name="RecruiterImportData",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("import_status", models.PositiveIntegerField(default=0)),
                (
                    "user_id",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                ("IP", models.CharField(default="", blank=True, max_length=20)),
                (
                    "SignupDate",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "FirstName",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "LastName",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Username",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "Password",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Email",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Newsletter",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "Notification1",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "Notification2",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "Validated",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "CompName",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "login_date",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "logout_date",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "login_count",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "last_request_time",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "lang",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "alert_last_run",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "alert_email",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "posts_balance",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "premium_posts_balance",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "subscription_can_view_resume",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "subscription_can_premium_post",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "subscription_can_post",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "newsletter_last_run",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "alert_query",
                    models.CharField(default="", blank=True, max_length=200),
                ),
                (
                    "can_view_blocked",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "alert_keywords",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "views_quota",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "p_posts_quota",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "posts_quota",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "views_quota_tally",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "p_posts_quota_tally",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "posts_quota_tally",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "quota_timestamp",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "profile_id",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "profile_date",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "expired",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "BusinessName",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Logo",
                    models.CharField(default="", blank=True, max_length=200),
                ),
                (
                    "BusinessType",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "ContactName",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Position",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "WebsiteURL",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "profile_Email",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "Telephone",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "Mobile",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "StreetAddress",
                    models.CharField(default="", blank=True, max_length=200),
                ),
                (
                    "Town",
                    models.CharField(default="", blank=True, max_length=100),
                ),
                (
                    "County",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                (
                    "Postcode",
                    models.CharField(default="", blank=True, max_length=20),
                ),
                (
                    "Country",
                    models.CharField(default="", blank=True, max_length=50),
                ),
                ("About", models.TextField(default="", blank=True)),
                (
                    "file_name",
                    models.CharField(default="", blank=True, max_length=200),
                ),
                (
                    "recruiter",
                    models.ForeignKey(
                        null=True,
                        to="job.Recruiter",
                        blank=True,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "Recruiter Import Data",
                "ordering": ("recruiter",),
            },
        ),
        migrations.CreateModel(
            name="Region",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("region_code", models.CharField(max_length=3)),
                ("name", models.CharField(max_length=100)),
                ("slug", models.SlugField(max_length=100)),
                ("alternate_region_code", models.CharField(max_length=3)),
                ("alternate_name", models.CharField(max_length=100)),
                ("alternate_slug", models.SlugField(max_length=100)),
                (
                    "country",
                    models.ForeignKey(
                        null=True, to="job.Country", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Region",
                "ordering": ("region_code",),
                "verbose_name_plural": "Regions",
            },
        ),
        migrations.CreateModel(
            name="RegionType",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("name", models.CharField(max_length=50)),
                ("slug", models.SlugField()),
            ],
            options={
                "verbose_name": "Region Type",
                "ordering": ("name",),
                "verbose_name_plural": "Region Types",
            },
        ),
        migrations.CreateModel(
            name="YearsExperience",
            fields=[
                (
                    "id",
                    models.AutoField(
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("slug", models.SlugField(max_length=20)),
                ("title", models.CharField(max_length=50)),
                ("order", models.PositiveIntegerField()),
            ],
            options={
                "verbose_name": "Experience as a legal Secretary",
                "ordering": ("slug",),
            },
        ),
        migrations.AddField(
            model_name="region",
            name="region_type",
            field=models.ForeignKey(
                null=True, to="job.RegionType", on_delete=models.CASCADE
            ),
        ),
        migrations.AddField(
            model_name="location",
            name="region",
            field=models.ForeignKey(
                null=True, to="job.Region", blank=True, on_delete=models.CASCADE
            ),
        ),
        migrations.AddField(
            model_name="jobfeed",
            name="recruiter",
            field=models.OneToOneField(
                default=None, to="job.Recruiter", on_delete=models.CASCADE
            ),
        ),
        migrations.AddField(
            model_name="jobbasket",
            name="recruiter",
            field=models.ForeignKey(
                to="job.Recruiter", on_delete=models.CASCADE
            ),
        ),
        migrations.AddField(
            model_name="job",
            name="classification",
            field=models.ForeignKey(
                null=True,
                to="job.JobClassification",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name="job",
            name="job_type",
            field=models.ForeignKey(
                null=True,
                to="job.JobType",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name="job",
            name="location",
            field=models.ForeignKey(
                null=True,
                to="job.Location",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name="job",
            name="recruiter",
            field=models.ForeignKey(
                default=None,
                null=True,
                to="job.Recruiter",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name="cv",
            name="cv_type",
            field=models.ForeignKey(to="job.CVType", on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name="candidatejoblocation",
            name="location",
            field=models.ForeignKey(
                null=True, to="job.Location", on_delete=models.CASCADE
            ),
        ),
        migrations.AddField(
            model_name="candidate",
            name="experience",
            field=models.ForeignKey(
                null=True,
                to="job.YearsExperience",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name="candidate",
            name="highest_education",
            field=models.ForeignKey(
                null=True,
                to="job.HighestEducation",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterUniqueTogether(
            name="jobtracking",
            unique_together=set([("candidate", "job", "click_date")]),
        ),
        migrations.AlterUniqueTogether(
            name="jobbasketitem", unique_together=set([("basket", "job")])
        ),
        migrations.AlterUniqueTogether(
            name="jobapplication", unique_together=set([("candidate", "job")])
        ),
    ]
