# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("job", "0013_auto_20160218_1445")]

    operations = [
        migrations.AlterField(
            model_name="candidate",
            name="allow_search",
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name="candidate",
            name="looking_for_work",
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name="job",
            name="location_text",
            field=models.CharField(
                verbose_name="Location", max_length=200, default=""
            ),
            preserve_default=False,
        ),
    ]
