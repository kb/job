# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def _init_state(model, slug, title):
    try:
        model.objects.get(slug=slug)
    except model.DoesNotExist:
        instance = model(**dict(slug=slug, title=title))
        instance.save()
        instance.full_clean()


def default_state(apps, schema_editor):
    state = apps.get_model("job", "CVType")
    _init_state(state, "member", "Membership Application")
    _init_state(state, "job-profile", "Profile CV")
    _init_state(state, "job-application", "Job Application CV")


class Migration(migrations.Migration):
    dependencies = [("job", "0002_auto_20150529_1941")]

    operations = [migrations.RunPython(default_state)]
