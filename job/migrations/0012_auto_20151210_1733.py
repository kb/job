# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("job", "0011_auto_20151009_1721")]

    operations = [
        migrations.RenameField(
            model_name="jobtracking",
            old_name="count",
            new_name="application_click_count",
        ),
        migrations.AddField(
            model_name="jobtracking",
            name="view_count",
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name="jobtracking",
            name="application_click_count",
            field=models.IntegerField(default=0),
        ),
    ]
