# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("job", "0012_auto_20151210_1733")]

    operations = [
        migrations.CreateModel(
            name="JobRole",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                        verbose_name="ID",
                    ),
                ),
                ("title", models.CharField(max_length=50)),
                ("order", models.PositiveIntegerField(default=1)),
                ("default", models.BooleanField(default=False)),
            ],
            options={
                "verbose_name": "Job Role",
                "ordering": ("order", "title"),
                "verbose_name_plural": "Job Roles",
            },
        ),
        migrations.CreateModel(
            name="JobRoleKeyword",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                        verbose_name="ID",
                    ),
                ),
                ("word", models.CharField(max_length=50)),
                (
                    "job_role",
                    models.ForeignKey(
                        to="job.JobRole", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Job Role Keyword",
                "ordering": ("job_role", "word"),
                "verbose_name_plural": "Job Role Keywords",
            },
        ),
        migrations.CreateModel(
            name="PracticeArea",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                        verbose_name="ID",
                    ),
                ),
                ("title", models.CharField(max_length=50)),
                ("order", models.PositiveIntegerField(default=1)),
                ("default", models.BooleanField(default=False)),
            ],
            options={
                "verbose_name": "Practice Area",
                "ordering": ("order", "title"),
                "verbose_name_plural": "Practice Areas",
            },
        ),
        migrations.CreateModel(
            name="PracticeAreaKeyword",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                        verbose_name="ID",
                    ),
                ),
                ("word", models.CharField(max_length=50)),
                (
                    "practice_area",
                    models.ForeignKey(
                        to="job.PracticeArea", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Practice Area Keyword",
                "ordering": ("practice_area", "word"),
                "verbose_name_plural": "Practice Area Keywords",
            },
        ),
        migrations.RemoveField(model_name="job", name="classification"),
        migrations.RemoveField(model_name="job", name="job_type"),
        migrations.AddField(
            model_name="job",
            name="location_text",
            field=models.CharField(null=True, max_length=200, blank=True),
        ),
        migrations.DeleteModel(name="JobClassification"),
        migrations.DeleteModel(name="JobType"),
        migrations.AddField(
            model_name="job",
            name="job_role",
            field=models.ForeignKey(
                null=True,
                to="job.JobRole",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name="job",
            name="practice_area",
            field=models.ForeignKey(
                null=True,
                to="job.PracticeArea",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
    ]
