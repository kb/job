# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="candidate",
            name="job_location_code",
            field=models.CharField(max_length=50, blank=True, null=True),
        ),
        migrations.AddField(
            model_name="candidate",
            name="job_location_radius",
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name="candidate",
            name="job_location_type",
            field=models.CharField(max_length=50, blank=True, null=True),
        ),
        migrations.AlterUniqueTogether(
            name="candidatejoblocation",
            unique_together=set([("candidate", "location")]),
        ),
    ]
