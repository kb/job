# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0006_jobimportdata")]

    operations = [
        migrations.AddField(
            model_name="recruiter",
            name="subscription_expires",
            field=models.DateField(null=True, blank=True),
        )
    ]
