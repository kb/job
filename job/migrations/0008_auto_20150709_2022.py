# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0007_recruiter_subscription_expires")]

    operations = [
        migrations.AddField(
            model_name="jobsettings",
            name="job_post_max_days_displayed",
            field=models.PositiveIntegerField(default=60),
        ),
        migrations.AlterField(
            model_name="jobsettings",
            name="job_post_allows_days_search",
            field=models.PositiveIntegerField(default=60),
        ),
    ]
