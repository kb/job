# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("job", "0016_auto_20160416_1416"),
    ]

    operations = [
        migrations.AddField(
            model_name="cv",
            name="date_deleted",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="cv",
            name="deleted",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="cv",
            name="user_deleted",
            field=models.ForeignKey(
                to=settings.AUTH_USER_MODEL,
                blank=True,
                related_name="+",
                null=True,
                on_delete=models.CASCADE,
            ),
        ),
    ]
