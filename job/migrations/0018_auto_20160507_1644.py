# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("block", "0011_auto_20151030_1551"),
        ("job", "0017_auto_20160505_1807"),
    ]

    operations = [
        migrations.CreateModel(
            name="LandingPage",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                    ),
                ),
                (
                    "location",
                    models.CharField(
                        blank=True,
                        help_text="Location e.g. Exeter",
                        max_length=30,
                    ),
                ),
                (
                    "job_role",
                    models.CharField(
                        blank=True,
                        help_text="Job Role e.g. Legal Secretary",
                        max_length=30,
                    ),
                ),
                (
                    "practice_area",
                    models.CharField(
                        blank=True,
                        help_text="Practice Area e.g. Conveyancing",
                        max_length=30,
                    ),
                ),
                (
                    "page",
                    models.OneToOneField(
                        to="block.Page", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name_plural": "Landing Pages",
                "verbose_name": "Landing Page",
            },
        ),
        migrations.AddField(
            model_name="jobsettings",
            name="landing_page_slug",
            field=models.SlugField(
                blank=True,
                help_text="Slug to use for landing pages",
                max_length=100,
            ),
        ),
        migrations.AddField(
            model_name="jobsettings",
            name="landing_page_template",
            field=models.ForeignKey(
                null=True,
                help_text="Template to use for landing pages",
                to="block.Template",
                blank=True,
                on_delete=models.CASCADE,
            ),
        ),
    ]
