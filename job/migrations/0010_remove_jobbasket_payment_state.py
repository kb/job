# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0009_jobbasket_checkout_state")]

    operations = []

    # operations = [
    #     migrations.RemoveField(
    #         model_name='jobbasket',
    #         name='payment_state',
    #     ),
    # ]
