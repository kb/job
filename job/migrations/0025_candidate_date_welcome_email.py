# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-02-05 20:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("job", "0024_job_applications_email")]

    operations = [
        migrations.AddField(
            model_name="candidate",
            name="date_welcome_email",
            field=models.DateTimeField(blank=True, null=True),
        )
    ]
