# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0004_auto_20150617_2146")]

    operations = [
        migrations.RemoveField(model_name="candidate", name="newsletter"),
        migrations.RemoveField(model_name="recruiter", name="newsletter"),
        migrations.AlterField(
            model_name="job",
            name="salary",
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
