# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("job", "0015_auto_20160412_1151")]

    operations = [
        migrations.AlterField(
            model_name="candidate",
            name="notify_new_jobs",
            field=models.BooleanField(
                default=True,
                help_text="Please untick this box if you don't want to receive job notification emails",
            ),
        )
    ]
