# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import checkout.models


class Migration(migrations.Migration):
    dependencies = [
        ("checkout", "0003_auto_20150903_0849"),
        ("job", "0008_auto_20150709_2022"),
    ]

    operations = [
        migrations.AddField(
            model_name="jobbasket",
            name="checkout_state",
            field=models.ForeignKey(
                default=checkout.models.default_checkout_state,
                to="checkout.CheckoutState",
                on_delete=models.CASCADE,
            ),
        )
    ]
