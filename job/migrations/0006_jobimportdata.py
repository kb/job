# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("job", "0005_auto_20150630_1205")]

    operations = [
        migrations.CreateModel(
            name="JobImportData",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "post_id",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "post_date",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "post_mode",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "user_id",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "pin_x",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "pin_y",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "approved",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "JobTitle",
                    models.CharField(blank=True, max_length=200, default=""),
                ),
                ("Description", models.TextField(blank=True, null=True)),
                (
                    "Classification",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "PostedBy",
                    models.CharField(blank=True, max_length=50, default=""),
                ),
                (
                    "Salary",
                    models.CharField(blank=True, max_length=50, default=""),
                ),
                (
                    "ContactTelephone",
                    models.CharField(blank=True, max_length=50, default=""),
                ),
                (
                    "Area",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "JobType",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "Location",
                    models.CharField(blank=True, max_length=200, default=""),
                ),
                (
                    "Deadline",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "applications",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "hits",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "reason",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "StartDate",
                    models.CharField(blank=True, max_length=50, default=""),
                ),
                (
                    "JobFunction",
                    models.CharField(blank=True, max_length=200, default=""),
                ),
                (
                    "Email",
                    models.CharField(blank=True, max_length=200, default=""),
                ),
                (
                    "guid",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "source",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "cached_summary",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "expired",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "app_type",
                    models.CharField(blank=True, max_length=20, default=""),
                ),
                (
                    "app_url",
                    models.CharField(blank=True, max_length=200, default=""),
                ),
                (
                    "JobReference",
                    models.CharField(blank=True, max_length=50, default=""),
                ),
                (
                    "ContactURL",
                    models.CharField(blank=True, max_length=200, default=""),
                ),
                (
                    "ContactName",
                    models.CharField(blank=True, max_length=200, default=""),
                ),
                (
                    "job",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        to="job.Job",
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={"verbose_name": "Job Import Data", "ordering": ("job",)},
        )
    ]
