# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.urls import reverse

from base.model_utils import TimeStampedModel


# result must be between 0 and 100
result_validator = [MinValueValidator(0), MaxValueValidator(100)]


class ContactError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class ContactManager(models.Manager):
    def contacts(self):
        """List of contacts excluding 'deleted'."""
        return self.model.objects.exclude(deleted=True)


class Contact(TimeStampedModel):
    """Contact.

    User should be a required field.

    """

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    title = models.CharField(max_length=100, blank=True)
    address_1 = models.CharField("Address", max_length=100, blank=True)
    address_2 = models.CharField("", max_length=100, blank=True)
    address_3 = models.CharField("", max_length=100, blank=True)
    town = models.CharField(max_length=100, blank=True)
    county = models.CharField(max_length=100, blank=True)
    postcode = models.CharField(max_length=20, blank=True)
    country = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=50, blank=True)
    register_date = models.DateField(blank=True, null=True)
    notes = models.TextField(blank=True)
    deleted = models.BooleanField(default=False)
    # moved from contact
    dob = models.DateField(blank=True, null=True)
    nationality = models.CharField(max_length=50, blank=True)
    mobile = models.CharField(max_length=50, blank=True)
    position = models.CharField(max_length=50, blank=True)
    company_name = models.CharField(max_length=100, blank=True)
    website = models.URLField(blank=True)
    objects = ContactManager()

    class Meta:
        ordering = ("user__last_name", "user__first_name")
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"

    def __str__(self):
        return "{} {} {}".format(
            self.title, self.user.first_name, self.user.last_name
        )

    def get_absolute_url(self):
        return reverse("member.contact.detail", args=[self.pk])

    @property
    def full_name(self):
        result = []
        if self.title:
            result.append(self.title)
        if self.user.first_name:
            result.append(self.user.first_name)
        if self.user.last_name:
            result.append(self.user.last_name)
        return " ".join(result)

    def get_summary_description(self):
        """Used by the 'search' app to display matching records."""
        return filter(
            None,
            (
                self.title,
                self.user.first_name,
                self.user.last_name,
                self.address_1,
                self.address_2,
                self.address_3,
                self.town,
                self.county,
                self.postcode,
                self.phone,
            ),
        )

    def get_job_experience(self):
        """Work out job experience from level and courses.

        Returns an Experience record to set a default Experience status
        """

        # import Experience here to avoid a circular import conflict
        from job.models import Experience

        # default to Trainee for now
        status = Experience.TRAINEE
        return Experience.objects.get(slug=status)

    @property
    def is_recruiter(self):
        """Is this contact a recruiter?"""
        result = False
        try:
            if self.recruiter:
                result = True
        except AttributeError:
            pass

        return result

    @property
    def is_candidate(self):
        """Is this contact a candidate?"""
        result = False
        try:
            if self.candidate and not self.candidate.is_deleted:
                result = True
        except AttributeError:
            pass

        return result

    def update_contact_index(self, transaction):
        pass
