# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [migrations.swappable_dependency(settings.AUTH_USER_MODEL)]

    operations = [
        migrations.CreateModel(
            name="Contact",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("title", models.CharField(max_length=100, blank=True)),
                (
                    "address_1",
                    models.CharField(
                        max_length=100, blank=True, verbose_name="Address"
                    ),
                ),
                (
                    "address_2",
                    models.CharField(
                        max_length=100, blank=True, verbose_name=""
                    ),
                ),
                (
                    "address_3",
                    models.CharField(
                        max_length=100, blank=True, verbose_name=""
                    ),
                ),
                ("town", models.CharField(max_length=100, blank=True)),
                ("county", models.CharField(max_length=100, blank=True)),
                ("postcode", models.CharField(max_length=20, blank=True)),
                ("country", models.CharField(max_length=100, blank=True)),
                ("phone", models.CharField(max_length=50, blank=True)),
                ("register_date", models.DateField(blank=True, null=True)),
                ("notes", models.TextField(blank=True)),
                ("deleted", models.BooleanField(default=False)),
                ("dob", models.DateField(blank=True, null=True)),
                ("nationality", models.CharField(max_length=50, blank=True)),
                ("mobile", models.CharField(max_length=50, blank=True)),
                ("position", models.CharField(max_length=50, blank=True)),
                ("company_name", models.CharField(max_length=100, blank=True)),
                ("website", models.URLField(blank=True)),
                (
                    "user",
                    models.OneToOneField(
                        to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Contact",
                "verbose_name_plural": "Contacts",
                "ordering": ("user__last_name", "user__first_name"),
            },
            bases=(models.Model,),
        )
    ]
