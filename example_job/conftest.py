# -*- encoding: utf-8 -*-
def pytest_configure(config):
    config.addinivalue_line(
        "markers", "elasticsearch: enable or disable tests using elasticsearch"
    )
