# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic import RedirectView

from .views import (
    ContactDetailView,
    HomeView,
    login_redirect,
    MyUpdateUserPasswordView,
    RecruitmentCandidateCreateProfileView,
    RecruitmentCandidateCreateView,
    RecruitmentCandidateDashView,
    RecruitmentCandidateDetailView,
    RecruitmentCandidateUpdateProfileView,
    RecruitmentCandidateUpdateView,
    RecruitmentCheckoutFailView,
    RecruitmentCheckoutSuccessView,
    RecruitmentJobApplicationCreateView,
    RecruitmentJobApplicationDetailView,
    RecruitmentJobApplicationRedirectView,
    RecruitmentJobCheckoutView,
    RecruitmentJobCreateView,
    RecruitmentJobDetailView,
    RecruitmentJobListView,
    RecruitmentJobPublishView,
    RecruitmentRecruiterCreateView,
    RecruitmentRecruiterDashView,
    RecruitmentRecruiterJobCreditView,
    RecruitmentRecruiterSearchView,
    RecruitmentRecruiterUpdateView,
    SettingsView,
    TestCandidateSelectedListReport,
    TestJobElasticSearchView,
    TestUserConsentUnsubscribeUpdateView,
)

admin.autodiscover()


urlpatterns = [
    path("admin/", admin.site.urls),
    re_path(r"^$", HomeView.as_view(), name="project.home"),
    re_path(r"^", include("login.urls")),
    re_path(r"^login-success/$", login_redirect, name="job.login.success"),
    re_path(
        r"^user/(?P<pk>\d+)/password/$",
        MyUpdateUserPasswordView.as_view(),
        name="update_user_password",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        ContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(r"^job/", include("job.urls")),
    re_path(
        r"^home/user/$",
        RedirectView.as_view(url=reverse_lazy("project.home")),
        name="project.dash",
    ),
    re_path(
        r"^recruitment/application/(?P<pk>\d+)/$",
        RecruitmentJobApplicationDetailView.as_view(),
        name="recruitment.application.detail",
    ),
    re_path(
        r"^recruitment/candidate/create/$",
        RecruitmentCandidateCreateView.as_view(),
        name="recruitment.candidate.create",
    ),
    re_path(
        r"^recruitment/candidate/create/profile/$",
        RecruitmentCandidateCreateProfileView.as_view(),
        name="recruitment.candidate.create.profile",
    ),
    re_path(
        r"^recruitment/candidate/update/$",
        RecruitmentCandidateUpdateView.as_view(),
        name="recruitment.candidate.update",
    ),
    re_path(
        r"^recruitment/candidate/update/profile/$",
        RecruitmentCandidateUpdateProfileView.as_view(),
        name="recruitment.candidate.update.profile",
    ),
    re_path(
        r"^recruitment/candidate/dashboard/$",
        RecruitmentCandidateDashView.as_view(),
        name="recruitment.candidate.dashboard",
    ),
    re_path(
        r"^recruitment/candidate/(?P<pk>\d+)/detail/$",
        RecruitmentCandidateDetailView.as_view(),
        name="recruitment.candidate.detail",
    ),
    re_path(
        r"^recruitment/checkout/fail/$",
        RecruitmentCheckoutFailView.as_view(),
        name="recruitment.checkout.fail",
    ),
    re_path(
        r"^recruitment/checkout/(?P<pk>\d+)/success/$",
        RecruitmentCheckoutSuccessView.as_view(),
        name="recruitment.checkout.success",
    ),
    re_path(
        r"^recruitment/candidate/job/(?P<pk>\d+)/$",
        RecruitmentJobDetailView.as_view(),
        name="recruitment.candidate.job.detail",
    ),
    re_path(
        r"^recruitment/job/(?P<job_pk>\d+)/application/$",
        RecruitmentJobApplicationCreateView.as_view(),
        name="recruitment.job.application",
    ),
    re_path(
        r"^recruitment/job/(?P<pk>\d+)/checkout/$",
        RecruitmentJobCheckoutView.as_view(),
        name="recruitment.job.checkout",
    ),
    re_path(
        r"^recruitment/job/search/$",
        RecruitmentJobListView.as_view(),
        name="recruitment.job.search",
    ),
    re_path(
        r"^recruitment/job/(?P<job_pk>\d+)/redirect/$",
        RecruitmentJobApplicationRedirectView.as_view(),
        name="recruitment.job.application.redirect",
    ),
    re_path(
        r"^recruitment/job/create/$",
        RecruitmentJobCreateView.as_view(),
        name="recruitment.job.create",
    ),
    re_path(
        r"^recruitment/recruiter/create/$",
        RecruitmentRecruiterCreateView.as_view(),
        name="recruitment.recruiter.create",
    ),
    re_path(
        r"^recruitment/recruiter/dashboard/$",
        RecruitmentRecruiterDashView.as_view(),
        name="recruitment.recruiter.dashboard",
    ),
    re_path(
        r"^recruitment/job-credits/$",
        RecruitmentRecruiterJobCreditView.as_view(),
        name="recruitment.recruiter.job-credits",
    ),
    re_path(
        r"^recruitment/job/(?P<pk>\d+)/publish/$",
        RecruitmentJobPublishView.as_view(),
        name="recruitment.job.publish",
    ),
    re_path(
        r"^recruitment/recruiter/search/$",
        RecruitmentRecruiterSearchView.as_view(),
        name="recruitment.recruiter.search",
    ),
    re_path(
        r"^recruitment/recruiter/update/$",
        RecruitmentRecruiterUpdateView.as_view(),
        name="recruitment.recruiter.update",
    ),
    re_path(
        r"^settings/$",
        SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(
        r"test/elastic/search/$",
        TestJobElasticSearchView.as_view(),
        name="test.job.elastic.search",
    ),
    re_path(
        r"test/report/candidate/selected/list/$",
        TestCandidateSelectedListReport.as_view(),
        name="report.candidate.selected.list",
    ),
    path(
        "unsubscribe/<str:token>/",
        TestUserConsentUnsubscribeUpdateView.as_view(),
        name="web.unsubscribe",
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
