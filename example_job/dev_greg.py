# -*- encoding: utf-8 -*-
from .base import *


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "temp.db",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}

INSTALLED_APPS += (
    # 'django.contrib.formtools',
    "django_extensions",
    "debug_toolbar",
)
