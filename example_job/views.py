# -*- encoding: utf-8 -*-
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    RedirectView,
    TemplateView,
    UpdateView,
)
from django.views.generic.edit import FormMixin

from base.view_utils import BaseMixin, RedirectNextMixin
from braces.views import AnonymousRequiredMixin, LoginRequiredMixin
from checkout.views import CheckoutMixin, CheckoutSuccessMixin
from example_job.models import Contact
from gdpr.views import UserConsentUnsubscribeUpdateMixin
from job.forms import JobSearchForm  # , JobBasketCheckoutForm
from job.models import JobBasket, JobSettings
from job.service import add_jobs_to_basket_discount
from job.views import (
    CandidateCreateMixin,
    CandidateCreateProfileMixin,
    CandidateDashMixin,
    CandidateDetailMixin,
    CandidateListMixin,
    CandidateLoginRequiredMixin,
    CandidateRecruiterOrStaffLoginRequiredMixin,
    CandidateUpdateCurrentUserMixin,
    CandidateUpdateCurrentUserProfileMixin,
    JobApplicationDetailMixin,
    JobApplicationMixin,
    JobApplicationRedirectMixin,
    JobCreateMixin,
    JobDetailMixin,
    JobElasticSearchMixin,
    JobListMixin,
    JobPublishMixin,
    RecruiterCreateMixin,
    RecruiterDashMixin,
    RecruiterJobCreditMixin,
    RecruiterLoginRequiredMixin,
    RecruiterOrStaffLoginRequiredMixin,
    RecruiterUpdateCurrentUserMixin,
)
from login.views import UpdateUserPasswordView


def login_redirect(request):
    is_recruiter = False
    is_candidate = False
    try:
        is_recruiter = request.user.contact.is_recruiter
        is_candidate = request.user.contact.is_candidate
    except AttributeError:
        pass

    if is_recruiter:
        return redirect(reverse("recruitment.recruiter.dashboard"))
    elif is_candidate:
        return redirect(reverse("recruitment.candidate.dashboard"))
    elif request.user.is_staff:
        return redirect(reverse("project.dash"))
    else:
        return redirect(reverse("project.home"))


class ContactDetailView(DetailView):
    model = Contact


class HomeView(JobListMixin, FormMixin, ListView):
    template_name = "example/home.html"
    form_class = JobSearchForm

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context.update(dict(job_settings=JobSettings.load()))
        return context


class MyUpdateUserPasswordView(UpdateUserPasswordView):
    def get_success_url(self):
        # contact = Contact.objects.get(user=self.object)
        return reverse("project.home")


class RecruitmentCandidateCreateView(
    AnonymousRequiredMixin, CandidateCreateMixin, BaseMixin, CreateView
):
    """View for testing 'CandidateCreateMixin'."""

    candidate_dash_url_name = "recruitment.candidate.dashboard"

    def get_success_url(self):
        return CandidateCreateMixin.get_success_url(self)


class RecruitmentCandidateCreateProfileView(
    LoginRequiredMixin, CandidateCreateProfileMixin, BaseMixin, CreateView
):
    """View for testing 'CandidateProfileMixin'."""

    candidate_dash_url_name = "recruitment.candidate.dashboard"

    def get_success_url(self):
        return CandidateCreateProfileMixin.get_success_url(self)


class RecruitmentCandidateUpdateView(
    CandidateLoginRequiredMixin,
    CandidateUpdateCurrentUserMixin,
    BaseMixin,
    UpdateView,
):
    pass


class RecruitmentCandidateUpdateProfileView(
    CandidateLoginRequiredMixin,
    CandidateUpdateCurrentUserProfileMixin,
    BaseMixin,
    UpdateView,
):
    """View for testing 'CandidateProfileMixin'."""

    def get_success_url(self):
        return CandidateUpdateCurrentUserProfileMixin.get_success_url(self)


class RecruitmentCandidateDashView(
    CandidateLoginRequiredMixin, CandidateDashMixin, BaseMixin, TemplateView
):
    """View for testing 'CandidateDashMixin'."""

    template_name = "example/candidate_detail.html"


class RecruitmentCandidateDetailView(
    RecruiterOrStaffLoginRequiredMixin,
    CandidateDetailMixin,
    BaseMixin,
    DetailView,
):
    """View for testing 'CandidateDetailMixin'."""

    template_name = "example/candidate_detail.html"


class RecruitmentJobCreateView(
    RecruiterLoginRequiredMixin, JobCreateMixin, BaseMixin, CreateView
):
    """View for testing 'JobCreateMixin'."""

    public_facing_page = True
    template_name = "example/job_form.html"


class RecruitmentJobApplicationCreateView(
    CandidateLoginRequiredMixin, JobApplicationMixin, BaseMixin, CreateView
):
    """View for testing 'JobApplicationMixin'."""

    template_name = "example/jobapplication_form.html"


class RecruitmentJobApplicationDetailView(
    CandidateRecruiterOrStaffLoginRequiredMixin,
    JobApplicationDetailMixin,
    FormMixin,
    BaseMixin,
    DetailView,
):
    """View for testing 'JobApplicationMixin'.

    PJK TODO Does this view need a ``FormMixin``?  Check the live site.

    """

    template_name = "example/jobapplication_detail.html"


class RecruitmentJobApplicationRedirectView(
    JobApplicationRedirectMixin, RedirectView
):
    """View for testing 'JobApplicationRedirectMixin'."""

    pass


class RecruitmentJobCheckoutView(
    RecruiterLoginRequiredMixin, CheckoutMixin, BaseMixin, UpdateView
):
    model = JobBasket
    # form_class = JobBasketCheckoutForm
    template_name = "example/job_basket_checkout_form.html"


class RecruitmentCheckoutFailView(BaseMixin, TemplateView):
    template_name = "example/recruitment_checkout_fail.html"


class RecruitmentCheckoutSuccessView(
    CheckoutSuccessMixin, BaseMixin, DetailView
):
    template_name = "example/recruitment_checkout_success.html"


class RecruitmentJobDetailView(JobDetailMixin, BaseMixin, DetailView):
    """View for testing 'JobDetailMixin'."""

    pass


class RecruitmentJobListView(FormMixin, JobListMixin, BaseMixin, ListView):
    # page_kwarg = 'page_no'
    paginate_by = 10
    form_class = JobSearchForm
    # template_name = 'example/home.html'
    template_name = "example/job_search.html"


class RecruitmentJobPublishView(JobPublishMixin, UpdateView):
    def get_success_url(self):
        return reverse("recruitment.recruiter.dashboard")


class RecruitmentRecruiterCreateView(
    AnonymousRequiredMixin, RecruiterCreateMixin, BaseMixin, CreateView
):
    """View for testing 'RecruiterForm'."""

    def get_success_url(self):
        return reverse("recruitment.recruiter.dashboard")


class RecruitmentRecruiterDashView(
    RecruiterLoginRequiredMixin, RecruiterDashMixin, BaseMixin, CreateView
):
    """View for testing 'RecruiterDashMixin'."""

    def add_jobs_to_basket(self, basket):
        add_jobs_to_basket_discount(basket)

    def url_payment(self, payment_pk):
        """This URL is only for testing."""
        return reverse("recruitment.recruiter.dashboard")

    def url_sorry(self):
        """This URL is only for testing."""
        return reverse("recruitment.recruiter.dashboard")

    def url_thankyou(self):
        return reverse("recruitment.recruiter.dashboard")


class RecruitmentRecruiterJobCreditView(
    RecruiterLoginRequiredMixin, RecruiterJobCreditMixin, BaseMixin, CreateView
):
    def get_context_data(self, **kwargs):
        """For testing purposes only."""
        context = super().get_context_data(**kwargs)
        context.update(dict(sub_heading="Job Credit View"))
        return context

    def url_payment(self, payment_pk):
        """This URL is only for testing."""
        return reverse("recruitment.recruiter.dashboard")

    def url_sorry(self):
        """This URL is only for testing."""
        return reverse("recruitment.recruiter.dashboard")

    def url_thankyou(self):
        return reverse("recruitment.recruiter.dashboard")


class RecruitmentRecruiterSearchView(
    RecruiterOrStaffLoginRequiredMixin, CandidateListMixin, BaseMixin, ListView
):
    paginate_by = 20


class RecruitmentRecruiterUpdateView(
    RecruiterLoginRequiredMixin,
    RecruiterUpdateCurrentUserMixin,
    BaseMixin,
    UpdateView,
):
    def get_success_url(self):
        return reverse("recruitment.recruiter.dashboard")


class SettingsView(BaseMixin, TemplateView):
    template_name = "example/settings.html"

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context.update(dict(job_settings=JobSettings.load()))
        return context


class TestCandidateSelectedListReport(BaseMixin, TemplateView):
    """The *proper* view is in the client project (not in this app).

    In the project it is called ``ReportCandidateSelectedListView``
    (it runs the ``CandidateListReport``).

    .. note:: The example template does not exist.
              This view and URL only exist so the
              ``job/templates/job/candidate_list.html``
              template does not throw an error when it is being tested.

    """

    template_name = "example/does-not-exist.html"


class TestJobElasticSearchView(JobElasticSearchMixin, TemplateView):
    template_name = "job/job_elastic_search.html"
    paginate_by = 3


class TestUserConsentUnsubscribeUpdateView(
    RedirectNextMixin, UserConsentUnsubscribeUpdateMixin, BaseMixin, UpdateView
):
    template_name = "example/user_consent_unsubscribe.html"
