# -*- encoding: utf-8 -*-
"""

Don't forget::

  job.tests.test_job_feed.py

"""
import pytest

from dateutil.relativedelta import relativedelta
from django.utils import timezone

from job.models import JobFeed
from job.tests.factories import (
    ContactFactory,
    JobFactory,
    JobFeedFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_current():
    JobFeedFactory(
        slug="a", recruiter=RecruiterFactory(contact=ContactFactory())
    )
    job_feed = JobFeedFactory(
        slug="b", recruiter=RecruiterFactory(contact=ContactFactory())
    )
    job_feed.set_deleted(UserFactory())
    JobFeedFactory(
        slug="c", recruiter=RecruiterFactory(contact=ContactFactory())
    )
    assert ["a", "c"] == [x.slug for x in JobFeed.objects.current()]


@pytest.mark.django_db
def test_jobs():
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    indeed_recruiter = RecruiterFactory(contact=ContactFactory())
    job_feed = JobFeedFactory(recruiter=indeed_recruiter)
    today = timezone.now()
    yesterday = timezone.now() - relativedelta(days=1)
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 1",
        order_key=2,
        publish=True,
        publish_date=today,
    )
    JobFactory(
        recruiter=indeed_recruiter,
        title="INDEED 2",
        order_key=2,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 2",
        order_key=1,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa_recruiter,
        title="ILSPA 1",
        order_key=1,
        publish=True,
        publish_date=today,
    )
    assert set(["INDEED 1", "INDEED 2"]) == set(
        [x.title for x in job_feed.jobs()]
    )


@pytest.mark.django_db
def test_stats():
    recruiter = RecruiterFactory(contact=ContactFactory())
    job_feed = JobFeedFactory(recruiter=recruiter)
    JobFactory(recruiter=recruiter)
    JobFactory(recruiter=recruiter)
    assert {"24 hours": 2, "week": 0} == job_feed.stats()


@pytest.mark.django_db
def test_str():
    feed_recruiter = RecruiterFactory(contact=ContactFactory(company_name="KB"))
    assert "KB ('kb')" == str(
        JobFeedFactory(slug="kb", recruiter=feed_recruiter)
    )


@pytest.mark.django_db
def test_str_no_recruiter():
    assert "('kb')" == str(JobFeedFactory(slug="kb", recruiter=None))
