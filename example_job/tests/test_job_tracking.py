# -*- encoding: utf-8 -*-
import pytest

from datetime import date

from job.models import JobTracking
from job.tests.factories import (
    CandidateFactory,
    JobFactory,
    JobTrackingFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_job_tracking():
    c = CandidateFactory(contact=ContactFactory())
    r = RecruiterFactory(contact=ContactFactory())
    j = JobFactory(recruiter=r)
    JobTracking.objects.track_application_click(c, j)
    tracking = JobTracking.objects.get(candidate=c, job=j)
    assert 1 == tracking.application_click_count


@pytest.mark.django_db
def test_job_tracking_application_click_count():
    c = CandidateFactory(contact=ContactFactory())
    r = RecruiterFactory(contact=ContactFactory())
    j = JobFactory(recruiter=r)
    JobTracking.objects.track_application_click(c, j)
    JobTracking.objects.track_application_click(c, j)
    tracking = JobTracking.objects.get(candidate=c, job=j)
    assert 2 == tracking.application_click_count


@pytest.mark.django_db
def test_job_tracking_view_count():
    c = CandidateFactory(contact=ContactFactory())
    r = RecruiterFactory(contact=ContactFactory())
    j = JobFactory(recruiter=r)
    JobTracking.objects.track_view(c, j)
    JobTracking.objects.track_view(c, j)
    tracking = JobTracking.objects.get(candidate=c, job=j)
    assert 2 == tracking.view_count


@pytest.mark.django_db
def test_tracking():
    """The tracking list a member of staff can see for a job."""
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    JobTrackingFactory(
        candidate=CandidateFactory(contact=ContactFactory()),
        job=job,
        click_date=date(2015, 1, 1),
        application_click_count=2,
        view_count=5,
    )
    user = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=user)
    tracking = job.tracking(user)
    assert [2] == [t.application_click_count for t in tracking]
    assert [5] == [t.view_count for t in tracking]


@pytest.mark.django_db
def test_tracking_multi_day():
    """The tracking list a member of staff can see for a job."""
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    JobTrackingFactory(
        candidate=CandidateFactory(contact=ContactFactory()),
        job=job,
        click_date=date(2015, 1, 1),
        application_click_count=2,
        view_count=5,
    )
    JobTrackingFactory(
        candidate=CandidateFactory(contact=ContactFactory()),
        job=job,
        click_date=date(2015, 1, 2),
        application_click_count=1,
        view_count=7,
    )
    user = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=user)
    tracking = job.tracking(user)
    assert [1, 2] == [t.application_click_count for t in tracking]
    assert [7, 5] == [t.view_count for t in tracking]


@pytest.mark.django_db
def test_tracking_not_staff():
    """Only a member of staff can see the clicks on a job."""
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    JobTrackingFactory(
        candidate=CandidateFactory(contact=ContactFactory()),
        job=job,
        click_date=date(2015, 1, 1),
        application_click_count=2,
    )
    user = UserFactory()
    ContactFactory(user=user)
    tracking = job.tracking(user)
    assert [] == [t.application_click_count for t in tracking]
    assert [] == [t.view_count for t in tracking]
