# -*- encoding: utf-8 -*-
"""

Don't forget::

  job.tests.test_service.py

"""
import json
import os
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.core.management import call_command
from django.utils import timezone
from http import HTTPStatus
from unittest import mock

from checkout.models import CheckoutState
from finance.tests.factories import VatSettingsFactory
from job.models import (
    FEED_RECRUITER,
    Job,
    JobFeed,
    JobFeedItem,
    PRODUCT_JOB_POST,
    PRODUCT_JOB_POST_DISCOUNT,
)
from job.service import (
    add_jobs_to_basket,
    add_jobs_to_basket_discount,
    DiginowXMLJobFeed,
    SimplyLawJsonJobFeed,
)
from job.tests.factories import (
    JobBasketFactory,
    JobBasketItemFactory,
    JobFactory,
    JobFeedFactory,
    JobFeedItemFactory,
    JobRoleFactory,
    JobRoleKeywordFactory,
    PracticeAreaFactory,
    PracticeAreaKeywordFactory,
    RecruiterFactory,
)
from stock.tests.factories import ProductFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_add_jobs_to_basket():
    VatSettingsFactory()
    recruiter = RecruiterFactory(contact=ContactFactory())
    basket = JobBasketFactory(recruiter=recruiter)
    JobFactory(publish=False, title="j1", recruiter=recruiter)
    JobFactory(publish=False, title="j2", recruiter=recruiter)
    ProductFactory(slug=PRODUCT_JOB_POST)
    # action
    add_jobs_to_basket(basket)
    # assert
    assert {"j1": PRODUCT_JOB_POST, "j2": PRODUCT_JOB_POST} == {
        i.job.title: i.product.slug for i in basket.jobbasketitem_set.all()
    }


@pytest.mark.django_db
def test_add_jobs_to_basket_discount_one():
    """No discount if the recruiter posts one job (and hasn't posted before)"""
    recruiter = RecruiterFactory(contact=ContactFactory())
    basket = JobBasketFactory(recruiter=recruiter)
    JobFactory(publish=False, title="j1", recruiter=recruiter)
    ProductFactory(slug=PRODUCT_JOB_POST)
    # action
    add_jobs_to_basket_discount(basket)
    # assert
    assert {"j1": PRODUCT_JOB_POST} == {
        i.job.title: i.product.slug for i in basket.jobbasketitem_set.all()
    }


@pytest.mark.django_db
def test_add_jobs_to_basket_discount_one_previous():
    """No discount if the recruiter posted one job within the last week."""
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    ProductFactory(slug=PRODUCT_JOB_POST)
    product = ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT)
    recruiter = RecruiterFactory(contact=ContactFactory())
    basket1 = JobBasketFactory(
        recruiter=recruiter, checkout_state=checkout_state
    )
    JobBasketItemFactory(
        basket=basket1,
        job=JobFactory(title="j1", recruiter=recruiter),
        product=product,
    )
    # now post another job
    JobFactory(publish=False, title="j3", recruiter=recruiter)
    basket2 = JobBasketFactory(recruiter=recruiter)
    add_jobs_to_basket_discount(basket2)
    # assert
    assert {"j3": PRODUCT_JOB_POST} == {
        i.job.title: i.product.slug for i in basket2.jobbasketitem_set.all()
    }


@pytest.mark.django_db
def test_add_jobs_to_basket_discount_two():
    """Discount if the recruiter posts two jobs at the same time."""
    recruiter = RecruiterFactory(contact=ContactFactory())
    basket = JobBasketFactory(recruiter=recruiter)
    JobFactory(publish=False, title="j1", recruiter=recruiter)
    JobFactory(publish=False, title="j2", recruiter=recruiter)
    ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT)
    # action
    add_jobs_to_basket_discount(basket)
    # assert
    assert {
        "j1": PRODUCT_JOB_POST_DISCOUNT,
        "j2": PRODUCT_JOB_POST_DISCOUNT,
    } == {i.job.title: i.product.slug for i in basket.jobbasketitem_set.all()}


@pytest.mark.django_db
def test_add_jobs_to_basket_discount_two_previous():
    """Discount if the recruiter posted two jobs within the last week."""
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    ProductFactory(slug=PRODUCT_JOB_POST)
    product = ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT)
    recruiter = RecruiterFactory(contact=ContactFactory())
    basket1 = JobBasketFactory(
        recruiter=recruiter, checkout_state=checkout_state
    )
    JobBasketItemFactory(
        basket=basket1,
        job=JobFactory(title="j1", recruiter=recruiter),
        product=product,
    )
    JobBasketItemFactory(
        basket=basket1,
        job=JobFactory(title="j2", recruiter=recruiter),
        product=product,
    )
    # now post another job
    JobFactory(publish=False, title="j3", recruiter=recruiter)
    basket2 = JobBasketFactory(recruiter=recruiter)
    add_jobs_to_basket_discount(basket2)
    # assert
    assert {"j3": PRODUCT_JOB_POST_DISCOUNT} == {
        i.job.title: i.product.slug for i in basket2.jobbasketitem_set.all()
    }


@pytest.mark.django_db
def test_add_jobs_to_basket_discount_two_previous_too_late():
    """No discount if the recruiter posted two jobs longer ago than a week."""
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    ProductFactory(slug=PRODUCT_JOB_POST)
    product = ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT)
    recruiter = RecruiterFactory(contact=ContactFactory())
    basket1 = JobBasketFactory(
        recruiter=recruiter, checkout_state=checkout_state
    )
    one_week_one_day = timezone.now() + relativedelta(days=-8)
    # the basket was created 8 days ago!
    basket1.created = one_week_one_day
    basket1.save()
    JobBasketItemFactory(
        basket=basket1,
        job=JobFactory(title="j1", recruiter=recruiter),
        product=product,
    )
    JobBasketItemFactory(
        basket=basket1,
        job=JobFactory(title="j2", recruiter=recruiter),
        product=product,
    )
    # now post another job
    JobFactory(publish=False, title="j3", recruiter=recruiter)
    basket2 = JobBasketFactory(recruiter=recruiter)
    add_jobs_to_basket_discount(basket2)
    # assert
    assert {"j3": PRODUCT_JOB_POST} == {
        i.job.title: i.product.slug for i in basket2.jobbasketitem_set.all()
    }


@mock.patch("urllib.request.urlopen")
@pytest.mark.django_db
def test_json_feed(mock_urlopen):
    """Import Simply Law Jobs.

    24/01/2025, No longer using ``requests``
    And I cannot get mock working with ``urlopen``!!!

    Switched to ``urlopen``.
    - https://dev.to/bowmanjd/http-calls-in-python-without-requests-or-other-external-dependencies-5aj1

    Simply Law jobs feed - failing to import
    - https://www.kbsoftware.co.uk/crm/ticket/7517/

    Test code copied to
    ``test_management_command.py``, ``test_process_feed_simple_law_jobs``.

    Ticket 3998, Added ``region`` to ``location`` so the import doesn't fail.

    """
    mock_data = {
        "jobs": [
            {
                "job": [
                    {
                        "jobref": 1034166,
                        "date": "2023-04-11 14:00:03",
                        "title": "Legal Secretary",
                        "company": "KB Legal",
                        "url": "https://www.simplylawjobs.com/job/1034166",
                        "country": "UK",
                        "region": None,
                        "city": "Bicester",
                        "description": "An leading Liverpool law firm is currently looking to recruit",
                        "closedate": "2023-05-09 14:00:03",
                        "practicearea": "Family",
                    }
                ]
            }
        ]
    }
    # mock
    cm = mock.MagicMock()
    cm.getcode.return_value = HTTPStatus.OK
    cm.read.return_value = json.dumps(mock_data)
    cm.status = HTTPStatus.OK
    cm.__enter__.return_value = cm
    mock_urlopen.return_value = cm
    # init
    call_command("init_app_job")
    # setup job roles
    job_role = JobRoleFactory(title="Legal Secretary", order=2)
    JobRoleKeywordFactory(job_role=job_role, word="secret")
    # setup practice areas
    practice_area = PracticeAreaFactory(title="Family", order=2)
    PracticeAreaKeywordFactory(practice_area=practice_area, word="family")
    PracticeAreaKeywordFactory(practice_area=practice_area, word="matrimonial")
    feed = SimplyLawJsonJobFeed(JobFeed.SIMPLY_LAW)
    assert 1 == feed.do_import()
    feed_item = JobFeedItem.objects.get(jobkey="1034166")
    assert "Legal Secretary" == feed_item.job.title
    assert date(2023, 5, 9) == feed_item.job.close_date
    assert (
        "An leading Liverpool law firm is currently looking to recruit"
        == feed_item.job.description
    )
    assert "Bicester" == feed_item.job.location_text
    assert "Family" == feed_item.job.practice_area.title
    assert "Legal Secretary" == feed_item.job.job_role.title
    assert feed_item.job.order_key == FEED_RECRUITER
    assert feed_item.job.publish is True
    assert feed_item.job.publish_date.date() == timezone.now().date()
    assert "KB Legal" == feed_item.company
    assert date(2023, 4, 11) == feed_item.date.date()
    assert "KB Legal" == feed_item.source
    assert "https://www.simplylawjobs.com/job/1034166" == feed_item.url
    # 34723 (Bicester) is a locality
    assert "Bicester" == feed_item.city
    # 10 (South East) and 55 (Oxfordshire) are regions
    # assert "Oxfordshire" == feed_item.state
    # 102 (UK) is a country
    assert "UK" == feed_item.country


@pytest.mark.django_db
def test_diginow_xml_feed():
    call_command("init_app_job")
    job_feed = JobFeed.objects.get(slug=JobFeed.LEGISTS)
    # setup job roles
    job_role = JobRoleFactory(title="Legal Secretary", order=2)
    JobRoleKeywordFactory(job_role=job_role, word="secret")
    # setup practice areas
    practice_area = PracticeAreaFactory(title="Property", order=2)
    PracticeAreaKeywordFactory(practice_area=practice_area, word="property")
    file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "diginow.xml"
    )
    feed = DiginowXMLJobFeed(job_feed.slug, file_name)
    # create a Diginow job which is no longer included in the XML file
    job = JobFactory(
        recruiter=RecruiterFactory(contact=ContactFactory()), publish=True
    )
    JobFeedItemFactory(feed=job_feed, job=job, jobkey="PK22")
    assert job.pk in [x.pk for x in Job.objects.current()]
    # import the jobs
    assert 1 == feed.do_import()
    feed_item = JobFeedItem.objects.get(jobkey="QEDGe61254")
    assert "20190107_RCL_1546882035" in feed_item.job.picture.name
    assert "Legal Secretary" == feed_item.job.title
    assert date(2019, 3, 8) == feed_item.job.close_date
    assert (
        "Legal Secretary<br>"
        "The role is to assist the property department<br>"
        "litigation team. <br><br>The position<br>"
        "record <li>A desire to build a career<br>"
        "To apply please.</li>"
    ) == feed_item.job.description
    assert "Nantwich" == feed_item.job.location_text
    assert "Property" == feed_item.job.practice_area.title
    assert "Legal Secretary" == feed_item.job.job_role.title
    assert feed_item.job.order_key == FEED_RECRUITER
    assert feed_item.job.publish is True
    assert feed_item.job.publish_date.date() == timezone.now().date()
    assert "KB Legal Recruitment" == feed_item.company
    assert date(2019, 1, 7) == feed_item.date.date()
    assert "KB Legal Recruitment" == feed_item.source
    assert "https://www.thelegists.co.uk/job-description/61254" == feed_item.url
    assert "Nantwich" == feed_item.city
    assert "Cheshire" == feed_item.state
    assert "UK" == feed_item.country
    # check the old job has been closed
    job.refresh_from_db()
    yesterday = timezone.now().date() + relativedelta(days=-1)
    assert yesterday == job.close_date
    assert job.pk not in [x.pk for x in Job.objects.current()]


@pytest.mark.django_db
def test_diginow_xml_feed_name_from_url():
    job_feed = JobFeedFactory(recruiter=None)
    feed = DiginowXMLJobFeed(job_feed.slug, "does-not-exist.xml")
    logo_url = "https://www.the.co.uk/img/logo/20190107_RCL_1546882035.png"
    assert "20190107_RCL_1546882035.png" == feed._name_from_url(logo_url)
