# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime

from job.models import ILSPA_RECRUITER, FEED_RECRUITER
from job.reports import JobsBoardMonthlyStats
from job.tests.factories import CandidateFactory, JobFactory, RecruiterFactory

from .factories import ContactFactory


@pytest.mark.django_db
def test_candidate_statistics():
    for c in range(1, 11):
        create_date = datetime(2015, 1, c, 0, 0, 0, tzinfo=pytz.utc)
        c = CandidateFactory(contact=ContactFactory())
        c.created = create_date
        c.save()
    for c in range(1, 6):
        create_date = datetime(2015, 2, c, 0, 0, 0, tzinfo=pytz.utc)
        c = CandidateFactory(contact=ContactFactory())
        c.created = create_date
        c.save()

    jobs_board_stats = JobsBoardMonthlyStats()
    # repeat the test several times to ensure that we get the same result
    for rep in range(0, 3):
        jobs_board_stats.colate_monthly_stats()
        for stat in jobs_board_stats.stats:
            if stat["year"] == 2015 and stat["month"] == 1:
                assert stat["candidates"]["month"] == 10
                assert stat["candidates"]["total"] == 10
            if stat["year"] == 2015 and stat["month"] == 2:
                assert stat["candidates"]["month"] == 5
                assert stat["candidates"]["total"] == 15


@pytest.mark.django_db
def test_recruiter_statistics():
    for c in range(1, 6):
        r = RecruiterFactory(contact=ContactFactory())
        r.created = datetime(2015, 1, c, 0, 0, 0, tzinfo=pytz.utc)
        r.save()
    for c in range(1, 8):
        r = RecruiterFactory(contact=ContactFactory())
        r.created = datetime(2015, 2, c, 0, 0, 0, tzinfo=pytz.utc)
        r.save()

    jobs_board_stats = JobsBoardMonthlyStats()
    # repeat the test several times to ensure that we get the same result
    for rep in range(0, 3):
        jobs_board_stats.colate_monthly_stats()
        for stat in jobs_board_stats.stats:
            if stat["year"] == 2015 and stat["month"] == 1:
                assert stat["recruiters"]["month"] == 5
                assert stat["recruiters"]["total"] == 5
            if stat["year"] == 2015 and stat["month"] == 2:
                assert stat["recruiters"]["month"] == 7
                assert stat["recruiters"]["total"] == 12


@pytest.mark.django_db
def test_job_statistics():
    ilspa_recruiter = RecruiterFactory(contact=ContactFactory())
    feed_recruiter = RecruiterFactory(contact=ContactFactory())
    for c in range(1, 10):
        create_date = datetime(2015, 1, c, 0, 0, 0, tzinfo=pytz.utc)
        if c % 2 == 1:
            j = JobFactory(recruiter=ilspa_recruiter, order_key=ILSPA_RECRUITER)
        else:
            j = JobFactory(recruiter=feed_recruiter, order_key=FEED_RECRUITER)
        j.created = create_date
        j.save()
    for c in range(1, 8):
        create_date = datetime(2015, 2, c, 0, 0, 0, tzinfo=pytz.utc)
        if c % 2 == 1:
            j = JobFactory(recruiter=feed_recruiter, order_key=ILSPA_RECRUITER)
        else:
            j = JobFactory(recruiter=feed_recruiter, order_key=FEED_RECRUITER)
        j.created = create_date
        j.save()

    jobs_board_stats = JobsBoardMonthlyStats()

    # repeat the test several times to ensure that we get the same result
    for rep in range(0, 3):
        jobs_board_stats.colate_monthly_stats()
        for stat in jobs_board_stats.stats:
            if stat["year"] == 2015 and stat["month"] == 1:
                assert stat["ilspa_jobs"]["month"] == 5
                assert stat["ilspa_jobs"]["total"] == 5
                assert stat["feed_jobs"]["month"] == 4
                assert stat["feed_jobs"]["total"] == 4
            if stat["year"] == 2015 and stat["month"] == 2:
                assert stat["ilspa_jobs"]["month"] == 4
                assert stat["ilspa_jobs"]["total"] == 9
                assert stat["feed_jobs"]["month"] == 3
                assert stat["feed_jobs"]["total"] == 7
