# -*- encoding: utf-8 -*-
import pytest

from job.tests.helper import check_contact

from .factories import ContactFactory


@pytest.mark.django_db
def test_properties():
    check_contact(ContactFactory())
