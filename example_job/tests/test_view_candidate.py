# -*- encoding: utf-8 -*-
import os
import pytest
import pytz

from datetime import datetime
from decimal import Decimal
from django.forms.widgets import CheckboxInput, HiddenInput
from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from example_job.models import Contact
from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory, ConsentFormSettingsFactory
from job.models import Candidate, CandidateJobLocation, CV, Experience
from job.tests.factories import (
    CandidateFactory,
    ContactFactory,
    CVFactory,
    ExperienceFactory,
    LocationFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.models import Mail, MailField, Message
from mail.tests.factories import MailTemplateFactory


def check_welcome_email(candidate):
    """Check the candidate email is sent."""
    assert candidate.welcome_email_sent is True
    assert timezone.now().date() == candidate.date_welcome_email.date()
    assert 1 == Mail.objects.count()
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert Candidate.CANDIDATE_WELCOME == message.template.slug
    mail = Mail.objects.get(email="web@pkimber.net")
    mail_field = MailField.objects.filter(mail=mail)
    result = {x.key: x.value for x in mail_field}
    register_date = result.pop("register_date")
    assert timezone.now().date().strftime("%d/%m/%Y") in register_date
    assert {"name": "Patrick", "username": "patrick"} == result
    assert "" == mail.message.subject
    assert "" == mail.message.description


@pytest.mark.django_db
def test_candidate_create_checkbox_widget(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_DATA_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    url = reverse("recruitment.candidate.create")
    response = client.get(url)
    assert 200 == response.status_code
    fields = response.context["form"].fields
    assert isinstance(fields["data_checkbox"].widget, CheckboxInput)
    assert isinstance(fields["notify_checkbox"].widget, CheckboxInput)
    assert isinstance(fields["newsletter_checkbox"].widget, CheckboxInput)
    assert isinstance(fields["search_checkbox"].widget, CheckboxInput)


@pytest.mark.django_db
def test_candidate_create_hidden_widget(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(
        slug=Candidate.GDPR_DATA_SLUG,
        consent_required=True,
        show_checkbox=False,
    )
    ConsentFactory(
        slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG,
        consent_required=True,
        show_checkbox=False,
    )
    ConsentFactory(
        slug=Candidate.GDPR_NEWSLETTER_SLUG,
        consent_required=True,
        show_checkbox=False,
    )
    ConsentFactory(
        slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG,
        consent_required=True,
        show_checkbox=False,
    )
    url = reverse("recruitment.candidate.create")
    response = client.get(url)
    assert 200 == response.status_code
    fields = response.context["form"].fields
    assert isinstance(fields["data_checkbox"].widget, HiddenInput)
    assert isinstance(fields["notify_checkbox"].widget, HiddenInput)
    assert isinstance(fields["newsletter_checkbox"].widget, HiddenInput)
    assert isinstance(fields["search_checkbox"].widget, HiddenInput)


def check_consent(consent, user, consent_expected):
    uc = UserConsent.objects.get(consent=consent)
    assert uc.user == user
    assert uc.consent_given == consent_expected


@pytest.mark.django_db
def test_candidate_create(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    data_consent = ConsentFactory(
        slug=Candidate.GDPR_DATA_SLUG, consent_required=True
    )
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    url = reverse("recruitment.candidate.create")
    data = {
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "username": "patrick",
        "postcode": "EX20",
        "password1": "pass",
        "password2": "pass",
        "data_checkbox": True,
        "captcha": "123",
        "g-recaptcha-response": "PASSED",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # ensure we are redirecting to the dashboard
    redirect_url = reverse("recruitment.candidate.create.profile")
    assert redirect_url == response.url
    # check we created a the candidate profile for the contact
    contact = Contact.objects.get(user__username="patrick")
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    assert candidate.looking_for_work is True
    check_welcome_email(candidate)
    check_consent(data_consent, contact.user, True)


@pytest.mark.django_db
def test_candidate_create_hidden_checkbox(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    data_consent = ConsentFactory(
        slug=Candidate.GDPR_DATA_SLUG,
        consent_required=True,
        show_checkbox=False,
    )
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    url = reverse("recruitment.candidate.create")
    data = {
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "username": "patrick",
        "postcode": "EX20",
        "password1": "pass",
        "password2": "pass",
        "data_checkbox": True,
        "captcha": "123",
        "g-recaptcha-response": "PASSED",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # ensure we are redirecting to the dashboard
    redirect_url = reverse("recruitment.candidate.create.profile")
    assert redirect_url == response.url
    # check we created a the candidate profile for the contact
    contact = Contact.objects.get(user__username="patrick")
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    assert candidate.looking_for_work is True
    check_welcome_email(candidate)
    check_consent(data_consent, contact.user, True)


@pytest.mark.django_db
def test_candidate_create_notify_new_jobs(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    data_consent = ConsentFactory(
        slug=Candidate.GDPR_DATA_SLUG, consent_required=True
    )
    notify_consent = ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    url = reverse("recruitment.candidate.create")
    data = {
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "username": "patrick",
        "postcode": "EX20",
        "password1": "pass",
        "password2": "pass",
        "data_checkbox": True,
        "notify_checkbox": True,
        "captcha": "123",
        "g-recaptcha-response": "PASSED",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # ensure we are redirecting to the dashboard
    redirect_url = reverse("recruitment.candidate.create.profile")
    assert redirect_url == response.url
    # check we created a the candidate profile for the contact
    contact = Contact.objects.get(user__username="patrick")
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    assert candidate.looking_for_work is True
    # replaced with the new gdpr user consent model
    # assert candidate.allow_search is True
    check_welcome_email(candidate)
    check_consent(data_consent, contact.user, True)
    check_consent(notify_consent, contact.user, True)
    # replaced with the new gdpr user consent model
    # assert candidate.notify_new_jobs is True


@pytest.mark.django_db
def test_candidate_create_fail_data_consent(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_DATA_SLUG, consent_required=True)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    url = reverse("recruitment.candidate.create")
    data = {
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "username": "patrick",
        "postcode": "EX20",
        "password1": "pass",
        "password2": "pass",
    }
    response = client.post(url, data)
    assert 200 == response.status_code

    assert response.context["form"].errors["data_checkbox"] is not None


@pytest.mark.django_db
def test_candidate_create_fail_notify_consent(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_DATA_SLUG)
    ConsentFactory(
        slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG, consent_required=True
    )
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    url = reverse("recruitment.candidate.create")
    data = {
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "username": "patrick",
        "postcode": "EX20",
        "password1": "pass",
        "password2": "pass",
    }
    response = client.post(url, data)
    assert 200 == response.status_code

    assert response.context["form"].errors["notify_checkbox"] is not None


@pytest.mark.django_db
def test_candidate_create_fail_newletter_consent(client):
    ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_DATA_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG, consent_required=True)
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    url = reverse("recruitment.candidate.create")
    data = {
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "username": "patrick",
        "postcode": "EX20",
        "password1": "pass",
        "password2": "pass",
    }
    response = client.post(url, data)
    assert 200 == response.status_code

    assert response.context["form"].errors["newsletter_checkbox"] is not None


@pytest.mark.django_db
def test_candidate_create_profile_blank(client):
    """Test 'CandidateCreateProfileMixin' with a county."""
    user = UserFactory(
        username="patrick", first_name="Patrick", email="web@pkimber.net"
    )
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    contact = ContactFactory(user=user)
    location = LocationFactory()
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    # check the factory creates this region
    assert location.region.region_code == "DEV"
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.create.profile")
    data = {
        "job_experience": experience.pk,
        "looking_for_work": True,
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    # ensure we are redirecting to the dashboard
    redirect_url = reverse("recruitment.candidate.dashboard")
    assert redirect_url in response["Location"]
    # check we created a the candidate profile for the contact
    contact = Contact.objects.get(user=user)
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    assert candidate.looking_for_work is True
    check_welcome_email(candidate)


@pytest.mark.django_db
def test_candidate_create_profile_blank_fails_with_redirect(client):
    """Test 'CandidateCreateProfileMixin' with a county."""
    user = UserFactory()
    contact = ContactFactory(user=user)
    location = LocationFactory()
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # check the factory creates this region
    assert location.region.region_code == "DEV"
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.create.profile") + "?next=some/link"
    response = client.post(url, {})
    # should not redirect should resend form with error
    assert 200 == response.status_code
    assert {
        "__all__": ["You must upload a CV"],
        "job_experience": ["This field is required."],
    } == response.context["form"].errors
    # check we did not create the candidate profile for the contact
    contact = Contact.objects.get(user=user)
    assert not contact.is_candidate


# @pytest.mark.django_db
# def test_candidate_create_profile_cv_succeeds_with_redirect(client):
#     """Test 'CandidateCreateProfileMixin' with a county."""
#     user = UserFactory()
#     contact = ContactFactory(user=user)
#     location = LocationFactory()
#     # check the factory creates this region
#     assert location.region.region_code == 'DEV'
#     # action
#     assert client.login(username=user.username, password=TEST_PASSWORD)
#     url = reverse('recruitment.candidate.create.profile') + "?next=some/link"
#     assert os.path.isfile('example_job/tests/data/cv.pdf')
#     cv_file = SimpleUploadedFile(
#             'cv.pdf',
#             open('example_job/tests/data/cv.pdf', 'rb', buffering=50),
#             content_type="application/pdf")
#     response = client.post(url, {'upload_cv': cv_file})
#     # should redirect on success
#     assert HTTPStatus.FOUND == response.status_code
#     # check we created a the candidate profile for the contact
#     contact = Contact.objects.get(user=user)
#     assert contact.is_candidate
#     # check the cv got saved
#     cv = CV.objects.latest(contact)
#     assert None != cv


@pytest.mark.django_db
def test_candidate_create_profile_postcode(client):
    """Test 'CandidateCreateProfileMixin' with a postcode."""
    user = UserFactory(
        username="patrick", first_name="Patrick", email="web@pkimber.net"
    )
    contact = ContactFactory(user=user)
    location = LocationFactory()
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # check the factory creates this postcode
    assert location.postcode == "EX20"
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.create.profile")
    response = client.post(
        url,
        {
            "job_experience": experience.pk,
            "data_type": "postcode",
            "code": "EX20",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    # check we created a candidate profile for the contact
    contact = Contact.objects.get(user=user)
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    check_welcome_email(candidate)


@pytest.mark.django_db
def test_candidate_dashboard(client):
    user = UserFactory()
    contact = ContactFactory(user=user)
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    CandidateFactory(contact=contact, job_experience=experience)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("recruitment.candidate.dashboard"))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_candidate_update(client):
    user = UserFactory(
        username="patrick",
        first_name="Apple",
        last_name="Orange",
        email="patrick@kbsoftware.co.uk",
    )
    contact = ContactFactory(user=user)
    CandidateFactory(
        contact=contact,
        job_experience=ExperienceFactory(slug=Experience.TRAINEE),
    )
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    data = {
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "postcode": "EX20",
    }
    url = reverse("recruitment.candidate.update")
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # ensure we are redirecting to the dashboard
    redirect_url = reverse("recruitment.candidate.dashboard")
    assert redirect_url == response.url
    user.refresh_from_db()
    assert user.first_name == "Patrick"
    assert user.last_name == "Kimber"
    assert user.email == "web@pkimber.net"
    contact.refresh_from_db()
    assert contact.title == "Mr"
    assert contact.address_1 == "High Street"
    assert contact.town == "Hatherleigh"
    assert contact.county == "Devon"
    assert contact.country == "UK"
    assert contact.mobile == "0123"
    assert contact.postcode == "EX20"


@pytest.mark.django_db
def test_candidate_update_profile(client):
    """I am a candidate editing my own profile."""
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # candidate
    user = UserFactory()
    candidate = CandidateFactory(contact=ContactFactory(user=user))
    cv = CVFactory(contact=candidate.contact)
    # login
    ExperienceFactory(slug=Experience.TRAINEE, title="Trainee")
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("recruitment.candidate.update.profile"))
    assert HTTPStatus.OK == response.status_code
    assert "latest_cv" in response.context
    assert cv == response.context["latest_cv"]
    assert "form" in response.context
    # for more information, search for 'consent_form_settings'
    assert "form_notice" in response.context
    form = response.context["form"]
    assert set(
        [
            "area",
            "code",
            "data_type",
            "job_experience",
            "newsletter_checkbox",
            "search_checkbox",
            "notify_checkbox",
            "picture",
            "radius",
            "upload_cv",
        ]
    ) == set(form.fields.keys())


@pytest.mark.django_db
def test_candidate_update_profile_blank(client):
    """Test 'CandidateCreateProfileMixin' with a county."""
    user = UserFactory(
        username="patrick", first_name="Patrick", email="web@pkimber.net"
    )
    contact = ContactFactory(user=user)
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    LocationFactory(postcode="EX20", town="Okehampton")
    candidate = CandidateFactory(
        contact=contact,
        job_experience=experience,
        job_location_code="EX20",
        job_location_radius=123,
        job_location_type="postcode",
    )
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.update.profile")
    response = client.post(url, {"job_experience": experience.pk})
    assert HTTPStatus.FOUND == response.status_code
    # ensure we are redirecting to the dashboard
    redirect_url = reverse("recruitment.candidate.dashboard")
    assert redirect_url in response["Location"]
    # check candidate profile exists for the contact
    contact = Contact.objects.get(user=user)
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    check_welcome_email(candidate)


@pytest.mark.django_db
def test_candidate_update_location_and_remove(client):
    LocationFactory(
        postcode="EX20",
        town="Okehampton",
        longitude=Decimal("-4.01882"),
        latitude=Decimal("50.75004"),
    )
    LocationFactory(
        postcode="EX1",
        town="Exeter",
        longitude=Decimal("-3.50479"),
        latitude=Decimal("50.72543"),
    )
    LocationFactory(
        postcode="EX10",
        town="Sidmouth",
        longitude=Decimal("-3.24461"),
        latitude=Decimal("50.69252"),
    )
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)

    user = UserFactory(username="can1")
    can = CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)

    job_locations = CandidateJobLocation.objects.filter(candidate=can)
    assert len(job_locations) == 0
    url = reverse("recruitment.candidate.update.profile")
    response = client.post(
        url,
        {
            "job_experience": experience.pk,
            "code": "EX20",
            "radius": 5,
            "data_type": "postcode",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    can.refresh_from_db()
    assert can.job_location_code == "EX20"
    assert can.job_location_type == "postcode"
    assert can.job_location_radius == 5
    job_locations = CandidateJobLocation.objects.filter(candidate=can)
    assert len(job_locations) == 1
    response = client.post(url, {"job_experience": experience.pk})
    assert HTTPStatus.FOUND == response.status_code
    can.refresh_from_db()
    assert not can.job_location_code
    assert not can.job_location_type
    assert not can.job_location_radius
    job_locations = CandidateJobLocation.objects.filter(candidate=can)
    assert len(job_locations) == 0


@pytest.mark.django_db
def test_candidate_update_profile_blank_fails_with_redirect(client):
    """Test 'CandidateCreateProfileMixin' with a county."""
    user = UserFactory()
    contact = ContactFactory(user=user)
    CandidateFactory(contact=contact)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    ExperienceFactory(slug="trainee", title="Trainee")
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.update.profile") + "?next=some/link"
    response = client.post(url, {})
    # should not redirect should resend form with error
    assert 200 == response.status_code
    # check candidate profile exists for the contact
    contact = Contact.objects.get(user=user)
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    assert candidate.welcome_email_sent is False


@pytest.mark.django_db
def test_candidate_update_profile_blank_cv_when_redirecting(client):
    """Test 'CandidateCreateProfileMixin' with a county."""
    user = UserFactory(
        username="patrick", first_name="Patrick", email="web@pkimber.net"
    )
    contact = ContactFactory(user=user)
    CandidateFactory(contact=contact)
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.update.profile") + "?next=some/link"
    response = client.post(url, {"job_experience": experience.pk})
    # should not redirect should resend form with error
    assert 200 == response.status_code
    # check candidate profile exists for the contact
    contact = Contact.objects.get(user=user)
    assert contact.is_candidate
    candidate = Candidate.objects.get(contact=contact)
    assert candidate.welcome_email_sent is False


@pytest.mark.django_db
@pytest.mark.parametrize("file_name", ["cv.pdf", "Sin\xe9adOMearaCV1.docx"])
def test_candidate_update_profile_upload_cv(client, file_name):
    user = UserFactory(
        username="patrick", first_name="Patrick", email="web@pkimber.net"
    )
    contact = ContactFactory(user=user)
    CandidateFactory(contact=contact)
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    MailTemplateFactory(slug=Candidate.CANDIDATE_WELCOME)
    with pytest.raises(CV.DoesNotExist):
        CV.objects.get(contact=contact)
    file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", file_name
    )
    assert client.login(username=user.username, password=TEST_PASSWORD)
    with open(file_name, "rb") as fp:
        # action
        url = reverse("recruitment.candidate.update.profile")
        response = client.post(
            url, {"job_experience": experience.pk, "upload_cv": fp}
        )
        assert HTTPStatus.FOUND == response.status_code
        # ensure we are redirecting to the dashboard
        redirect_url = reverse("recruitment.candidate.dashboard")
        assert redirect_url in response["Location"]
        # check candidate profile exists for the contact
        contact = Contact.objects.get(user=user)
        assert contact.is_candidate
        cv = CV.objects.get(contact=contact)
        assert "/media-private/job/candidate/cv/" in cv.file_name.path
    candidate = Candidate.objects.get(contact=contact)
    check_welcome_email(candidate)


@pytest.mark.django_db
def test_candidate_update_profile_welcome_already_sent(client):
    """Update the profile - but don't re-send the welcome email."""
    user = UserFactory()
    contact = ContactFactory(user=user)
    experience = ExperienceFactory(slug=Experience.TRAINEE)
    date_welcome_email = datetime(2017, 12, 31, 0, 0, 0, tzinfo=pytz.utc)
    candidate = CandidateFactory(
        contact=contact,
        job_experience=experience,
        date_welcome_email=date_welcome_email,
    )
    ConsentFormSettingsFactory(slug="generic-form")
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ConsentFactory(slug=Candidate.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    assert candidate.welcome_email_sent is True
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.update.profile")
    response = client.post(url, {"job_experience": experience.pk})
    assert HTTPStatus.FOUND == response.status_code
    # ensure we are redirecting to the dashboard
    redirect_url = reverse("recruitment.candidate.dashboard")
    assert redirect_url == response.url
    # check candidate profile exists for the contact
    contact = Contact.objects.get(user=user)
    assert contact.is_candidate
    candidate.refresh_from_db()
    assert candidate.welcome_email_sent is True
    assert candidate.date_welcome_email == date_welcome_email
    assert 0 == Mail.objects.count()
    assert 0 == Message.objects.count()
