# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal
from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from finance.tests.factories import VatSettingsFactory
from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory
from job.models import (
    Candidate,
    Experience,
    JobBasket,
    JobCredit,
    PRODUCT_JOB_POST,
    PRODUCT_JOB_POST_DISCOUNT,
    Recruiter,
)
from job.tests.factories import (
    CandidateFactory,
    ExperienceFactory,
    JobFactory,
    RecruiterFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from stock.tests.factories import ProductFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_candidate_detail(client):
    consent = ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    user = UserFactory()
    RecruiterFactory(
        contact=ContactFactory(user=user), search_expires=timezone.now()
    )
    candidate = CandidateFactory(
        contact=ContactFactory(user=UserFactory()),
        job_experience=ExperienceFactory(slug=Experience.TRAINEE),
    )
    UserConsent.objects.set_consent(consent, True, candidate.contact.user)
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.detail", args=[candidate.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "candidate" in response.context
    assert candidate == response.context["candidate"]
    assert "object" in response.context
    assert candidate == response.context["object"]


@pytest.mark.django_db
def test_candidate_detail_for_recruiter_no_consent(client):
    ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    user = UserFactory()
    RecruiterFactory(
        contact=ContactFactory(user=user), search_expires=timezone.now()
    )
    candidate = CandidateFactory(contact=ContactFactory(user=UserFactory()))
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.detail", args=[candidate.pk])
    response = client.get(url)
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_recruiter_dashboard(client):
    user = UserFactory()
    RecruiterFactory(contact=ContactFactory(user=user))
    ConsentFactory(slug=Recruiter.GDPR_NEWSLETTER_SLUG)

    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("recruitment.recruiter.dashboard"))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_recruiter_dashboard_payment(client):
    """Recruiter pay for a job posting."""
    VatSettingsFactory()
    ProductFactory(slug=PRODUCT_JOB_POST, price=Decimal("150.00"))
    ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT, price=Decimal("100.00"))
    user = UserFactory(username="r")
    r = RecruiterFactory(contact=ContactFactory(user=user))
    JobFactory(publish=False, recruiter=r, title="Django Programmer")
    JobFactory(publish=False, recruiter=r, title="python Programmer")
    # recruiter login
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.recruiter.dashboard")
    # action
    response = client.post(url)
    # assert
    assert 302 == response.status_code
    assert 1 == JobBasket.objects.count()
    basket = JobBasket.objects.first()
    assert 2 == basket.jobbasketitem_set.count()
    assert 1 == len(basket.checkout_description)
    # 2 x 100 pounds + VAT
    assert Decimal("240.00") == basket.checkout_total


@pytest.mark.django_db
def test_recruiter_job_credits(client):
    user = UserFactory()
    RecruiterFactory(contact=ContactFactory(user=user))
    ProductFactory(slug=PRODUCT_JOB_POST, price=Decimal("150.00"))
    ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT, price=Decimal("100.00"))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("recruitment.recruiter.job-credits"))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_recruiter_job_credits_post(client):
    user = UserFactory()
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    ProductFactory(slug=PRODUCT_JOB_POST, price=Decimal("150.00"))
    ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT, price=Decimal("100.00"))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.post(
        reverse("recruitment.recruiter.job-credits"), data={"credits": 3}
    )
    assert 302 == response.status_code, response.context["form"].errors
    job_credit = JobCredit.objects.get(recruiter=recruiter)
    url = reverse("recruitment.job.checkout", args=[job_credit.pk])
    assert url == response.url


@pytest.mark.django_db
def test_recruiter_update(client):
    user = UserFactory()
    contact = ContactFactory(user=user)
    RecruiterFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    data = {
        "company_name": "KB",
        "title": "Mr",
        "first_name": "Patrick",
        "last_name": "Kimber",
        "email": "web@pkimber.net",
        "address_1": "High Street",
        "town": "Hatherleigh",
        "county": "Devon",
        "country": "UK",
        "mobile": "0123",
        "postcode": "EX20",
    }
    response = client.post(reverse("recruitment.recruiter.update"), data)
    assert 302 == response.status_code, response.context["form"].errors
    url = reverse("recruitment.recruiter.dashboard")
    assert url == response.url
    user.refresh_from_db()
    assert user.first_name == "Patrick"
    assert user.last_name == "Kimber"
    assert user.email == "web@pkimber.net"
    contact.refresh_from_db()
    assert contact.company_name == "KB"
    assert contact.title == "Mr"
    assert contact.address_1 == "High Street"
    assert contact.town == "Hatherleigh"
    assert contact.county == "Devon"
    assert contact.country == "UK"
    assert contact.mobile == "0123"
    assert contact.postcode == "EX20"
