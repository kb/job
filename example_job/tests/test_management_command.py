# -*- encoding: utf-8 -*-
import pytest
import responses

from django.core.management import call_command
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory
from job.models import Candidate, FEED_RECRUITER
from job.tasks import process_feed_simply_law_jobs
from job.tests.factories import (
    ContactFactory,
    JobFactory,
    JobFeedFactory,
    JobRoleFactory,
    JobRoleKeywordFactory,
    PracticeAreaFactory,
    PracticeAreaKeywordFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory
from mail.tests.factories import MailTemplateFactory


@pytest.mark.django_db
@pytest.mark.elasticsearch
@responses.activate
def test_process_feed_simple_law_jobs():
    """Import Simply Law Jobs.

    Test code copied from ``test_service.py``, ``test_json_feed``.

    """
    call_command("init_app_job")
    # setup job roles
    job_role = JobRoleFactory(title="Legal Secretary", order=2)
    JobRoleKeywordFactory(job_role=job_role, word="secret")
    # setup practice areas
    practice_area = PracticeAreaFactory(title="Family", order=2)
    PracticeAreaKeywordFactory(practice_area=practice_area, word="family")
    PracticeAreaKeywordFactory(practice_area=practice_area, word="matrimonial")
    # mock
    null = None
    responses.add(
        responses.GET,
        "https://www.simplylawjobs.com/export/third-party/jobs/ilspa.json",
        json={
            "jobs": [
                {
                    "job": [
                        {
                            "category": "Insolvency & Debt Recovery,Partner",
                            "jobref": 1027121,
                            "date": "2023-03-02 11:01:59",
                            "title": "Legal Secretary",
                            "company": "G2 Legal Limited",
                            "email": "london@g2legal.com",
                            "url": "https://www.simplylawjobs.com/job/1027121",
                            "salary": "From \u00a3100000",
                            "jobtype": "full-time",
                            "country": "UK",
                            "region": null,
                            "city": "Hampshire",
                            "description": "Are you an experienced qualified Insolvency Solicitor looking to take that step up to Partnership?   Or are you currently a Partner but looking for a new challenge for 2023?<br> <br> My client is a leading, Legal 150, Southampton-based law firm with a great reputation and a proven track record for success. They are looking to grow their successful Insolvency team in this this critical hire at Partner level. The team handles both non-contentious and contentious insolvency matters to include corporate insolvency, restructuring, and solvent reorganisation matters. <br><br>You will be experienced in restructuring and insolvency transactional and technical advisory work, but may also have some contentious experience.<br><br>Management experience is required and you will be involved in the growth and development of the team. My client offers a generous benefits package, including flexible hybrid working. <br><br>Your role as an Insolvency Associate Solicitor/Senior Associate will involve: <br>\r\n<ul><li>Exposure to high quality work from day one </li><li>Excellent career opportunities to progress from the outset</li><li>Being active in business development, gaining new clients and growing the department </li></ul>\r\n<br>My client is eager to shortlist for interviews as soon as possible.<br> <br> Please contact Chris Rodriguez at G2 Legal ASAP to discuss this Insolvency Partner role based in Southampton, or send over your up-to-date CV confidentially via the link below.<br><br>(Please note that salary is just a guideline).<br><br><strong>#G2MSS</strong>",
                            "image": "https://assets-jb.fmg-services.co.uk/SLJ/uploads/company/1_500/c8a6e2b543c27bb1a226dda90e5a2c8d81f570b5.jpg",
                            "closedate": "2023-03-30 11:01:59",
                            "companytype": "Recruitment Agency",
                            "practicearea": "Insolvency & Debt Recovery",
                            "role": "Partner",
                            "AdditionalFields": "",
                        }
                    ]
                }
            ]
        },
        status=200,
    )
    assert 1 == process_feed_simply_law_jobs()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_rebuild_job_index():
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=recruiter)
    for counter in range(1, 5):
        JobFactory(
            recruiter=recruiter,
            title="title{}".format(counter),
            order_key=FEED_RECRUITER,
            publish_date=(timezone.now() + relativedelta(hours=-counter)),
        )
    call_command("rebuild_job_index")


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_refresh_job_index():
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=recruiter)
    for counter in range(1, 5):
        JobFactory(
            recruiter=recruiter,
            title="title{}".format(counter),
            order_key=FEED_RECRUITER,
            publish_date=(timezone.now() + relativedelta(hours=-counter)),
        )
    call_command("refresh_job_index")


@pytest.mark.django_db
def test_send_job_notify_email():
    MailTemplateFactory(slug=Candidate.MAIL_TEMPLATE_NOTIFY_JOBS)
    user = UserFactory(email="test@test.com")
    ContactFactory(user=user)
    UserConsent.objects.set_consent(
        ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG),
        True,
        user=user,
    )
    call_command("send_job_notify_email", "test@test.com")
