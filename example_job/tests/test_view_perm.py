# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth.models import User
from django.urls import reverse

from base.tests.test_utils import PermTestCase
from login.tests.fixture import perm_check
from job.tests.factories import (
    CandidateFactory,
    JobBasketFactory,
    JobCreditFactory,
    JobFactory,
    RecruiterFactory,
)

from .factories import ContactFactory


def _create_contact(user_name):
    user = User.objects.get(username=user_name)
    ContactFactory(user=user)


@pytest.mark.django_db
def test_job_candidate_update(perm_check):
    candidate = CandidateFactory(contact=ContactFactory())
    url = reverse("job.candidate.update", args=[candidate.contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_job_delete(perm_check):
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("job.delete", args=[job.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_job_detail(perm_check):
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("job.detail", args=[job.pk])
    perm_check.staff(url)


# @pytest.mark.django_db
# def test_job_checkout_basket_detail(perm_check):
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     basket = JobBasketFactory(recruiter=recruiter)
#     url = reverse("job.checkout.basket.detail", args=[basket.pk])
#     perm_check.staff(url)


@pytest.mark.django_db
def test_job_list(perm_check):
    url = reverse("job.list", args=["0"])
    perm_check.staff(url)


@pytest.mark.django_db
def test_job_recruiter_job_create(perm_check):
    recruiter = RecruiterFactory(contact=ContactFactory())
    url = reverse("job.recruiter.job.create", args=[recruiter.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_job_recruiter_update(perm_check):
    recruiter = RecruiterFactory(contact=ContactFactory())
    url = reverse("job.recruiter.update", args=[recruiter.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_job_update(perm_check):
    _create_contact("web")
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("job.update", args=[job.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_jobcredit_mark_paid(perm_check):
    credit = JobCreditFactory(
        recruiter=RecruiterFactory(contact=ContactFactory()), credits=1
    )
    url = reverse("job.credit.mark-paid", args=[credit.pk])
    perm_check.staff(url)


# @pytest.mark.django_db
# def test_jobcredit_detail(perm_check):
#     credit = JobCreditFactory(
#         recruiter=RecruiterFactory(contact=ContactFactory()), credits=1
#     )
#     url = reverse("job.checkout.credit.detail", args=[credit.pk])
#     perm_check.staff(url)


@pytest.mark.django_db
def test_update(perm_check):
    _create_contact("web")
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("job.update", kwargs=dict(pk=job.pk))
    perm_check.staff(url)


# class TestViewPerm(PermTestCase):
# def setUp(self):
#     self.setup_users()
#     # create contact records for users
#     self._create_contact("staff")
#     self._create_contact("web")

# def _create_contact(self, user_name):
#     user = User.objects.get(username=user_name)
#     ContactFactory(user=user)

# def test_job_application(self):
#     job = JobFactory(
#         recruiter=RecruiterFactory(contact=ContactFactory()
#         ))
#     application = JobApplicationFactory(
#         candidate=CandidateFactory(contact=ContactFactory()),
#         job=job,
#     )
#     url = reverse('job.application', args=[application.pk])
#     self.assert_staff_only(url)

# def test_job_application_detail(self):
#     job = JobFactory(recruiter=RecruiterFactory(
#         contact=ContactFactory()
#         ))
#     application = JobApplicationFactory(
#         candidate=CandidateFactory(contact=ContactFactory()),
#         job=job,
#     )
#     url = reverse('job.application.detail', args=[application.pk])
#     self.assert_staff_only(url)

# def test_job_candidate_detail(self):
#     candidate = CandidateFactory(contact=ContactFactory())
#     url = reverse('job.candidate.detail', args=[candidate.pk])
#     self.assert_staff_only(url)

# def test_job_recruiter_detail(self):
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     url = reverse('job.recruiter.detail', args=[recruiter.pk])
#     self.assert_staff_only(url)
