# -*- encoding: utf-8 -*-
import pytest

from dateutil.relativedelta import relativedelta
from django.utils import timezone

from job.models import Job, JobSettings
from job.tests.factories import (
    JobFactory,
    JobFeedFactory,
    JobFeedItemFactory,
    RecruiterFactory,
)
from .factories import ContactFactory


def _setup_search():
    # indeed.com
    recruiter_feed = RecruiterFactory(
        contact=ContactFactory(company_name="Indeed")
    )
    job_feed = JobFeedFactory(recruiter=recruiter_feed)
    # option 1
    JobFeedItemFactory(
        company="KB Software",
        feed=job_feed,
        job=JobFactory(title="1", recruiter=recruiter_feed),
    )
    # option 2
    JobFeedItemFactory(
        source="Hatherleigh",
        feed=job_feed,
        job=JobFactory(title="2", recruiter=recruiter_feed),
    )
    # option 3
    recruiter = RecruiterFactory(
        contact=ContactFactory(company_name="Cornwall Farmers")
    )
    JobFactory(title="3", recruiter=recruiter)


def _setup_period_search():
    recruiter = RecruiterFactory(
        contact=ContactFactory(company_name="Impatient Watchmaker Limited")
    )
    job_settings = JobSettings.load()
    today = timezone.now()
    max_days_displayed = job_settings.job_post_max_days_displayed
    stop_display_date = today + relativedelta(days=-max_days_displayed)
    # vary publish date (all close dates valid)
    # valid
    JobFactory(
        title="A",
        description="Published today",
        recruiter=recruiter,
        publish=True,
        publish_date=timezone.now(),
        close_date=timezone.now().date() + relativedelta(days=2),
    )
    # valid
    JobFactory(
        title="B",
        description="Published on cut off date",
        recruiter=recruiter,
        publish=True,
        publish_date=stop_display_date,
        close_date=timezone.now().date() + relativedelta(days=2),
    )
    # valid
    JobFactory(
        title="C",
        description="published on the Day after the cut off date",
        recruiter=recruiter,
        publish=True,
        publish_date=stop_display_date + relativedelta(days=1),
        close_date=timezone.now().date() + relativedelta(days=2),
    )
    # invalid
    JobFactory(
        title="D",
        description="published on the Day before the cut off date",
        recruiter=recruiter,
        publish=True,
        publish_date=stop_display_date + relativedelta(days=-1),
        close_date=timezone.now().date() + relativedelta(days=2),
    )
    # vary close date (all publish dates valid)
    # valid
    JobFactory(
        title="E",
        description="Closes today",
        recruiter=recruiter,
        publish_date=timezone.now() + relativedelta(days=-1),
        publish=True,
        close_date=timezone.now().date(),
    )
    # valid
    JobFactory(
        title="F",
        description="closes tomorrow",
        recruiter=recruiter,
        publish=True,
        publish_date=timezone.now() + relativedelta(days=-1),
        close_date=timezone.now().date() + relativedelta(days=1),
    )
    # invalid
    JobFactory(
        title="G",
        description="closed yesterday",
        recruiter=recruiter,
        publish=True,
        publish_date=timezone.now() + relativedelta(days=-1),
        close_date=timezone.now().date() + relativedelta(days=-1),
    )
    # both close date and publish date invalid
    JobFactory(
        title="H",
        description=(
            "closed yesterday, published more than the display period ago"
        ),
        recruiter=recruiter,
        publish=True,
        publish_date=stop_display_date + relativedelta(days=-1),
        close_date=timezone.now().date() + relativedelta(days=-1),
    )
    # unpublished (closing date valid)
    JobFactory(
        title="I",
        description=(
            "closed yesterday, published more than the display period ago"
        ),
        recruiter=recruiter,
        publish=False,
        close_date=timezone.now().date() + relativedelta(days=1),
    )


@pytest.mark.django_db
def test_search_all():
    JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title=None,
        recruiter=None,
    )
    assert 2 == qs.count()


@pytest.mark.django_db
def test_search_recruiter():
    recruiter = RecruiterFactory(contact=ContactFactory(company_name="Patrick"))
    JobFactory(title="1", recruiter=recruiter)
    JobFactory(title="2", recruiter=RecruiterFactory(contact=ContactFactory()))
    JobFactory(title="3", recruiter=recruiter)
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title=None,
        recruiter="PAT",
    )
    assert ["1", "3"] == [j.title for j in qs.order_by("title")]


@pytest.mark.django_db
def test_search_recruiter_name_option_1():
    """Search in the same fields as 'recruiter_name' (see 'Job' model).

    The fields are::

      if self.recruiter.is_feed:
          if self.jobfeeditem.company:
              return self.jobfeeditem.company           # option 1
          elif self.jobfeeditem.source:
              return self.jobfeeditem.source            # option 2
      return self.recruiter.full_name                   # option 3

    """
    _setup_search()
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title=None,
        recruiter="kb",
    )
    assert ["1"] == [j.title for j in qs]


@pytest.mark.django_db
def test_search_recruiter_name_option_2():
    """Search in the same fields as 'recruiter_name' (see 'Job' model).

    The fields are::

      if self.recruiter.is_feed:
          if self.jobfeeditem.company:
              return self.jobfeeditem.company           # option 1
          elif self.jobfeeditem.source:
              return self.jobfeeditem.source            # option 2
      return self.recruiter.full_name                   # option 3

    """
    _setup_search()
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title=None,
        recruiter="hatherleigh",
    )
    assert ["2"] == [j.title for j in qs]


@pytest.mark.django_db
def test_search_recruiter_name_option_3():
    """Search in the same fields as 'recruiter_name' (see 'Job' model).

    The fields are::

      if self.recruiter.is_feed:
          if self.jobfeeditem.company:
              return self.jobfeeditem.company           # option 1
          elif self.jobfeeditem.source:
              return self.jobfeeditem.source            # option 2
      return self.recruiter.full_name                   # option 3

    """
    _setup_search()
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title=None,
        recruiter="farm",
    )
    assert ["3"] == [j.title for j in qs]


@pytest.mark.django_db
def test_search_recruiter_name_not_feed():
    """Search in the same fields as 'recruiter_name' (see 'Job' model).

    The fields are::

      if self.recruiter.is_feed:
          if self.jobfeeditem.company:
              return self.jobfeeditem.company           # option 1
          elif self.jobfeeditem.source:
              return self.jobfeeditem.source            # option 2
      return self.recruiter.full_name                   # option 3

    """
    _setup_search()
    # do NOT find indeed
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title=None,
        recruiter="indeed",
    )
    assert 0 == qs.count()


@pytest.mark.django_db
def test_search_title():
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFactory(title="Semi-Skimmed Milk", recruiter=recruiter)
    JobFactory(title="Cheese", recruiter=recruiter)
    JobFactory(title="Whole Milk", recruiter=recruiter)
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title="milk",
        recruiter=None,
    )
    assert ["Semi-Skimmed Milk", "Whole Milk"] == [j.title for j in qs]


@pytest.mark.django_db
def test_job_current():
    _setup_period_search()
    qs = Job.objects.current()
    # not expecting D, G and H
    assert ["A", "B", "C", "E", "F"] == [j.title for j in qs]


@pytest.mark.django_db
def test_job_current_include_unpublished():
    _setup_period_search()
    qs = Job.objects.current(include_unpublished=True)
    # not expecting D, G and H
    assert ["A", "B", "C", "E", "F", "I"] == [j.title for j in qs]


@pytest.mark.django_db
def test_job_current_include_closed():
    _setup_period_search()
    qs = Job.objects.current(include_closed=True)
    # not expecting I
    assert ["A", "B", "C", "D", "E", "F", "G", "H"] == [j.title for j in qs]


@pytest.mark.django_db
def test_job_current_include_unpublished_and_closed():
    _setup_period_search()
    qs = Job.objects.current(include_unpublished=True, include_closed=True)
    # expecting all
    assert ["A", "B", "C", "D", "E", "F", "G", "H", "I"] == [
        j.title for j in qs
    ]


@pytest.mark.django_db
def test_search_display_period():
    _setup_period_search()
    qs = Job.objects.search(
        region=None,
        postcode=None,
        postcode_range=None,
        title=None,
        recruiter=None,
    )
    # not expecting D, G and H
    assert ["A", "F", "E", "C", "B"] == [j.title for j in qs]
