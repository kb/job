# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.db import connection
from django.utils import timezone

from gdpr.models import Consent, UserConsent
from gdpr.tests.factories import ConsentFactory
from job.models import Candidate, Region, Experience
from job.tests.factories import (
    CandidateFactory,
    CandidateJobLocationFactory,
    CountryFactory,
    CVFactory,
    ExperienceFactory,
    HighestEducationFactory,
    LocationFactory,
    RegionFactory,
    YearsExperienceFactory,
)
from login.tests.factories import UserFactory
from .factories import ContactFactory


def setup_search():
    consent = ConsentFactory(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    ExperienceFactory(slug=Experience.TRAINEE)
    eng = CountryFactory(name="England", alpha2_code="EN", country_code="ENG")
    devon = RegionFactory(name="Devon", slug="devon")
    gloucestershire = RegionFactory(
        name="Gloucestershire", slug="gloucestershire"
    )
    blackpool = RegionFactory(name="Blackpool", slug="blackpool")
    ex1 = LocationFactory(
        postcode="EX1",
        latitude="50.72543",
        longitude="-3.50479",
        town="Exeter",
        region=devon,
        country=eng,
    )
    LocationFactory(
        postcode="EX2",
        latitude="50.70998",
        longitude="-3.51705",
        town="Exeter",
        region=devon,
        country=eng,
    )
    ex3 = LocationFactory(
        postcode="EX3",
        latitude="50.68645",
        longitude="-3.45824",
        town="Exeter",
        region=devon,
        country=eng,
    )
    ex39 = LocationFactory(
        postcode="EX39",
        latitude="51.01394",
        longitude="-4.24557",
        town="Abbotsham",
        region=devon,
        country=eng,
    )
    fy1 = LocationFactory(
        postcode="FY1",
        latitude="53.81551",
        longitude="-3.04509",
        town="Blackpool",
        region=blackpool,
        country=eng,
    )
    LocationFactory(
        postcode="FY2",
        latitude="53.84524",
        longitude="-3.03823",
        town="Blackpool",
        region=blackpool,
        country=eng,
    )
    LocationFactory(
        postcode="GL11",
        latitude="51.68875",
        longitude="-2.35591",
        town="Dursley",
        region=gloucestershire,
        country=eng,
    )
    ye1 = ExperienceFactory(slug="00-02-years", order=1)
    ye2 = ExperienceFactory(slug="03-06-years", order=2)
    ye3 = ExperienceFactory(slug="07-09-years", order=3)
    ExperienceFactory(slug="11+02-years", order=4)
    # candidate 1
    can1 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(username="can1", first_name="Tin", last_name="Can")
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        experience=YearsExperienceFactory(),
        job_experience=ye1,
    )
    UserConsent.objects.set_consent(consent, True, can1.contact.user)
    CandidateJobLocationFactory(candidate=can1, location=ex1)
    CVFactory(contact=can1.contact)

    # candidate 2
    can2 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can2",
                first_name="Oil",
                last_name="Can",
                last_login=timezone.now() - relativedelta(months=4),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye2,
    )
    UserConsent.objects.set_consent(consent, True, can2.contact.user)
    CandidateJobLocationFactory(candidate=can2, location=ex3)
    CVFactory(contact=can2.contact)

    # candidate 3
    can3 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can3",
                first_name="Coke",
                last_name="Can",
                last_login=timezone.now() - relativedelta(months=1),
            )
        ),
        highest_education=HighestEducationFactory(),
        experience=YearsExperienceFactory(),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can3.contact.user)
    CandidateJobLocationFactory(candidate=can3, location=ex39)
    CVFactory(contact=can3.contact)

    # candidate 4
    can4 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can4",
                first_name="Wii",
                last_name="Can",
                last_login=timezone.now() - relativedelta(months=13),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye1,
    )
    UserConsent.objects.set_consent(consent, True, can4.contact.user)
    can4.created = datetime(2014, 1, 3, 15, 15, 0, tzinfo=pytz.utc)
    can4.save()
    CandidateJobLocationFactory(candidate=can4, location=fy1)
    CVFactory(contact=can4.contact)

    # candidate 5
    can5 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(username="can5", first_name="Can", last_name="Can")
        ),
        highest_education=HighestEducationFactory(),
        # replaced with the new gdpr user consent model
        # allow_search=False,
    )
    UserConsent.objects.set_consent(consent, False, can5.contact.user)
    CVFactory(contact=can5.contact)


def setup_last_login_search():
    setup_search()
    consent = Consent.objects.get(slug=Candidate.GDPR_RECRUITER_SEARCH_SLUG)
    Experience.objects.get(slug="00-02-years")
    Experience.objects.get(slug="03-06-years")
    ye3 = Experience.objects.get(slug="07-09-years")
    # candidate 6
    can6 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can6",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-3)
                    + relativedelta(days=1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can6.contact.user)
    # candidate 7
    can7 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can7",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-3)
                    + relativedelta(days=-1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can7.contact.user)

    # candidate 8
    can8 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can8",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-6)
                    + relativedelta(days=1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can8.contact.user)

    # candidate 9
    can9 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can9",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-6)
                    + relativedelta(days=-1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can9.contact.user)

    # candidate 10
    can10 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can10",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-9)
                    + relativedelta(days=1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can10.contact.user)

    # candidate 11
    can11 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can11",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-9)
                    + relativedelta(days=-1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can11.contact.user)

    # candidate 12
    can12 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can12",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-12)
                    + relativedelta(days=1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can12.contact.user)

    # candidate 13
    can13 = CandidateFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="can13",
                first_name="Rusty",
                last_name="Can",
                last_login=(
                    timezone.now()
                    + relativedelta(months=-12)
                    + relativedelta(days=-1)
                ),
            )
        ),
        # replaced with the new gdpr user consent model
        # allow_search=True,
        job_experience=ye3,
    )
    UserConsent.objects.set_consent(consent, True, can13.contact.user)

    # for can in Candidate.objects.order_by('contact__user__last_login'):
    #     print(can.contact.user.username, can.contact.user.last_login)


def _get_region_devon():
    return Region.objects.get(slug="devon")


def _get_region_gloucestershire():
    return Region.objects.get(slug="gloucestershire")


def _get_region_blackpool():
    return Region.objects.get(slug="blackpool")


def _get_job_experience_0_2():
    return Experience.objects.get(slug="00-02-years")


def _get_job_experience_3_6():
    return Experience.objects.get(slug="03-06-years")


def _get_job_experience_7_9():
    return Experience.objects.get(slug="07-09-years")


def _get_job_experience_10plus():
    return Experience.objects.get(slug="11+02-years")


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_all():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=None,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 4 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_all_staff(django_assert_num_queries):
    """A member of staff will search all candidates.

    .. note:: A recruiter search (disabled on the live site for now) will only
              return candidates who have consented to a recruiter search.

    """
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=None,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=True,
    )
    assert 5 == qs.count()
    with django_assert_num_queries(1):
        [x.pk for x in qs.all()]
    assert ["can5", "can3", "can2", "can1", "can4"] == [
        x.contact.user.username for x in qs
    ]


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_region_devon():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=_get_region_devon(),
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 3 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_region_gloucestershire():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=_get_region_gloucestershire(),
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 0 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_postcode_ex1_5():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode="EX1",
        postcode_range=5,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 2 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_postcode_ex1_25():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode="EX1",
        postcode_range=25,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 2 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_postcode_ex1_50():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode="EX1",
        postcode_range=50,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 3 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_postcode_ex1_100():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode="EX1",
        postcode_range=100,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 3 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_postcode_ex1_999():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode="EX1",
        postcode_range=999,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 4 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_postcode_gl11_5():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode="GL11",
        postcode_range=5,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 0 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_postcode_gl11_25():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode="GL11",
        postcode_range=25,
        job_experience=None,
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 0 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_job_experience_0_2():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=_get_job_experience_0_2(),
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 2 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_job_experience_3_6():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=_get_job_experience_3_6(),
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 1 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_job_experience_7_9():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=_get_job_experience_7_9(),
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 1 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_job_experience_10plus():
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=_get_job_experience_10plus(),
        last_login=None,
        order_by=None,
        is_staff=None,
    )
    assert 0 == qs.count()


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_3_months():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=3,
        order_by="-last_login",
        is_staff=None,
    )
    assert 2 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can3", "can6"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_6_months():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=6,
        order_by="-last_login",
        is_staff=None,
    )
    assert 5 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can3", "can6", "can7", "can2", "can8"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_9_months():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=9,
        order_by="-last_login",
        is_staff=None,
    )
    assert 7 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can3", "can6", "can7", "can2", "can8", "can9", "can10"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_12_months():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=12,
        order_by="-last_login",
        is_staff=None,
    )
    assert 9 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert [
        "can3",
        "can6",
        "can7",
        "can2",
        "can8",
        "can9",
        "can10",
        "can11",
        "can12",
    ] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_unlimited():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=0,
        order_by="-last_login",
        is_staff=None,
    )
    assert 12 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert [
        "can1",
        "can3",
        "can6",
        "can7",
        "can2",
        "can8",
        "can9",
        "can10",
        "can11",
        "can12",
        "can13",
        "can4",
    ] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_name(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="name",
        is_staff=None,
    )

    result = [c.contact.user.username for c in qs]
    assert ["can3", "can2", "can1", "can4"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_name_descending(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="-name",
        is_staff=None,
    )
    result = [c.contact.user.username for c in qs]
    assert ["can4", "can1", "can2", "can3"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_job_experience(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="job_experience",
        is_staff=None,
    )

    result = [c.contact.user.username for c in qs]
    assert ["can1", "can4", "can2", "can3"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_job_experience_descending(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="-job_experience",
        is_staff=None,
    )

    result = [c.contact.user.username for c in qs]
    assert ["can3", "can2", "can4", "can1"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_last_login(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="last_login",
        is_staff=None,
    )

    result = [c.contact.user.username for c in qs]
    assert ["can4", "can2", "can3", "can1"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_last_login_descending(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="-last_login",
        is_staff=None,
    )

    result = [c.contact.user.username for c in qs]
    assert ["can1", "can3", "can2", "can4"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_registered(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="created",
        is_staff=None,
    )

    result = [c.contact.user.username for c in qs]
    assert ["can4", "can1", "can2", "can3"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_order_by_registered_descending(client):
    setup_search()
    qs = Candidate.objects.search(
        Candidate.MAILSHOT_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=None,
        order_by="-created",
        is_staff=None,
    )

    result = [c.contact.user.username for c in qs]
    assert ["can3", "can2", "can1", "can4"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_3_months_position():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.POSITION_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=3,
        order_by="-last_login",
        is_staff=None,
    )
    assert 1 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can3"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_6_months_position():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.POSITION_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=6,
        order_by="-last_login",
        is_staff=None,
    )
    assert 2 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can3", "can2"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_9_months_position():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.POSITION_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=9,
        order_by="-last_login",
        is_staff=None,
    )
    assert 2 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can3", "can2"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_12_months_position():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.POSITION_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=12,
        order_by="-last_login",
        is_staff=None,
    )
    assert 2 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can3", "can2"] == result


@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
@pytest.mark.django_db
def test_search_last_login_unlimited_position():
    setup_last_login_search()
    qs = Candidate.objects.search(
        Candidate.POSITION_SEARCH,
        region=None,
        postcode=None,
        postcode_range=0,
        job_experience=None,
        last_login=0,
        order_by="-last_login",
        is_staff=None,
    )
    assert 4 == qs.count()
    result = [c.contact.user.username for c in qs]
    assert ["can1", "can3", "can2", "can4"] == result
