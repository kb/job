# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.urls import reverse
from django.utils import timezone

from base.url_utils import url_with_querystring
from job.models import FEED_RECRUITER, ILSPA_RECRUITER
from job.search import JobIndex
from job.tests.factories import JobFactory, JobFeedFactory, RecruiterFactory
from search.search import SearchIndex
from .factories import ContactFactory


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_elastic_search(client):
    """

    Test data copied from:
    ``example_job/tests/test_job.py``, ``test_index_job``

    """
    exp = date.today() + relativedelta(years=1)
    recruiter = RecruiterFactory(
        contact=ContactFactory(), subscription_expires=exp
    )
    JobFactory(recruiter=recruiter, title="Legal Secretary", publish=False)
    job_1 = JobFactory(recruiter=recruiter, title="Legal Secretary - Wills")
    job_1.publish_job(job_1.recruiter.contact.user)
    job_2 = JobFactory(recruiter=recruiter, title="Legal Secretary - PI")
    job_2.publish_job(job_2.recruiter.contact.user)
    index = SearchIndex(JobIndex())
    index.drop_create()
    assert 2 == index.update()
    url = url_with_querystring(
        reverse("test.job.elastic.search"), job_role="legal secretary"
    )
    response = client.get(url)
    assert 200 == response.status_code
    assert "job_list" in response.context
    job_list = response.context["job_list"]
    assert 2 == len(job_list)
    assert ["Legal Secretary - PI", "Legal Secretary - Wills"] == [
        x.data.title for x in job_list
    ]
    assert [job_2.pk, job_1.pk] == [x.pk for x in job_list]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_elastic_search_latest_jobs(client):
    """If the query parameters are empty, we get the latest jobs.

    Test data copied from ``test_latest`` in ``job/tests/test_view.py``.

    .. note:: This test uses the ``JobElasticSearchMixin`` with a
              ``paginate_by`` value of 3.

    """
    ilspa = RecruiterFactory(contact=ContactFactory())
    indeed = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=indeed)
    today = timezone.now()
    yesterday = timezone.now() - relativedelta(days=1)
    JobFactory(
        recruiter=indeed,
        title="Indeed1",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=today,
    )
    JobFactory(
        recruiter=indeed,
        title="Indeed2",
        order_key=FEED_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa,
        title="ILSPA2",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=yesterday,
    )
    JobFactory(
        recruiter=ilspa,
        title="ILSPA1",
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=today,
    )
    response = client.get(reverse("test.job.elastic.search"))
    assert 200 == response.status_code
    assert "job_list" in response.context
    job_list = response.context["job_list"]
    assert 3 == len(job_list)
    assert ["ILSPA1", "ILSPA2", "Indeed1"] == [j.data.title for j in job_list]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_elastic_search_latest_jobs_pagination_page_1(client):
    """If the query parameters are empty, we get the latest jobs.

    .. note:: This test uses the ``JobElasticSearchMixin`` with a
              ``paginate_by`` value of 3.

    .. note:: We decrease the ``publish_date`` by an hour for each job so
              the order is predictable.

    """
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=recruiter)
    for counter in range(1, 10):
        JobFactory(
            recruiter=recruiter,
            title="title{}".format(counter),
            order_key=FEED_RECRUITER,
            publish_date=(timezone.now() + relativedelta(hours=-counter)),
        )
    response = client.get(reverse("test.job.elastic.search"))
    assert 200 == response.status_code
    assert "job_list" in response.context
    job_list = response.context["job_list"]
    assert 3 == len(job_list), [j.data.title for j in job_list]
    assert ["title1", "title2", "title3"] == [j.data.title for j in job_list]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_elastic_search_latest_jobs_pagination_page_2(client):
    """If the query parameters are empty, we get the latest jobs.

    .. note:: This test uses the ``JobElasticSearchMixin`` with a
              ``paginate_by`` value of 3.

    .. note:: We decrease the ``publish_date`` by an hour for each job so
              the order is predictable.

    """
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=recruiter)
    for counter in range(1, 10):
        JobFactory(
            recruiter=recruiter,
            title="title{}".format(counter),
            order_key=FEED_RECRUITER,
            publish_date=(timezone.now() + relativedelta(hours=-counter)),
        )
    url = url_with_querystring(reverse("test.job.elastic.search"), page_no=2)
    response = client.get(url)
    assert 200 == response.status_code
    assert "job_list" in response.context
    job_list = response.context["job_list"]
    assert 3 == len(job_list)
    assert ["title4", "title5", "title6"] == [j.data.title for j in job_list]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_elastic_search_latest_jobs_pagination_page_invalid(client):
    """If the query parameters are empty, we get the latest jobs."""
    url = url_with_querystring(reverse("test.job.elastic.search"), page_no="XY")
    response = client.get(url)
    assert 200 == response.status_code
    assert "page_obj" in response.context
    page_obj = response.context["page_obj"]
    assert 1 == page_obj.number
