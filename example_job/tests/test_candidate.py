# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from freezegun import freeze_time

from example_job.tests.factories import ContactFactory
from gdpr.models import UserConsent, UserConsentUnsubscribe
from gdpr.tests.factories import ConsentFactory
from job.models import Candidate, ILSPA_RECRUITER, Job, Notify
from job.tests.factories import (
    CandidateFactory,
    LocationFactory,
    JobApplicationFactory,
    JobFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory
from mail.models import Message, RejectedEmail
from mail.tests.factories import (
    MailFactory,
    MailTemplateFactory,
    MessageFactory,
    NotifyFactory,
)


@pytest.mark.django_db
def test_email_latest_jobs():
    # consent
    consent = ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # candidate 1
    contact_1 = ContactFactory(user=UserFactory(email="a@test.com"))
    candidate_1 = CandidateFactory(contact=contact_1)
    UserConsent.objects.set_consent(consent, True, candidate_1.contact.user)
    # candidate 2
    contact_2 = ContactFactory(user=UserFactory(email="b@test.com"))
    candidate_2 = CandidateFactory(contact=contact_2)
    UserConsent.objects.set_consent(consent, False, candidate_2.contact.user)
    # candidate 3
    contact_3 = ContactFactory(user=UserFactory(email="c@test.com"))
    candidate_3 = CandidateFactory(contact=contact_3)
    UserConsent.objects.set_consent(consent, True, candidate_3.contact.user)
    assert {
        "a@test.com": contact_1.pk,
        "c@test.com": contact_3.pk,
    } == Candidate.objects.email_latest_jobs()


@pytest.mark.django_db
def test_latest_jobs_as_html():
    with freeze_time(date(2019, 3, 20)):
        job_1 = JobFactory(
            location=LocationFactory(postcode="EX20", town="Okehampton"),
            order_key=ILSPA_RECRUITER,
            publish=True,
            publish_date=timezone.now() + relativedelta(days=-1),
            close_date=timezone.now() + relativedelta(days=21),
            recruiter=RecruiterFactory(contact=ContactFactory()),
            salary="£20,000-£24,000",
            title="ILSPA",
        )
        job_2 = JobFactory(
            location=LocationFactory(postcode="EX17", town="Crediton"),
            order_key=ILSPA_RECRUITER,
            publish=True,
            publish_date=timezone.now() + relativedelta(days=-1),
            close_date=timezone.now() + relativedelta(days=21),
            recruiter=RecruiterFactory(contact=ContactFactory()),
            salary="£10,000-£11,000",
            title="KB",
        )
        expect = (
            "<strong>ILSPA</strong><br />"
            "Salary: £20,000-£24,000<br />"
            "Location: Okehampton (EX20)<br />"
            "Closing Date: Wednesday 10 April 2019<br />"
            "<a href="
            '"http://localhost:8000/recruitment/candidate/job/{}/"'
            ">Click here to view and apply...</a><br />"
            "<br />"
            "<strong>KB</strong><br />"
            "Salary: £10,000-£11,000<br />"
            "Location: Crediton (EX17)<br />"
            "Closing Date: Wednesday 10 April 2019<br />"
            "<a href="
            '"http://localhost:8000/recruitment/candidate/job/{}/"'
            ">Click here to view and apply...</a><br />"
        ).format(job_1.pk, job_2.pk)
        assert expect == Candidate.objects.jobs_as_html(Job.objects.latest())


@pytest.mark.django_db
def test_job_applications():
    """Check the 'job_applications' are in most recent first."""
    candidate = CandidateFactory(contact=ContactFactory())
    for title in ("j1", "j2", "j3"):
        job = JobFactory(
            title=title, recruiter=RecruiterFactory(contact=ContactFactory())
        )
        JobApplicationFactory(candidate=candidate, job=job)
    assert ["j3", "j2", "j1"] == [
        a.job.title for a in candidate.job_applications()
    ]


@pytest.mark.django_db
def test_send_email_latest_jobs():
    NotifyFactory(email="patrick@kbsoftware.co.uk")
    # consent
    consent = ConsentFactory(slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG)
    # candidate a
    contact_a = ContactFactory(
        user=UserFactory(email="a@test.com", first_name="Charlie")
    )
    candidate_a = CandidateFactory(contact=contact_a)
    UserConsent.objects.set_consent(consent, True, candidate_a.contact.user)
    # candidate b - rejected email
    contact_b = ContactFactory(
        user=UserFactory(email="b@test.com", first_name="Berties")
    )
    candidate_b = CandidateFactory(contact=contact_b)
    UserConsent.objects.set_consent(consent, True, candidate_b.contact.user)
    RejectedEmail.objects.init_rejected_mailing(
        "B@test.com",
        timezone.now(),
        Candidate.MAIL_TEMPLATE_NOTIFY_JOBS,
        "spam",
    )
    # candidate c - no consent
    contact_c = ContactFactory(user=UserFactory(email="c@test.com"))
    candidate_c = CandidateFactory(contact=contact_c)
    UserConsent.objects.set_consent(consent, False, candidate_c.contact.user)
    #
    template = MailTemplateFactory(slug=Candidate.MAIL_TEMPLATE_NOTIFY_JOBS)
    with freeze_time(date(2019, 6, 20)):
        message = MessageFactory(content_object=contact_c, template=template)
        MailFactory(email="c@pkimber.net", message=message)
    job = JobFactory(
        location=LocationFactory(postcode="EX20", town="Okehampton"),
        order_key=ILSPA_RECRUITER,
        publish=True,
        publish_date=datetime(2019, 6, 17, 6, 0, 0, tzinfo=pytz.utc),
        close_date=date(2019, 8, 20),
        recruiter=RecruiterFactory(contact=ContactFactory()),
        salary="£20,000-£24,000",
        title="ILSPA",
    )
    with freeze_time("2019-06-25"):
        assert 1 == Candidate.objects.send_email_latest_jobs()
    # consent
    user_consent = UserConsent.objects.get(
        user=contact_a.user, consent__slug=Candidate.GDPR_JOBS_NOTIFICATION_SLUG
    )
    unsubscribe = UserConsentUnsubscribe.objects.get(user_consent=user_consent)
    # contact_a
    qs = Message.objects.for_content_object(contact_a)
    assert 1 == qs.count()
    message = qs.first()
    assert Candidate.MAIL_TEMPLATE_NOTIFY_JOBS == message.template.slug
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert {
        "name": "Charlie",
        "jobs": (
            "<strong>ILSPA</strong><br />"
            "Salary: £20,000-£24,000<br />"
            "Location: Okehampton (EX20)<br />"
            "Closing Date: Tuesday 20 August 2019<br />"
            "<a href="
            '"http://localhost:8000/recruitment/candidate/job/{}/"'
            ">Click here to view and apply...</a><br />".format(job.pk)
        ),
        "unsub": "http://localhost:8000/unsubscribe/{}/".format(
            unsubscribe.token
        ),
    } == {x.key: x.value for x in mail.mailfield_set.all()}
    # contact_b
    qs = Message.objects.for_content_object(contact_c)
    assert 1 == qs.count()
    message = qs.first()
    # notify
    qs = Message.objects.for_content_object(Notify.objects.first())
    assert 1 == qs.count()
    message = qs.first()
    assert "Jobs Board - Notify Candidates" == message.subject
    assert "Sent 1 reminder emails" == message.description
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert "patrick@kbsoftware.co.uk" == mail.email


@pytest.mark.django_db
def test_candidate_delete():
    user = UserFactory()
    contact = ContactFactory(user=user)
    candidate = CandidateFactory(contact=contact)
    assert contact.is_candidate
    candidate.set_deleted(user)
    assert not contact.is_candidate
