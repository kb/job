# -*- encoding: utf-8 -*-
import pytest
import xmltodict

from django.utils import timezone
from dateutil.relativedelta import relativedelta
from unittest import mock

from job.models import FEED_RECRUITER
from job.search import JobIndex
from job.tasks import (
    process_feeds,
    rebuild_job_index,
    refresh_job_index,
    send_email_latest_jobs,
    update_job_index,
)
from job.tests.factories import (
    JobFactory,
    JobFeedFactory,
    JobSettingsFactory,
    LocationFactory,
    RecruiterFactory,
)
from search.search import SearchIndex
from .factories import ContactFactory


@mock.patch("urllib.request.urlopen")
@pytest.mark.django_db
def test_process_feeds(mock_urlopen):
    """

    TODO PJK Create a proper test for 'process_feeds'.
    Need to mock feeds so we can test the result

    """
    cm = mock.MagicMock()
    cm.getcode.return_value = 200
    cm.read.return_value = xmltodict.unparse(
        {"source": {"test-xml-for-totally-and-secs": 200}}
    )
    cm.__enter__.return_value = cm
    mock_urlopen.return_value = cm

    totally_legal = RecruiterFactory(contact=ContactFactory())
    secsinthecity = RecruiterFactory(contact=ContactFactory())
    JobSettingsFactory(default_latitude=52.47872, default_longitude=-1.90723)
    LocationFactory(latitude=52.47872, longitude=-1.90723)
    JobFeedFactory(
        slug="totally-legal",
        recruiter=totally_legal,
        url=(
            "https://www.totallylegal.com/StandardFeeds/"
            "c9e8d253-0951-40b7-a29b-7b27c2352024.xml"
        ),
    )
    JobFeedFactory(
        slug="secsinthecity",
        recruiter=secsinthecity,
        url=(
            "https://www.secsinthecity.co.uk/StandardFeeds/"
            "03a7c19f-45a7-4db3-96c6-b40c3b180eee.xml"
        ),
    )
    assert process_feeds() is None


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_rebuild_job_index():
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=recruiter)
    for counter in range(1, 5):
        JobFactory(
            recruiter=recruiter,
            title="title{}".format(counter),
            order_key=FEED_RECRUITER,
            publish_date=(timezone.now() + relativedelta(hours=-counter)),
        )
    count, index_count = rebuild_job_index()
    assert 4 == count
    assert 4 == index_count


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_refresh_job_index():
    index = SearchIndex(JobIndex())
    index.drop_create()
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=recruiter)
    for counter in range(1, 5):
        JobFactory(
            recruiter=recruiter,
            title="title{}".format(counter),
            order_key=FEED_RECRUITER,
            publish_date=(timezone.now() + relativedelta(hours=-counter)),
        )
    count, index_count = refresh_job_index()
    assert 4 == count
    assert 4 == index_count


@pytest.mark.django_db
def test_send_email_latest_jobs():
    assert 0 == send_email_latest_jobs()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_update_job_index():
    index = SearchIndex(JobIndex())
    index.drop_create()
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFeedFactory(recruiter=recruiter)
    job = JobFactory(
        recruiter=recruiter,
        title="My Job Title",
        order_key=FEED_RECRUITER,
        publish_date=timezone.now(),
    )
    count, index_count = update_job_index(job.pk)
    assert 1 == count
    assert 1 == index_count
