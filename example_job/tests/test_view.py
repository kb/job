# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from base.tests.model_maker import clean_and_save
from gdpr.tests.factories import ConsentFactory, ConsentFormSettingsFactory
from job.management.commands import init_app_job
from job.models import Job, Recruiter
from job.tests.factories import (
    CandidateFactory,
    CVFactory,
    JobApplication,
    JobFactory,
    RecruiterFactory,
    JobRoleFactory,
    PracticeAreaFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.models import Message, MailTemplate
from mail.tests.factories import MailTemplateFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_candidate_application(client):
    """Candidate apply for a job.

    Long, complicated test code... but it does the job I suppose!

    """
    r = RecruiterFactory(
        contact=ContactFactory(
            user=UserFactory(
                username="r",
                first_name="Rec",
                last_name="Wreck",
                email="rec@wreck.com",
            )
        )
    )
    j = JobFactory(recruiter=r, title="Django Programmer")
    # candidate
    user = UserFactory(username="c", first_name="Can", last_name="Core")
    contact = ContactFactory(user=user)
    CVFactory(contact=contact)
    candidate = CandidateFactory(contact=contact)
    # run the management command to create the mail template
    command = init_app_job.Command()
    command.handle()
    # candidate login
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.job.application", args=[j.pk])
    # action
    response = client.post(url, {"cover": "I apply!"})
    # assert
    assert HTTPStatus.FOUND == response.status_code
    # one message
    messages = Message.objects.all()
    assert 1 == messages.count()
    message = messages.first()
    # containing one 'mail' (the message for one person)
    mails = message.mail_set.all()
    assert 1 == mails.count()
    mail = mails.first()
    assert mail.email == "rec@wreck.com"
    # containing several bits of data
    fields = mail.mailfield_set.all()
    result = {f.key: f.value for f in fields}
    # check the result contains a date field
    result.pop("date")
    # find the job application
    a = JobApplication.objects.get(candidate=candidate)
    assert {
        "name": "Rec",
        "candidate": "Can Core",
        "job": "Django Programmer",
        "url": f"http://testserver/recruitment/application/{a.pk}/",
    } == result


@pytest.mark.django_db
def test_candidate_application_alt_email(client):
    """Candidate apply for a job.

    Long, complicated test code... but it does the job I suppose!

    """
    r = RecruiterFactory(
        contact=ContactFactory(
            user=UserFactory(username="r", first_name="Rec", last_name="Wreck")
        )
    )
    j = JobFactory(
        recruiter=r,
        title="Django Programmer",
        applications_email="other@wreck.com",
    )
    # candidate
    user = UserFactory(username="c", first_name="Can", last_name="Core")
    contact = ContactFactory(user=user)
    CVFactory(contact=contact)
    candidate = CandidateFactory(contact=contact)
    # run the management command to create the mail template
    command = init_app_job.Command()
    command.handle()
    # candidate login
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.job.application", args=[j.pk])
    # action
    response = client.post(url, {"cover": "I apply!"})
    # assert
    assert HTTPStatus.FOUND == response.status_code
    # one message
    messages = Message.objects.all()
    assert 1 == messages.count()
    message = messages.first()
    # containing one 'mail' (the message for one person)
    mails = message.mail_set.all()
    assert 1 == mails.count()
    mail = mails.first()
    assert mail.email == "other@wreck.com"
    # containing several bits of data
    fields = mail.mailfield_set.all()
    result = {f.key: f.value for f in fields}
    # check the result contains a date field
    result.pop("date")
    # find the job application
    a = JobApplication.objects.get(candidate=candidate)
    assert {
        "name": "Rec",
        "candidate": "Can Core",
        "job": "Django Programmer",
        "url": f"http://testserver/recruitment/application/{a.pk}/",
    } == result


# @pytest.mark.django_db
# def test_job_checkout_invoice(client, mocker):
#     mocker.patch('stripe.Charge.create')
#     mocker.patch('stripe.Customer.create')
#     MailTemplateFactory(slug=JobBasket.MAIL_TEMPLATE_JOB_PAYMENT)
#     VatSettingsFactory()
#     user = UserFactory()
#     contact = ContactFactory(user=user)
#     recruiter = RecruiterFactory(contact=contact)
#     j1 = JobFactory(publish=False, title='j1', recruiter=recruiter)
#     basket = JobBasketFactory(recruiter=recruiter)
#     JobBasketItemFactory(job=j1, basket=basket)
#     # session
#     session = client.session
#     session[CONTENT_OBJECT_PK] = basket.pk
#     session.save()
#     # candidate login
#     assert client.login(username=user.username, password=TEST_PASSWORD)
#     url = reverse('recruitment.job.checkout', args=[basket.pk])
#     # action
#     data = {
#         'action': CheckoutAction.INVOICE,
#         'token': 'abc',
#         'company_name': 'KB',
#         'address_1': 'My Address',
#         'town': 'Hatherleigh',
#         'county': 'Devon',
#         'postcode': 'EX20',
#         'country': 'UK',
#         'contact_name': 'Patrick',
#         'email': 'test@test.com',
#     }
#     response = client.post(url, data=data)
#     # assert
#     assert HTTPStatus.FOUND == response.status_code, response.context['form'].errors
#     assert '/recruitment/checkout/' in response['Location']
#     assert '/success/' in response['Location']
#     j1.refresh_from_db()
#     # pay by invoice does not publish the job
#     assert j1.publish is False
#     assert j1.publish_date is None


# @pytest.mark.django_db
# def test_job_checkout_payment(client):
#     with mock.patch(
#                 'stripe.Charge.create'
#             ), mock.patch(
#                 'stripe.Customer.create'
#             ) as mock_customer_create:
#         # mock return
#         data = {'id': 'xyz'}
#         return_value = namedtuple('ReturnValue', data.keys())(**data)
#         mock_customer_create.return_value = return_value
#         # factories
#         MailTemplateFactory(slug=JobBasket.MAIL_TEMPLATE_JOB_PAYMENT)
#         VatSettingsFactory()
#         user = UserFactory()
#         contact = ContactFactory(user=user)
#         recruiter = RecruiterFactory(contact=contact)
#         j1 = JobFactory(publish=False, title='j1', recruiter=recruiter)
#         j2 = JobFactory(publish=False, title='j2', recruiter=recruiter)
#         basket = JobBasketFactory(recruiter=recruiter)
#         JobBasketItemFactory(job=j1, basket=basket)
#         JobBasketItemFactory(job=j2, basket=basket)
#         # session
#         session = client.session
#         session[CONTENT_OBJECT_PK] = basket.pk
#         session.save()
#         # candidate login
#         assert client.login(username=user.username, password=TEST_PASSWORD)
#         url = reverse('recruitment.job.checkout', args=[basket.pk])
#         # action
#         data = {
#             'action': CheckoutAction.PAYMENT,
#             'token': 'abc',
#         }
#         response = client.post(url, data=data)
#         # assert
#         assert HTTPStatus.FOUND == response.status_code
#         assert '/recruitment/checkout/' in response['Location']
#         assert '/success/' in response['Location']
#         j1.refresh_from_db()
#         assert j1.publish is True
#         assert j1.publish_date.date() == date.today()


@pytest.mark.django_db
def test_recruiter_create(client):
    MailTemplateFactory(
        slug=Recruiter.RECRUITER_WELCOME,
        title="Recruiter Welcome",
        template_type=MailTemplate.MANDRILL,
    )
    ConsentFactory(slug=Recruiter.GDPR_NEWSLETTER_SLUG)
    ConsentFactory(slug=Recruiter.GDPR_DATA_SLUG, show_checkbox=False)
    ConsentFormSettingsFactory(slug="generic-form")
    url = reverse("recruitment.recruiter.create")
    data = {
        "company_name": "Rouge Recruit",
        "title": "Ms",
        "first_name": "R",
        "last_name": "Rouge",
        "email": "test@test.com",
        "address_1": "High Street",
        "town": "Crediton",
        "county": "Devon",
        "country": "UK",
        "postcode": "EX17 123",
        "username": "rouge",
        "password1": "pass",
        "password2": "pass",
        "phone": "123",
        "mobile": "456",
        "captcha": "123",
        "g-recaptcha-response": "PASSED",
    }
    response = client.post(url, data)
    # assert
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    recruiter = Recruiter.objects.get(contact__user__username="rouge")
    assert "123" == recruiter.contact.phone
    assert "456" == recruiter.contact.mobile
    # check the welcome email
    messages = Message.objects.all()
    assert 1 == messages.count()
    message = messages.first()
    # containing one 'mail' (the message for one person)
    mails = message.mail_set.all()
    assert 1 == mails.count()
    mail = mails.first()
    assert mail.email == "test@test.com"
    # containing several bits of data
    fields = mail.mailfield_set.all()
    result = {f.key: f.value for f in fields}
    # check the result contains a date field
    assert result.pop("register_date") is not None
    assert {"name": "R", "username": "rouge"} == result


@pytest.mark.django_db
def test_update(client):
    user = UserFactory(username="staff", is_staff=True, email="aa@bb.com")
    job = clean_and_save(
        JobFactory(
            title="Cashier",
            recruiter=RecruiterFactory(contact=ContactFactory()),
            location_text="London",
        )
    )
    JobRoleFactory(title="role1")
    JobRoleFactory(title="role3")
    job_role = JobRoleFactory(title="role3")
    PracticeAreaFactory(title="area1")
    PracticeAreaFactory(title="area2")
    PracticeAreaFactory(title="area3")
    practice_area = PracticeAreaFactory(title="area4")

    form_data = dict(
        area=job.location.form_description,
        code=job.location.postcode,
        location_text="Nottingham",
        close_date=job.close_date,
        description="Updated job Description",
        recruiter=job.recruiter.pk,
        salary=job.salary,
        title="Legal Secretary",
        job_role=job_role.pk,
        practice_area=practice_area.pk,
        applications_email="aa@bb.com",
    )
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.post(
        reverse("job.update", kwargs=dict(pk=job.pk)), form_data
    )
    assert HTTPStatus.FOUND == response.status_code
    Job.objects.get(title="Legal Secretary")
