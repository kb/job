# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta

from job.search import JobIndex
from job.tests.factories import (
    JobFactory,
    PracticeAreaFactory,
    RecruiterFactory,
)
from search.search import SearchIndex
from .factories import ContactFactory


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_job_title():
    """

    Test data copied from:
    ``example_job/tests/test_job.py``, ``test_index_job``

    """
    exp = date.today() + relativedelta(years=1)
    recruiter = RecruiterFactory(
        contact=ContactFactory(), subscription_expires=exp
    )
    job = JobFactory(
        recruiter=recruiter,
        title="Legal Secretary - Personal Injury Department",
    )
    job.publish_job(job.recruiter.contact.user)
    index = SearchIndex(JobIndex())
    index.drop_create()
    assert 1 == index.update()
    criteria = {
        "location": "Hatherleigh",
        "job_role": "Secretary",
        "practice_area": "Farming",
    }
    result, total = index.search(criteria)
    assert 1 == total
    assert [job.pk] == [x.pk for x in result]
    assert ["job"] == [x.document_type for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_job_location():
    exp = date.today() + relativedelta(years=1)
    recruiter = RecruiterFactory(
        contact=ContactFactory(), subscription_expires=exp
    )
    job = JobFactory(
        recruiter=recruiter,
        title="Legal Secretary - Personal Injury Department",
        location_text="Hatherleigh",
    )
    job.publish_job(job.recruiter.contact.user)
    index = SearchIndex(JobIndex())
    index.drop_create()
    assert 1 == index.update()
    criteria = {
        "location": "Hatherleigh",
        "job_role": "Customer Service",
        "practice_area": "Farming",
    }
    result, total = index.search(criteria)
    assert 1 == total
    assert [job.pk] == [x.pk for x in result]
    assert ["job"] == [x.document_type for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_job_practice_area():
    exp = date.today() + relativedelta(years=1)
    recruiter = RecruiterFactory(
        contact=ContactFactory(), subscription_expires=exp
    )
    job = JobFactory(
        recruiter=recruiter,
        title="Legal Secretary - Personal Injury Department",
        location_text="Exeter",
        practice_area=PracticeAreaFactory(title="Conveyancing"),
    )
    job.publish_job(job.recruiter.contact.user)
    index = SearchIndex(JobIndex())
    index.drop_create()
    assert 1 == index.update()
    criteria = {
        "location": "Hatherleigh",
        "job_role": "Customer Service",
        "practice_area": "Conveyancing",
    }
    result, total = index.search(criteria)
    assert 1 == total
    assert [job.pk] == [x.pk for x in result]
    assert ["job"] == [x.document_type for x in result]
