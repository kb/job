# -*- encoding: utf-8 -*-
import pytest

from job.models import JobCredit
from job.tests.factories import JobCreditFactory, RecruiterFactory
from login.tests.factories import UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_pks_with_email():
    c1 = JobCreditFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(user=UserFactory(email="code@pkimber.net"))
        )
    )
    c2 = JobCreditFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(
                user=UserFactory(email="patrick@pkimber.net")
            )
        )
    )
    # deleted
    c3 = JobCreditFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(
                user=UserFactory(email="code@pkimber.net"), deleted=True
            ),
        )
    )
    # mixed case email
    c4 = JobCreditFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(user=UserFactory(email="code@PKimber.net"))
        )
    )
    result = JobCredit.objects.pks_with_email(
        "code@pkimber.net", [c1.pk, c2.pk, c3.pk, c4.pk]
    )
    assert 2 == len(result)
    assert set([c1.pk, c4.pk]) == set([x for x in result])
