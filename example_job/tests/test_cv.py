# -*- encoding: utf-8 -*-
import pytest

from job.models import CV, CVType
from job.tests.factories import CVFactory, CVTypeFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_str():
    contact = ContactFactory()
    cv_type = CVTypeFactory(
        slug=CVType.MEMBERSHIP_APPLICATION, title="Membership Application"
    )
    obj = CVFactory(contact=contact, cv_type=cv_type)
    assert "Membership Application" in str(obj)
    assert "job/candidate/cv/" in str(obj)


@pytest.mark.django_db
def test_manager_contact():
    contact = ContactFactory()
    cv_type = CVTypeFactory()
    for title in ["a", "b", "c"]:
        CVFactory(contact=contact, cv_type=cv_type, title=title)
    result = [obj.title for obj in CV.objects.contact(contact)]
    assert ["c", "b", "a"] == result


@pytest.mark.django_db
def test_manager_contact_deleted():
    contact = ContactFactory()
    cv_type = CVTypeFactory()
    for title in ["a", "b", "c"]:
        CVFactory(contact=contact, cv_type=cv_type, title=title)
    CVFactory(contact=contact, cv_type=cv_type, title="d", deleted=True)
    result = [obj.title for obj in CV.objects.contact(contact)]
    assert ["c", "b", "a"] == result


@pytest.mark.django_db
def test_manager_latest():
    contact = ContactFactory()
    CVFactory(contact=contact, title="a")
    latest = CV.objects.latest(contact)
    assert "a" == latest.title


@pytest.mark.django_db
def test_manager_latest_deleted():
    contact = ContactFactory()
    cv = CVFactory(contact=contact, title="a")
    cv.set_deleted(contact.user)
    assert CV.objects.latest(contact) is None


@pytest.mark.django_db
def test_manager_latest_none():
    contact = ContactFactory()
    assert CV.objects.latest(contact) is None
