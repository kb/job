# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal
from django.urls import reverse

from finance.tests.factories import VatSettingsFactory
from job.models import JobBasket
from job.service import PRODUCT_JOB_POST_DISCOUNT
from job.tests.factories import JobFactory, RecruiterFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from stock.tests.factories import ProductFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_pay_for_two_jobs_discount(client):
    VatSettingsFactory()
    # create a recruiter and an un-published job
    user = UserFactory()
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    JobFactory(publish=False, recruiter=recruiter)
    JobFactory(publish=False, recruiter=recruiter)
    # create the product
    ProductFactory(slug=PRODUCT_JOB_POST_DISCOUNT, price=Decimal("100"))
    # action
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.recruiter.dashboard")
    response = client.post(url)
    assert 302 == response.status_code
    # check we created a single basket
    assert 1 == JobBasket.objects.count()
    basket = JobBasket.objects.first()
    assert 1 == len(basket.checkout_description)
    # check we included the VAT
    assert Decimal("240") == basket.checkout_total
