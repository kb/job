# -*- encoding: utf-8 -*-
import factory

from login.tests.factories import UserFactory

from example_job.models import Contact


class ContactFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Contact

    @factory.sequence
    def company_name(n):
        return "company_name_{:02d}".format(n)
