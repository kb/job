# -*- encoding: utf-8 -*-
import pytest

from datetime import date

from job.tests.factories import (
    CandidateFactory,
    RecruiterFactory,
    JobApplicationFactory,
    JobFactory,
)
from login.tests.factories import UserFactory

from .factories import ContactFactory


@pytest.mark.django_db
def test_applications_candidate(client):
    """The list of applications a candidate can see for a job.
    The candidate can only see their application (if they have applied)!
    """
    user = UserFactory()
    c1 = CandidateFactory(contact=ContactFactory(user=user))
    c2 = CandidateFactory(contact=ContactFactory())
    r1 = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=r1)
    JobApplicationFactory(candidate=c1, job=job, cover="a1")
    JobApplicationFactory(candidate=c2, job=job, cover="a2")
    assert ["a1"] == [a.cover for a in job.applications(user)]


@pytest.mark.django_db
def test_applications_no_data():
    """There are no applications for this job."""
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert [] == [a.cover for a in job.applications(user)]


@pytest.mark.django_db
def test_applications_recruiter():
    """The list of applications a recruiter can see for a job.
    A recruiter can see all the applications for a posted job.
    """
    c1 = CandidateFactory(contact=ContactFactory())
    c2 = CandidateFactory(contact=ContactFactory())
    user = UserFactory()
    r1 = RecruiterFactory(contact=ContactFactory(user=user))
    job = JobFactory(recruiter=r1)
    JobApplicationFactory(
        candidate=c1, job=job, apply_date=date(2015, 1, 1), cover="a1"
    )
    JobApplicationFactory(
        candidate=c2, job=job, apply_date=date(2015, 1, 2), cover="a2"
    )
    assert ["a2", "a1"] == [a.cover for a in job.applications(user)]


@pytest.mark.django_db
def test_applications_recruiter_ignore_pending():
    """The list of applications a recruiter can see for a job.

    A recruiter can see all the applications for a posted job.

    """
    c1 = CandidateFactory(contact=ContactFactory())
    c2 = CandidateFactory(contact=ContactFactory())
    c3 = CandidateFactory(contact=ContactFactory())
    user = UserFactory()
    r1 = RecruiterFactory(contact=ContactFactory(user=user))
    job = JobFactory(recruiter=r1)
    JobApplicationFactory(
        candidate=c1, job=job, apply_date=date(2015, 1, 1), cover="a1"
    )
    JobApplicationFactory(candidate=c2, job=job, cover="a2")
    JobApplicationFactory(
        candidate=c3, job=job, apply_date=date(2015, 2, 1), cover="a3"
    )
    assert ["a3", "a1"] == [a.cover for a in job.applications(user)]


@pytest.mark.django_db
def test_applications_staff():
    """The list of applications a member of staff can see for a job."""
    c1 = CandidateFactory(contact=ContactFactory())
    c2 = CandidateFactory(contact=ContactFactory())
    c3 = CandidateFactory(contact=ContactFactory())
    r1 = RecruiterFactory(contact=ContactFactory())
    RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=r1)
    JobApplicationFactory(
        candidate=c1, job=job, apply_date=date(2015, 1, 1), cover="a1"
    )
    JobApplicationFactory(candidate=c2, job=job, cover="a2")
    JobApplicationFactory(
        candidate=c3, job=job, apply_date=date(2015, 2, 1), cover="a3"
    )
    user = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=user)
    qs = job.applications(user).order_by("cover")
    assert ["a1", "a2", "a3"] == [a.cover for a in qs]
