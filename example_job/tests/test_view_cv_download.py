# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from job.models import JobError
from job.tests.factories import CVFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_anonymous(client):
    """Anonymous user, so response should be 403 (permission denied)."""
    url = reverse("job.cv.download", args=[99])
    response = client.get(url)
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_download_cv_web_user(client):
    """Standard download."""
    user = UserFactory()
    contact = ContactFactory(user=user)
    cv = CVFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("job.cv.download", args=[cv.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_download_cv_web_user_2(client):
    """The CV record has no file attached.

    This should probably not be possible, but Opbeat sent the error."

    """
    user = UserFactory()
    contact = ContactFactory(user=user)
    cv = CVFactory(contact=contact, file_name=None)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("job.cv.download", args=[cv.pk])
    with pytest.raises(JobError) as e:
        client.get(url)
    assert "No file for CV" in str(e.value)
