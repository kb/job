# -*- encoding: utf-8 -*-
import pytest

from job.tests.factories import (
    JobFactory,
    JobFeedFactory,
    JobFeedItemFactory,
    RecruiterFactory,
)
from job.models import JobFeedItem
from .factories import ContactFactory


@pytest.mark.django_db
def test_is_new_job_key():
    feed = JobFeedFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    assert JobFeedItem.objects.is_new_job_key(feed, "a456") is True


@pytest.mark.django_db
def test_is_new_job_key_exists():
    feed = JobFeedFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    JobFeedItemFactory(feed=feed, jobkey="a123")
    assert JobFeedItem.objects.is_new_job_key(feed, "a123") is False


@pytest.mark.django_db
def test_str():
    recruiter = RecruiterFactory(contact=ContactFactory(company_name="Indeed"))
    feed = JobFeedFactory(recruiter=recruiter)
    job = JobFactory(title="Farmer", recruiter=recruiter)
    feed_item = JobFeedItemFactory(feed=feed, job=job, jobkey="a123")
    assert "Indeed: a123 (Farmer)" == str(feed_item)


@pytest.mark.django_db
def test_str_no_job():
    recruiter = RecruiterFactory(contact=ContactFactory(company_name="Indeed"))
    feed = JobFeedFactory(recruiter=recruiter)
    feed_item = JobFeedItemFactory(feed=feed, jobkey="a123")
    assert "Indeed: a123" == str(feed_item)


@pytest.mark.django_db
def test_str_no_job_or_recruiter():
    feed = JobFeedFactory(recruiter=None)
    feed_item = JobFeedItemFactory(feed=feed, jobkey="a123")
    assert "a123" == str(feed_item)
