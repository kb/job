# -*- encoding: utf-8 -*-
import pytest

from job.models import JobBasket
from job.tests.factories import JobBasketFactory, RecruiterFactory
from login.tests.factories import UserFactory
from .factories import ContactFactory

# def _create_job_basket():
#     """Create a job basket for testing purposes."""
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     j1 = JobFactory(publish=False, title="j1", recruiter=recruiter)
#     j2 = JobFactory(publish=False, title="j2", recruiter=recruiter)
#     basket = JobBasketFactory(recruiter=recruiter)
#     JobBasketItemFactory(job=j1, basket=basket)
#     JobBasketItemFactory(job=j2, basket=basket)
#     return basket
#
#
# @pytest.mark.django_db
# def test_job_basket_factory():
#     """Test the job basket factory."""
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     basket = JobBasketFactory(recruiter=recruiter)
#     # test the '__str__' method
#     str(basket)
#
#
# @pytest.mark.django_db
# def test_job_basket_item_factory():
#     """Test the job basket item factory."""
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     job1 = JobFactory(recruiter=recruiter)
#     job2 = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
#     basket = JobBasketFactory(recruiter=recruiter)
#     JobBasketItemFactory(job=job1, basket=basket)
#     item = JobBasketItemFactory(job=job2, basket=basket)
#     assert 2 == basket.jobbasketitem_set.count()
#     # test the '__str__' method
#     str(item)
#
#
# @pytest.mark.django_db
# def test_payment_batch():
#     """Test the payment factory."""
#     CheckoutFactory(
#         action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
#         content_object=_create_job_basket(),
#     )
#
#
# @pytest.mark.django_db
# def test_payment_batch_duplicate_job_not_paid():
#     """Attempt to pay for a job which previously failed payment."""
#     MailTemplateFactory(slug=JobBasket.MAIL_TEMPLATE_JOB_PAYMENT)
#     VatSettingsFactory()
#     fail = CheckoutState.objects.get(slug=CheckoutState.FAIL)
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     j = JobFactory(publish=False, title="j1", recruiter=recruiter)
#     b1 = JobBasketFactory(recruiter=recruiter, checkout_state=fail)
#     JobBasketItemFactory(job=j, basket=b1)
#     b2 = JobBasketFactory(recruiter=recruiter)
#     JobBasketItemFactory(job=j, basket=b2)
#     check_checkout(b2)
#
#
# @pytest.mark.django_db
# def test_checkout_can_charge_paid():
#     """Attempt to pay for the job twice."""
#     paid = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     j = JobFactory(publish=False, title="j1", recruiter=recruiter)
#     b1 = JobBasketFactory(recruiter=recruiter, checkout_state=paid)
#     JobBasketItemFactory(job=j, basket=b1)
#     b2 = JobBasketFactory(recruiter=recruiter)
#     JobBasketItemFactory(job=j, basket=b2)
#     with pytest.raises(JobError) as excinfo:
#         check_checkout(b2)
#     assert "has already been paid" in str(excinfo.value)
#
#
# @pytest.mark.django_db
# def test_checkout_success():
#     MailTemplateFactory(slug=JobBasket.MAIL_TEMPLATE_JOB_PAYMENT)
#     product = ProductFactory(
#         slug=PRODUCT_JOB_POST, name="Post", price=Decimal("50")
#     )
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     job = JobFactory(publish=False, recruiter=recruiter)
#     basket = JobBasketFactory(recruiter=recruiter)
#     JobBasketItemFactory(job=job, basket=basket, product=product)
#     checkout = CheckoutFactory(
#         content_object=basket,
#         action=CheckoutAction.objects.payment,
#         description=", ".join(basket.checkout_description),
#         state=CheckoutState.objects.success,
#         total=Decimal("50"),
#     )
#     basket.checkout_success(checkout)
#     # check email template context
#     assert 1 == Message.objects.count()
#     message = Message.objects.first()
#     assert 1 == message.mail_set.count()
#     mail = message.mail_set.first()
#     assert 3 == mail.mailfield_set.count()
#     assert {
#         "description": "1 x Post @ 50.00 + VAT",
#         "name": recruiter.checkout_name,
#         "total": "£{:.2f}".format(Decimal("50")),
#     } == {f.key: f.value for f in mail.mailfield_set.all()}
#
#
# @pytest.mark.django_db
# def test_payment_batch_diff_recruiter():
#     """Creating a payment with jobs from multiple recruiters should fail."""
#     r1 = RecruiterFactory(contact=ContactFactory())
#     r2 = RecruiterFactory(contact=ContactFactory())
#     j1 = JobFactory(publish=False, title="j1", recruiter=r1)
#     j2 = JobFactory(publish=False, title="j2", recruiter=r2)
#     basket = JobBasketFactory(recruiter=r1)
#     JobBasketItemFactory(job=j1, basket=basket)
#     JobBasketItemFactory(job=j2, basket=basket)
#     with pytest.raises(JobError) as excinfo:
#         check_checkout(basket)
#     assert "jobs are linked to different" in str(excinfo.value)
#
#
# @pytest.mark.django_db
# def test_payment_batch_no_jobs():
#     """Attempting to create a payment with no jobs should fail."""
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     basket = JobBasketFactory(recruiter=recruiter)
#     with pytest.raises(JobError) as excinfo:
#         check_checkout(basket)
#     assert "No job postings in the basket" in str(excinfo.value)
#
#
# @pytest.mark.django_db
# def test_payment_batch_properties():
#     """Test the payment properties."""
#     MailTemplateFactory(slug=JobBasket.MAIL_TEMPLATE_JOB_PAYMENT)
#     VatSettingsFactory()
#     check_checkout(_create_job_basket())
#
#
# # @pytest.mark.django_db
# # def test_link_to_checkout():
# #     VatSettingsFactory()
# #     recruiter = RecruiterFactory(contact=ContactFactory())
# #     job1 = JobFactory(recruiter=recruiter)
# #     job2 = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
# #     basket = JobBasketFactory(recruiter=recruiter)
# #     check_checkout(basket)
#
#
# @pytest.mark.django_db
# def test_basket_prepare_payment():
#     """Test the payment calculation."""
#     VatSettingsFactory()
#     contact = ContactFactory(user=UserFactory(email="t@t.com"))
#     recruiter = RecruiterFactory(contact=contact)
#     basket = JobBasketFactory(recruiter=recruiter)
#     product = ProductFactory(name="Discount", price=Decimal("5"))
#     JobBasketItemFactory(
#         job=JobFactory(recruiter=recruiter), basket=basket, product=product
#     )
#     JobBasketItemFactory(
#         job=JobFactory(recruiter=recruiter), basket=basket, product=product
#     )
#     JobBasketItemFactory(
#         job=JobFactory(recruiter=recruiter),
#         basket=basket,
#         product=ProductFactory(name="Full", price=Decimal("20")),
#     )
#     # action
#     assert Decimal("36") == basket.checkout_total
#     assert [
#         "2 x Discount @ 5.00 + VAT",
#         "1 x Full @ 20.00 + VAT",
#     ] == basket.checkout_description


@pytest.mark.django_db
def test_pks_with_email():
    c1 = JobBasketFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(user=UserFactory(email="code@pkimber.net"))
        )
    )
    c2 = JobBasketFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(
                user=UserFactory(email="patrick@pkimber.net")
            )
        )
    )
    # deleted
    c3 = JobBasketFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(
                user=UserFactory(email="code@pkimber.net"), deleted=True
            ),
        )
    )
    # mixed case email
    c4 = JobBasketFactory(
        recruiter=RecruiterFactory(
            contact=ContactFactory(user=UserFactory(email="code@PKimber.net"))
        )
    )
    assert set([c1.pk, c4.pk]) == set(
        [
            x
            for x in JobBasket.objects.pks_with_email(
                "code@pkimber.net", [c1.pk, c2.pk, c3.pk, c4.pk]
            )
        ]
    )
