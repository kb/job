# -*- encoding: utf-8 -*-
import pytest

from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from django.urls import reverse

from base.url_utils import url_with_querystring
from finance.tests.factories import VatSettingsFactory
from job.models import (
    Candidate,
    Experience,
    Job,
    JobError,
    JobSettings,
    PRODUCT_JOB_POST,
)
from job.tests.factories import (
    ExperienceFactory,
    JobCreditFactory,
    JobFactory,
    JobRoleFactory,
    LocationFactory,
    PracticeAreaFactory,
    RecruiterFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from stock.tests.factories import ProductFactory
from .factories import ContactFactory


def _login_and_create_job(client, user, loc):
    job_role = JobRoleFactory(title="role1")
    practice_area = PracticeAreaFactory(title="area1")
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.post(
        reverse("recruitment.job.create"),
        dict(
            area=loc.form_description,
            close_date=date.today() + timedelta(days=7),
            code=loc.postcode,
            location_text="Belfast",
            description="New Job",
            recruiter=user.contact.recruiter.pk,
            salary="£Neg",
            title="Legal Secretary",
            job_role=job_role.pk,
            practice_area=practice_area.pk,
            applications_email="aa@bb.com",
        ),
    )
    assert 302 == response.status_code
    job = Job.objects.get(title="Legal Secretary")
    assert not job.publish


def _submit_from_dashboard(client, expected_publish_value):
    response = client.post(reverse("recruitment.recruiter.dashboard"), {})
    assert 302 == response.status_code

    job = Job.objects.get(title="Legal Secretary")
    assert expected_publish_value == job.publish


@pytest.mark.django_db
def test_job_create(client):
    user = UserFactory(username="rec1")
    RecruiterFactory(contact=ContactFactory(user=user))
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    job = Job.objects.get(title="Legal Secretary")
    assert not job.publish


@pytest.mark.django_db
def test_job_create_no_close_date(client):
    user = UserFactory(username="rec1")
    job_role = JobRoleFactory()
    practice_area = PracticeAreaFactory()
    RecruiterFactory(contact=ContactFactory(user=user))
    loc = LocationFactory(postcode="EX1")

    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.post(
        reverse("recruitment.job.create"),
        dict(
            area=loc.form_description,
            code=loc.postcode,
            description="New Job",
            location_text="London",
            job_role=job_role.pk,
            practice_area=practice_area.pk,
            recruiter=user.contact.recruiter.pk,
            salary="£Neg",
            title="Legal Secretary",
        ),
    )
    assert 200 == response.status_code
    assert (
        b"A closing date for applications must be specified" in response.content
    )
    job = Job.objects.filter(title="Legal Secretary")
    assert job.count() == 0


@pytest.mark.django_db
def test_job_delete(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    response = client.post(reverse("job.delete", args=[job.pk]))
    assert 302 == response.status_code
    job.refresh_from_db()
    assert job.deleted


@pytest.mark.django_db
def test_job_delete_closed(client):
    """Cannot delete a job where the closing date is in the past."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    job = JobFactory(
        recruiter=RecruiterFactory(contact=ContactFactory()),
        close_date=date.today() + relativedelta(months=-1),
    )
    response = client.post(reverse("job.delete", args=[job.pk]))
    assert 302 == response.status_code
    # errors = response.context['form'].errors
    # assert ['Job closing date cannot be in the past.'] == errors['__all__']
    job.refresh_from_db()
    assert job.deleted is True


@pytest.mark.django_db
def test_job_publish_staff(client):
    user = UserFactory(username="rec1")
    RecruiterFactory(contact=ContactFactory(user=user))
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    job = Job.objects.get(title="Legal Secretary")
    assert not job.publish

    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username="staff", password=TEST_PASSWORD)
    response = client.post(reverse("job.publish", args=[job.pk]))
    assert 302 == response.status_code

    job = Job.objects.get(title="Legal Secretary")
    assert job.publish


@pytest.mark.django_db
def test_job_publish_recruiter_cannot_publish(client):
    # job_post_chargeable is true (default) - recruiter has neither credits
    # nor a subscription
    user = UserFactory(username="rec1")
    RecruiterFactory(contact=ContactFactory(user=user))
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    job = Job.objects.get(title="Legal Secretary")
    assert not job.publish

    response = client.post(reverse("recruitment.job.publish", args=[job.pk]))
    assert 302 == response.status_code

    job = Job.objects.get(title="Legal Secretary")
    assert not job.publish


@pytest.mark.django_db
def test_job_publish_recruiter_no_charge(client):
    job_settings = JobSettings.load()
    job_settings.job_post_chargeable = False
    job_settings.save()
    # job_post_chargeable is false - recruiter has neither credits
    # nor a subscription but can still publish a job
    user = UserFactory(username="rec1")
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    assert recruiter.can_publish_job
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    job = Job.objects.get(title="Legal Secretary")
    assert not job.publish

    response = client.post(reverse("recruitment.job.publish", args=[job.pk]))
    assert 302 == response.status_code

    job = Job.objects.get(title="Legal Secretary")
    assert job.publish


@pytest.mark.django_db
def test_job_publish_recruiter_no_charge_no_credit_used(client):
    job_settings = JobSettings.load()
    job_settings.job_post_chargeable = False
    job_settings.save()
    # job_post_chargeable is false - recruiter has neither credits
    # nor a subscription does not use credits publish a job
    product = ProductFactory(slug=PRODUCT_JOB_POST)
    user = UserFactory(username="rec1")
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    credit = JobCreditFactory(
        recruiter=recruiter, product=product, credits=1, available_credits=1
    )

    assert recruiter.can_publish_job
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    job = Job.objects.get(title="Legal Secretary")
    assert not job.publish
    assert credit.available_credits == 1

    response = client.post(reverse("recruitment.job.publish", args=[job.pk]))
    assert 302 == response.status_code

    job.refresh_from_db()
    credit.refresh_from_db()
    assert job.publish
    assert credit.available_credits == 1


@pytest.mark.django_db
def test_job_create_subscription_valid(client):
    user = UserFactory(username="rec1")
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    recruiter.subscription_expires = date.today() + timedelta(days=1)
    recruiter.save()
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    _submit_from_dashboard(client, True)


@pytest.mark.django_db
def test_job_create_subscription_expires_today(client):
    user = UserFactory(username="rec1")
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    recruiter.subscription_expires = date.today()
    recruiter.save()
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    _submit_from_dashboard(client, True)


@pytest.mark.django_db
def test_job_create_subscription_expired(client):
    VatSettingsFactory()
    ProductFactory(slug="job-post")
    user = UserFactory(username="rec1")
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    recruiter.subscription_expires = date.today() - timedelta(days=1)
    recruiter.save()
    loc = LocationFactory(postcode="EX1")

    _login_and_create_job(client, user, loc)
    _submit_from_dashboard(client, False)


"""
@pytest.mark.django_db
def test_candidate_application(client):
    # Candidate apply for a job.
    # Long, complicated test code... but it does the job I suppose!
    r = RecruiterFactory(contact=ContactFactory(
        user=UserFactory(username='r', first_name='Rec', last_name='Wreck')
    ))
    j = JobFactory(recruiter=r, title='Django Programmer')
    # candidate
    user = UserFactory(username='c', first_name='Can', last_name='Core')
    contact = ContactFactory(user=user)
    CVFactory(contact=contact)
    candidate = CandidateFactory(contact=contact)
    # run the management command to create the mail template
    command = init_app_job.Command()
    command.handle()
    # candidate login
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse('recruitment.job.application', args=[j.pk])
    # action
    response = client.post(url, {'cover': 'I apply!'})
    # assert
    assert 302 == response.status_code
    # one message
    messages = Message.objects.all()
    assert 1 == messages.count()
    message = messages.first()
    # containing one 'mail' (the message for one person)
    mails = message.mail_set.all()
    assert 1 == mails.count()
    mail = mails.first()
    # containing several bits of data
    fields = mail.mailfield_set.all()
    result = {f.key: f.value for f in fields}
    # check the result contains a date field
    result.pop('DATE')
    # find the job application
    a = JobApplication.objects.get(candidate=candidate)
    assert {
        'NAME': 'Rec Wreck',
        'CANDIDATE': 'Can Core',
        'JOB': 'Django Programmer',
        'URL': 'http://testserver/recruitment/application/{}/'.format(a.pk),
    } == result
"""


@pytest.mark.django_db
def test_job_list(client):
    LocationFactory(postcode="EX20", town="Okehampton")
    url = url_with_querystring(
        reverse("recruitment.job.search"), area="EX20", data_type="postcode"
    )
    response = client.get(url)
    assert 200 == response.status_code


@pytest.mark.django_db
def test_job_view_no_candidate(client):
    """Auto create a candidate record.

    PK 06/02/2018, Why do we auto-create the candidate record?

    """
    user = UserFactory()
    contact = ContactFactory(user=user)
    recruiter = RecruiterFactory(contact=ContactFactory(user=UserFactory()))
    job = JobFactory(recruiter=recruiter)
    ExperienceFactory(slug=Experience.TRAINEE)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.job.detail", args=[job.pk])
    response = client.get(url)
    assert 200 == response.status_code
    # should auto-create a candidate record for this contact
    Candidate.objects.get(contact=contact)


@pytest.mark.django_db
def test_job_view_no_contact(client):
    user = UserFactory(username="pat")
    recruiter = RecruiterFactory(contact=ContactFactory(user=UserFactory()))
    job = JobFactory(recruiter=recruiter)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.candidate.job.detail", args=[job.pk])
    with pytest.raises(JobError) as e:
        client.get(url)
    assert (
        "Cannot view job detail - user 'pat' does not have a contact record"
    ) in str(e.value)
