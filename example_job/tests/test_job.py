# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.urls import reverse
from django.utils import timezone
from unittest import mock

from base.tests.model_maker import clean_and_save
from job.models import Job, JobSettings, FEED_RECRUITER, PRODUCT_JOB_POST
from job.search import JobIndex
from job.tasks import update_job_index
from job.tests.factories import (
    JobCreditFactory,
    JobFactory,
    JobFeedFactory,
    JobFeedItemFactory,
    JobSettingsFactory,
    LocationFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory
from search.search import SearchIndex
from stock.tests.factories import ProductFactory
from .factories import ContactFactory


def _job_data():
    JobSettingsFactory(job_post_max_days_displayed=10)
    publish_date = timezone.now() + relativedelta(days=-11)
    yesterday = date.today() + relativedelta(days=-1)
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFactory(title="j1", recruiter=recruiter)
    JobFactory(title="j2", recruiter=recruiter, close_date=yesterday)
    JobFactory(title="j3", recruiter=recruiter, publish=False)
    JobFactory(title="j4", recruiter=recruiter).set_deleted(UserFactory())
    JobFactory(title="j5", recruiter=recruiter, publish_date=publish_date)
    JobFactory(title="j6", recruiter=recruiter, order_key=FEED_RECRUITER)


@pytest.mark.django_db
def test_current():
    with mock.patch("elasticsearch.Elasticsearch.delete"):
        _job_data()  # creating a job, writes to Elasticsearch
    assert ["j1", "j6"] == [x.title for x in Job.objects.current()]


@pytest.mark.django_db
def test_current_all():
    with mock.patch("elasticsearch.Elasticsearch.delete"):
        _job_data()  # creating a job, writes to Elasticsearch
    assert ["j1", "j2", "j3", "j5", "j6"] == [
        x.title for x in Job.objects.current_all()
    ]


@pytest.mark.django_db
def test_current_include_closed():
    with mock.patch("elasticsearch.Elasticsearch.delete"):
        _job_data()  # creating a job, writes to Elasticsearch
    assert ["j1", "j2", "j5", "j6"] == [
        x.title for x in Job.objects.current(include_closed=True)
    ]


@pytest.mark.django_db
def test_current_include_unpublished():
    with mock.patch("elasticsearch.Elasticsearch.delete"):
        _job_data()  # creating a job, writes to Elasticsearch
    assert ["j1", "j3", "j6"] == [
        x.title for x in Job.objects.current(include_unpublished=True)
    ]


@pytest.mark.django_db
def test_current_our():
    with mock.patch("elasticsearch.Elasticsearch.delete"):
        _job_data()  # creating a job, writes to Elasticsearch
    assert ["j1"] == [x.title for x in Job.objects.current_our()]


@pytest.mark.django_db
def test_factory():
    JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))


@pytest.mark.django_db
def test_get_absolute_url():
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    expect = reverse("recruitment.candidate.job.detail", kwargs=dict(pk=job.pk))
    assert expect == job.get_absolute_url()


@pytest.mark.django_db
def test_publish_job_subscription():
    # recruiter has subscription
    exp = date.today() + relativedelta(years=1)
    recruiter = RecruiterFactory(
        contact=ContactFactory(), subscription_expires=exp
    )
    job = JobFactory(
        recruiter=recruiter,
        publish=False,
        title="Legal Secretary Personal Injury Department",
    )
    assert job.publish is False
    assert job.publish_date is None
    job.publish_job(job.recruiter.contact.user)
    assert job.publish is True
    assert job.publish_date is not None
    assert job.job_credit is None


@pytest.mark.django_db
def test_recruiter_can_publish_fail(client):
    # job_post_chargeable is true (default) - recruiter has neither credits
    # nor a subscription
    user = UserFactory(username="rec1")
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    assert not recruiter.can_publish_job


@pytest.mark.django_db
def test_recruiter_can_publish_no_charge(client):
    job_settings = JobSettings.load()
    job_settings.job_post_chargeable = False
    job_settings.save()
    # job_post_chargeable is false - recruiter has neither credits
    # nor a subscription but can still publish a job
    user = UserFactory(username="rec1")
    recruiter = RecruiterFactory(contact=ContactFactory(user=user))
    assert recruiter.can_publish_job


@pytest.mark.django_db
def test_publish_job_job_credit():
    product = ProductFactory(slug=PRODUCT_JOB_POST)
    recruiter = RecruiterFactory(contact=ContactFactory())
    credit = JobCreditFactory(
        recruiter=recruiter, product=product, credits=1, available_credits=1
    )
    job = JobFactory(
        recruiter=recruiter,
        publish=False,
        title="Legal Secretary Personal Injury Department",
    )
    assert job.publish is False
    assert job.publish_date is None
    job.publish_job(recruiter.contact.user)
    assert job.publish is True
    assert job.publish_date is not None
    assert job.job_credit == credit


@pytest.mark.django_db
def test_publish_job_job_credit_used_fails():
    product = ProductFactory(slug=PRODUCT_JOB_POST)
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobCreditFactory(
        recruiter=recruiter, product=product, credits=1, available_credits=0
    )
    job = JobFactory(
        recruiter=recruiter,
        publish=False,
        title="Legal Secretary Personal Injury Department",
    )
    assert job.publish is False
    assert job.publish_date is None
    result = job.publish_job(recruiter.contact.user)
    assert result is False
    assert job.publish is False
    assert job.publish_date is None
    assert job.job_credit is None


@pytest.mark.django_db
def test_publish_job_staff():
    staff = UserFactory(is_staff=True)
    ContactFactory(user=staff)
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(
        recruiter=recruiter,
        publish=False,
        title="Legal Secretary Personal Injury Department",
    )
    assert job.publish is False
    assert job.publish_date is None
    job.publish_job(staff)
    assert job.publish is True
    assert job.publish_date is not None
    assert job.job_credit is None


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_index_job():
    index = SearchIndex(JobIndex())
    index.drop_create()
    result, total = index.search({"job_role": "legal"})
    assert 0 == total
    assert [] == [x.pk for x in result]
    # recruiter has subscription - simplifies publish
    exp = date.today() + relativedelta(years=1)
    recruiter = RecruiterFactory(
        contact=ContactFactory(), subscription_expires=exp
    )
    obj = JobFactory(
        recruiter=recruiter,
        title="Legal Secretary \u2014 Personal Injury Department",
    )
    obj.publish_job(obj.recruiter.contact.user)
    update_job_index(obj.pk)
    result, total = index.search({"job_role": "legal"})
    assert 1 == total
    assert [obj.pk] == [x.pk for x in result]


@pytest.mark.django_db
def test_is_current():
    next_week = date.today() + relativedelta(days=7)
    obj = JobFactory(
        close_date=next_week,
        recruiter=RecruiterFactory(contact=ContactFactory()),
        title="Legal Secretary in Bath",
    )
    assert obj.is_current


@pytest.mark.django_db
def test_is_current_in_the_past():
    last_week = date.today() + relativedelta(days=-7)
    obj = JobFactory(
        close_date=last_week,
        recruiter=RecruiterFactory(contact=ContactFactory()),
        title="Legal Secretary in Bath",
    )
    assert not obj.is_current


@pytest.mark.django_db
def test_is_from_feed():
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=recruiter)
    # job feed
    feed = JobFeedFactory(recruiter=recruiter)
    JobFeedItemFactory(job=job, feed=feed)
    assert job.is_from_feed is True


@pytest.mark.django_db
def test_is_from_feed_not():
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    assert job.is_from_feed is False


@pytest.mark.django_db
def test_location_name_location_town():
    recruiter = RecruiterFactory(contact=ContactFactory())
    location = LocationFactory(town="Hatherleigh")
    job = JobFactory(recruiter=recruiter, location=location)
    assert "Hatherleigh" == job.location_name


@pytest.mark.django_db
def test_location_name_location_text():
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=recruiter, location_text="Holsworthy")
    assert "Holsworthy" == job.location_name


@pytest.mark.django_db
def test_location_name_feed_item_city():
    recruiter = RecruiterFactory(contact=ContactFactory())
    location = LocationFactory(town="Plymouth")
    job = JobFactory(recruiter=recruiter, location=location)
    feed = JobFeedFactory(recruiter=recruiter)
    JobFeedItemFactory(job=job, feed=feed, city="Plymouth")
    assert "Plymouth" == job.location_name


@pytest.mark.django_db
def test_location_name_feed_item_city_and_town():
    recruiter = RecruiterFactory(contact=ContactFactory())
    location = LocationFactory(town="Tavistock")
    job = JobFactory(recruiter=recruiter, location=location)
    feed = JobFeedFactory(recruiter=recruiter)
    JobFeedItemFactory(job=job, feed=feed, city="Plymouth")
    assert "Plymouth (Tavistock)" == job.location_name


@pytest.mark.django_db
def test_job():
    next_week = date.today() + relativedelta(days=7)
    clean_and_save(
        JobFactory(
            close_date=next_week,
            recruiter=RecruiterFactory(contact=ContactFactory()),
            title="Legal Secretary in Bath",
            location_text="Bath",
        )
    )


# @pytest.mark.django_db
# def test_job_in_the_past():
#     """Cannot create a job in the past."""
#     last_week = date.today() + relativedelta(days=-7)
#     with pytest.raises(ValidationError) as excinfo:
#         clean_and_save(JobFactory(
#             recruiter=RecruiterFactory(contact=ContactFactory()),
#             location_text="London",
#             close_date=last_week,
#             title='Legal Secretary in Bath'
#         ))
#     assert 'Job closing date cannot be in the past' in str(excinfo.value)


@pytest.mark.django_db
def test_recruiter_address():
    contact = ContactFactory(
        address_1="1",
        address_2="2",
        address_3="3",
        town="t",
        county="c",
        postcode="x",
        country="k",
    )
    recruiter = RecruiterFactory(contact=contact)
    job = JobFactory(recruiter=recruiter)
    assert "1, 2, 3, t, c, x, k" == job.recruiter_address


@pytest.mark.django_db
def test_recruiter_name_option_1():
    """Check the 'recruiter_name' method (see 'Job' model).  Test option 1.

    The options are::

      if self.recruiter.is_feed:
          if self.jobfeeditem.company:
              return self.jobfeeditem.company           # option 1
          elif self.jobfeeditem.source:
              return self.jobfeeditem.source            # option 2
      return self.recruiter.full_name                   # option 3

    """
    recruiter_feed = RecruiterFactory(
        contact=ContactFactory(company_name="Indeed")
    )
    job_feed = JobFeedFactory(recruiter=recruiter_feed)
    job = JobFactory(title="1", recruiter=recruiter_feed)
    JobFeedItemFactory(company="KB Software", feed=job_feed, job=job)
    assert "KB Software" == job.recruiter_name


@pytest.mark.django_db
def test_recruiter_name_option_2():
    """Check the 'recruiter_name' method (see 'Job' model).  Test option 2.

    The options are::

      if self.recruiter.is_feed:
          if self.jobfeeditem.company:
              return self.jobfeeditem.company           # option 1
          elif self.jobfeeditem.source:
              return self.jobfeeditem.source            # option 2
      return self.recruiter.full_name                   # option 3
    """
    recruiter_feed = RecruiterFactory(
        contact=ContactFactory(company_name="Indeed")
    )
    job_feed = JobFeedFactory(recruiter=recruiter_feed)
    job = JobFactory(title="1", recruiter=recruiter_feed)
    JobFeedItemFactory(feed=job_feed, job=job, source="Hatherleigh")
    assert "Hatherleigh" == job.recruiter_name


@pytest.mark.django_db
def test_recruiter_name_option_3():
    """Check the 'recruiter_name' method (see 'Job' model).  Test option 3.

    The options are::

      if self.recruiter.is_feed:
          if self.jobfeeditem.company:
              return self.jobfeeditem.company           # option 1
          elif self.jobfeeditem.source:
              return self.jobfeeditem.source            # option 2
      return self.recruiter.full_name                   # option 3
    """
    recruiter = RecruiterFactory(
        contact=ContactFactory(company_name="Cornwall Farmers")
    )
    job = JobFactory(recruiter=recruiter)
    assert "Cornwall Farmers" == job.recruiter_name


@pytest.mark.django_db
def test_str():
    recruiter = RecruiterFactory(contact=ContactFactory(company_name="ACT"))
    job = JobFactory(recruiter=recruiter, title="CTM")
    assert "ACT - CTM - Okehampton" == str(job)
