# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal

from checkout.models import CheckoutAction, CheckoutState
from checkout.tests.factories import CheckoutFactory, CheckoutAdditionalFactory
from checkout.tests.helper import check_checkout
from finance.tests.factories import VatSettingsFactory
from job.models import PRODUCT_JOB_POST_SUBSCRIPTION, Recruiter
from job.tests.factories import (
    CandidateFactory,
    CVFactory,
    JobApplicationFactory,
    JobFactory,
    RecruiterFactory,
)
from login.tests.factories import UserFactory
from mail.models import Message
from mail.tests.factories import MailTemplateFactory
from stock.tests.factories import ProductFactory
from .factories import ContactFactory


# @pytest.mark.django_db
# def test_checkout():
#     MailTemplateFactory(slug=Recruiter.MAIL_TEMPLATE_JOB_SUBSCRIPTION)
#     VatSettingsFactory()
#     ProductFactory(slug=PRODUCT_JOB_POST_SUBSCRIPTION)
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     check_checkout(recruiter)
#
#
# @pytest.mark.django_db
# def test_checkout_can_charge():
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     assert recruiter.checkout_can_charge
#
#
# @pytest.mark.django_db
# def test_checkout_can_charge_renew():
#     expired = date.today() + relativedelta(months=-1)
#     recruiter = RecruiterFactory(
#         contact=ContactFactory(), subscription_expires=expired
#     )
#     assert recruiter.checkout_can_charge
#
#
# @pytest.mark.django_db
# def test_checkout_can_charge_not():
#     recruiter = RecruiterFactory(
#         contact=ContactFactory(), subscription_expires=date.today()
#     )
#     assert not recruiter.checkout_can_charge
#
#
# @pytest.mark.django_db
# def test_checkout_description():
#     VatSettingsFactory()
#     ProductFactory(
#         slug=PRODUCT_JOB_POST_SUBSCRIPTION,
#         name="Year Subscription",
#         price=Decimal("320"),
#     )
#     recruiter = RecruiterFactory(
#         contact=ContactFactory(), subscription_expires=date.today()
#     )
#     assert [
#         "1 x Year Subscription @ 320.00 + 64.00 VAT"
#     ] == recruiter.checkout_description
#
#
# @pytest.mark.django_db
# def test_checkout_success_invoice():
#     VatSettingsFactory()
#     MailTemplateFactory(slug=Recruiter.MAIL_TEMPLATE_JOB_SUBSCRIPTION_INVOICE)
#     ProductFactory(
#         slug=PRODUCT_JOB_POST_SUBSCRIPTION,
#         name="Subscription",
#         price=Decimal("100"),
#     )
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     checkout = CheckoutFactory(
#         content_object=recruiter,
#         action=CheckoutAction.objects.invoice,
#         description=", ".join(recruiter.checkout_description),
#         state=CheckoutState.objects.success,
#         total=Decimal("120"),
#     )
#     CheckoutAdditionalFactory(
#         checkout=checkout, company_name="KB", email="test@pkimber.net"
#     )
#     recruiter.checkout_success(checkout)
#     # check subscription expires
#     assert recruiter.subscription_expires is None
#     # check invoice data
#     assert ("KB", "test@pkimber.net") == tuple(checkout.invoice_data)
#     # check email template context
#     assert 1 == Message.objects.count()
#     message = Message.objects.first()
#     template_slug = Recruiter.MAIL_TEMPLATE_JOB_SUBSCRIPTION_INVOICE
#     assert message.template.slug == template_slug
#     assert 1 == message.mail_set.count()
#     mail = message.mail_set.first()
#     assert 4 == mail.mailfield_set.count()
#     assert {
#         "description": "1 x Subscription @ 100.00 + 20.00 VAT",
#         "invoice": "KB, test@pkimber.net",
#         "name": recruiter.checkout_name,
#         "total": "£{:.2f}".format(Decimal("120")),
#     } == {f.key: f.value for f in mail.mailfield_set.all()}
#
#
# @pytest.mark.django_db
# def test_checkout_success_payment():
#     VatSettingsFactory()
#     MailTemplateFactory(slug=Recruiter.MAIL_TEMPLATE_JOB_SUBSCRIPTION)
#     ProductFactory(
#         slug=PRODUCT_JOB_POST_SUBSCRIPTION,
#         name="Subscription",
#         price=Decimal("100"),
#     )
#     recruiter = RecruiterFactory(contact=ContactFactory())
#     checkout = CheckoutFactory(
#         content_object=recruiter,
#         action=CheckoutAction.objects.payment,
#         description=", ".join(recruiter.checkout_description),
#         state=CheckoutState.objects.success,
#         total=Decimal("120"),
#     )
#     recruiter.checkout_success(checkout)
#     # check subscription expires
#     expect = date.today() + relativedelta(years=1)
#     assert expect == recruiter.subscription_expires
#     # check email template context
#     assert 1 == Message.objects.count()
#     message = Message.objects.first()
#     assert message.template.slug == Recruiter.MAIL_TEMPLATE_JOB_SUBSCRIPTION
#     assert 1 == message.mail_set.count()
#     mail = message.mail_set.first()
#     assert 3 == mail.mailfield_set.count()
#     assert {
#         "description": "1 x Subscription @ 100.00 + 20.00 VAT",
#         "name": recruiter.checkout_name,
#         "total": "£{:.2f}".format(Decimal("120")),
#     } == {f.key: f.value for f in mail.mailfield_set.all()}


@pytest.mark.django_db
def test_jobs_pending():
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFactory(publish=False, title="j1", recruiter=recruiter)
    JobFactory(publish=True, title="j2", recruiter=recruiter)
    JobFactory(publish=False, title="j3", recruiter=recruiter)
    qs = recruiter.jobs_pending()
    assert ["j1", "j3"] == [j.title for j in qs]


@pytest.mark.django_db
def test_jobs_published():
    recruiter = RecruiterFactory(contact=ContactFactory())
    JobFactory(publish=False, title="j1", recruiter=recruiter)
    JobFactory(
        publish=True,
        title="j2",
        recruiter=recruiter,
        publish_date=datetime(2015, 5, 7, 0, 0, 0, tzinfo=pytz.utc),
    )
    JobFactory(publish=False, title="j3", recruiter=recruiter)
    JobFactory(
        publish=True,
        title="j4",
        recruiter=recruiter,
        publish_date=datetime(2015, 5, 5, 0, 0, 0, tzinfo=pytz.utc),
    )
    qs = recruiter.jobs_published()
    assert ["j2", "j4"] == [j.title for j in qs]


@pytest.mark.django_db
def test_can_download_cv():
    candidate = CandidateFactory(contact=ContactFactory())
    recruiter = RecruiterFactory(contact=ContactFactory())
    cv = CVFactory(contact=candidate.contact)
    job = JobFactory(recruiter=recruiter)
    JobApplicationFactory(candidate=candidate, job=job)
    assert recruiter.can_download_cv(cv)


@pytest.mark.django_db
def test_can_download_cv_multi():
    """Candidate has applied for more than one job."""
    candidate = CandidateFactory(contact=ContactFactory())
    recruiter = RecruiterFactory(contact=ContactFactory())
    cv = CVFactory(contact=candidate.contact)
    JobApplicationFactory(
        candidate=candidate, job=JobFactory(recruiter=recruiter)
    )
    JobApplicationFactory(
        candidate=candidate, job=JobFactory(recruiter=recruiter)
    )
    assert recruiter.can_download_cv(cv)


@pytest.mark.django_db
def test_can_download_cv_not():
    """CV is not connected to the recruiter, so it can't be downloaded."""
    candidate = CandidateFactory(contact=ContactFactory())
    cv = CVFactory(contact=candidate.contact)
    recruiter = RecruiterFactory(contact=ContactFactory())
    assert not recruiter.can_download_cv(cv)


@pytest.mark.django_db
def test_pks_with_email():
    c1 = RecruiterFactory(
        contact=ContactFactory(user=UserFactory(email="code@pkimber.net"))
    )
    c2 = RecruiterFactory(
        contact=ContactFactory(user=UserFactory(email="patrick@pkimber.net"))
    )
    # deleted
    c3 = RecruiterFactory(
        contact=ContactFactory(
            user=UserFactory(email="code@pkimber.net"), deleted=True
        ),
    )
    # mixed case email
    c4 = RecruiterFactory(
        contact=ContactFactory(user=UserFactory(email="code@PKimber.net"))
    )
    assert [c1.pk, c4.pk] == [
        x
        for x in Recruiter.objects.pks_with_email(
            "code@pkimber.net", [c1.pk, c2.pk, c3.pk, c4.pk]
        )
    ]
