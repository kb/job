# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from job.models import Experience, JobError
from job.tests.factories import (
    CandidateFactory,
    CVFactory,
    ExperienceFactory,
    JobFactory,
    JobFeedFactory,
    JobFeedItemFactory,
    RecruiterFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_feed_job(client):
    """A candidate cannot apply a job from a feed."""
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=recruiter)
    job = JobFactory(recruiter=recruiter)
    JobFeedItemFactory(feed=feed, job=job, url="https://www.pkimber.net/howto/")
    url = reverse("recruitment.job.application", args=[job.pk])
    with pytest.raises(JobError) as e:
        client.get(url)
    assert "Cannot apply for a job imported from a feed" in str(e.value)


@pytest.mark.django_db
def test_ilspa_job(client):
    """Check a candidate can apply for an ILSPA job."""
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("recruitment.job.application", args=[job.pk])
    response = client.get(url)
    assert 200 == response.status_code


@pytest.mark.django_db
def test_ilspa_job_not_logged_in(client):
    """You must be logged in to apply for an ILSPA job."""
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("recruitment.job.application", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code, response["Location"]
    assert "/accounts/login" in response["Location"]


@pytest.mark.django_db
def test_redirect_candidate_cv(client):
    """User is a candidate and has a CV - so apply for the job."""
    user = UserFactory()
    contact = ContactFactory(user=user)
    CandidateFactory(contact=contact)
    CVFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    expect = reverse("recruitment.job.application", args=[job.pk])
    assert expect in response["Location"]


@pytest.mark.django_db
def testredirect_candidate_cv_not(client):
    """User does not have a CV - so set-up a candidate profile."""
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    expect = reverse("recruitment.candidate.update.profile")
    assert expect in response["Location"]


@pytest.mark.django_db
def test_redirect_job_feed(client):
    """View job from a feed - no need to login."""
    recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=recruiter)
    job = JobFactory(recruiter=recruiter)
    JobFeedItemFactory(feed=feed, job=job, url="https://www.pkimber.net/howto/")
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    assert "https://www.pkimber.net/howto/" in response["Location"]


@pytest.mark.django_db
def test_redirect_not_candidate(client):
    """User is not a candidate - so set-up a candidate profile."""
    user = UserFactory()
    ExperienceFactory(slug=Experience.TRAINEE)
    ContactFactory(user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    job = JobFactory(recruiter=RecruiterFactory(contact=ContactFactory()))
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    expect = reverse("recruitment.candidate.update.profile")
    assert expect in response["Location"]
