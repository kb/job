# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from job.models import Experience, JobTracking
from job.tests.factories import (
    CandidateFactory,
    ExperienceFactory,
    JobFactory,
    JobFeedFactory,
    JobFeedItemFactory,
    RecruiterFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_job_view_not_logged_in(client):
    """Track job views
    We track requests to 'JobDetailMixin' for non feed jobs
    if user is not logged in we record with candidate == None
    """
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=recruiter)
    url = reverse("recruitment.candidate.job.detail", args=[job.pk])
    response = client.get(url)
    assert 200 == response.status_code
    track = JobTracking.objects.get(job=job)
    assert 1 == track.view_count
    assert track.candidate is None


@pytest.mark.django_db
def test_job_view_not_logged_in_feed(client):
    """Track job views
    Feed job requests to 'JobDetailMixin' are not tracked.
    """
    recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=recruiter)
    job = JobFactory(recruiter=recruiter)
    JobFeedItemFactory(feed=feed, job=job, url="https://www.pkimber.net/howto/")
    url = reverse("recruitment.candidate.job.detail", args=[job.pk])
    response = client.get(url)
    assert 200 == response.status_code
    with pytest.raises(JobTracking.DoesNotExist) as execinfo:
        track = JobTracking.objects.get(job=job)
        assert track is None
    assert "JobTracking matching query does not exist." in str(execinfo.value)


@pytest.mark.django_db
def test_job_view_candidate(client):
    """Track job views
    We track requests to 'JobDetailMixin'.
    if user is logged in we record the candidate
    """
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=recruiter)
    ExperienceFactory(slug=Experience.TRAINEE)
    url = reverse("recruitment.candidate.job.detail", args=[job.pk])
    response = client.get(url)
    assert 200 == response.status_code
    track = JobTracking.objects.get(job=job)
    assert 1 == track.view_count
    assert user.username == track.candidate.contact.user.username


@pytest.mark.django_db
def test_job_application_click_direct(client):
    """Track job application clicks (not from a feed).
    We track requests to 'JobApplicationRedirectMixin'.  The UI says ``Apply
    for this job`` for a posted job and ``View/apply for this job...`` for jobs
    imported from a feed.
    """
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=recruiter)
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    assert reverse("recruitment.candidate.create") in response["Location"]
    tracks = JobTracking.objects.filter(job=job)
    assert 0 == tracks.count()


@pytest.mark.django_db
def test_job_candidate(client):
    """Track job application clicks (not from a feed).
    We track requests to 'JobApplicationRedirectMixin'.  The UI says ``Apply
    for this job`` for a posted job and ``View/apply for this job...`` for jobs
    imported from a feed.
    """
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=recruiter)
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    track = JobTracking.objects.get(job=job)
    assert 1 == track.application_click_count
    assert user.username == track.candidate.contact.user.username


@pytest.mark.django_db
def test_job_candidate_two(client):
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    job = JobFactory(recruiter=recruiter)
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    response = client.get(url)
    assert 302 == response.status_code
    track = JobTracking.objects.get(job=job)
    assert 2 == track.application_click_count
    assert user.username == track.candidate.contact.user.username


@pytest.mark.django_db
def test_job_application_click_feed(client):
    """Track job views (not from a feed).
    In this test, the user is not logged in.
    """
    recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=recruiter)
    job = JobFactory(recruiter=recruiter)
    JobFeedItemFactory(feed=feed, job=job, url="https://www.pkimber.net/howto/")
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    track = JobTracking.objects.get(job=job)
    assert 1 == track.application_click_count
    assert track.candidate is None


@pytest.mark.django_db
def test_job_feed_candidate(client):
    user = UserFactory()
    CandidateFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=recruiter)
    job = JobFactory(recruiter=recruiter)
    JobFeedItemFactory(feed=feed, job=job, url="https://www.pkimber.net/howto/")
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    track = JobTracking.objects.get(job=job)
    assert 1 == track.application_click_count
    assert user.username == track.candidate.contact.user.username


@pytest.mark.django_db
def test_job_feed_contact(client):
    """We only track clicks if the user is a candidate.

    In this test, the user is a contact, but not a candidate, so the click is
    recorded, but is not assigned to the user/contact.

    """
    user = UserFactory()
    ExperienceFactory(slug=Experience.TRAINEE)
    contact = ContactFactory(user=user)
    assert contact.is_candidate is False
    assert client.login(username=user.username, password=TEST_PASSWORD)
    recruiter = RecruiterFactory(contact=ContactFactory())
    feed = JobFeedFactory(recruiter=recruiter)
    job = JobFactory(recruiter=recruiter)
    JobFeedItemFactory(feed=feed, job=job, url="https://www.pkimber.net/howto/")
    url = reverse("recruitment.job.application.redirect", args=[job.pk])
    response = client.get(url)
    assert 302 == response.status_code
    track = JobTracking.objects.get(job=job)
    assert 1 == track.application_click_count
    assert track.candidate.contact == contact
    assert track.candidate.contact.is_candidate is True
