# -*- encoding: utf-8 -*-
import pytest

from django.db import connection
from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from base.tests.test_utils import assert_redirect
from job.tests.factories import RecruiterFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from .factories import ContactFactory
from .test_candidate_search import setup_search


@pytest.mark.django_db
def test_recruiter_candidate_search_not_posted_a_job(client):
    setup_search()
    user = UserFactory()
    RecruiterFactory(contact=ContactFactory(user=user))
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("recruitment.recruiter.search")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    check = (
        "Please post a job in order to have access to our candidate database"
    )
    assert check == response.context_data["info_message"]
    assert [] == list(response.context_data["object_list"])


@pytest.mark.django_db
def test_recruiter_candidate_search_not_logged_in(client):
    setup_search()
    user = UserFactory()
    RecruiterFactory(
        contact=ContactFactory(user=user), search_expires=timezone.now().date()
    )
    url = reverse("job.candidate.list")
    client.logout()
    response = client.get(url)
    assert_redirect(response, "/accounts/login/")


@pytest.mark.django_db
def test_staff_candidate_search_not_staff_login(client):
    setup_search()
    user = UserFactory(username="web")
    ContactFactory(user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("job.candidate.list")
    response = client.get(url)
    assert_redirect(response, "/accounts/login/")


@pytest.mark.django_db
@pytest.mark.skipif(connection.vendor == "sqlite", reason="requires postgres")
def test_staff_candidate_search(client, django_assert_num_queries):
    setup_search()
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("job.candidate.list")
    with django_assert_num_queries(12):
        response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    # check
    qs = response.context_data["object_list"]
    assert ["can5", "can3", "can2", "can1", "can4"] == [
        x.contact.user.username for x in qs
    ]
