#!/bin/bash

# treat unset variables as an error when substituting.
set -u

DATABASE=dev_app_job_${USER}

# drop the databases - if they exist
psql -X -U postgres -c "DROP DATABASE ${DATABASE}"

# exit immediately if a command exits with a nonzero exit status.
set -e
psql -X -U postgres -c "CREATE DATABASE ${DATABASE} TEMPLATE=template0 ENCODING='utf-8';"

pytest -x

django-admin.py migrate --noinput
django-admin.py demo-data-login
django-admin.py init_app_job
django-admin.py demo_data_job

# import the location data
django-admin.py import_country --path "example_job/tests/data/uk-country.csv"
django-admin.py import_region --path "example_job/tests/data/region.csv"
django-admin.py import_postcode --path "example_job/tests/data/postcodes.csv"

django-admin.py runserver
