job
***

Django application for a jobs site.

ElasticSearch
=============

The ``Job`` class, allows ``null`` values for ``title``, ``Recruiter``,
``Location``, ``JobPracticeAra`` and ``JobRole``.

- Q: Are there jobs with blank values in these fields?
  A: Yes - Probably some of the jobs imported from Indeed (the Indeed feed has
     now been removed), ideally we'd do integrity checks at the model level but
     since we can't control the feed at the moment integrity is imposed at the
     create / update view level.
- Q: Checking up to date jobs, and they have a title, town and region, but they
     don't have a practice area or job role?
  A: That's one of the things being introduced with ticket 1339

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-job
  source venv-job/bin/activate

  pip install -r requirements/local.txt

To create an empty Postgres *test* database::

  # this command will drop the database if it already exists.
  psql -X -U postgres -c "DROP DATABASE test_app_job"
  psql -X -U postgres -c "CREATE DATABASE test_app_job TEMPLATE=template0 ENCODING='utf-8';"

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

If you **do not** have Elasticsearch installed::

  pytest -x -m "not elasticsearch"

.. note:: We should add Elasticsearch to our GitLab testing configuration, so
          we don't have to include / exclude Elasticsearch tests:
          https://www.docker.elastic.co/

To test ElasticSearch::

  # check ElasticSearch is running
  http GET http://localhost:9200/

  django-admin.py demo_data_job
  django-admin.py init_job_search

Feed - Simply Law Jobs
----------------------

The username, password and identifier for the Simply Law Jobs feed will be
auto-filled by the ``init_app_job`` management command if you set the following
in your ``.private`` file::

  set -x TEST_SIMPLY_LAW_IDENTIFIER "My Jobs"
  set -x TEST_SIMPLY_LAW_USERNAME "my-user"
  set -x TEST_SIMPLY_LAW_PASSWORD "my-pass"

.. note:: This is in ``fish`` shell format.

Usage
=====

To set-up the test database and load location data::

  ./init_dev.sh

.. note:: I don't think the search forms are working, so make sure you login to
          see the list of jobs etc.

Release
=======

https://www.kbsoftware.co.uk/docs/
